/*
 * Copyright (C) 2018-2020 Cray UK
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef MMB_TOPOLOGY_H
#define MMB_TOPOLOGY_H

#include <stdlib.h>
#include <hwloc.h>

#include "../common/mmb_error.h"

/**
 * @name mmb_topology
 * @{ */

/**
 * @brief Initialize and load the memory structure from libhwloc.
 *
 * @param [in] topology Pointer to the topology field of the mmbManager
 *                      structure.
 * @param [out] layers uint64_t array of size at least \c MMB_MEMLAYER__MAX.
 *                     This array is to return the cumulated size of the
 *                     detected layers.
 * @param [out] ex_cons Boolean array of size at least
 *                      \c MMB_EXECUTION_CONTEXT__MAX.
 * @param [out] numa_sizes Capacity of memory per NUMA node, indexed on
 *                         NUMA node's id.  Heap allocated, must be freed.
 *
 * @returns \c MMB_OK on success, an error type otherwise.
 */
mmbError
mmb_topology_init(hwloc_topology_t *topo,
                  uint64_t * restrict layers,
                  uint64_t * restrict ex_cons,
                  uint64_t * restrict numa_sizes[]);

/**
 * @brief Release the topology structure.
 *
 * @param [in] topology The corresponding structure field.
 */
void mmb_topology_finalize(hwloc_topology_t topology);

/**  @} */

#endif /* MMB_TOPOLOGY_H */
