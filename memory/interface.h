/*
 * Copyright (C) 2018-2020 Cray UK
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef MMB_INTERFACE_H
#define MMB_INTERFACE_H

#include <stdlib.h>
#include <stdbool.h>

#include "mmb_memory.h"
#include "mmb_memory_options.h"
#include "i_memory.h"

/**
 * @name mmb_memory
 * @{ */

/**
 * @name mmbMemInterface
 * @{ */

/**
 * @brief The dispatch table entry.
 *
 * Every memory provider needs to implement this set on function to provide a
 * working memory interface.
 */
struct mmb_dispatch_provider_entry {
  const char *name; /**< printable name */
  /**
   * Size of provider specific data.
   * This value is used to retrieve the addresses of the beginning of strategy
   * specific data in the interface structure.
   */
  size_t data_size;
  /**
   * The function checking whether one \c mmbProvider is suitable for
   * this \c mmbLayer.
   */
  mmbError (*supports)(const mmbMemLayer layer, bool need_numa_support,
                       bool *is_supported);
  /**
   * The function initializing the third-party library if required. May be
   * \c NULL.
   */
  mmbError(*init)(const mmbProviderInitParam *param);
  /**
   * The function finalizing the third-party library if required. May be
   * \c NULL.
   */
  mmbError(*finalize)(void);
  struct {
    bool enabled; /**< if non-0: interface is enabled in this library build */
    /** the function implementing mmb_meminterface_init for the provider */
    mmbError(*init)(mmbMemInterface *interface);
    /** the function implementing mmb_meminterface_finalize for the provider */
    mmbError(*finalize)(mmbMemInterface *interface);
    /** the function implementing mmb_allocate() */
    mmbError(*allocate)(const size_t n_bytes, mmbMemInterface *interface,
                        const mmbAllocateOptions *opts, void **allocation);
    /** the function implementing mmb_free() */
    mmbError(*free)(void *allocation, mmbMemInterface *interface,
                    const mmbAllocateOptions *opts, const size_t n_bytes);
    /** the function implementing mmb_copy_nd() */
    mmbError (*copy_nd)(void *dst, mmbMemInterface *dst_inter,
                        const mmbAllocateOptions *dst_opts,
                        const size_t *doffset, const size_t *dpitch,
                        const void *src, const mmbMemInterface *src_inter,
                        const mmbAllocateOptions *src_opts,
                        const size_t *soffset, const size_t *spitch,
                        const size_t ndims, const size_t *dims);
  } ops[MMB_EXECUTION_CONTEXT__MAX];
};

/**
 * @brief Create a new mmbMemInterface handle and return a pointer to it.
 *
 * @param [in] provider Enumerate value, different from
 *                      \c MMB_PROVIDER_DEFAULT.
 * @param [in] provider_opts Provider specific options structure
 * @param [in] strategy Enumarate value, different from
 *                      \c MMB_STRATEGY_DEFAULT.
 * @param [out] out_interface An handle to the allocated interface structure.
 *
 * @return \c MMB_OK on success, an error type otherwise.
 */
mmbError mmb_meminterface_create(const mmbProvider provider,
                                 mmbProviderOptions provider_opts,
                                 const mmbStrategy strategy,
                                 const char *name,
                                 mmbMemInterface **out_interface);

/**
 * @brief Destroy the interface pointed by the \p interface handle.
 *
 * @param [in] interface Interface previously allocated with
 *                       \p mmb_meminterface_create.
 *
 * @return \c MMB_OK on success, an error type otherwise.
 */
mmbError mmb_meminterface_destroy(mmbMemInterface *interface);

/**
 * @brief Initialize the strategies and memory provider used by this
 *        mmbInterface.
 *
 * @param [in] interface An handle allocated with \p mmb_meminterface_create.
 *
 * @return \c MMB_OK on success, an error type otherwise.
 */
mmbError mmb_meminterface_init(mmbMemInterface *interface);

/**
 * @brief Finalize the data specificaly used for the strategies and the memory
 *        provider of the \p interface mmbInterface.
 *
 * @param [in] interface An handle allocated with \p mmb_meminterface_create.
 *
 * @return \c MMB_OK on success, an error type otherwise.
 */
mmbError mmb_meminterface_finalize(mmbMemInterface *interface);

/**  @} */

/**  @} */

#endif /* MMB_INTERFACE_H */
