/*
 * Copyright (C) 2018-2021 Cray UK
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef MMB_MEMORY_H
#define MMB_MEMORY_H

#include <stdlib.h>
#include <stdio.h>
#include <stddef.h>
#include <stdbool.h>
#include "mmb_error.h"

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @name mmb_memory
 * @{ */

/**
 * @name mmbStrategy
 * @{ */

/**
 * @name Public API
 * @{ */

typedef enum mmbMemLayer {
  MMB_DRAM = 0,
  MMB_GDRAM,
  MMB_HBM,
  MMB_NVDIMM, // Non-volatile
  MMB_NADRAM, // Network attached
  MMB_SSD,
  MMB_MEMLAYER__MAX
} mmbMemLayer;

/**  @} */

/**  @} */

/**
 * @name mmbStrategy
 * @{ */

/**
 * @name Public API
 * @{ */

typedef enum mmbStrategy {
  MMB_STRATEGY_ANY = -2,
  MMB_STRATEGY_DEFAULT = -1,
  MMB_STRATEGY_NONE,
  MMB_THREAD_SAFE,
  MMB_LIMITED_GREEDY,
  MMB_POOLED,
  MMB_STATISTICS,
  MMB_POOLED_STATISTICS,
  MMB_STRATEGY__MAX
} mmbStrategy;

/**  @} */

/**  @} */

/**
 * @name mmbExecutionContext
 * @{ */

/**
 * @name Public API
 * @{ */

typedef enum mmbExecutionContext {
  MMB_EXECUTION_CONTEXT_NONE = -2,
  MMB_EXECUTION_CONTEXT_DEFAULT = -1,
  MMB_CPU,
  MMB_GPU_CUDA,
  MMB_GPU_HIP,
  MMB_OPENCL,
  MMB_EXECUTION_CONTEXT__MAX
} mmbExecutionContext;

/**  @} */

/**  @} */

/**
 * @name mmbMemInterface
 * @{ */

/**
 * @name Public API
 * @{ */

typedef enum mmbProvider {
  MMB_PROVIDER_ANY = -2,
  MMB_PROVIDER_DEFAULT = -1,
  MMB_SYSTEM,
  MMB_SICM,
  MMB_UMPIRE,
  MMB_JEMALLOC,
  MMB_PROVIDER__MAX
} mmbProvider;

/* Early declaration, with definition lower, but symbols have to be reserved
 * here */
struct mmbMemSpace;

typedef struct mmbMemInterface mmbMemInterface;

mmbError mmb_meminterface_get_layer(const mmbMemInterface *interface,
                                    mmbMemLayer *layer);

mmbError mmb_meminterface_get_execution_context(const mmbMemInterface *interface,
                                                mmbExecutionContext *ex_con);

mmbError mmb_meminterface_get_space(mmbMemInterface *interface,
                                    struct mmbMemSpace **space);

mmbError mmb_meminterface_get_provider(const mmbMemInterface *interface,
                                       mmbProvider *provider);

mmbError mmb_meminterface_get_strategy(const mmbMemInterface *interface,
                                       mmbStrategy *strategy);

mmbError mmb_meminterface_get_name(const mmbMemInterface *interface,
                                   char **name);

/**  @} */

/**  @} */

/**
 * @name mmbMemSpace
 * @{ */

/**
 * @name Public API
 * @{ */

/* Early declaration, with definition lower, but symbols have to be reserved
 * here */
struct mmbManager;
struct mmbMemInterfaceConfig;
struct mmbMemSpaceConfig;

typedef struct mmbMemSpace mmbMemSpace;

mmbError mmb_memspace_create_interface(mmbMemSpace *space,
                                       const struct mmbMemInterfaceConfig *config_opts,
                                       mmbMemInterface **out_interface);

mmbError mmb_memspace_request_interface(mmbMemSpace *space,
                                        const struct mmbMemInterfaceConfig *config_opts,
                                        mmbMemInterface **out_interface);

mmbError mmb_memspace_destroy_interface(mmbMemInterface *interface);

mmbError mmb_memspace_configure_defaults(const struct mmbManager *mngr,
                                         const struct mmbMemSpaceConfig *config_opts,
                                         mmbMemSpace *space);

mmbError mmb_memspace_get_layer(const mmbMemSpace *space, mmbMemLayer *layer);

mmbError mmb_memspace_get_execution_context(const mmbMemSpace *space,
                                            mmbExecutionContext *ex_con);

/**  @} */

/**  @} */

/**
 * @name mmbManager
 * @{ */

/**
 * @name Public API
 * @{ */

typedef struct mmbManager mmbManager;

mmbError mmb_manager_create(mmbManager **out_mngr);
mmbError mmb_manager_destroy(mmbManager *mngr);
/* /!\ config_opts->mem_size is given in bytes, not MiB. */
mmbError mmb_manager_register_memory(mmbManager *mngr, const mmbMemLayer layer,
                                     const mmbExecutionContext ex_context,
                                     const struct mmbMemSpaceConfig *config_opts,
                                     mmbMemSpace **out_space);
mmbError mmb_manager_request_space(mmbManager *mngr, const mmbMemLayer in_layer,
                                   const mmbExecutionContext ex_context,
                                   const struct mmbMemSpaceConfig *config_opts,
                                   mmbMemSpace **out_space);
mmbError mmb_manager_report_state(mmbManager *mngr, FILE *out);

mmbError mmb_manager_discovery_is_enabled(int *is_enabled);

/* Statistics */
mmbError mmb_manager_get_interface_current_statistics(mmbManager *mngr,
                                                      mmbMemInterface *interface,
                                                      size_t *n_allocations,
                                                      size_t *bytes_allocated);
mmbError mmb_manager_get_interface_total_statistics(mmbManager *mngr,
                                                    mmbMemInterface *interface,
                                                    size_t *n_allocations,
                                                    size_t *bytes_allocated);
mmbError mmb_manager_get_stats_record_count(mmbManager *mngr, size_t *count);

/**  @} */

/**  @} */

/**
 * @name mmbAllocation
 * @{ */

/**
 * @name Public API
 * @{ */

typedef struct {
  bool is_numa_allocation;
  union {
    struct {
      int node_id;
    } numa;
  };
} mmbAllocateOptions;

typedef struct mmbAllocation {
  void         *ptr;
  size_t        n_bytes;
  struct mmbMemInterface *interface;
  mmbAllocateOptions opts;
  bool          owned;
} mmbAllocation;

mmbError mmb_allocation_create_wrapped(void* in_ptr, const size_t n_bytes,
                                       mmbMemInterface *interface,
                                       mmbAllocation **out_allocation);
mmbError mmb_allocation_create_wrapped_opts(void* in_ptr,
                                            const size_t n_bytes,
                                            mmbMemInterface *interface,
                                            const mmbAllocateOptions *opts,
                                            mmbAllocation **out_allocation);
mmbError mmb_allocation_destroy(mmbAllocation *in_allocation);

mmbError mmb_allocate(const size_t n_bytes, mmbMemInterface *interface,
                      mmbAllocation **out_allocation);
mmbError mmb_allocate_opts(const size_t n_bytes,
                           mmbMemInterface *interface,
                           const mmbAllocateOptions *opts,
                           mmbAllocation **out_allocation);
mmbError mmb_free(mmbAllocation *allocation);

/**
 * The amount of data to copy is equal to
 * MIN( \p dest->n_bytes, \p src->n_bytes ).
 */
mmbError mmb_copy(mmbAllocation *dest, mmbAllocation *src);

/**
 * @brief Copies an array (\p width bytes long) from the memory
 *        area pointed to by \p src at offset \p soffset to the memory
 *        area pointed to by \p dst at offset \p doffset.
 *
 * @param [in] dst Destination mmbAllocation
 * @param [in] doffset Offset at the begining of the destination allocation
 * @param [in] src Source mmbAllocation
 * @param [in] soffset Offset at the begining of the source allocation
 * @param [in] width Width of matrix transfer (columns in bytes)
 *
 * @return \c MMB_OK on success, an error type otherwise.
 */
mmbError mmb_copy_1d(mmbAllocation *dst, const size_t doffset,
                     mmbAllocation *src, const size_t soffset,
                     const size_t width);

/**
 * @brief Copies a matrix (\p height rows of \p width bytes each) from the
 *        memory area pointed to by \p src at offset \p sxoffset + \p syoffset
 *        * \p sxpitch to the memory area pointed to by \p dst at offset \p
 *        dxoffset + \p dyoffset * \p dxpitch.
 *
 * @param [in] dst Destination mmbAllocation
 * @param [in] dxoffset Offset at the begining of the destination allocation in x
 * @param [in] dyoffset Offset at the begining of the destination allocation in y
 * @param [in] dxpitch Pitch of destination memory for rows
 * @param [in] src Source mmbAllocation
 * @param [in] sxoffset Offset at the begining of the source allocation in x
 * @param [in] syoffset Offset at the begining of the source allocation in y
 * @param [in] sxpitch Pitch of source memory for rows
 * @param [in] width Width of matrix transfer (columns in bytes)
 * @param [in] height Height of matrix transfer (rows)
 *
 * @return \c MMB_OK on success, an error type otherwise.
 */
mmbError mmb_copy_2d(mmbAllocation *dst, const size_t dxoffset, const size_t dyoffset,
                     const size_t dxpitch,
                     mmbAllocation *src, const size_t sxoffset, const size_t syoffset,
                     const size_t spitch,
                     const size_t width, const size_t height);

/**
 * @brief Copies a matrix (\p depth slicesi of \p height rows of \p width bytes
 *        each) from the memory area pointed to by \p src at offset \p sxoffset
 *        + \p syoffset * \p sxpitch + \p szoffset * \p sypitch to the memory
 *        area pointed to by \p dst at offset \p dxoffset + \p dyoffset *
 *        \p dxpitch + \p dzoffset * \p dypitch.
 *
 * @param [in] dst Destination mmbAllocation
 * @param [in] dxoffset Offset at the begining of the destination allocation in x
 * @param [in] dyoffset Offset at the begining of the destination allocation in y
 * @param [in] dzoffset Offset at the begining of the destination allocation in z
 * @param [in] dxpitch Pitch of destination memory for rows
 * @param [in] dypitch Pitch of destination memory for slices
 * @param [in] src Source mmbAllocation
 * @param [in] sxoffset Offset at the begining of the source allocation in x
 * @param [in] syoffset Offset at the begining of the source allocation in y
 * @param [in] szoffset Offset at the begining of the source allocation in z
 * @param [in] sxpitch Pitch of source memory for rows
 * @param [in] sypitch Pitch of source memory for slices
 * @param [in] width Width of matrix transfer (columns in bytes)
 * @param [in] height Height of matrix transfer (rows)
 * @param [in] depth Depth of matrix transfer (slices)
 *
 * @return \c MMB_OK on success, an error type otherwise.
 */
mmbError mmb_copy_3d(mmbAllocation *dst, const size_t dxoffset, const size_t dyoffset,
                     const size_t dzoffset, const size_t dxpitch, const size_t dypitch,
                     mmbAllocation *src, const size_t sxoffset, const size_t syoffset,
                     const size_t szoffset, const size_t sxpitch, const size_t sypitch,
                     const size_t width, const size_t height, const size_t depth);

/**
 * @brief Copies a n-D matrix from the
 *        memory area pointed to by \p src at offset \p soffset to the
 *        memory area pointed to by \p dst at offset \p doffset.
 *
 * @param [inout] dst Destination mmbAllocation
 * @param [in] doffset Offset in each dimensions at the begining of the
 *                     destination allocation.
 * @param [in] dpitch Pitch for each dimension of destination memory
 * @param [in] src Source mmbAllocation
 * @param [in] soffset Offset in each dimension at the begining of the source
 *                     allocation.
 * @param [in] spitch Pitch for each dimension of source memory.
 * @param [in] ndims Diemnsionality of both allocations and of the copied seciton.
 * @param [in] dims Dimensions of the copied section.  The first dimension is
 *                  given in bytes, then following dimensions are given in
 *                  number of previous dimensions.
 *
 * @return \c MMB_OK on success, an error type otherwise.
 */
mmbError mmb_copy_nd(mmbAllocation *dst, const size_t *doffset, const size_t *dpitch,
                     mmbAllocation *src, const size_t *soffset, const size_t *spitch,
                     const size_t ndims, const size_t *dims);

mmbError mmb_allocation_get_slice(mmbAllocation *in_alloc,
                                  const ptrdiff_t offset, const size_t n_bytes,
                                  mmbAllocation **out_allocation);
mmbError mmb_allocation_get_ptr(const mmbAllocation *allocation, void **out_ptr);
/**
 * @brief Returns the memory space used to allocate the memory for the given
 *        \c mmbAllocation.
 *
 * @param [in]  alloc Handle to a valid mmbAllocation
 * @param [out] space Pointer to a allocated mmbMemSpace
 *
 * @return \c MMB_OK on success, an error type otherwise.
 */
mmbError mmb_allocation_get_space(const mmbAllocation *alloc,
                                  mmbMemSpace **space);
/**
 * @brief Returns the layer the provided allocation is located to.
 *
 * @param [in]  alloc Handle to a valid mmbAllocation
 * @param [out] layer Pointer to a allocated mmbMemLayer
 *
 * @return \c MMB_OK on success, an error type otherwise.
 */
mmbError mmb_allocation_get_layer(const mmbAllocation *alloc, mmbMemLayer *layer);
/**
 * @brief Returns the execution context requested for the provided allocation.
 *
 * @param [in]  alloc Handle to a valid mmbAllocation
 * @param [out] ex_context Pointer to a allocated mmbExecutionContext
 *
 * @return \c MMB_OK on success, an error type otherwise.
 */
mmbError mmb_allocation_get_execution_context(const mmbAllocation *alloc,
                                              mmbExecutionContext *ex_context);
/**
 * @brief Returns the memory interface used to allocate the memory for the given
 *        \c mmbAllocation.
 *
 * @param [in]  alloc Handle to a valid mmbAllocation
 * @param [out] interface Pointer to a allocated mmbMemInterface
 *
 * @return \c MMB_OK on success, an error type otherwise.
 */
mmbError mmb_allocation_get_interface(const mmbAllocation *alloc,
                                      mmbMemInterface **interface);
/**
 * @brief Returns the memory provider used to allocate the memory for the given
 *        \c mmbAllocation.
 *
 * @param [in]  alloc Handle to a valid mmbAllocation
 * @param [out] provider Pointer to a allocated mmbProvider
 *
 * @return \c MMB_OK on success, an error type otherwise.
 */
mmbError mmb_allocation_get_provider(const mmbAllocation *alloc,
                                     mmbProvider *provider);
/**
 * @brief Returns the strategy used to allocate the memory for the given
 *        \c mmbAllocation.
 *
 * @param [in]  alloc Handle to a valid mmbAllocation
 * @param [out] strat Pointer to a allocated mmbStrategy
 *
 * @return \c MMB_OK on success, an error type otherwise.
 */
mmbError mmb_allocation_get_strategy(const mmbAllocation *alloc,
                                     mmbStrategy *strat);
/**
 * @brief Returns the \p mmb_allocate_opts options memory provider used
 *        to allocate the memory for the given \c mmbAllocation.
 *
 * \p options cannot be a pointer to \p alloc->opts.
 *
 * @param [in]  alloc Handle to a valid mmbAllocation
 * @param [out] options Pointer to the mmbAllocateOptions object
 *
 * @return \c MMB_OK on success, an error type otherwise.
 */
mmbError mmb_allocation_get_options(const mmbAllocation * alloc,
                                    mmbAllocateOptions * options);

/**
 * @brief Allocate a new mmbAllocateOptions object and returns it in \p opts.
 *
 * @param [out] opts Handle for the returned object
 *
 * @return \c MMB_OK on success, an error type otherwise.
 */
mmbError mmb_allocate_options_create(mmbAllocateOptions **opts);

/**
 * @brief Free a mmbAllocateOptions object allocated with
 *        \c mmb_allocate_options_create.
 *
 * @param [out] opts Pointer to a mmbAllocateOptions structure.
 *
 * @return \c MMB_OK on success, an error type otherwise.
 */
mmbError mmb_allocate_options_destroy(mmbAllocateOptions *opts);

/**  @} */

/**  @} */

/**  @} */

#ifdef __cplusplus
} /* extern "C" */
#endif

#endif /* MMB_MEMORY_H */
