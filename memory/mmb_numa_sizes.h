/*
 * Copyright (C) 2018-2021 Cray UK
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef MMB_NUMASIZES_H
#define MMB_NUMASIZES_H

#include <stdlib.h>
#include "mmb_error.h"
#include "mmb_set.h"

#ifdef __cplusplus
extern "C" {
#endif

typedef mmbSet_uint64_t mmbNumaSizes;

extern const mmbNumaSizes mmb_numasizes_empty;

mmbError mmb_numasizes_create(const size_t ndim, mmbNumaSizes **out_ns);
mmbError mmb_numasizes_create_fill(const size_t ndim, const uint64_t* values, mmbNumaSizes **out_ns);
mmbError mmb_numasizes_set(mmbNumaSizes *in_ns, uint64_t nsN, ...);
mmbError mmb_numasizes_copy(mmbNumaSizes *dst, const mmbNumaSizes *src);
mmbError mmb_numasizes_copy_buffer(uint64_t *dst, const mmbNumaSizes *src);
mmbError mmb_numasizes_fill(mmbNumaSizes *dst, const uint64_t *src, const size_t nelt);
mmbError mmb_numasizes_resize(mmbNumaSizes *in_ns, const size_t ndim);
mmbError mmb_numasizes_destroy(mmbNumaSizes *in_ns);
int mmb_numasizes_cmp(const mmbNumaSizes *s1, const mmbNumaSizes *s2);
mmbError mmb_numasizes_set_buf_addr(mmbNumaSizes *in_ns, uint64_t *buffer);
mmbError mmb_numasizes_get_size(const mmbNumaSizes *in_ns, size_t *size);
mmbError mmb_numasizes_get_sizeof(const mmbNumaSizes *in_ns, size_t *size);

#ifdef __cplusplus
}
#endif

#endif /* MMB_NUMASIZES_H */
