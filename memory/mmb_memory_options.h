/*
 * Copyright (C) 2020      Cray UK
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef MMB_MEMORY_OPTIONS_H
#define MMB_MEMORY_OPTIONS_H

#include <stdint.h>

#include "mmb_error.h"
#include "mmb_memory.h"
#include "mmb_numa_sizes.h"
#include "mmb_provider_options.h"

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @name mmbMemInterface
 * @{ */

/**
 * @name Public API
 * @{ */

/* Providers Initialization Parameters */
 typedef union {
   char __placeholder;
 } mmbProviderInitParam;

/* Handle to Providers Initialization Parameters, used to initialize multiple
 * providers at once (indexed on mmbProvider). */
typedef mmbProviderInitParam mmbProviderInitParamSet [MMB_PROVIDER__MAX];

#define MMB_PROVIDER_DEFAULT_INIT_PARAMS {                                   \
  [MMB_SYSTEM]   = { 0 /* no specific option */ },                           \
  [MMB_SICM]     = { 0 /* no specific option */ },                           \
  [MMB_UMPIRE]   = { 0 /* no specific option */ },                           \
  [MMB_JEMALLOC] = { 0 /* no specific option */ },                           \
}

/**  @} */

/**  @} */

/**
 * @name mmbStrategy
 * @{ */

/**
 * @name Public API
 * @{ */

typedef union {
  char __placeholder;
  struct {
    size_t pool_size;
  } pooled;
} mmbStrategyInitParam;

/* Handle to Strategy Initialization Parameters, used to initialize multiple
 * providers at once (indexed on mmbStrategy). */
typedef mmbStrategyInitParam mmbStrategyInitParamSet [MMB_STRATEGY__MAX];

#define MMB_STRATEGY_DEFAULT_INIT_PARAMS {                                  \
  [MMB_STRATEGY_NONE]     = { 0 /* no specific option */ },                 \
  [MMB_POOLED]            = { .pooled = { .pool_size = 1024*1024 } },       \
  [MMB_THREAD_SAFE]       = { 0 /* no specific option */ },                 \
  [MMB_STATISTICS]        = { 0 /* no specific option */ },                 \
}

/**  @} */

/**  @} */

/**
 * @name mmbMemoryOptions
 * @{ */

/**
 * @name Public API
 * @{ */

typedef struct s_mmbMemoryOptions {
  mmbProviderInitParamSet providers_opts;
  mmbStrategyInitParamSet strategies_opts;
} mmbMemoryOptions;

/**  @} */

/**  @} */

/**
 * @name mmbMemInterface
 * @{ */

/**
 * @name Public API
 * @{ */

typedef struct mmbMemInterfaceConfig {
  /** Default memory provider configuration. */
  mmbProvider provider;
  /* Provider specific configuration options */
  mmbProviderOptions provider_opts;
  /** Default memory allocation strategy configuration. */
  mmbStrategy strategy;
  /**
   * Default value is NULL/"" (the two options are equivalent).  The value is
   * not "owned" by the structure.  It's the user's responsability to ensure
   * the string's lifetime is at least the same as the structure's.
   */
  const char *name;
} mmbMemInterfaceConfig;

#define MMB_MEMINTERFACE_CONFIG_DEFAULT                                     \
  (const mmbMemInterfaceConfig) {                                           \
    .provider = MMB_PROVIDER_ANY,                                           \
    .strategy = MMB_STRATEGY_ANY,                                           \
    .name = "default",                                                      \
    .provider_opts = {.ptr = NULL}                                          \
  }

/**
 * @brief Allocate a default configuration for mmbMemInterface.
 *
 * @param [out] interface_out Pointer to a handle. Cannot be \c NULL.
 * @return \c MMB_OK on success, an error type otherwise.
 */
mmbError
mmb_meminterface_config_create_default(mmbMemInterfaceConfig **interface_opts);

/**
 * @brief Deallocate a configuration for mmbMemInterface.
 *
 * @param [in] interface_opts Handle to deallocate. Cannot be \c NULL.
 * @return \c MMB_OK on success, an error type otherwise.
 */
mmbError
mmb_meminterface_config_destroy(mmbMemInterfaceConfig *interface_opts);

/**
 * @brief Set default parameter to the interface configuration object.
 *
 * Default provider is \c MMB_PROVIDER_ANY.
 * Default strategy is \c MMB_STRATEGY_ANY.
 *
 * @param [inout]   interface_opts  The interface configuration object.
 * @return \c MMB_OK on success, an error type otherwise.
 */
mmbError
mmb_meminterface_config_set_default(mmbMemInterfaceConfig *interface_opts);

/**
 * @brief Configure the provider of an interface configuration structure.
 *
 * @param [inout]   interface_opts  The interface configuration object.
 * @param [in]      provider    The required provider.
 * @return \c MMB_OK on success, an error type otherwise.
 */
mmbError
mmb_meminterface_config_set_provider(mmbMemInterfaceConfig *interface_opts,
                                     mmbProvider provider);

/**
 * @brief Configure the strategy of an interface configuration structure.
 *
 * @param [inout]   interface_opts  The interface configuration object.
 * @param [in]      strategy    The required strategy.
 * @return \c MMB_OK on success, an error type otherwise.
 */
mmbError
mmb_meminterface_config_set_strategy(mmbMemInterfaceConfig *interface_opts,
                                     mmbStrategy strategy);

/**
 * @brief Configure the name of an interface configuration structure.
 *
 * @param [inout]   interface_opts  The interface configuration object.
 * @param [in]      name            The given name. Can be \c NULL.
 * @return \c MMB_OK on success, an error type otherwise.
 */
mmbError
mmb_meminterface_config_set_name(mmbMemInterfaceConfig *interface_opts,
                                 const char * name);

/**  @} */

/**  @} */

/**
 * @name mmbMemSpace
 * @{ */

/**
 * @name Public API
 * @{ */

typedef enum mmbSizeConfigAction {
  MMB_SIZE_NONE = -2,   /**< Reset the size to 0. */
  MMB_SIZE_ANY = -1,    /**< Keep the size unchanged. */
  MMB_SIZE_SET = 0,     /**< Set/Request given value. */
  MMB_SIZE_INVALID,
} mmbSizeConfigAction;

typedef struct mmbSizeConfig {
  mmbSizeConfigAction action;
  bool is_numa_aware;
  union {
    size_t mem_size;
    mmbNumaSizes numa_sizes;
  };
} mmbSizeConfig;

#define MMB_MEMSPACE_SIZE_CONFIG_INIT_DEFAULT                               \
  (const mmbSizeConfig) {                                                   \
    .action = MMB_SIZE_SET,                                                 \
    .is_numa_aware = false,                                                 \
    .mem_size = SIZE_MAX, /* unlimited */                                   \
  }

#define MMB_MEMSPACE_SIZE_CONFIG_DEFAULT                                    \
  (const mmbSizeConfig) {                                                   \
    .action = MMB_SIZE_ANY,                                                 \
    .is_numa_aware = false,                                                 \
    .mem_size = SIZE_MAX, /* unlimited */                                   \
  }

typedef struct mmbMemSpaceConfig {
  /**
   * Lower bound of the size of the memory space request, expressed in bytes.
   * The memory space will expose at least \p mem_size available bytes.
   */
  struct mmbSizeConfig size_opts;
  mmbMemInterfaceConfig interface_opts;
} mmbMemSpaceConfig;

#define MMB_MEMSPACE_CONFIG_INIT_DEFAULT                                    \
  (const mmbMemSpaceConfig) {                                               \
    .size_opts = MMB_MEMSPACE_SIZE_CONFIG_INIT_DEFAULT,                     \
    .interface_opts = MMB_MEMINTERFACE_CONFIG_DEFAULT,                      \
  }

#define MMB_MEMSPACE_CONFIG_DEFAULT                                         \
  (const mmbMemSpaceConfig) {                                               \
    .size_opts = MMB_MEMSPACE_SIZE_CONFIG_DEFAULT,                          \
    .interface_opts = MMB_MEMINTERFACE_CONFIG_DEFAULT,                      \
  }

/**
 * @brief Allocate a default initialization configuration for mmbMemSpace.
 *
 * @param [out] space_out Pointer to a handle. Cannot be \c NULL
 * @return \c MMB_OK on success, an error type otherwise.
 */
mmbError mmb_memspace_config_create_init_default(mmbMemSpaceConfig **space_opts);

/**
 * @brief Allocate a default configuration for mmbMemSpace.
 *
 * @param [out] space_out Pointer to a handle. Cannot be \c NULL.
 * @return \c MMB_OK on success, an error type otherwise.
 */
mmbError mmb_memspace_config_create_default(mmbMemSpaceConfig **space_opts);

/**
 * @brief Deallocate a configuration for mmbMemSpace.
 *
 * @param [in] space_opts Handle to deallocate. Cannot be \c NULL.
 * @return \c MMB_OK on success, an error type otherwise.
 */
mmbError mmb_memspace_config_destroy(mmbMemSpaceConfig *space_opts);

/**
 * @brief Edit the mmbMemSpace size related fields of the configuration handle.
 *
 * The \p action parameter is a flag that inform on how to consider the \p size
 * parameter.  If \p action equals \c MMB_SIZE_ANY the \p size parameter is
 * disregarded.  If \p action equals \c MMB_SIZE_SET \p size defines the
 * minimum size that must be available when requesting a space using
 * \p space_opts; if \p space_opts is used to reconfigure the mmbMemSpace,
 * \p size defines the new limit of the memory size.  This limit cannot be
 * smaller than the amount of memory already allocated into the given
 * mmbMemSpace.  If \p action equals \c MMB_SIZE_NONE, the \p size parameter is
 * ignored, and no size requirement shall be enforced when requesting a new
 * mmbMemSpace; if the configuration handle is used to modify a mmbMemSpace,
 * \c MMB_SIZE_NONE is a synonym to
 * \p mmb_memspace_config_size_opts(\p space_opts, \c MMB_SIZE_SET, \c 0).
 *
 * @param [inout] space_out Pointer to a handle. Cannot be \c NULL.
 * @param [in]    action    Size configuration type.
 * @param [in]    size      Requested size.
 * @return \c MMB_OK on success, an error type otherwise.
 */
mmbError mmb_memspace_config_size(mmbMemSpaceConfig *space_opts,
                                  mmbSizeConfigAction action,
                                  size_t size);

/**
 * @brief Defines the requested size.
 *
 * This function implies setting the \c action field to equals \c MMB_SIZE_SET.
 *
 * @param [inout] space_out Pointer to a handle. Cannot be \c NULL.
 * @param [in]    size      Requested size.
 * @return \c MMB_OK on success, an error type otherwise.
 */
mmbError mmb_memspace_config_set_size(mmbMemSpaceConfig *space_opts, size_t size);

/**
 * @brief Defines the requested size to \c MMB_SIZE_ANY.
 *
 * This function sets the \c action field to equals \c MMB_SIZE_ANY.
 *
 * @param [inout] space_out Pointer to a handle. Cannot be \c NULL.
 * @return \c MMB_OK on success, an error type otherwise.
 */
mmbError mmb_memspace_config_any_size(mmbMemSpaceConfig *space_opts);

/**
 * @brief Configure an interface configuration structure.
 *
 * @param [inout]   space_opts  The interface configuration object.
 * @param [in]      provider    The required provider.
 * @param [in]      strategy    The required strategy.
 * @return \c MMB_OK on success, an error type otherwise.
 */
mmbError mmb_memspace_config_interface(mmbMemSpaceConfig *space_opts,
                                       mmbProvider provider,
                                       mmbStrategy strategy,
                                       const char *name);

/**
 * @brief Set default parameter to the interface configuration object.
 *
 * Default provider is \c MMB_PROVIDER_ANY.
 * Default strategy is \c MMB_STRATEGY_ANY.
 *
 * @param [inout]   space_opts  The interface configuration object.
 * @return \c MMB_OK on success, an error type otherwise.
 */
mmbError mmb_memspace_config_interface_set_default(mmbMemSpaceConfig *space_opts);

/**
 * @brief Configure the provider of an interface configuration structure.
 *
 * @param [inout]   space_opts  The interface configuration object.
 * @param [in]      provider    The required provider.
 * @return \c MMB_OK on success, an error type otherwise.
 */
mmbError mmb_memspace_config_interface_set_provider(mmbMemSpaceConfig *space_opts,
                                                    mmbProvider provider);

/**
 * @brief Configure the strategy of an interface configuration structure.
 *
 * @param [inout]   space_opts  The interface configuration object.
 * @param [in]      strategy    The required strategy.
 * @return \c MMB_OK on success, an error type otherwise.
 */
mmbError mmb_memspace_config_interface_set_strategy(mmbMemSpaceConfig *space_opts,
                                                    mmbStrategy strategy);

/**
 * @brief Configure the name of an interface configuration structure.
 *
 * @param [inout]   interface_opts  The interface configuration object.
 * @param [in]      name            The given name. Can be \c NULL.
 * @return \c MMB_OK on success, an error type otherwise.
 */
mmbError mmb_memspace_config_interface_set_name(mmbMemSpaceConfig *space_opts,
                                                const char *name);

/**  @} */

/**  @} */

#ifdef __cplusplus
}
#endif

#endif /* MMB_MEMORY_OPTIONS_H */
