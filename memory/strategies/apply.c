/*
 * Copyright (C) 2018-2020 Cray UK
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <stdlib.h>
#include <stdarg.h>

#include "mmb_logging.h"
#include "mmb_error.h"
#include "mmb_memory.h"

#include "../i_memory.h"
#include "i_strategy.h"
#include "apply.h"

/**
 * @name mmb_memory
 * @{ */

/**
 * @name mmbStrategy
 * @{ */

mmbError mmb_strat_init_interface__apply(mmbMemInterface *interface, ...)
{
  va_list args;
  va_start(args, interface);
  mmb_strat_init_va_fct fwd_init_va = va_arg(args, mmb_strat_init_va_fct);
  void *fwd_data = va_arg(args, void *);
  mmbError stat = fwd_init_va(interface, fwd_data, args);
  va_end(args);
  return stat;
}

mmbError mmb_strat_finalize_interface__apply(mmbMemInterface *interface, ...)
{
  va_list args;
  va_start(args, interface);
  mmb_strat_finalize_va_fct fwd_finalize_va = va_arg(args, mmb_strat_finalize_va_fct);
  void *fwd_data = va_arg(args, void *);
  mmbError stat = fwd_finalize_va(interface, fwd_data, args);
  va_end(args);
  return stat;
}

mmbError mmb_strat_allocate__apply(const size_t n_bytes,
                                   mmbMemInterface *interface,
                                   const mmbAllocateOptions *opts,
                                   mmbAllocation *out_allocation,
                                   ...)
{
  va_list args;
  va_start(args, out_allocation);
  mmb_strat_allocate_va_fct fwd_alloc = va_arg(args, mmb_strat_allocate_va_fct);
  void *fwd_data = va_arg(args, void *);
  mmbError stat = fwd_alloc(n_bytes, interface, opts, fwd_data,
                            &out_allocation->ptr, args);
  va_end(args);
  return stat;
}

mmbError mmb_strat_free__apply(mmbAllocation *allocation, ...)
{
  va_list args;
  va_start(args, allocation);
  mmb_strat_free_va_fct fwd_free = va_arg(args, mmb_strat_free_va_fct);
  void *fwd_data = va_arg(args, void *);
  mmbError stat = fwd_free(allocation->ptr, fwd_data, allocation->interface,
                           &allocation->opts, allocation->n_bytes, args);
  va_end(args);
  return stat;
}

mmbError mmb_strat_copy_nd__apply(mmbAllocation *dst,
                                  const size_t *doffset, const size_t *dpitch,
                                  const mmbAllocation *src,
                                  const size_t *soffset, const size_t *spitch,
                                  const size_t ndims, const size_t *dims, ...)
{
  va_list args;
  va_start(args, dims);
  mmb_strat_copy_nd_va_fct fwd_copy_nd = va_arg(args, mmb_strat_copy_nd_va_fct);
  void *fwd_data = va_arg(args, void *);
  mmbError stat;
  stat = fwd_copy_nd(dst->ptr, fwd_data, dst->interface, &dst->opts, doffset, dpitch,
                     src->ptr, src->interface, &src->opts, soffset, spitch,
                     ndims, dims, args);
  va_end(args);
  return stat;
}

/**  @} */

/**  @} */
