/*
 * Copyright (C) 2020      Cray UK
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <pthread.h>

#include "mmb_error.h"
#include "mmb_logging.h"
#include "mmb_memory.h"

#include "../i_memory.h"
#include "../strategy.h"
#include "apply.h"
#include "i_strategy.h"
#include "i_thread_safe.h"

/**
 * @name mmb_memory
 * @{ */

/**
 * @name mmbStrategy
 * @{ */

/**
 * @name Thread Safe Strategy
 * @{ */

/**
 * @name public API
 * @{ */

mmbError mmb_strat_init_interface__thread_safe(mmbMemInterface *interface)
{
  /* data is already allocated in interface: Compute offset to strat data */
  const mmbProvider provider = interface->provider;
  unsigned char *data = interface->data + mmb_provider_get_data_size(provider);
  return mmb_strat_init_interface__apply(interface,
      mmb_strategies_va_table[MMB_THREAD_SAFE].init_interface, data,
      mmb_strategies_va_table[MMB_STRATEGY_NONE].init_interface, NULL);
}

mmbError mmb_strat_finalize_interface__thread_safe(mmbMemInterface *interface)
{
  /* data is already allocated in interface: Compute offset to strat data */
  const mmbProvider provider = interface->provider;
  unsigned char *data = interface->data + mmb_provider_get_data_size(provider);
  return mmb_strat_finalize_interface__apply(interface,
      mmb_strategies_va_table[MMB_THREAD_SAFE].finalize_interface, data,
      mmb_strategies_va_table[MMB_STRATEGY_NONE].finalize_interface, NULL);
}

mmbError mmb_strat_allocate__thread_safe(const size_t n_bytes,
                                         mmbMemInterface *interface,
                                         const mmbAllocateOptions *opts,
                                         mmbAllocation *allocation)
{
  /* data is already allocated in interface: Compute offset to strat data */
  const mmbProvider provider = interface->provider;
  unsigned char *data = interface->data + mmb_provider_get_data_size(provider);
  return mmb_strat_allocate__apply(n_bytes, interface, opts, allocation,
      mmb_strategies_va_table[MMB_THREAD_SAFE].allocate, data,
      mmb_strategies_va_table[MMB_STRATEGY_NONE].allocate, NULL);
}

mmbError mmb_strat_free__thread_safe(mmbAllocation *allocation)
{
  /* data is already allocated in interface: Compute offset to strat data */
  const mmbProvider provider = allocation->interface->provider;
  unsigned char *data =
    allocation->interface->data + mmb_provider_get_data_size(provider);
  return mmb_strat_free__apply(allocation,
      mmb_strategies_va_table[MMB_THREAD_SAFE].free, data,
      mmb_strategies_va_table[MMB_STRATEGY_NONE].free, NULL);
}

mmbError mmb_strat_copy_nd__thread_safe(mmbAllocation *dst,
                                        const size_t *doffset, const size_t *dpitch,
                                        const mmbAllocation *src,
                                        const size_t *soffset, const size_t *spitch,
                                        size_t ndims, const size_t *dims)
{
  /* data is already allocated in interface: Compute offset to strat data */
  const mmbProvider provider = dst->interface->provider;
  unsigned char *data = dst->interface->data + mmb_provider_get_data_size(provider);
  return mmb_strat_copy_nd__apply(
      dst, doffset, dpitch, src, soffset, spitch, ndims, dims,
      mmb_strategies_va_table[MMB_THREAD_SAFE].copy_nd, data,
      mmb_strategies_va_table[MMB_STRATEGY_NONE].copy_nd, NULL);
}

/**  @} */

/** va_arg versions */

/**
 * @name private API
 * @{ */

mmbError mmb_strat_init_interface_va__thread_safe(mmbMemInterface *interface,
                                                  void *d,
                                                  va_list args)
{
  mmbError err = MMB_OK;
  int ierr;

  /** initialize specific data */
  struct mmb_strat_thread_safe_data *data = d;
  ierr = pthread_mutex_init(&data->mutex, NULL);
  if (0 != ierr) {
    MMB_ERR("Failed to initialize mutex (err=%d).\n", ierr);
    return MMB_ERROR;
  }

  /** forward call info decode */
  mmb_strat_init_va_fct fwd_init = va_arg(args, mmb_strat_init_va_fct);
  void *fwd_data = va_arg(args, void*);

  pthread_mutex_lock(&data->mutex);

  err = fwd_init(interface, fwd_data, args);

  pthread_mutex_unlock(&data->mutex);

  return err;
}

mmbError mmb_strat_finalize_interface_va__thread_safe(mmbMemInterface *interface,
                                                      void *d,
                                                      va_list args)
{
  mmbError err = MMB_OK;
  int ierr;

  /**
   * destroy specific data  but only after releasing the lock and finalizing
   * underlying structures.
   */
  struct mmb_strat_thread_safe_data *data = d;

  /** forward call info decode */
  mmb_strat_finalize_va_fct fwd_finalize = va_arg(args, mmb_strat_finalize_va_fct);
  void *fwd_data = va_arg(args, void *);

  pthread_mutex_lock(&data->mutex);

  err = fwd_finalize(interface, fwd_data, args);

  pthread_mutex_unlock(&data->mutex);

  /* finalize our data */
  ierr = pthread_mutex_destroy(&data->mutex);
  if (0 != ierr) {
    MMB_ERR("Failed to destroy mutex (err=%d).\n", ierr);
    /* in case of error, proritize the forwarding
     * of the error over sending our plain one.   */
    if (MMB_OK == err)
      err = MMB_ERROR;
  }

  return err;
}

mmbError mmb_strat_allocate_va__thread_safe(const size_t     n_bytes,
                                            mmbMemInterface *interface,
                                            const mmbAllocateOptions *opts,
                                            void            *d,
                                            void           **allocation,
                                            va_list          args)
{
  mmbError err;
  /** Check parameters */
  if (NULL == d) {
    MMB_ERR("Data parameter cannot be NULL.\n");
    return MMB_INVALID_ARG;
  }
  struct mmb_strat_thread_safe_data *data = d;

  /** forward call info decode */
  mmb_strat_allocate_va_fct fwd_alloc = va_arg(args, mmb_strat_allocate_va_fct);
  void *fwd_data = va_arg(args, void *);

  pthread_mutex_lock(&data->mutex);

  err = fwd_alloc(n_bytes, interface, opts, fwd_data, allocation, args);

  pthread_mutex_unlock(&data->mutex);

  return err;
}

mmbError mmb_strat_free_va__thread_safe(void            *allocation,
                                        void            *d,
                                        mmbMemInterface *interface,
                                        const mmbAllocateOptions *opts,
                                        const size_t     n_bytes,
                                        va_list          args)
{
  mmbError err = MMB_OK;
  /** Check parameters */
  if (NULL == d) {
    MMB_WARN("Data parameter should not be NULL.\n");
    return MMB_INVALID_ARG;
  }
  struct mmb_strat_thread_safe_data *data = d;

  /* forward call info decode */
  mmb_strat_free_va_fct fwd_free = va_arg(args, mmb_strat_free_va_fct);
  void *fwd_data = va_arg(args, void *);

  pthread_mutex_lock(&data->mutex);

  err = fwd_free(allocation, fwd_data, interface, opts, n_bytes, args);

  pthread_mutex_unlock(&data->mutex);

  return err;
}

mmbError mmb_strat_copy_nd_va__thread_safe(void *dst, void *dst_data,
                                           mmbMemInterface *dst_interface,
                                           const mmbAllocateOptions *dst_opts,
                                           const size_t *doffset,
                                           const size_t *dpitch,
                                           const void * src,
                                           const mmbMemInterface *src_interface,
                                           const mmbAllocateOptions *src_opts,
                                           const size_t *soffset,
                                           const size_t *spitch,
                                           const size_t ndims, const size_t *dims,
                                           va_list args)
{
  (void) dst_data;
  mmbError err = MMB_OK;
  /** Check parameters */
  if (NULL == dst_data) {
    MMB_WARN("Data parameter should not be NULL.\n");
    return MMB_INVALID_ARG;
  }
  /** forward call */
  mmb_strat_copy_nd_va_fct fwd_copy_nd = va_arg(args, mmb_strat_copy_nd_va_fct);
  void *fwd_data = va_arg(args, void *);
  err = fwd_copy_nd(dst, fwd_data, dst_interface, dst_opts, doffset, dpitch,
                    src, src_interface, src_opts, soffset, spitch,
                    ndims, dims, args);
  return err;
}

/**  @} */

/**  @} */

/**  @} */

/**  @} */

