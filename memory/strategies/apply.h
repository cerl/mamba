/*
 * Copyright (C) 2018-2020 Cray UK
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef MMB_APPLY_STRATEGY_H
#define MMB_APPLY_STRATEGY_H

#include <stddef.h>
#include <stdarg.h>

#include "../../common/mmb_error.h"
#include "../mmb_memory.h"

#include "../i_memory.h"

/**
 * @name mmb_memory
 * @{ */

/**
 * @name mmbStrategy
 * @{ */

/**
 * @brief Initialize the argument variable list parameter for the given
 *        interface initialization.
 *
 * @param [inout] interface Handle to an interface structure.
 * @param [in] ... List of initializing function and their parameters.
 *
 * @return \c MMB_OK on success, an error type otherwise.
 */
mmbError mmb_strat_init_interface__apply(mmbMemInterface *interface, ...);

/**
 * @brief Initialize the argument variable list parameter for the given
 *        interface finalizing.
 *
 * @param [in] interface Handle to an interface structure.
 * @param [in] ... List of finalizing function and their parameters.
 *
 * @return \c MMB_OK on success, an error type otherwise.
 */
mmbError mmb_strat_finalize_interface__apply(mmbMemInterface *interface, ...);

/**
 * @brief Initialize the argument variable list parameter for the given
 *        interface allocation functionality.
 *
 * @param [in] n_bytes Size of the requested allocation.
 * @param [in] interface Reference for the interface structure.
 * @param [in] opts Pointer to allocation options handle.  May be \c NULL.
 * @param [out] allocation Handle to an allocation structure.
 * @param [in] ... List of allocate function and their parameters.
 *
 * @return \c MMB_OK on success, an error type otherwise.
 */
mmbError mmb_strat_allocate__apply(const size_t n_bytes,
                                   mmbMemInterface *interface,
                                   const mmbAllocateOptions *opts,
                                   mmbAllocation *allocation,
                                   ...);

/**
 * @brief Initialize the argument variable list parameter for the given
 *        interface free functionality.
 *
 * @param [in] allocation Allocation handle to be free'd.
 * @param [in] ... List of free function and their parameters.
 *
 * @return \c MMB_OK on success, an error type otherwise.
 */
mmbError mmb_strat_free__apply(mmbAllocation *allocation, ...);

/**
 * @brief Initialize the argument variable list parameter for the given
 *        interface copy_nd functionality.
 *
 * This function applies the offset to the buffer pointed by \p dest and \p src.
 *
 * @param [inout] dest Handle to the allocation structre of the written data
 * @param [in] doffset Offset in each dimensions at the begining of the
 *                     destination allocation.
 * @param [in] dpitch Pitch for each dimension of destination memory
 * @param [in] src Handle to the allocation structure of the read data
 * @param [in] soffset Offset in each dimension at the begining of the source
 *                     allocation.
 * @param [in] spitch Pitch for each dimension of source memory.
 * @param [in] ndims Diemnsionality of both allocations and of the copied seciton.
 * @param [in] dims Dimensions of the copied section.  The first dimension is
 *                  given in bytes, then following dimensions are given in
 *                  number of previous dimensions.
 * @param [in] ... List of copy function and their parameters.
 *
 * @return \c MMB_OK on success, an error type otherwise.
 */
mmbError mmb_strat_copy_nd__apply(mmbAllocation *dest,
                                  const size_t *doffset, const size_t *dpitch,
                                  const mmbAllocation *src,
                                  const size_t *soffset, const size_t *spitch,
                                  const size_t ndims, const size_t *dims, ...);

/**  @} */

/**  @} */

#endif /* MMB_APPLY_STRATEGY_H */
