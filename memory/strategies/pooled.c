/*
 * Copyright (C) 2018-2021 Cray UK
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <stdlib.h>
#include <stdarg.h>
#include <string.h>

#include "mmb_error.h"
#include "mmb_logging.h"
#include "mmb_memory.h"

#include "../i_memory.h"
#include "../strategy.h"
#include "apply.h"
#include "i_strategy.h"
#include "i_pooled.h"

/**
 * @name mmb_memory
 * @{ */

/**
 * @name mmbStrategy
 * @{ */

/**
 * @name Pooled Strategy
 * @{ */

/**
 * @name public API
 * @{ */

mmbError mmb_strat_init_interface__pooled(mmbMemInterface *interface)
{
  /* data is already allocated in interface: Compute offset to strat data */
  const mmbProvider provider = interface->provider;
  unsigned char *data = interface->data + mmb_provider_get_data_size(provider);
  return mmb_strat_init_interface__apply(interface,
      mmb_strategies_va_table[MMB_POOLED].init_interface, data,
      mmb_strategies_va_table[MMB_STRATEGY_NONE].init_interface, NULL);
}

mmbError mmb_strat_finalize_interface__pooled(mmbMemInterface *interface)
{
  /* data is already allocated in interface: Compute offset to strat data */
  const mmbProvider provider = interface->provider;
  unsigned char *data = interface->data + mmb_provider_get_data_size(provider);
  return mmb_strat_finalize_interface__apply(interface,
      mmb_strategies_va_table[MMB_POOLED].finalize_interface, data,
      mmb_strategies_va_table[MMB_STRATEGY_NONE].finalize_interface, NULL);
}

mmbError mmb_strat_allocate__pooled(const size_t n_bytes,
                                    mmbMemInterface *interface,
                                    const mmbAllocateOptions *opts,
                                    mmbAllocation *allocation)
{
  /* data is already allocated in interface: Compute offset to strat data */
  const mmbProvider provider = interface->provider;
  unsigned char *data = interface->data + mmb_provider_get_data_size(provider);
  return mmb_strat_allocate__apply(n_bytes, interface, opts, allocation,
      mmb_strategies_va_table[MMB_POOLED].allocate, data,
      mmb_strategies_va_table[MMB_STRATEGY_NONE].allocate, NULL);
}

mmbError mmb_strat_free__pooled(mmbAllocation *allocation)
{
  /* data is already allocated in interface: Compute offset to strat data */
  const mmbProvider provider = allocation->interface->provider;
  unsigned char *data =
    allocation->interface->data + mmb_provider_get_data_size(provider);
  return mmb_strat_free__apply(allocation,
      mmb_strategies_va_table[MMB_POOLED].free, data,
      mmb_strategies_va_table[MMB_STRATEGY_NONE].free, NULL);
}

mmbError mmb_strat_copy_nd__pooled(mmbAllocation *dst,
                                   const size_t *doffset, const size_t *dpitch,
                                   const mmbAllocation *src,
                                   const size_t *soffset, const size_t *spitch,
                                   const size_t ndims, const size_t *dims)
{
  /* data is already allocated in interface: Compute offset to strat data */
  const mmbProvider provider = dst->interface->provider;
  unsigned char *data = dst->interface->data + mmb_provider_get_data_size(provider);
  return mmb_strat_copy_nd__apply(
          dst, doffset, dpitch, src, soffset, spitch, ndims, dims,
          mmb_strategies_va_table[MMB_POOLED].copy_nd, data,
          mmb_strategies_va_table[MMB_STRATEGY_NONE].copy_nd, NULL);
}

/**  @} */

/** va_arg versions */

/**
 * @name private API
 * @{ */

static size_t POOLED_SLAB_SIZE = 1024*1024;

mmbError mmb_strat_init__pooled(const mmbStrategyInitParam *param)
{
  if (NULL != param)
    if (0 < param->pooled.pool_size)
      POOLED_SLAB_SIZE = param->pooled.pool_size;
    else {
      MMB_ERR("Pooled strategy cannot work with slab that small (%zu).\n",
          param->pooled.pool_size);
      return MMB_INVALID_ARG;
    }
  else
    POOLED_SLAB_SIZE = 1024*1024;
  return MMB_OK;
}

mmbError mmb_strat_init_interface_va__pooled(mmbMemInterface *interface, void *d,
                                             va_list args)
{
  (void) d;
  mmb_strat_init_va_fct fwd_init = va_arg(args, mmb_strat_init_va_fct);
  void *fwd_data = va_arg(args, void*);
  return fwd_init(interface, fwd_data, args);
}

mmbError mmb_strat_finalize_interface_va__pooled(mmbMemInterface *interface,
                                                 void *d,
                                                 va_list args)
{
  /** destroy specific data */
  struct mmb_strat_pooled_data *data = d;
  while (data->next) {
    struct mmb_strat_pooled_data *tmp = data->next;

#ifndef NDEBUG
    if (NULL != tmp->data_) {
      MMB_WARN("Unreleased allocated data in slab starting at address %p.\n",
           tmp->data_);
    }
#endif /* not(NDEBUG) */

    data->next = tmp->next;
    free(tmp);
  }

#ifndef NDEBUG
  if (NULL != data->data_) {
    MMB_WARN("Unreleased allocated data in slab starting at address %p.\n",
         data->data_);
  }
#endif /* not(NDEBUG) */

  memset(data, 0, sizeof *data );

  /** forward call */
  mmb_strat_finalize_va_fct fwd_finalize = va_arg(args, mmb_strat_finalize_va_fct);
  void *fwd_data = va_arg(args, void *);
  return fwd_finalize(interface, fwd_data, args);
}

static struct mmb_strat_pooled_data *
find_suitable_slab(const size_t n_bytes, struct mmb_strat_pooled_data *slabs)
{
  /** Find suitable slab (enough space left) or an empty one.
   * The last one always is an "empty" (zero'd) one. */
  while ((0 == slabs->length && NULL != *(void **)&slabs->last)
            || (0 < slabs->length && n_bytes > slabs->length))
  {
    slabs = slabs->next;
  }
  return slabs;
}

static mmbError init_slab(struct mmb_strat_pooled_data *slab,
                          const size_t                  n_bytes,
                          mmbMemInterface              *interface,
                          const mmbAllocateOptions     *opts,
                          va_list                       args)
{
  mmbError err = MMB_OK;
  /* Empty slab */
  if (NULL == slab->next) {
    /* Because of possible deadlock, we have to first allocate the structures
     * to be used before forwarding the allocation call, as in case of error
     * there would be no way to release the memory. */
    slab->next = calloc(1, sizeof *slab);
    if (NULL == slab->next) {
      MMB_ERR("Memory exhaustion. Cannot allocate next slab metadata. Abort.\n");
      return MMB_OUT_OF_MEMORY;
    }
  }
  /** Compute the required size to allocate for the new slice */
  size_t alloc_size = POOLED_SLAB_SIZE;
  while (n_bytes > alloc_size) {
    alloc_size *= 1.4;
  }
  /** forward call info decode */
  mmb_strat_allocate_va_fct fwd_alloc = va_arg(args, mmb_strat_allocate_va_fct);
  void *fwd_data = va_arg(args, void *);
  /** allocate slab */
  err = fwd_alloc(alloc_size, interface, opts, fwd_data, &slab->data_, args);
  if (MMB_OK != err) {
    MMB_WARN("Unable to allocate new memory slab of size %zu bytes (%d).\n",
         alloc_size, err);
    return err;
  }
  /* Set metadata about the slab */
  slab->length = alloc_size;
  return err;
}

static mmbError fini_slab(struct mmb_strat_pooled_data *slab,
                          mmbMemInterface              *interface,
                          const mmbAllocateOptions     *opts,
                          va_list                       args)
{
  mmbError err = MMB_OK;
  /** If the slab has been totally emptied, free the memory */
  if (NULL == *(void **)&slab->last) {
    /* forward call info decode */
    mmb_strat_free_va_fct fwd_free = va_arg(args, mmb_strat_free_va_fct);
    void *fwd_data = va_arg(args, void *);
    err = fwd_free(slab->data_, fwd_data, interface, opts, slab->length, args);

    /* Reset length to 0 so the slab appear free
     * to be reinitialized in find_suitable_slab. */
    slab->length = 0;

#ifndef NDEBUG
    slab->data_ = NULL;
#endif /* not(NDEBUG) */

  }

  return err;
}

/**
 * @name Inlined metadata allocation mode
 * @{ */

/* slab inlined allocated elements */
struct strat_pooled_ielt {
  struct strat_pooled_ielt *prev;
  struct mmb_strat_pooled_data *slab;
  size_t length;
  unsigned char ptr[];
};

static mmbError
allocate_pooled_inline(const size_t                  n_bytes,
                       mmbMemInterface              *interface,
                       const mmbAllocateOptions     *opts,
                       struct mmb_strat_pooled_data *data,
                       struct strat_pooled_ielt    **out_elt,
                       va_list                       args)
{
  mmbError err = MMB_OK;

  /** Find suitable slab */
  struct strat_pooled_ielt *new_elt = NULL;
  data = find_suitable_slab(n_bytes + sizeof *new_elt, data);

  /** Check if new allocation is required */
  if (0 == data->length) { /* Empty slab */
    /* If we have to allocate a new slab we have to be able to fit the data +
     * the fake 0 sized element at the beginning + the new element metadata. */
    err = init_slab(data, n_bytes + 2 * sizeof *new_elt, interface, opts, args);
    if (MMB_OK != err) {
      MMB_ERR("Cannot initialise new slab.\n");
      return err;
    }
    /* Update metadata for the newly allocated slab (fake elt header). */
    data->length -= sizeof *new_elt;
    data->last.inlined = data->data_;
    /* fake elt of size 0 in the beginning of the
     * slab to manage the data freeing properly. */
    data->last.inlined->prev = NULL;
    data->last.inlined->length = 0;
    data->last.inlined->slab = data;
  }

  /** provide some memory from the available slab */
  struct strat_pooled_ielt *last = data->last.inlined;
  new_elt = (struct strat_pooled_ielt *) (last->ptr + last->length);
  new_elt->prev      = last;
  new_elt->length    = n_bytes;
  new_elt->slab      = data;

  /** Update slab size with the size of the inlined element */
  data->last.inlined = new_elt;
  data->length      -= n_bytes + sizeof *new_elt;

  /** Set for returned value */
  *out_elt = new_elt;

  return err;
}

static mmbError
free_pooled_inline(void                         *allocation_,
                   struct mmb_strat_pooled_data *d,
                   mmbMemInterface              *interface,
                   const mmbAllocateOptions     *opts,
                   const size_t                  n_bytes,
                   va_list                       args)
{
  (void) d;
  (void) n_bytes;
  unsigned char *allocation = allocation_;
  mmbError err = MMB_OK;

  /** retrieve allocation metadata stored in front of the allocation */
  struct strat_pooled_ielt *crt_elt  =
    (struct strat_pooled_ielt *) (allocation - sizeof *crt_elt);
  struct strat_pooled_ielt *next     =
    (struct strat_pooled_ielt *) (crt_elt->ptr + crt_elt->length);
  struct mmb_strat_pooled_data *data = crt_elt->slab;

  if (data->last.inlined == crt_elt) { /** tail allocated slice */
    if (NULL == crt_elt->prev->prev) { /** Last element of the slab */
      crt_elt->prev->length += crt_elt->length + sizeof *crt_elt;
      next = crt_elt;
      crt_elt = crt_elt->prev;
      next->prev = NULL;
    }
    data->length       += crt_elt->length + sizeof *crt_elt;
    /* Don't reset data->last to not NULL as it is the sign that
     * the slab is totally free'd and fini_slab has work to do. */
    data->last.inlined  = crt_elt->prev;
    crt_elt->prev = NULL;
  } else {
    /** case where we are a bit in the middle of the allocated slices */
    crt_elt->prev->length += crt_elt->length + sizeof *crt_elt;
    next->prev = crt_elt->prev;
  }

  /** Potentially free the slab if no more memory is using it */
  fini_slab(data, interface, opts, args);

  return err;
}

/**  @} */

/**
 * @name External metadata allocation mode
 * @{ */

/* slab inlined allocated elements */
struct strat_pooled_elt {
  struct strat_pooled_elt *prev, *next;
  size_t length;
  void  *ptr;
};

static mmbError
allocate_pooled(const size_t                  n_bytes,
                mmbMemInterface              *interface,
                const mmbAllocateOptions     *opts,
                struct mmb_strat_pooled_data *data,
                struct strat_pooled_elt     **out_elt,
                va_list                       args)
{
  mmbError err = MMB_OK;

  /** Find suitable slab */
  data = find_suitable_slab(n_bytes, data);

  /** Check if new allocation is required */
  if (0 == data->length) { /* Empty slab */
    err = init_slab(data, n_bytes, interface, opts, args);
    if (MMB_OK != err) {
      MMB_ERR("Cannot initialise new slab.\n");
      return err;
    }
    /* fake elt of size 0 in the beginning of the slab to manage the data
     * freeing properly */
    data->last.plain         = malloc(sizeof *data->last.plain);
    if (NULL == data->last.plain) {
      MMB_ERR("Failed to allocate allocation metadata.\n");
      return MMB_OUT_OF_MEMORY;
    }
    data->last.plain->ptr    = data->data_;
    data->last.plain->prev   = NULL;
    data->last.plain->length = 0;
  }

  /** Allocate and set new element */
  struct strat_pooled_elt *new_elt = malloc(sizeof *new_elt);
  new_elt->length   = n_bytes;
  new_elt->next     = NULL;
  new_elt->prev     = data->last.plain;
  new_elt->ptr      =
    (unsigned char *) data->last.plain->ptr + data->last.plain->length;

  /** Insert new element and update the slab metadata */
  data->length          -= n_bytes;
  data->last.plain->next = new_elt;
  data->last.plain       = new_elt;

  /** Set for returned value */
  *out_elt = new_elt;

  return err;
}

static mmbError
free_pooled(void                         *allocation,
            struct mmb_strat_pooled_data *data,
            mmbMemInterface              *interface,
            const mmbAllocateOptions     *opts,
            const size_t                  n_bytes,
            va_list                       args)
{
  (void) n_bytes;
  mmbError err = MMB_OK;

  /** Retrieve the proper slab that contains the allocation */
  while (NULL == data->data_ || NULL == data->last.plain
          || data->data_ > allocation
          || data->last.plain->ptr < allocation)
  {
    if (NULL == data->next) {
      MMB_ERR("The alloc_handle (%p) cannot be found in any slice.\n",
          allocation);
      return MMB_ERROR;
    }

    data = data->next;
  }

  /** Retrieve the proper metadata */
  struct strat_pooled_elt *alloc_handle = data->last.plain;
  while (NULL != alloc_handle && alloc_handle->ptr != allocation) {
    alloc_handle = alloc_handle->prev;
  }

  /** Check we found the right data */
  if (NULL == alloc_handle) {
    MMB_ERR("The alloc_handle (%p) cannot be found for slice between %p and %p.\n",
        allocation, data->data_, data->last.plain->ptr);
    return MMB_ERROR;
  }

  if (NULL == alloc_handle->next) { /** tail allocated slice */
    if (NULL == alloc_handle->prev->prev) { /** Last element of the slab */
      alloc_handle->prev->length += alloc_handle->length;
      alloc_handle = alloc_handle->prev;
      free(alloc_handle->next);
    } else {
      alloc_handle->prev->next = NULL;
    }
    data->length += alloc_handle->length;
    /* Don't reset data->last to not NULL as it is the
     * sign that the slab is totally free'd. */
    data->last.plain = alloc_handle->prev;
    free(alloc_handle);
  } else {
    /** case where we are a bit in the middle of the allocated slices */
    alloc_handle->prev->length += alloc_handle->length;
    alloc_handle->prev->next = alloc_handle->next;
    alloc_handle->next->prev = alloc_handle->prev;
    free(alloc_handle);
  }

  /** Potentially free the slab if no more memory is using it */
  err = fini_slab(data, interface, opts, args);

  return err;
}

/**  @} */

/**
 * @brief Align the end of the requested allocation size on \p align bytes.
 *
 * @param [in] size Requested size
 * @param [in] align Alignement requirement
 *
 * @return A size greater than \p size that is factor of \p align.
 */
/* static inline size_t align_on(const size_t size, const size_t align) { */
  /* TODO: might be changed for (size + 1 + ((align - 1) & ~size)), when align
   *       is a power of 2 to avoid doing the costly modulo operation, but that
   *       would add `align` unneccesary bytes if `size` is already aligned.
   */
/*  return size + align - size % align;
}*/

mmbError mmb_strat_allocate_va__pooled(const size_t     n_bytes,
                                       mmbMemInterface *interface,
                                       const mmbAllocateOptions *opts,
                                       void            *d,
                                       void           **allocation,
                                       va_list          args)
{
  mmbError err;
  /** Check parameters */
  if (NULL == d) {
    MMB_ERR("Data parameter cannot be NULL.\n");
    return MMB_INVALID_ARG;
  }

  /** Inline allocation supported? */
  union {
    struct strat_pooled_ielt *inlined;
    struct strat_pooled_elt  *plain;
  } new_elt;
  void *new_elt_ptr = NULL;
  // size_t size = align_on(n_bytes, 8); /* Align on 64 bits */
  switch (interface->space->ex_context) {
    case MMB_CPU:
      err = allocate_pooled_inline(n_bytes, interface, opts, d, &new_elt.inlined, args);
      new_elt_ptr = new_elt.inlined->ptr;
      break;
    default:
      err = allocate_pooled(n_bytes, interface, opts, d, &new_elt.plain, args);
      new_elt_ptr = new_elt.plain->ptr;
  }

  if (MMB_OK != err) {
    MMB_ERR("Unable to allocate %zu bytes of memory (%d).\n", n_bytes, err);
    return err;
  }

  /** return the allocated slice (only if no error) */
  *allocation = new_elt_ptr;

  return err;
}

mmbError mmb_strat_free_va__pooled(void            *allocation,
                                   void            *d,
                                   mmbMemInterface *interface,
                                   const mmbAllocateOptions *opts,
                                   const size_t     n_bytes,
                                   va_list          args)
{
  mmbError err = MMB_OK;
  /** Check parameters */
  if (NULL == d) {
    MMB_WARN("Data parameter should not be NULL.\n");
    return MMB_INVALID_ARG;
  }

  /** Inline allocation supported? */
  switch (interface->space->ex_context) {
    case MMB_CPU:
      err = free_pooled_inline(allocation, d, interface, opts, n_bytes, args);
      break;
    default:
      err = free_pooled(allocation, d, interface, opts, n_bytes, args);
  }

  return err;
}

mmbError mmb_strat_copy_nd_va__pooled(void *dst, void *dst_data,
                                      mmbMemInterface *dst_interface,
                                      const mmbAllocateOptions *dst_opts,
                                      const size_t *doffset, const size_t *dpitch,
                                      const void * src,
                                      const mmbMemInterface *src_interface,
                                      const mmbAllocateOptions *src_opts,
                                      const size_t *soffset, const size_t *spitch,
                                      const size_t ndims, const size_t *dims,
                                      va_list args)
{
  (void) dst_data;
  /* POC of chaining strategies. just forward */
  /** forward call */
  mmb_strat_copy_nd_va_fct fwd_copy_nd = va_arg(args, mmb_strat_copy_nd_va_fct);
  void *fwd_data = va_arg(args, void *);
  return fwd_copy_nd(dst, fwd_data, dst_interface, dst_opts, doffset, dpitch,
                     src, src_interface, src_opts, soffset, spitch,
                     ndims, dims, args);
}

/**  @} */

/**  @} */

/******************************************************************************/

/**
 * @name Examples of dummy strategies
 * @{ */

/** thread_safe_pooled */
mmbError mmb_strat_init_interface__pooled_thread_safe(mmbMemInterface *interface)
{
  const mmbProvider provider = interface->provider;
  unsigned char *thread_data, *pooled_data;
  thread_data = interface->data + mmb_provider_get_data_size(provider);
  pooled_data = thread_data + mmb_strategies_va_table[MMB_THREAD_SAFE].data_size;
  return mmb_strat_init_interface__apply(interface,
      mmb_strategies_va_table[MMB_THREAD_SAFE].init_interface, thread_data,
      mmb_strategies_va_table[MMB_POOLED].init_interface, pooled_data,
      mmb_strategies_va_table[MMB_STRATEGY_NONE].init_interface, NULL);
}

/** thread_safe_pooled_thread_safe_memory */
mmbError mmb_strat_init_interface__pooled_full_thread_safe(mmbMemInterface *interface)
{
  const mmbProvider provider = interface->provider;
  unsigned char *pool_thread_data, *pooled_data, *mem_thread_data;
  pool_thread_data = interface->data + mmb_provider_get_data_size(provider);
  pooled_data = pool_thread_data + mmb_strategies_va_table[MMB_THREAD_SAFE].data_size;
  mem_thread_data = pooled_data + mmb_strategies_va_table[MMB_POOLED].data_size;
  return mmb_strat_init_interface__apply(interface,
      mmb_strategies_va_table[MMB_THREAD_SAFE].init_interface, pool_thread_data,
      mmb_strategies_va_table[MMB_POOLED].init_interface, pooled_data,
      mmb_strategies_va_table[MMB_THREAD_SAFE].init_interface, mem_thread_data,
      mmb_strategies_va_table[MMB_STRATEGY_NONE].init_interface, NULL);
}

/**  @} */

/**  @} */

/**  @} */
