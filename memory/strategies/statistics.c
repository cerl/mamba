/*
 * Copyright (C) 2021      Cray UK
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <stdlib.h>
#include <stdarg.h>
#include <string.h>

#include "mmb_error.h"
#include "mmb_logging.h"
#include "mmb_memory.h"

#include "../i_memory.h"
#include "../strategy.h"
#include "../statistics.h"
#include "apply.h"
#include "i_strategy.h"
#include "i_statistics.h"

#include "../../common/i_mmb_hash.h"

/**
 * @name mmb_memory
 * @{ */

/**
 * @name mmbStrategy
 * @{ */

/**
 * @name Statistics Strategy
 * @{ */

mmbError mmb_strat_stats_check_interface(mmbMemInterface *interface)
{
  if (NULL == interface) {
    MMB_ERR("Invalid interface handle, cannot be NULL.");
    return MMB_INVALID_INTERFACE;
  }
  if (!(    MMB_STATISTICS == interface->strategy
        ||  MMB_POOLED_STATISTICS == interface->strategy)) {
    MMB_ERR("Invalid handle (wrongs strategy: %s instead of %s or %s).\n",
        mmb_strategy_get_string(interface->strategy),
        mmb_strategy_get_string(MMB_STATISTICS),
        mmb_strategy_get_string(MMB_POOLED_STATISTICS));
    return MMB_INVALID_INTERFACE;
  }
  return MMB_OK;
}

/**
 * @name public API
 * @{ */

mmbError mmb_strat_init_interface__stats(mmbMemInterface *interface)
{
  mmbError stat = MMB_OK;
  /* data is already allocated in interface: Compute offset to strat data */
  const mmbProvider provider = interface->provider;
  unsigned char *data = interface->data + mmb_provider_get_data_size(provider);
  stat = mmb_strat_init_interface__apply(interface,
      mmb_strategies_va_table[MMB_STATISTICS].init_interface, data,
      mmb_strategies_va_table[MMB_STRATEGY_NONE].init_interface, NULL);
  return stat;
}

mmbError mmb_strat_finalize_interface__stats(mmbMemInterface *interface)
{
  mmbError stat = MMB_OK;
  /* data is already allocated in interface: Compute offset to strat data */
  const mmbProvider provider = interface->provider;
  unsigned char *data = interface->data + mmb_provider_get_data_size(provider);
  stat = mmb_strat_finalize_interface__apply(interface,
      mmb_strategies_va_table[MMB_STATISTICS].finalize_interface, data,
      mmb_strategies_va_table[MMB_STRATEGY_NONE].finalize_interface, NULL);
  return stat;
}

mmbError mmb_strat_allocate__stats(const size_t n_bytes,
                                    mmbMemInterface *interface,
                                    const mmbAllocateOptions *opts,
                                    mmbAllocation *allocation)
{
  mmbError stat = MMB_OK;
  /* data is already allocated in interface: Compute offset to strat data */
  const mmbProvider provider = interface->provider;
  unsigned char *data = interface->data + mmb_provider_get_data_size(provider);
  stat = mmb_strat_allocate__apply(n_bytes, interface, opts, allocation,
      mmb_strategies_va_table[MMB_STATISTICS].allocate, data,
      mmb_strategies_va_table[MMB_STRATEGY_NONE].allocate, NULL);
  return stat;
}

mmbError mmb_strat_free__stats(mmbAllocation *allocation)
{
  mmbError stat = MMB_OK;
  /* data is already allocated in interface: Compute offset to strat data */
  const mmbProvider provider = allocation->interface->provider;
  unsigned char *data =
    allocation->interface->data + mmb_provider_get_data_size(provider);
  stat = mmb_strat_free__apply(allocation,
      mmb_strategies_va_table[MMB_STATISTICS].free, data,
      mmb_strategies_va_table[MMB_STRATEGY_NONE].free, NULL);
  return stat;
}

mmbError mmb_strat_copy_nd__stats(mmbAllocation *dst,
                                  const size_t *doffset, const size_t *dpitch,
                                  const mmbAllocation *src,
                                  const size_t *soffset, const size_t *spitch,
                                  const size_t ndims, const size_t *dims)
{
  /* data is already allocated in interface: Compute offset to strat data */
  const mmbProvider provider = dst->interface->provider;
  unsigned char *data = dst->interface->data + mmb_provider_get_data_size(provider);
  return mmb_strat_copy_nd__apply(
          dst, doffset, dpitch, src, soffset, spitch, ndims, dims,
          mmb_strategies_va_table[MMB_STATISTICS].copy_nd, data,
          mmb_strategies_va_table[MMB_STRATEGY_NONE].copy_nd, NULL);
}

/**  @} */

/** va_arg versions */

/**
 * @name private API
 * @{ */

mmbError mmb_strat_init_interface_va__stats(mmbMemInterface *interface, void *d,
                                            va_list args)
{
  struct mmb_strat_stats_data *data = d;
  mmbAllocStats *stats = interface->space->stats;
  mmbError stat = mmb_stats_register_interface(stats, interface, data);
  if (MMB_OK != stat) {
    MMB_ERR("Unable to register interface into statistics recording hashmap.\n");
    return stat;
  }
  pthread_mutex_init(&data->stats_mtx, NULL);
  mmb_strat_init_va_fct fwd_init = va_arg(args, mmb_strat_init_va_fct);
  void *fwd_data = va_arg(args, void*);
  return fwd_init(interface, fwd_data, args);
}

mmbError mmb_strat_finalize_interface_va__stats(mmbMemInterface *interface,
                                                void *d,
                                                va_list args)
{
  struct mmb_strat_stats_data *data = d;
  mmbAllocStats *stats = interface->space->stats;
  mmbError stat = mmb_stats_deregister_interface(stats, interface, data);
  if (MMB_OK != stat) {
    MMB_ERR("Unable to register interface into statistics recording hashmap.\n");
    return stat;
  }
  pthread_mutex_destroy(&data->stats_mtx);
  /** forward call */
  mmb_strat_finalize_va_fct fwd_finalize = va_arg(args, mmb_strat_finalize_va_fct);
  void *fwd_data = va_arg(args, void *);
  return fwd_finalize(interface, fwd_data, args);
}

mmbError mmb_strat_allocate_va__stats(const size_t     n_bytes,
                                       mmbMemInterface *interface,
                                       const mmbAllocateOptions *opts,
                                       void            *d,
                                       void           **allocation,
                                       va_list          args)
{
  mmbError err;
  /** Check parameters */
  if (NULL == d) {
    MMB_WARN("Data parameter cannot be NULL.\n");
    return MMB_INVALID_ARG;
  }
  /** forward call info decode */
  mmb_strat_allocate_va_fct fwd_alloc = va_arg(args, mmb_strat_allocate_va_fct);
  void *fwd_data = va_arg(args, void *);
  err = fwd_alloc(n_bytes, interface, opts, fwd_data, allocation, args);
  if (MMB_OK != err) {
    MMB_WARN("Unable to allocate %zu bytes of memory.\n", n_bytes);
    return err;
  }
  struct mmb_strat_stats_data *data = d;
  mmb_strat_stats_lock_data(data);
  data->current_allocations += 1;
  data->current_allocated += n_bytes;
  data->total_allocations += 1;
  data->total_allocated += n_bytes;
  mmb_strat_stats_unlock_data(data);
  return err;
}

mmbError mmb_strat_free_va__stats(void            *allocation,
                                   void            *d,
                                   mmbMemInterface *interface,
                                   const mmbAllocateOptions *opts,
                                   const size_t     n_bytes,
                                   va_list          args)
{
  mmbError err = MMB_OK;
  /** Check parameters */
  if (NULL == d) {
    MMB_WARN("Data parameter should not be NULL.\n");
    return MMB_INVALID_ARG;
  }
  /** forward call info decode */
  mmb_strat_free_va_fct fwd_free = va_arg(args, mmb_strat_free_va_fct);
  void *fwd_data = va_arg(args, void *);
  err = fwd_free(allocation, fwd_data, interface, opts, n_bytes, args);
  if (MMB_OK != err) {
    MMB_ERR("Unable to allocate %zu bytes of memory.\n", n_bytes);
    return err;
  }
  struct mmb_strat_stats_data *data = d;
  mmb_strat_stats_lock_data(data);
  data->current_allocations -= 1;
  data->current_allocated -= n_bytes;
  mmb_strat_stats_unlock_data(data);
  return err;
}

mmbError mmb_strat_copy_nd_va__stats(void *dst, void *dst_data,
                                      mmbMemInterface *dst_interface,
                                      const mmbAllocateOptions *dst_opts,
                                      const size_t *doffset, const size_t *dpitch,
                                      const void * src,
                                      const mmbMemInterface *src_interface,
                                      const mmbAllocateOptions *src_opts,
                                      const size_t *soffset, const size_t *spitch,
                                      const size_t ndims, const size_t *dims,
                                      va_list args)
{
  (void) dst_data;
  /* POC of chaining strategies. just forward */
  /** forward call */
  mmb_strat_copy_nd_va_fct fwd_copy_nd = va_arg(args, mmb_strat_copy_nd_va_fct);
  void *fwd_data = va_arg(args, void *);
  return fwd_copy_nd(dst, fwd_data, dst_interface, dst_opts, doffset, dpitch,
                     src, src_interface, src_opts, soffset, spitch, ndims, dims,
                     args);
}

/**  @} */

/**  @} */

/**
 * @name Pooled with Statistics Strategy
 * @{ */

/**
 * @name public API
 * @{ */

mmbError mmb_strat_init_interface__pooled_stats(mmbMemInterface *interface)
{
  mmbError stat = MMB_OK;
  /* data is already allocated in interface: Compute offset to strat data */
  const mmbProvider provider = interface->provider;
  unsigned char *pooled_data = interface->data + mmb_provider_get_data_size(provider);
  unsigned char *stats_data =
    pooled_data + mmb_strategies_va_table[MMB_POOLED].data_size;
  stat = mmb_strat_init_interface__apply(interface,
      mmb_strategies_va_table[MMB_POOLED].init_interface, pooled_data,
      mmb_strategies_va_table[MMB_STATISTICS].init_interface, stats_data,
      mmb_strategies_va_table[MMB_STRATEGY_NONE].init_interface, NULL);
  return stat;
}

mmbError mmb_strat_finalize_interface__pooled_stats(mmbMemInterface *interface)
{
  mmbError stat = MMB_OK;
  /* data is already allocated in interface: Compute offset to strat data */
  const mmbProvider provider = interface->provider;
  unsigned char *pooled_data = interface->data + mmb_provider_get_data_size(provider);
  unsigned char *stats_data =
    pooled_data + mmb_strategies_va_table[MMB_POOLED].data_size;
  stat = mmb_strat_finalize_interface__apply(interface,
      mmb_strategies_va_table[MMB_POOLED].finalize_interface, pooled_data,
      mmb_strategies_va_table[MMB_STATISTICS].finalize_interface, stats_data,
      mmb_strategies_va_table[MMB_STRATEGY_NONE].finalize_interface, NULL);
  return stat;
}

mmbError mmb_strat_allocate__pooled_stats(const size_t n_bytes,
                                    mmbMemInterface *interface,
                                    const mmbAllocateOptions *opts,
                                    mmbAllocation *allocation)
{
  mmbError stat = MMB_OK;
  /* data is already allocated in interface: Compute offset to strat data */
  const mmbProvider provider = interface->provider;
  unsigned char *pooled_data = interface->data + mmb_provider_get_data_size(provider);
  unsigned char *stats_data =
    pooled_data + mmb_strategies_va_table[MMB_POOLED].data_size;
  stat = mmb_strat_allocate__apply(n_bytes, interface, opts, allocation,
      mmb_strategies_va_table[MMB_POOLED].allocate, pooled_data,
      mmb_strategies_va_table[MMB_STATISTICS].allocate, stats_data,
      mmb_strategies_va_table[MMB_STRATEGY_NONE].allocate, NULL);
  return stat;
}

mmbError mmb_strat_free__pooled_stats(mmbAllocation *allocation)
{
  mmbError stat = MMB_OK;
  /* data is already allocated in interface: Compute offset to strat data */
  const mmbProvider provider = allocation->interface->provider;
  unsigned char *pooled_data =
    allocation->interface->data + mmb_provider_get_data_size(provider);
  unsigned char *stats_data =
    pooled_data + mmb_strategies_va_table[MMB_POOLED].data_size;
  stat = mmb_strat_free__apply(allocation,
      mmb_strategies_va_table[MMB_POOLED].free, pooled_data,
      mmb_strategies_va_table[MMB_STATISTICS].free, stats_data,
      mmb_strategies_va_table[MMB_STRATEGY_NONE].free, NULL);
  return stat;
}

mmbError mmb_strat_copy_nd__pooled_stats(mmbAllocation *dst,
                                         const size_t *doffset,
                                         const size_t *dpitch,
                                         const mmbAllocation *src,
                                         const size_t *soffset,
                                         const size_t *spitch,
                                         const size_t ndims,
                                         const size_t *dims)
{
  /* data is already allocated in interface: Compute offset to strat data */
  const mmbProvider provider = dst->interface->provider;
  unsigned char *pooled_data =
    dst->interface->data + mmb_provider_get_data_size(provider);
  unsigned char *stats_data =
    pooled_data + mmb_strategies_va_table[MMB_POOLED].data_size;
  return mmb_strat_copy_nd__apply(
      dst, doffset, dpitch, src, soffset, spitch, ndims, dims,
      mmb_strategies_va_table[MMB_POOLED].copy_nd, pooled_data,
      mmb_strategies_va_table[MMB_STATISTICS].copy_nd, stats_data,
      mmb_strategies_va_table[MMB_STRATEGY_NONE].copy_nd, NULL);
}

/**  @} */

/**  @} */

/**  @} */

/**  @} */
