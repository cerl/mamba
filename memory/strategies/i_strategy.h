/*
 * Copyright (C) 2018-2020 Cray UK
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef MMB_I_STRATEGY_H
#define MMB_I_STRATEGY_H

#include <stdlib.h>
#include <stdarg.h>

#include "../../common/mmb_error.h"
#include "../mmb_memory.h"
#include "../mmb_memory_options.h"

#include "../i_memory.h"

/**
 * @name mmb_memory
 * @{ */

/**
 * @name mmbStrategy
 * @{ */

/**
 * @name Private API
 *
 * This describe the set of functions needed to create new strategy building blocks.
 *
 * @{ */

typedef const size_t mmb_strat_data_size;
typedef mmbError (*mmb_strat_init_va_fct)(mmbMemInterface *interface,
                                          void *data, va_list args);
typedef mmbError (*mmb_strat_finalize_va_fct)(mmbMemInterface *interface,
                                              void *data, va_list args);
typedef mmbError (*mmb_strat_allocate_va_fct)(const size_t n_bytes,
                                              mmbMemInterface *interface,
                                              const mmbAllocateOptions *opts,
                                              void *data, void **allocation,
                                              va_list args);
typedef mmbError (*mmb_strat_free_va_fct)(void *allocation, void *data,
                                          mmbMemInterface *interface,
                                          const mmbAllocateOptions *opts,
                                          const size_t n_bytes, va_list args);
typedef mmbError (*mmb_strat_copy_nd_va_fct)(void *dst,
                                             void *dst_data,
                                             mmbMemInterface *dst_inter,
                                             const mmbAllocateOptions *dst_opts,
                                             const size_t *doffset,
                                             const size_t *dpitch,
                                             const void *src,
                                             const mmbMemInterface *src_inter,
                                             const mmbAllocateOptions *src_opts,
                                             const size_t *soffset,
                                             const size_t *spitch,
                                             const size_t ndims,
                                             const size_t *dims,
                                             va_list args);

/**
 * @brief Strategy dispatch table entry.
 *
 * These functions will be used to create a strategy by the strategy builder.
 */
struct mmb_dispatch_strategy_va_entry {
  mmbError                  (*init)     (const mmbStrategyInitParam *);
  mmbError                  (*finalize) (void);
  mmb_strat_init_va_fct     init_interface;
  mmb_strat_finalize_va_fct finalize_interface;
  mmb_strat_allocate_va_fct allocate;
  mmb_strat_free_va_fct     free;
  mmb_strat_copy_nd_va_fct  copy_nd;
  mmb_strat_data_size       data_size;
};

extern const struct mmb_dispatch_strategy_va_entry
mmb_strategies_va_table[MMB_STRATEGY__MAX];

/**  @} */

/**  @} */

/**  @} */

#endif /* MMB_I_STRATEGY_H */
