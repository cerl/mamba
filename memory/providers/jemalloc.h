/*
 * Copyright (C) 2020      Hewlett-Packard Enterprise
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef MMB_JEMALLOC_H
#define MMB_JEMALLOC_H

#include <stdlib.h>

#include "../../common/mmb_error.h"
#include "../mmb_memory.h"
#include "../i_memory.h"

/**
 * @name mmb_memory
 * @{ */

/**
 * @name mmbMemInterface
 * @{ */

/**
 * @name jemalloc allocation
 * @{ */

/**
 * @brief Place holder. This provider doesn't really need initialization.
 *
 * @param [in] interface Handle to the structure to initialize.
 *
 * @return \c MMB_OK on success, an error type otherwise.
 */
mmbError mmb_init__jemalloc(mmbMemInterface *interface);

/**
 * @brief Place holder. This provider doesn't really need finalization.
 *
 * @param [in] interface Handle to the structure to finalize.
 *
 * @return \c MMB_OK on success, an error type otherwise.
 */
mmbError mmb_finalize__jemalloc(mmbMemInterface *interface);

/**
 * @brief Check whether the requested layer is supported by jemalloc.
 *
 * @param [in] layer Requested layer.
 * @param [in] need_numa_support Whether the allocation needs NUMA support.
 * @param [out] is_supported Handle for returned value.
 *
 * @return \c MMB_OK on success, an error type otherwise.
 */
mmbError mmb_supports__jemalloc(const mmbMemLayer layer, bool need_numa_support,
                                bool *is_supported);

/**
 * @brief Placeholder as this interface does not require extra data.
 *
 * This parameter is required to initialize the static table in memory.c.
 * This value is used to compute the required size for an
 * \c struct mmbMemInterface.
 */
#define mmb_data_size__jemalloc 0

/**
 * @brief Allocate \p n_bytes of data in DRAM using jemalloc library.
 *
 * @param [in] n_bytes Size of the requested allocation.
 * @param [in] interface Handle to the interface data structure.
 * @param [in] opts Handle to the options used for allocating the buffer.  May
 *                  be \c NULL.
 * @param [out] allocation Pointer to the allocated data.
 *
 * @return \c MMB_OK on success, an error type otherwise.
 */
mmbError mmb_allocate__jemalloc(const size_t n_bytes,
                                mmbMemInterface *interface,
                                const mmbAllocateOptions *opts,
                                void **allocation);

/**
 * @brief Free the memory and structures allocated by
 *        \p mmb_allocate__jemalloc.
 *
 * @param [in] allocation Data to be free'd.
 * @param [in] inter Interface managing the given data.
 * @param [in] opts Handle to the options used for allocating the buffer.  May
 *                  be \c NULL.
 * @param [in] n_bytes Size of the allocation to free.
 *
 * @return \c MMB_OK on success, an error type otherwise.
 */
mmbError mmb_free__jemalloc(void *allocation, mmbMemInterface *inter,
                            const mmbAllocateOptions *opts,
                            const size_t n_bytes);

/**  @} */

/**  @} */

/**  @} */

#endif /* MMB_JEMALLOC_H */

