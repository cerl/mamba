/*
 * Copyright (C) 2018-2020 Cray UK
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
/* for sysconf */
#include <unistd.h> 


#if HAVE_MEMKIND
#include <memkind.h>
#include <hbwmalloc.h>
#include <mmb_pmem.h>
#else
static inline int hbw_check_available() { return 1; }
#endif /* HAVE_MEMKIND */

#include "mmb_error.h"
#include "mmb_logging.h"
#include "mmb_memory.h"

#include "../i_memory.h"
#include "../interface.h"
#include "../numa_utils.h"
#include "system.h"
#include "system_cuda.h"
#include "system_opencl.h"
#include "system_hip.h"

/**
 * @name mmb_memory
 * @{ */

/**
 * @name mmbMemInterface
 * @{ */

/**
 * @name GNU/Linux system memory provider
 * @{ */

static
void * i_mmb_copy__system_malloc(void *dst, const size_t doffset,
                                 const void *src, const size_t soffset,
                                 const size_t width);

static
mmbError mmb_copy_1d__system_malloc(void *dst, const size_t doffset,
                                    const void *src, const size_t soffset,
                                    const size_t width);

static
mmbError mmb_copy_2d__system_malloc(void *dst,
                                    const size_t doffset, const size_t dpitch,
                                    const void *src,
                                    const size_t soffset, const size_t spitch,
                                    const size_t width, const size_t height);

static
mmbError mmb_copy_3d__system_malloc(void *dst,
                                    const size_t doffset, const size_t *dpitchs,
                                    const void *src,
                                    const size_t soffset, const size_t *spitchs,
                                    const size_t *dims);

mmbError mmb_init__system(mmbMemInterface *interface)
{
  mmbError stat = MMB_OK;
  mmbMemLayer layer = interface->space->layer;
  mmbExecutionContext ex_con = interface->space->ex_context;

  MMB_DEBUG("Calling linux initializer()\n");

  if (MMB_GDRAM == layer && MMB_OPENCL == ex_con) {
    stat = mmb_init__system_opencl(interface);
    if (MMB_OK != stat) {
      MMB_ERR("Failed to initialize OpenCL interface.\n");
      return stat;
    }
  }

#ifdef HAVE_MEMKIND
  if(MMB_NVDIMM == layer && MMB_CPU == ex_con) {
    const char *nv_path = NULL;
    int mk_err;
    if(interface->provider_opts.pmem == NULL)  {
      /* TODO: This is leaked if used */
      interface->provider_opts.pmem = calloc(1, sizeof(mmb_pmem_t));    
    }

    /* Construct kind if necessary */
    if(interface->provider_opts.pmem->kind != NULL) {
      MMB_DEBUG("Using user-provided memkind pmem kind\n");   
      /* Check available */
      mk_err = memkind_check_available(interface->provider_opts.pmem->kind);
      if(mk_err) {
        MMB_ERR("Memkind indicates pmem is not available for user-provided kind.\n");
        stat = MMB_ERROR;
        goto BAILOUT;
      }
    } else {
      /* Check for PMEM path, environment takes priority if found */
      nv_path = getenv(MMB_CONFIG_NV_PATH_DEFAULT_ENV_NAME);
      if(nv_path == NULL) {
        if(interface->provider_opts.pmem->path == NULL) {
          MMB_ERR("Interfaces to NVDIMM require file path to storage location via env or provider options.\n");
          stat = MMB_ERROR;
          goto BAILOUT;
        }
        nv_path = interface->provider_opts.pmem->path;
      } else {
        MMB_DEBUG("Using NV path %s from environment\n", nv_path);
      }

      /* Create memkind for PMEM path */
      mk_err = memkind_create_pmem(nv_path, 0, &interface->provider_opts.pmem->kind);
      if(mk_err) {
        MMB_ERR("Unable to create pmem partition at path: %s\n", nv_path);
        stat = MMB_ERROR;
        goto BAILOUT;
      }
      /* Check available */
      mk_err = memkind_check_available(interface->provider_opts.pmem->kind);
      if(mk_err) {
        MMB_ERR("Memkind indicates pmem is not available at path: %s\n", nv_path);
        stat = MMB_ERROR;
        goto BAILOUT;
      }
    }
  }
#endif

BAILOUT:
  return stat;
}

mmbError mmb_finalize__system(mmbMemInterface *interface)
{
  mmbError stat = MMB_OK;
  mmbMemLayer layer = interface->space->layer;
  mmbExecutionContext ex_con = interface->space->ex_context;

  MMB_DEBUG("Calling linux finalizer()\n");

  if (MMB_GDRAM == layer && MMB_OPENCL == ex_con) {
    stat = mmb_finalize__system_opencl(interface);
    if (MMB_OK != stat) {
      MMB_ERR("Failed to finalize OpenCL interface.\n");
      return stat;
    }
  }

#ifdef HAVE_MEMKIND
  if(MMB_NVDIMM == layer && MMB_CPU == ex_con) {
    /* Destroy kind TODO: We shouldnt do this if the user provided their own kind... */
    int mk_err = memkind_destroy_kind(interface->provider_opts.pmem->kind);
    if(mk_err) {
      MMB_ERR("Unable to destroy pmem memkind\n");
      stat = MMB_ERROR;
      goto BAILOUT;
    }
  }
#endif

BAILOUT:
  return stat;
}

mmbError mmb_supports__system(const mmbMemLayer layer, bool need_numa_support,
                              bool *is_supported)
{
  if (NULL == is_supported) {
    return MMB_INVALID_ARG;
  }
  switch (layer) {
    case MMB_DRAM:
    case MMB_GDRAM:
      *is_supported = true;
      break;
    case MMB_HBM:
      *is_supported = (0 == hbw_check_available());
      break;
    case MMB_NVDIMM:
  #ifdef HAVE_MEMKIND
      /* Cant do proper check_available until we have a valid memkind, which is
        on interface construction, so just check if memkind is supported */
      *is_supported = true;
      break;
  #endif
    case MMB_NADRAM:
    case MMB_SSD:
    default:
      *is_supported = false;
      break;
  }
  if (MMB_OK == mmb_numa_available())
    need_numa_support = true;
  else {
#if HAVE_NUMAIF_H
    need_numa_support = true;
#else
    need_numa_support = !need_numa_support;
#endif
  }
  *is_supported &= need_numa_support;
  return MMB_OK;
}

/**
 * @name GNU/Linux malloc DRAM memory provider
 * @{ */

static mmbError mmb_allocate_default__system_malloc(const size_t n_bytes,
                                     mmbMemInterface *interface,
                                     const mmbAllocateOptions *opts,
                                     void **out_allocation)
{
  (void) opts;
  mmbError stat = MMB_OK;

  MMB_DEBUG("Calling system_malloc allocator()\n");

  void *ptr = NULL;
  switch (interface->space->layer) {
    case MMB_DRAM:
      ptr = malloc(n_bytes);
      break;
    case MMB_HBM:
#if HAVE_MEMKIND
      ptr = hbw_malloc(n_bytes);
      break;
#endif /* HAVE_MEMKIND */
    case MMB_NVDIMM:
#if HAVE_MEMKIND
      ptr = memkind_malloc(interface->provider_opts.pmem->kind, n_bytes);
      break;
#endif /* HAVE_MEMKIND */
    default:
      MMB_ERR("Invalid layer (%s), unavailable for system provider.\n",
              mmb_layer_get_string(interface->space->layer));
      stat = MMB_ERROR;
      goto BAILOUT;
  }
  if(NULL == ptr) {
    MMB_ERR("Could not allocate %zu bytes for mmbAllocation\n", n_bytes);
    stat = MMB_OUT_OF_MEMORY;
    goto BAILOUT;
  }
  *out_allocation = ptr;

BAILOUT:
  return stat;
}

/**
 * @brief Allocate \p n_bytes of data in a specific NUMA node in a Unix
 *        environment.
 *
 * This function requires to have libnuma installed.
 *
 * @param [in] n_bytes Size of the requested allocation.
 * @param [in] interface Handle to the interface data structure.
 * @param [in] opts Handle to the options used for allocating the buffer.  May
 *                  not be \c NULL.
 * @param [out] allocation Pointer to the allocated data.
 *
 * @return \c MMB_OK on success, an error type otherwise.
 */
static mmbError mmb_allocate_numa__system_malloc(const size_t n_bytes,
                                                 mmbMemInterface *interface,
                                                 const mmbAllocateOptions *opts,
                                                 void **out_allocation)
{
  (void) interface;
  mmbError stat = MMB_OK;

  MMB_DEBUG("Calling system_malloc_numa allocator()\n");

  if (MMB_OK != mmb_numa_available()) {
    MMB_WARN("libnuma is not available. System provider cannot continue.\n");
    return MMB_UNSUPPORTED;
  }
  if (NULL == opts || 0 > opts->numa.node_id) {
    MMB_ERR("No NUMA id provided, or invalid ID.\n");
    return MMB_INVALID_ARG;
  }

  void *ptr = NULL;
  switch (interface->space->layer) {
    case MMB_DRAM:
    case MMB_HBM:
      stat = mmb_numa_alloc_on_node(n_bytes, opts->numa.node_id, &ptr);
      break;
    default:
      MMB_ERR("Invalid layer (%s), unavailable for system provider numa allocator.\n",
              mmb_layer_get_string(interface->space->layer));
      stat = MMB_ERROR;
      goto BAILOUT;
  }
  if (MMB_OK != stat) {
    MMB_ERR("Could not allocate %zu bytes on node %d for mmbAllocation\n",
            n_bytes, opts->numa.node_id);
    goto BAILOUT;
  }
  *out_allocation = ptr;

BAILOUT:
  return stat;
}

/**
 * @brief Allocate \p n_bytes of data in DRAM in a Unix environment, aligned on
 *        the begining of a memory page.
 *
 * @param [in] n_bytes Size of the requested allocation.
 * @param [in] interface Handle to the interface data structure.
 * @param [in] opts Handle to the options used for allocating the buffer.  May
 *                  be \c NULL.
 * @param [out] allocation Pointer to the allocated data.
 *
 * @return \c MMB_OK on success, an error type otherwise.
 */
static mmbError mmb_allocate_aligned__system_malloc(const size_t n_bytes,
                                                    mmbMemInterface *interface,
                                                    const mmbAllocateOptions *opts,
                                                    void **out_allocation)
{
  (void) interface;
  (void) opts;
  int err = 0;
  mmbError stat = MMB_OK;

  MMB_DEBUG("Calling system_malloc_numa allocator()\n");

  const long page_size = sysconf(_SC_PAGESIZE);

  void *ptr = NULL;
  switch (interface->space->layer) {
    case MMB_DRAM:
      err = posix_memalign(&ptr, page_size, n_bytes);
      break;
    default:
      MMB_ERR("Invalid layer (%s), unavailable for system provider aligned allocator.\n",
              mmb_layer_get_string(interface->space->layer));
      stat = MMB_ERROR;
      goto BAILOUT;
  }
  if(0 != err || NULL == ptr) {
    MMB_ERR("Could not allocate %zu bytes of aligned memory for mmbAllocation. "
            " (err=%d)\n",
            n_bytes, opts->numa.node_id, err);
    stat = MMB_OUT_OF_MEMORY;
    goto BAILOUT;
  }
  *out_allocation = ptr;

BAILOUT:
  return stat;
}

mmbError mmb_allocate__system_malloc(const size_t n_bytes,
                                     mmbMemInterface *interface,
                                     const mmbAllocateOptions *opts,
                                     void **out_allocation)
{
  /* silence warnings */
  (void) mmb_allocate_numa__system_malloc;
  (void) mmb_allocate_aligned__system_malloc;
  mmbError stat = MMB_OK;
  mmbError (*allocate)(const size_t, mmbMemInterface*, const mmbAllocateOptions*, void**);
  if (NULL == opts || !opts->is_numa_allocation) {
    allocate = mmb_allocate_default__system_malloc;
    stat = allocate(n_bytes, interface, opts, out_allocation);
  } else {
#if HAVE_NUMA
    allocate = mmb_allocate_numa__system_malloc;
    stat = allocate(n_bytes, interface, opts, out_allocation);
#elif HAVE_NUMAIF_H
    void *ptr = NULL;
    allocate = mmb_allocate_aligned__system_malloc;
    stat = allocate(n_bytes, interface, opts, &ptr);
    if (MMB_OK == stat) {
      stat = mmb_numa_move_page(&ptr, n_bytes, opts->numa.node_id);
      if (MMB_OK == stat)
        *out_allocation = ptr;
      else
        mmb_free__system_malloc(ptr, interface, opts, n_bytes);
    }
#else
    MMB_ERR("Missing libnuma, operation unsupported.\n");
    stat = MMB_UNSUPPORTED;
#endif
  }
  return stat;
}

static mmbError mmb_free_default__system_malloc(void *allocation,
                                                mmbMemInterface *interface,
                                                const mmbAllocateOptions *opts,
                                                const size_t n_bytes)
{
  (void) opts;
  (void) n_bytes;
  mmbError stat = MMB_OK;

  MMB_DEBUG("Calling system_malloc deallocator(%p)\n", allocation);

  switch (interface->space->layer) {
    case MMB_DRAM:
      free(allocation);
      break;
    case MMB_HBM:
#if HAVE_MEMKIND
      hbw_free(allocation);
      break;
#endif /* HAVE_MEMKIND */
    case MMB_NVDIMM:
#if HAVE_MEMKIND
      memkind_free(interface->provider_opts.pmem->kind, allocation);
      break;
#endif /* HAVE_MEMKIND */
    default:
      stat = MMB_INVALID_LAYER;
      goto BAILOUT;
  }

BAILOUT:
  return stat;
}

/**
 * @brief Free the memory and structures allocated in a specific NUMA node in a
 *        Unix environment by \p mmb_allocate_numa__system_malloc.
 *
 * This function requires to have libnuma installed.
 *
 * @param [in] allocation Data to be free'd.
 * @param [in] inter Interface managing the given data.
 * @param [in] opts Handle to the options used for allocating the buffer.  May
 *                  be \c NULL.
 * @param [in] n_bytes Size of the allocation to free.
 *
 * @return \c MMB_OK on success, an error type otherwise.
 */
static mmbError mmb_free_numa__system_malloc(void *allocation,
                                             mmbMemInterface *interface,
                                             const mmbAllocateOptions *opts,
                                             const size_t n_bytes)
{
  (void) opts;
  (void) n_bytes;
  mmbError stat = MMB_OK;

  MMB_DEBUG("Calling system_malloc_numa deallocator(%p)\n", allocation);

  switch (interface->space->layer) {
    case MMB_DRAM:
    case MMB_HBM:
      mmb_numa_free(allocation, n_bytes);
      break;
    default:
      stat = MMB_INVALID_LAYER;
      goto BAILOUT;
  }

BAILOUT:
  return stat;
}

mmbError mmb_free__system_malloc(void *allocation, mmbMemInterface *interface,
                                 const mmbAllocateOptions *opts,
                                 const size_t n_bytes)
{
  /* Silence warnings */
  (void)mmb_free_numa__system_malloc;
  mmbError stat = MMB_OK;
  mmbError (*free)(void *, mmbMemInterface*, const mmbAllocateOptions*, const size_t);
  if (NULL == opts || !opts->is_numa_allocation) {
    free = mmb_free_default__system_malloc;
    stat = free(allocation, interface, opts, n_bytes);
  } else {
#if HAVE_NUMA
    free = mmb_free_numa__system_malloc;
    stat = free(allocation, interface, opts, n_bytes);
#else
    free = mmb_free_default__system_malloc;
    stat = free(allocation, interface, opts, n_bytes);
#endif
  }
  return stat;
}

static
void * i_mmb_copy__system_malloc(void *dst, const size_t doffset,
                                 const void *src, const size_t soffset,
                                 const size_t width)
{
  dst = (unsigned char *) dst + doffset;
  src = (const unsigned char *) src + soffset;
  return memcpy(dst, src, width);
}

static
mmbError mmb_copy_1d__system_malloc(void *dst,
                                    const size_t doffset,
                                    const void *src,
                                    const size_t soffset,
                                    const size_t width)
{
  mmbError stat = MMB_OK;

  MMB_DEBUG("Calling system_malloc copy_1d()\n");

  MMB_DEBUG("copying %zu bytes from %p to %p.\n", width, src, dst);

  i_mmb_copy__system_malloc(dst, doffset, src, soffset, width);

  return stat;
}

static
mmbError mmb_copy_2d__system_malloc(void *dst,
                                    const size_t doffset, const size_t dpitch,
                                    const void *src,
                                    const size_t soffset, const size_t spitch,
                                    const size_t width, const size_t height)
{
  mmbError stat = MMB_OK;

  MMB_DEBUG("Calling system_malloc copy()\n");

  MMB_DEBUG("copying 2d matrix [%zu][%zu] from %p to %p.\n", width, height, src, dst);

  for (size_t row = 0; height > row; ++row)
    i_mmb_copy__system_malloc(dst, row * dpitch + doffset,
                              src, row * spitch + soffset, width);

  return stat;
}

static
mmbError mmb_copy_3d__system_malloc(void *dst,
                                    const size_t doffset, const size_t *dpitchs,
                                    const void *src,
                                    const size_t soffset, const size_t *spitchs,
                                    const size_t *dims)
{
  mmbError stat = MMB_OK;

  MMB_DEBUG("Calling system_malloc copy()\n");

  MMB_DEBUG("copying 3d matrix [%zu][%zu][%zu] from %p to %p.\n",
            dims[0], dims[1], dims[2] * spitchs[2], src, dst);

  for (size_t slice = 0; dims[0] > slice; ++slice)
    for (size_t row = 0; dims[1] > row; ++row)
      i_mmb_copy__system_malloc(dst, slice * dpitchs[0] + row * dpitchs[1] + doffset,
                                src, slice * spitchs[0] + row * spitchs[1] + soffset,
                                dims[2] * spitchs[2]);

  return stat;
}

mmbError mmb_copy_nd__system_malloc(void *dst, mmbMemInterface *dst_inter,
                                    const mmbAllocateOptions *dst_opts,
                                    const size_t *doffset, const size_t *dpitch,
                                    const void *src, const mmbMemInterface *src_inter,
                                    const mmbAllocateOptions *src_opts,
                                    const size_t *soffset, const size_t *spitch,
                                    const size_t ndims, const size_t *dims)
{
  (void) dst_opts;
  (void) src_opts;

  if (dst_inter->space->ex_context != src_inter->space->ex_context) {
    return MMB_INVALID_EXECUTION_CONTEXT;
  }

  switch (ndims) {
    case 1:
      return mmb_copy_1d__system_malloc(dst, doffset[0] * dpitch[0],
                                        src, soffset[0] * spitch[0],
                                        dims[0] * spitch[0]);
    case 2:
      return mmb_copy_2d__system_malloc(dst,
                                        doffset[0] * dpitch[0] + doffset[1] * dpitch[1],
                                        dpitch[0],
                                        src,
                                        soffset[0] * spitch[0] + soffset[1] * spitch[1],
                                        spitch[0],
                                        dims[1] * spitch[1], dims[0]);
    case 3:
      return mmb_copy_3d__system_malloc(dst,
                                        doffset[0] * dpitch[0]
                                          + doffset[1] * dpitch[1]
                                          + doffset[2] * dpitch[2],
                                        dpitch,
                                        src,
                                        soffset[0] * spitch[0]
                                          + soffset[1] * spitch[1]
                                          + soffset[2] * spitch[2],
                                        spitch,
                                        dims);
    default:
      return MMB_UNIMPLEMENTED;
  }
}

/**  @} */

/**  @} */

/**  @} */

/**  @} */
