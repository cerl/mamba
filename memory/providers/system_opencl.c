/*
 * Copyright (C) 2018-2020 Cray UK
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <stdlib.h>
#include <string.h>

#ifdef __APPLE__
#include <OpenCL/opencl.h>
#else
#include <CL/opencl.h>
#endif

#include "mmb_error.h"
#include "mmb_logging.h"
#include "../i_memory.h"
#include "mmb_opencl.h"
#include "system_opencl.h"

/**
 * @name mmb_memory
 * @{ */

/**
 * @name mmbMemInterface
 * @{ */

/**
 * @name Linux native memory interface.
 * @{ */

/**
 * @name Linux OpenCL memory interface
 * @{ */

#ifndef MAX_PLATFORMS
#define MAX_PLATFORMS 8
#endif /* MAX_PLATFORMS */
#ifndef MAX_DEVICES
#define MAX_DEVICES 16
#endif /* MAX_DEVICES */
#ifndef MAX_DEVICE_NAME
#define MAX_DEVICE_NAME 1024
#endif /* MAX_DEVICE_NAME */

static
mmbError mmb_copy_1d__system_opencl(void *dst, mmbMemInterface *dst_inter,
                                    const mmbAllocateOptions *dst_opts,
                                    const size_t doffset,
                                    const void *src, const mmbMemInterface *src_inter,
                                    const mmbAllocateOptions *src_opts,
                                    const size_t soffset,
                                    const size_t width);

static
mmbError mmb_copy_2d__system_opencl(void *dst, mmbMemInterface *dst_inter,
                                    const mmbAllocateOptions *dst_opts,
                                    const size_t *doffset, const size_t *dpitch,
                                    const void *src, const mmbMemInterface *src_inter,
                                    const mmbAllocateOptions *src_opts,
                                    const size_t *soffset, const size_t *spitch,
                                    const size_t *dims);

static
mmbError mmb_copy_3d__system_opencl(void *dst, mmbMemInterface *dst_inter,
                                    const mmbAllocateOptions *dst_opts,
                                    const size_t *doffset, const size_t *dpitch,
                                    const void *src, const mmbMemInterface *src_inter,
                                    const mmbAllocateOptions *src_opts,
                                    const size_t *soffset, const size_t *spitch,
                                    const size_t *dims);

/**
 * @brief Returns the stringified version of the error code/symbol returned by
 *        OpenCL.
 *
 * @param [in] ocl_err OpenCL error code
 *
 * @return a constant string containing the name of the error code.
 */
static inline const char * mmb_opencl_error_decode(const cl_int ocl_err);

/**
 * @brief Returns the Mamba error code equivalent (if any) to the error
 *        code/symbol returned by OpenCL.
 *
 * @param [in] ocl_err OpenCL error code
 *
 * @return The corresponding mmbError symbol.
 */
static inline mmbError mmb_opencl_error_to_mamba(const cl_int ocl_err);

mmbError mmb_init__system_opencl(mmbMemInterface *interface)
{
  (void) interface;
  mmbError stat = MMB_OK;
  // cl_int ocl_err;

  // /* Get list of platforms */
  // cl_uint num_platforms = 0;
  // cl_platform_id platforms[MAX_PLATFORMS];
  // ocl_err = clGetPlatformIDs(MAX_PLATFORMS, platforms, &num_platforms);
  // if (CL_SUCCESS != ocl_err) {
  //   MMB_ERR("Unable to gather plarforms ID's (%s).\n",
  //       mmb_opencl_error_decode(ocl_err));
  //   stat = MMB_ERROR;
  //   goto BAILOUT;
  // }

  // /* Get list of devices */
  // cl_uint total_devices = 0;
  // cl_device_id devices[MAX_DEVICES];
  // for (cl_uint p = 0; p < num_platforms; ++p) {
  //   cl_uint num_devices = 0;
  //   ocl_err = clGetDeviceIDs(platforms[p], CL_DEVICE_TYPE_ALL,
  //                        MAX_DEVICES - total_devices, devices + total_devices,
  //                        &num_devices);
  //   if (CL_SUCCESS != ocl_err) {
  //     MMB_ERR("Failed to get device name. (%s).\n",
  //         mmb_opencl_error_decode(ocl_err));
  //     stat = MMB_ERROR;
  //     goto BAILOUT;
  //   }
  //   total_devices += num_devices;
  // }

  // /* Print list of devices */
  // char name[MAX_DEVICE_NAME];
  // size_t name_len = MAX_DEVICE_NAME;
  // MMB_NOISE("Available OpenCL devices:\n");
  // for (cl_uint d = 0; d < total_devices; ++d) {
  //   ocl_err = clGetDeviceInfo(devices[d], CL_DEVICE_NAME,
  //                         name_len, name, &name_len);
  //   if (CL_SUCCESS == ocl_err) {
  //     MMB_NOISE("\t%2d: %s%s\n", d, name,
  //           MAX_DEVICE_NAME > name_len ? " (possibly truncated)" : "");
  //   } else {
  //     MMB_DEBUG("\t%2d: (err: %s)\n", d, mmb_opencl_error_decode(ocl_err));
  //   }
  // }

  // /* Use first device unless OCL_DEVICE environment variable is given */
  // cl_uint device_index = 0;
  // char *dev_env = getenv("OCL_DEVICE");
  // if (NULL != dev_env) {
  //   char *end;
  //   device_index = strtol(dev_env, &end, 10);
  //   if (0 < strlen(end)) {
  //     MMB_ERR("Invalid OCL_DEVICE variable \"%s\".\n", dev_env);
  //     stat = MMB_ERROR;
  //     goto BAILOUT;
  //   }
  // }

  // if (device_index >= total_devices) {
  //   MMB_ERR("Device index set to %d but only %d devices available.\n",
  //       device_index, total_devices);
  //   stat = MMB_ERROR;
  //   goto BAILOUT;
  // }

  // /* Print OpenCL device name */
  // ocl_err = clGetDeviceInfo(devices[device_index], CL_DEVICE_NAME,
  //                       name_len, name, &name_len);
  // if (CL_SUCCESS == ocl_err) {
  //   MMB_DEBUG("Selected OpenCL device: %s%s (index=%d).\n", name,
  //         MAX_DEVICE_NAME > name_len ? " (possibly truncated)" : "",
  //         device_index);
  // } else {
  //   MMB_DEBUG("Unable to retrieve device's name (err: %s).\n",
  //         mmb_opencl_error_decode(ocl_err));
  // }

  /* Update handle's OpenCL data */
  //mmb_opencl_t *opencl_data = interface->provider_opts.ocl;
  // opencl_data->device  = devices[device_index];
  // opencl_data->context = NULL; /* TODO: Find how to get the context from the user */
  // opencl_data->queue   = NULL; /* TODO: Find how to get the queue from the user */
  //MMB_DEBUG("Ocl data device: %p, context %p, queue: %p\n", opencl_data->device, opencl_data->context, opencl_data->queue);


  MMB_DEBUG("OpenCL backend initialised\n");

//BAILOUT:
  return stat;
}

mmbError mmb_finalize__system_opencl(mmbMemInterface *interface)
{
  (void) interface;
  return MMB_OK;
}

mmbError mmb_allocate__system_opencl(const size_t n_bytes,
                                     mmbMemInterface *interface,
                                     const mmbAllocateOptions *opts,
                                     void **out_allocation)
{
  (void) opts;
  mmb_opencl_t *opencl_data = interface->provider_opts.ocl;
  if (NULL == opencl_data) {
    MMB_ERR("Invalid OpenCL data structure. Parameters are missing.\n");
    return MMB_INVALID_INTERFACE;
  }
  cl_context ctx = opencl_data->context;
  mmbError stat = MMB_OK;

  MMB_DEBUG("Calling system_opencl allocator()\n");

  cl_int ocl_err;
  void *ptr = clCreateBuffer(ctx, CL_MEM_READ_WRITE, n_bytes, NULL, &ocl_err);
  if(CL_SUCCESS != ocl_err) {
    switch (ocl_err) {
      case CL_INVALID_BUFFER_SIZE:
        MMB_ERR("Could not allocate %zu bytes for mmbAllocation.\n", n_bytes);
        stat = MMB_INVALID_ARG;
        break;
      case CL_OUT_OF_HOST_MEMORY:
        MMB_ERR("Could not allocate %zu bytes for mmbAllocation.\n", n_bytes);
        stat = MMB_OUT_OF_MEMORY;
        break;
      case CL_MEM_OBJECT_ALLOCATION_FAILURE:
      case CL_INVALID_CONTEXT:
      case CL_INVALID_VALUE:
      case CL_INVALID_HOST_PTR:
        /* Nothing special to do with those error (implementation error). */
        /* FALLTHROUGH */
      default:
        MMB_ERR("Could not allocate %zu bytes for mmbAllocation: %s\n",
            n_bytes, mmb_opencl_error_decode(ocl_err));
        stat = MMB_ERROR;
    }
    return stat;
  }

  *out_allocation = ptr;

  return stat;
}

mmbError mmb_free__system_opencl(void *allocation, mmbMemInterface *inter,
                                 const mmbAllocateOptions *opts,
                                 const size_t n_bytes)
{
  (void) inter;
  (void) opts;
  (void) n_bytes;
  mmbError stat = MMB_OK;

  MMB_DEBUG("Calling system_opencl deallocator(%p)\n", allocation);

  cl_int ocl_err = clReleaseMemObject(allocation);
  switch (ocl_err) {
    case CL_SUCCESS:
      break;
    case CL_INVALID_MEM_OBJECT:
      MMB_ERR("Error while freeing allocation: %s\n", mmb_opencl_error_decode(ocl_err));
      stat = MMB_INVALID_ARG;
      break;
    default:
      MMB_ERR("Error while freeing allocation: %s\n", mmb_opencl_error_decode(ocl_err));
      stat = MMB_ERROR;
  }

  return stat;
}

static
mmbError mmb_copy_1d__system_opencl(void *dst, mmbMemInterface *dst_inter,
                                    const mmbAllocateOptions *dst_opts,
                                    const size_t doffset,
                                    const void *src, const mmbMemInterface *src_inter,
                                    const mmbAllocateOptions *src_opts,
                                    const size_t soffset,
                                    const size_t width)
{
  (void) dst_opts;
  (void) src_opts;
  mmbError stat = MMB_OK;

  MMB_DEBUG("Calling system_opencl copy()\n");

  cl_int ocl_err = CL_SUCCESS;
  const mmbExecutionContext dst_ex_con = dst_inter->space->ex_context;
  const mmbExecutionContext src_ex_con = src_inter->space->ex_context;
  char const *cpy_kind_str;
  if (MMB_OPENCL == dst_ex_con && MMB_CPU == src_ex_con) {
    mmb_opencl_t *opencl_data = dst_inter->provider_opts.ocl;
    if (NULL == opencl_data) {
      MMB_ERR("Invalid OpenCL data structure. Parameters are missing.\n");
      return MMB_INVALID_INTERFACE;
    }
    src = (const unsigned char *) src + soffset;
    cl_command_queue ocl_q   = opencl_data->queue;
    cpy_kind_str = "H2D";
    cl_mem cl_dst = (cl_mem)dst;
    ocl_err = clEnqueueWriteBuffer(ocl_q, cl_dst, CL_TRUE, doffset, width, src,
                                   0, NULL, NULL);
  } else if (MMB_CPU == dst_ex_con && MMB_OPENCL == src_ex_con) {
    mmb_opencl_t *opencl_data = src_inter->provider_opts.ocl;
    if (NULL == opencl_data) {
      MMB_ERR("Invalid OpenCL data structure. Parameters are missing.\n");
      return MMB_INVALID_INTERFACE;
    }
    dst = (unsigned char *) dst + doffset;
    cl_command_queue ocl_q   = opencl_data->queue;
    cpy_kind_str = "D2H";
    cl_mem cl_src = (cl_mem)src;
    ocl_err = clEnqueueReadBuffer(ocl_q, cl_src, CL_TRUE, soffset, width, dst,
                                  0, NULL, NULL);
  } else if (MMB_OPENCL == dst_ex_con && MMB_OPENCL == src_ex_con) {
    mmb_opencl_t *opencl_data = dst_inter->provider_opts.ocl;
    if (NULL == opencl_data) {
      MMB_ERR("Invalid OpenCL data structure. Parameters are missing.\n");
      return MMB_INVALID_INTERFACE;
    }
    cl_command_queue ocl_q   = opencl_data->queue;
    cpy_kind_str = "D2D";
    cl_mem cl_src = (cl_mem)src;
    cl_mem cl_dst = (cl_mem)dst;
    ocl_err = clEnqueueCopyBuffer(ocl_q, cl_src, cl_dst, soffset, doffset, width,
                                  0, NULL, NULL);
  } else {
    MMB_ERR("Invalid execution context for source or destination of the copy.\n");
    stat = MMB_INVALID_ARG;
    goto BAILOUT;
  }

  MMB_DEBUG("copying %zu bytes from %p(+%zu) to %p(+%zu) (%s)\n",
            width, src, soffset, dst, doffset, cpy_kind_str);

  /* Check return status */
  switch (ocl_err) {
    case CL_SUCCESS:
      break;
    default:
      MMB_ERR("Copy failed from %s to %s with error %d (%s).\n",
          MMB_OPENCL == src_ex_con ? "device" : "host",
          MMB_OPENCL == dst_ex_con ? "device" : "host",
          ocl_err, mmb_opencl_error_decode(ocl_err));
  }

  stat = mmb_opencl_error_to_mamba(ocl_err);

BAILOUT:
  return stat;
}

static
mmbError mmb_copy_2d__system_opencl(void *dst, mmbMemInterface *dst_inter,
                                    const mmbAllocateOptions *dst_opts,
                                    const size_t *doffset, const size_t *dpitch,
                                    const void *src, const mmbMemInterface *src_inter,
                                    const mmbAllocateOptions *src_opts,
                                    const size_t *soffset, const size_t *spitch,
                                    const size_t *dims)
{
  const size_t doffset_3d[3] = { 0, doffset[0], doffset[1] };
  const size_t dpitch_3d[3] = { 0, dpitch[0], dpitch[1] };
  const size_t soffset_3d[3] = { 0, soffset[0], soffset[1] };
  const size_t spitch_3d[3] = { 0, spitch[0], spitch[1] };
  const size_t dims_3d[3] = { 1, dims[0], dims[1] };
  return mmb_copy_3d__system_opencl(dst, dst_inter, dst_opts, doffset_3d, dpitch_3d,
                                    src, src_inter, src_opts, soffset_3d, spitch_3d,
                                    dims_3d);
}

static
mmbError mmb_copy_3d__system_opencl(void *dst, mmbMemInterface *dst_inter,
                                    const mmbAllocateOptions *dst_opts,
                                    const size_t *in_doffset, const size_t *dpitch,
                                    const void *src, const mmbMemInterface *src_inter,
                                    const mmbAllocateOptions *src_opts,
                                    const size_t *in_soffset, const size_t *spitch,
                                    const size_t *in_dims)
{
  (void) dst_opts;
  (void) src_opts;
  mmbError stat = MMB_OK;

  MMB_DEBUG("Calling system_opencl copy_3d()\n");

  const size_t dims[3] = { in_dims[2] * spitch[2], in_dims[1], in_dims[0] };
  const size_t doffset[3] = { in_doffset[2], in_doffset[1], in_doffset[0] };
  const size_t soffset[3] = { in_soffset[2], in_soffset[1], in_soffset[0] };

  cl_int ocl_err = CL_SUCCESS;
  const mmbExecutionContext dst_ex_con = dst_inter->space->ex_context;
  const mmbExecutionContext src_ex_con = src_inter->space->ex_context;
  char const *cpy_kind_str;
  /* if (MMB_CPU == dst_ex_con && MMB_CPU == src_ex_con) { */
  /*   cpy_kind_str = "H2H"; */
  /*   memcpy(dst, src, dims[0]); */
  /* } else */
  if (MMB_OPENCL == dst_ex_con && MMB_CPU == src_ex_con) {
    mmb_opencl_t *opencl_data = dst_inter->provider_opts.ocl;
    if (NULL == opencl_data) {
      MMB_ERR("Invalid OpenCL data structure. Parameters are missing.\n");
      return MMB_INVALID_INTERFACE;
    }
    cl_command_queue ocl_q   = opencl_data->queue;
    cpy_kind_str = "H2D";
    cl_mem cl_dst = (cl_mem)dst;
    ocl_err = clEnqueueWriteBufferRect(ocl_q, cl_dst, CL_TRUE, doffset, soffset,
                                       dims, dpitch[1], dpitch[0], spitch[1],
                                       spitch[0], src, 0, NULL, NULL);
  } else if (MMB_CPU == dst_ex_con && MMB_OPENCL == src_ex_con) {
    mmb_opencl_t *opencl_data = src_inter->provider_opts.ocl;
    if (NULL == opencl_data) {
      MMB_ERR("Invalid OpenCL data structure. Parameters are missing.\n");
      return MMB_INVALID_INTERFACE;
    }
    cl_command_queue ocl_q   = opencl_data->queue;
    cpy_kind_str = "D2H";
    cl_mem cl_src = (cl_mem)src;
    ocl_err = clEnqueueReadBufferRect(ocl_q, cl_src, CL_TRUE, soffset, doffset,
                                      dims, spitch[1], spitch[0], dpitch[1],
                                      dpitch[0], dst, 0, NULL, NULL);
  } else if (MMB_OPENCL == dst_ex_con && MMB_OPENCL == src_ex_con) {
    mmb_opencl_t *opencl_data = dst_inter->provider_opts.ocl;
    if (NULL == opencl_data) {
      MMB_ERR("Invalid OpenCL data structure. Parameters are missing.\n");
      return MMB_INVALID_INTERFACE;
    }
    cl_command_queue ocl_q   = opencl_data->queue;
    cpy_kind_str = "D2D";
    cl_mem cl_src = (cl_mem)src;
    cl_mem cl_dst = (cl_mem)dst;
    ocl_err = clEnqueueCopyBufferRect(ocl_q, cl_src, cl_dst, soffset, doffset,
                                      dims, spitch[1], spitch[0], dpitch[1],
                                      dpitch[0], 0, NULL, NULL);
  } else {
    MMB_ERR("Invalid execution context for source or destination of the copy.\n");
    stat = MMB_INVALID_ARG;
    goto BAILOUT;
  }

  MMB_DEBUG("copying 3d matrix [%zu][%zu][%zu] from %p[%zu][%zu][%zu] to "
            "%p[%zu][%zu][%zu] (%s).\n",
            in_dims[0], in_dims[1], in_dims[2] * spitch[2], src, in_soffset[0],
            in_soffset[1], in_soffset[2], dst, in_doffset[0], in_soffset[1],
            in_soffset[2], cpy_kind_str);

  /* Check return status */
  switch (ocl_err) {
    case CL_SUCCESS:
      break;
    default:
      MMB_ERR("Copy failed from %s to %s with error %d (%s).\n",
          MMB_OPENCL == src_ex_con ? "device" : "host",
          MMB_OPENCL == dst_ex_con ? "device" : "host",
          ocl_err, mmb_opencl_error_decode(ocl_err));
  }

  stat = mmb_opencl_error_to_mamba(ocl_err);

BAILOUT:
  return stat;
}

mmbError mmb_copy_nd__system_opencl(void *dst, mmbMemInterface *dst_inter,
                                    const mmbAllocateOptions *dst_opts,
                                    const size_t *doffset, const size_t *dpitch,
                                    const void *src, const mmbMemInterface *src_inter,
                                    const mmbAllocateOptions *src_opts,
                                    const size_t *soffset, const size_t *spitch,
                                    const size_t ndims, const size_t *dims)
{
  switch (ndims) {
    case 1:
      return mmb_copy_1d__system_opencl(dst, dst_inter, dst_opts, doffset[0] * dpitch[0],
                                        src, src_inter, src_opts, soffset[0] * spitch[0],
                                        dims[0] * spitch[0]);
    case 2:
      return mmb_copy_2d__system_opencl(dst, dst_inter, dst_opts, doffset, dpitch,
                                        src, src_inter, src_opts, soffset, spitch,
                                        dims);
    case 3:
      return mmb_copy_3d__system_opencl(dst, dst_inter, dst_opts, doffset, dpitch,
                                        src, src_inter, src_opts, soffset, spitch,
                                        dims);
    default:
      return MMB_UNIMPLEMENTED;
  }
}

#ifndef MMB_CASE_TO_STR
#define MMB_CASE_TO_STR(a) case a: return #a
#endif /* MMB_CASE_TO_STR */

static inline const char *
mmb_opencl_error_decode(const cl_int ocl_err)
{
  switch(ocl_err) {
    /* run-time and JIT compiler errors */
    MMB_CASE_TO_STR(CL_SUCCESS);
    MMB_CASE_TO_STR(CL_DEVICE_NOT_FOUND);
    MMB_CASE_TO_STR(CL_DEVICE_NOT_AVAILABLE);
    MMB_CASE_TO_STR(CL_COMPILER_NOT_AVAILABLE);
    MMB_CASE_TO_STR(CL_MEM_OBJECT_ALLOCATION_FAILURE);
    MMB_CASE_TO_STR(CL_OUT_OF_RESOURCES);
    MMB_CASE_TO_STR(CL_OUT_OF_HOST_MEMORY);
    MMB_CASE_TO_STR(CL_PROFILING_INFO_NOT_AVAILABLE);
    MMB_CASE_TO_STR(CL_MEM_COPY_OVERLAP);
    MMB_CASE_TO_STR(CL_IMAGE_FORMAT_MISMATCH);
    MMB_CASE_TO_STR(CL_IMAGE_FORMAT_NOT_SUPPORTED);
    MMB_CASE_TO_STR(CL_BUILD_PROGRAM_FAILURE);
    MMB_CASE_TO_STR(CL_MAP_FAILURE);
    MMB_CASE_TO_STR(CL_MISALIGNED_SUB_BUFFER_OFFSET);
    MMB_CASE_TO_STR(CL_EXEC_STATUS_ERROR_FOR_EVENTS_IN_WAIT_LIST);
    MMB_CASE_TO_STR(CL_COMPILE_PROGRAM_FAILURE);
    MMB_CASE_TO_STR(CL_LINKER_NOT_AVAILABLE);
    MMB_CASE_TO_STR(CL_LINK_PROGRAM_FAILURE);
    MMB_CASE_TO_STR(CL_DEVICE_PARTITION_FAILED);
    MMB_CASE_TO_STR(CL_KERNEL_ARG_INFO_NOT_AVAILABLE);

    /* compile-time errors */
    MMB_CASE_TO_STR(CL_INVALID_VALUE);
    MMB_CASE_TO_STR(CL_INVALID_DEVICE_TYPE);
    MMB_CASE_TO_STR(CL_INVALID_PLATFORM);
    MMB_CASE_TO_STR(CL_INVALID_DEVICE);
    MMB_CASE_TO_STR(CL_INVALID_CONTEXT);
    MMB_CASE_TO_STR(CL_INVALID_QUEUE_PROPERTIES);
    MMB_CASE_TO_STR(CL_INVALID_COMMAND_QUEUE);
    MMB_CASE_TO_STR(CL_INVALID_HOST_PTR);
    MMB_CASE_TO_STR(CL_INVALID_MEM_OBJECT);
    MMB_CASE_TO_STR(CL_INVALID_IMAGE_FORMAT_DESCRIPTOR);
    MMB_CASE_TO_STR(CL_INVALID_IMAGE_SIZE);
    MMB_CASE_TO_STR(CL_INVALID_SAMPLER);
    MMB_CASE_TO_STR(CL_INVALID_BINARY);
    MMB_CASE_TO_STR(CL_INVALID_BUILD_OPTIONS);
    MMB_CASE_TO_STR(CL_INVALID_PROGRAM);
    MMB_CASE_TO_STR(CL_INVALID_PROGRAM_EXECUTABLE);
    MMB_CASE_TO_STR(CL_INVALID_KERNEL_NAME);
    MMB_CASE_TO_STR(CL_INVALID_KERNEL_DEFINITION);
    MMB_CASE_TO_STR(CL_INVALID_KERNEL);
    MMB_CASE_TO_STR(CL_INVALID_ARG_INDEX);
    MMB_CASE_TO_STR(CL_INVALID_ARG_VALUE);
    MMB_CASE_TO_STR(CL_INVALID_ARG_SIZE);
    MMB_CASE_TO_STR(CL_INVALID_KERNEL_ARGS);
    MMB_CASE_TO_STR(CL_INVALID_WORK_DIMENSION);
    MMB_CASE_TO_STR(CL_INVALID_WORK_GROUP_SIZE);
    MMB_CASE_TO_STR(CL_INVALID_WORK_ITEM_SIZE);
    MMB_CASE_TO_STR(CL_INVALID_GLOBAL_OFFSET);
    MMB_CASE_TO_STR(CL_INVALID_EVENT_WAIT_LIST);
    MMB_CASE_TO_STR(CL_INVALID_EVENT);
    MMB_CASE_TO_STR(CL_INVALID_OPERATION);
    MMB_CASE_TO_STR(CL_INVALID_GL_OBJECT);
    MMB_CASE_TO_STR(CL_INVALID_BUFFER_SIZE);
    MMB_CASE_TO_STR(CL_INVALID_MIP_LEVEL);
    MMB_CASE_TO_STR(CL_INVALID_GLOBAL_WORK_SIZE);
    MMB_CASE_TO_STR(CL_INVALID_PROPERTY);
    MMB_CASE_TO_STR(CL_INVALID_IMAGE_DESCRIPTOR);
    MMB_CASE_TO_STR(CL_INVALID_COMPILER_OPTIONS);
    MMB_CASE_TO_STR(CL_INVALID_LINKER_OPTIONS);
    MMB_CASE_TO_STR(CL_INVALID_DEVICE_PARTITION_COUNT);

    // /* extension errors */
    // MMB_CASE_TO_STR(CL_INVALID_GL_SHAREGROUP_REFERENCE_KHR);
    // MMB_CASE_TO_STR(CL_PLATFORM_NOT_FOUND_KHR);
    // MMB_CASE_TO_STR(CL_INVALID_D3D10_DEVICE_KHR);
    // MMB_CASE_TO_STR(CL_INVALID_D3D10_RESOURCE_KHR);
    // MMB_CASE_TO_STR(CL_D3D10_RESOURCE_ALREADY_ACQUIRED_KHR);
    // MMB_CASE_TO_STR(CL_D3D10_RESOURCE_NOT_ACQUIRED_KHR);
    default: return "Unknown OpenCL error";
  }
}

#undef MMB_CASE_TO_STR

static inline mmbError
mmb_opencl_error_to_mamba(const cl_int ocl_err)
{
  switch(ocl_err) {
    /* run-time and JIT compiler errors */
    case CL_SUCCESS:
      return MMB_OK;

    case CL_DEVICE_NOT_FOUND:
      return MMB_NOT_FOUND;

    case CL_OUT_OF_RESOURCES:
    case CL_OUT_OF_HOST_MEMORY:
      return MMB_OUT_OF_MEMORY;

    case CL_DEVICE_NOT_AVAILABLE:
    case CL_COMPILER_NOT_AVAILABLE:
    case CL_MEM_OBJECT_ALLOCATION_FAILURE:
    case CL_PROFILING_INFO_NOT_AVAILABLE:
    case CL_MEM_COPY_OVERLAP:
    case CL_IMAGE_FORMAT_MISMATCH:
    case CL_IMAGE_FORMAT_NOT_SUPPORTED:
    case CL_BUILD_PROGRAM_FAILURE:
    case CL_MAP_FAILURE:
    case CL_MISALIGNED_SUB_BUFFER_OFFSET:
    case CL_EXEC_STATUS_ERROR_FOR_EVENTS_IN_WAIT_LIST:
    case CL_COMPILE_PROGRAM_FAILURE:
    case CL_LINKER_NOT_AVAILABLE:
    case CL_LINK_PROGRAM_FAILURE:
    case CL_DEVICE_PARTITION_FAILED:
    case CL_KERNEL_ARG_INFO_NOT_AVAILABLE:
      return MMB_ERROR;

    /* compile-time errors */
    case CL_INVALID_VALUE:
    case CL_INVALID_DEVICE_TYPE:
    case CL_INVALID_PLATFORM:
    case CL_INVALID_DEVICE:
    case CL_INVALID_CONTEXT:
    case CL_INVALID_QUEUE_PROPERTIES:
    case CL_INVALID_COMMAND_QUEUE:
    case CL_INVALID_HOST_PTR:
    case CL_INVALID_MEM_OBJECT:
    case CL_INVALID_IMAGE_FORMAT_DESCRIPTOR:
    case CL_INVALID_IMAGE_SIZE:
    case CL_INVALID_SAMPLER:
    case CL_INVALID_BINARY:
    case CL_INVALID_BUILD_OPTIONS:
    case CL_INVALID_PROGRAM:
    case CL_INVALID_PROGRAM_EXECUTABLE:
    case CL_INVALID_KERNEL_NAME:
    case CL_INVALID_KERNEL_DEFINITION:
    case CL_INVALID_KERNEL:
    case CL_INVALID_ARG_INDEX:
    case CL_INVALID_ARG_VALUE:
    case CL_INVALID_KERNEL_ARGS:
    case CL_INVALID_WORK_DIMENSION:
    case CL_INVALID_WORK_GROUP_SIZE:
    case CL_INVALID_WORK_ITEM_SIZE:
    case CL_INVALID_GLOBAL_OFFSET:
    case CL_INVALID_EVENT_WAIT_LIST:
    case CL_INVALID_EVENT:
    case CL_INVALID_OPERATION:
    case CL_INVALID_GL_OBJECT:
    case CL_INVALID_BUFFER_SIZE:
    case CL_INVALID_MIP_LEVEL:
    case CL_INVALID_GLOBAL_WORK_SIZE:
    case CL_INVALID_PROPERTY:
    case CL_INVALID_IMAGE_DESCRIPTOR:
    case CL_INVALID_COMPILER_OPTIONS:
    case CL_INVALID_LINKER_OPTIONS:
    case CL_INVALID_DEVICE_PARTITION_COUNT:
      return MMB_INVALID_ARG;

    case CL_INVALID_ARG_SIZE:
      return MMB_INVALID_SIZE;
  }
  return MMB_ERROR;
}

// mmbError mmb_meminterface_initparams_create_opencl(mmb_opencl_initparams** iprms, 
//                                                   cl_context ctx, 
//                                                   cl_command_queue q) {
//   mmbError stat = MMB_OK;
  
//   if(!iprms) {
//     MMB_ERR("Init params struct pointer may not be NULL\n");
//     stat = MMB_INVALID_ARG;
//     goto BAILOUT;
//   }
  
//   mmb_opencl_initparams* params = (mmb_opencl_initparams*)malloc(sizeof(mmb_opencl_initparams));
//   if(!params) {
//     MMB_ERR("Unable to allocate initparams structure\n");
//     stat = MMB_OUT_OF_MEMORY;
//     goto BAILOUT;
//   }
  
//   params->context = ctx;
//   params->queue = q;
//   *iprms = params;

// BAILOUT:
//   return stat;
// }
/**  @} */

/**  @} */

/**  @} */

/**  @} */
