/*
 * Copyright (C) 2018-2020 Cray UK
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <stdlib.h>
#include <string.h>

#include "mmb_error.h"
#include "mmb_logging.h"
#include "../i_memory.h"
#include "system_hip.h"

/**
 * @name mmb_memory
 * @{ */

/**
 * @name mmbMemInterface
 * @{ */

/**
 * @name Linux native memory interface.
 * @{ */

/**
 * @name GNU/Linux ROCM memory interface
 * @{ */

#if HAVE_ROCM

#include <hip/hip_runtime_api.h>

mmbError mmb_allocate__system_hip(const size_t n_bytes,
                                  mmbMemInterface *interface,
                                  const mmbAllocateOptions *opts,
                                  void **out_allocation)
{
  (void) interface;
  (void) opts;
  mmbError stat = MMB_OK;

  MMB_DEBUG("Calling system_hip allocator()\n");

  void *ptr = NULL;
  hipError_t hip_err = hipMalloc(&ptr, n_bytes);
  if(hipSuccess != hip_err) {
    switch (hip_err) {
      case hipErrorInvalidValue:
        MMB_ERR("Could not allocate %zu bytes for mmbAllocation.\n", n_bytes);
        stat = MMB_INVALID_ARG;
        break;
      case hipErrorMemoryAllocation:
        MMB_ERR("Could not allocate %zu bytes for mmbAllocation.\n", n_bytes);
        stat = MMB_OUT_OF_MEMORY;
        break;
      default:
        MMB_ERR("Could not allocate %zu bytes for mmbAllocation: %s\n",
            n_bytes, hipGetErrorString(hip_err));
        stat = MMB_ERROR;
    }
    return stat;
  }

  MMB_DEBUG("Successfully allocated with hip allocator\n");

  *out_allocation = ptr;

  return stat;
}

mmbError mmb_free__system_hip(void *allocation, mmbMemInterface *interface,
                              const mmbAllocateOptions *opts,
                              const size_t n_bytes)
{
  (void) interface;
  (void) opts;
  (void) n_bytes;
  mmbError stat = MMB_OK;

  MMB_DEBUG("Calling system_hip deallocator(%p)\n", allocation);

  hipError_t hip_err = hipFree(allocation);
  switch (hip_err) {
    case hipSuccess:
      break;
    case hipErrorInvalidDevicePointer:
      MMB_ERR("Error while freeing allocation: %s\n", hipGetErrorString(hip_err));
      stat = MMB_INVALID_ARG;
      break;
    default:
      MMB_ERR("Error while freeing allocation: %s\n", hipGetErrorString(hip_err));
      stat = MMB_ERROR;
  }

  return stat;
}

mmbError mmb_copy_nd__system_hip(void *dst, mmbMemInterface *dst_inter,
                                 const mmbAllocateOptions *dst_opts,
                                 const size_t *doffset, const size_t *dpitch,
                                 const void *src, const mmbMemInterface *src_inter,
                                 const mmbAllocateOptions *src_opts,
                                 const size_t *soffset, const size_t *spitch,
                                 const size_t ndims, const size_t *dims)
{
  (void) dst_opts;
  (void) src_opts;
  mmbError stat = MMB_OK;

  MMB_DEBUG("Calling system_hip copy_2d()\n");

  hipError_t hip_err;
  const mmbExecutionContext dst_ex_con = dst_inter->space->ex_context;
  const mmbExecutionContext src_ex_con = src_inter->space->ex_context;
  enum hipMemcpyKind cpy_kind;
  char const *cpy_kind_str;
  if (MMB_CPU == dst_ex_con && MMB_CPU == src_ex_con) {
    cpy_kind = hipMemcpyHostToHost;
    cpy_kind_str = "H2H";
  } else if (MMB_GPU_HIP == dst_ex_con && MMB_CPU == src_ex_con) {
    cpy_kind = hipMemcpyHostToDevice;
    cpy_kind_str = "H2D";
  } else if (MMB_CPU == dst_ex_con && MMB_GPU_HIP == src_ex_con) {
    cpy_kind = hipMemcpyDeviceToHost;
    cpy_kind_str = "D2H";
  } else if (MMB_GPU_HIP == dst_ex_con && MMB_GPU_HIP == src_ex_con) {
    cpy_kind = hipMemcpyDeviceToDevice;
    cpy_kind_str = "D2D";
  } else {
    MMB_ERR("Invalid execution context for source or destination of the copy.\n");
    stat = MMB_INVALID_ARG;
    goto BAILOUT;
  }

  if (MMB_LOG_DEBUG <= MMB_MAX_LOG_LEVEL) {
    int line_s = 0;
    const size_t max_line = (MMB_LOG_INTEGER_LENGTH(size_t)+1) * ndims;
    char line[max_line];
    line[0] = '\0';
    for (size_t d = 0; ndims > d; ++d)
      line_s += snprintf(&line[line_s], max_line - line_s, "%2s%zu",
                         0 == d ? " [" : "][",
                         dims[d] * (ndims - 1 > d ? 1 : spitch[ndims-1]));
    MMB_DEBUG("copying %zud matrix%s] from %p to %p (%s).\n",
              ndims, line, src, dst, cpy_kind_str);
  }

  for (size_t d = 0; ndims > d; ++d) {
    dst = (unsigned char *) dst + doffset[d] * dpitch[d];
    src = (const unsigned char *) src + soffset[d] * spitch[d];
  }

  switch (ndims) {
    case 1:
      hip_err = hipMemcpy(dst, src, dims[0] * spitch[0], cpy_kind);
      break;
    case 2:
      hip_err = hipMemcpy2D(dst, dpitch[0], src, spitch[0],
                            dims[1] * spitch[1], dims[0], cpy_kind);
      break;
    case 3:
      {
        struct hipMemcpy3DParms params3d = {
          .srcPtr = make_hipPitchedPtr((void *)src, spitch[1], dims[2] * spitch[2], dims[1]),
          .dstPtr = make_hipPitchedPtr(dst, dpitch[1], dims[2] * dpitch[2], dims[1]),
          .extent = make_hipExtent(dims[2] * spitch[2], dims[1], dims[0]),
          .kind = cpy_kind,
        };
        hip_err = hipMemcpy3D(&params3d);
      }
      break;
    default:
      stat = MMB_UNIMPLEMENTED;
      goto BAILOUT;
  }

  switch (hip_err) {
    case hipSuccess:
      stat = MMB_OK;
      break;
    case hipErrorInvalidValue:
      stat = MMB_INVALID_ARG;
      MMB_ERR("Error while copying data: %s\n", hipGetErrorString(hip_err));
      break;
    default:
      stat = MMB_ERROR;
      MMB_ERR("Error while copying data: %s\n", hipGetErrorString(hip_err));
      break;
  }

BAILOUT:
  return stat;
}

#endif /* HAVE_ROCM */

/**  @} */

/**  @} */

/**  @} */

/**  @} */
