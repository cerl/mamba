/*
 * Copyright (C) 2018-2020 Cray UK
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef MMB_SICM_H
#define MMB_SICM_H

#include <stddef.h>
#include <sicm_low.h>

#include "../../common/mmb_error.h"
#include "../mmb_memory.h"

#include "../i_memory.h"

/**
 * @name mmb_memory
 * @{ */

/**
 * @name mmbMemInterface
 * @{ */

/**
 * @name SICM memory provider
 * @{ */

/**
 * @brief Data structure used to retain allocation device for the SICM based
 *        allocations. */
struct mmbSICM {
  sicm_device *device;
};

/**
 * @brief size of the \p mmbSICM structure.
 *
 * This parameter is required to initialize the static table in memory.c.
 * This value is used to compute the required size for an
 * \c struct mmbMemInterface.
 */
#define mmb_data_size__sicm sizeof(struct mmbSICM)

/**
 * @brief Initialize the SICM third-party library.
 *
 * This function calls the sicm_init() function which initializes the list of
 * available devices. This function is not thread-safe. This function must be
 * called before any interface initialization function.
 *
 * @param [in] param Handle to the parameters. May be \c NULL.
 *
 * @return \c MMB_OK on success, an error type otherwise.
 */
mmbError mmb_init__sicm(const mmbProviderInitParam *param);

/**
 * @brief Finalize the SICM providing interface.
 *
 * Calls sicm_fini() function which free the list of devices if not in use and
 * set to \c NULL the internal pointer to the list of devices. This function is
 * not thread-safe. No interface initialization can be called once this
 * function has been called.
 *
 * @return \c MMB_OK.
 */
mmbError mmb_finalize__sicm(void);

/**
 * @brief Initialize the interface for SICM third-party library.
 *
 * @param [in] interface Handle to the structure to initialize.
 *
 * @return \c MMB_OK on success, an error type otherwise.
 */
mmbError mmb_init_interface__sicm(mmbMemInterface *interface);

/**
 * @brief Finalize the interface for SICM providing interface
 *
 * @param [in] interface Handle to the structure to finalize.
 *
 * @return \c MMB_OK on success, an error type otherwise.
 */
mmbError mmb_finalize_interface__sicm(mmbMemInterface *interface);

/**
 * @brief Check whether the requested layer is supported by the SICM interface.
 *
 * @param [in] layer Requested layer.
 * @param [out] is_supported Handle for returned value.
 *
 * @return \c MMB_OK on success, an error type otherwise.
 */
mmbError mmb_supports__sicm(const mmbMemLayer layer, bool need_numa_support,
                            bool *is_supported);

/**
 * @brief Allocate \p n_bytes of data using the SICM interface.
 *
 * @param [in] n_bytes Size of the requested allocation.
 * @param [in] interface Handle to the interface data structure.
 * @param [in] opts Handle to the options used for allocating the buffer.  May
 *                  be \c NULL.
 * @param [out] allocation Handle to the allocated structure.
 *
 * @return \c MMB_OK on success, an error type otherwise.
 */
mmbError mmb_allocate__sicm(const size_t n_bytes,
                            mmbMemInterface *interface,
                            const mmbAllocateOptions *opts,
                            void **allocation);

/**
 * @brief Free the memory and structures allocated by
 *        \p mmb_allocate__sicm.
 *
 * @param [in] allocation Data to be free'd.
 * @param [in] interface Interface managing the given data.
 * @param [in] opts Handle to the options used for allocating the buffer.  May
 *                  be \c NULL.
 * @param [in] n_bytes Size of the allocation to free.
 *
 * @return \c MMB_OK on success, an error type otherwise.
 */
mmbError mmb_free__sicm(void *allocation, mmbMemInterface *interface,
                        const mmbAllocateOptions *opts,
                        const size_t n_bytes);

/**  @} */

/**  @} */

/**  @} */

#endif /* MMB_SICM_H */
