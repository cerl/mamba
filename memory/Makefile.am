# /*
#  * Copyright (C) 2018-2020 Cray UK
#  *
#  * Redistribution and use in source and binary forms, with or without
#  * modification, are permitted provided that the following conditions are
#  * met:
#  *
#  * 1. Redistributions of source code must retain the above copyright
#  *    notice, this list of conditions and the following disclaimer.
#  *
#  * 2. Redistributions in binary form must reproduce the above copyright
#  *    notice, this list of conditions and the following disclaimer in the
#  *    documentation and/or other materials provided with the distribution.
#  *
#  * 3. Neither the name of the copyright holder nor the names of its
#  *    contributors may be used to endorse or promote products derived from
#  *    this software without specific prior written permission.
#  *
#  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
#  * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
#  * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
#  * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
#  * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
#  * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
#  * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
#  * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
#  * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
#  * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
#  * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#  */

 
AM_CPPFLAGS = -I$(top_srcdir)/common -I$(top_srcdir)/interop @CUDA_CPPFLAGS@ @SICM_CPPFLAGS@ @UMPIRE_CPPFLAGS@ @HWLOC_CPPFLAGS@ @JEMALLOC_CPPFLAGS@ @MEMKIND_CPPFLAGS@ @HIP_ROCM_CPPFLAGS@
AM_CFLAGS = @CUDA_CFLAGS@ @HWLOC_CFLAGS@ @PTHREAD_CFLAGS@
AM_LDFLAGS = @CUDA_LDFLAGS@ @SICM_LDFLAGS@ @UMPIRE_LDFLAGS@ @HWLOC_LDFLAGS@ @JEMALLOC_LDFLAGS@ @MEMKIND_LDFLAGS@ @HIP_ROCM_LDFLAGS@ @NUMA_LDFLAGS@

noinst_LTLIBRARIES = libmmb-mem.la

libmmb_mem_la_LIBADD = @CUDA_LIBS@ @LIBSICM@ @LIBUMPIRE@ @PTHREAD_LIBS@ @LIBJEMALLOC@ @LIBMEMKIND@ @CL_LIBS@ @HIP_ROCM_LIBS@ @LIBNUMA@

interface_headers = interface.h         \
                    providers/conf_malloc_providers.h \
                    providers/system.h

interface_sources = providers/system.c

strategy_headers = strategy.h           \
                   strategies/apply.h   \
                   strategies/basic.h   \
                   strategies/statistics.h  \
                   strategies/pooled.h  \
                   strategies/thread_safe.h

strategy_iheaders = strategies/i_strategy.h \
                    strategies/i_basic.h    \
                    strategies/i_statistics.h  \
                    strategies/i_pooled.h   \
                    strategies/i_thread_safe.h

strategy_sources = strategy.c           \
                   strategies/apply.c   \
                   strategies/basic.c   \
                   strategies/statistics.c  \
                   strategies/pooled.c  \
                   strategies/thread_safe.c

if WITH_CUDA
    interface_sources += providers/system_cuda.c
    interface_headers += providers/system_cuda.h
endif

if WITH_OPENCL
    interface_sources += providers/system_opencl.c
    interface_headers += providers/system_opencl.h
endif

if HIP_ROCM
    interface_sources += providers/system_hip.c
    interface_headers += providers/system_hip.h
endif

if WITH_SICM
    interface_sources += providers/sicm.c
    interface_headers += providers/sicm.h
endif

if WITH_UMPIRE
    interface_sources += providers/umpire.c providers/umpire_register_wrap.cpp
    interface_headers += providers/umpire.h
endif

if WITH_JEMALLOC
    interface_sources += providers/jemalloc.c
    interface_headers += providers/jemalloc.h
endif

if ENABLE_DISCOVERY
    topology_headers = topology.h
    topology_sources = topology.c
    libmmb_mem_la_LIBADD += @LIBHWLOC@
endif

oldincludedir = @includedir@/memory

include_HEADERS = mmb_memory.h           \
                  mmb_memory_options.h   \
                  mmb_numa_sizes.h       \
                  mmb_provider_options.h

noinst_headers = \
        i_memory.h           \
        memory_config.h      \
        memory_options.h     \
        allocation.h         \
        statistics.h         \
        numa_utils.h         \
        $(interface_headers) \
        $(strategy_headers)  \
        $(strategy_iheaders) \
        $(topology_headers)

libmmb_mem_la_SOURCES = \
        memory.c             \
        memory_options.c     \
        allocation.c         \
        statistics.c         \
        numa_sizes.c         \
        numa_utils.c         \
        $(interface_sources) \
        $(strategy_sources)  \
        $(topology_sources)  \
        $(noinst_headers)

libmmb_memory_la_SHORTNAME = mmb-mem
