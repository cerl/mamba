/*
 * Copyright (C) 2018-2020 Cray UK
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef MMB_STRATEGY_H
#define MMB_STRATEGY_H

#include <stdlib.h>
#include <stdbool.h>

#include "../common/mmb_error.h"
#include "mmb_memory.h"
#include "mmb_memory_options.h"

#include "i_memory.h"

/**
 * @name mmb_memory
 * @{ */

/**
 * @name mmbStrategy
 * @{ */

/**
 * @name Public API
 *
 * This describe the set of functions needed to create new strategy builder.
 *
 * @{ */

typedef mmbError (*mmb_strat_init_fct)(mmbMemInterface *interface);
typedef mmbError (*mmb_strat_finalize_fct)(mmbMemInterface *interface);
typedef mmbError (*mmb_strat_allocate_fct)(const size_t n_bytes,
                                           mmbMemInterface *interface,
                                           const mmbAllocateOptions *opts,
                                           mmbAllocation *allocation);
typedef mmbError (*mmb_strat_free_fct)(mmbAllocation *allocation);
typedef mmbError (*mmb_strat_copy_nd_fct)(mmbAllocation *dst,
                                          const size_t *doffset,
                                          const size_t *dpitch,
                                          const mmbAllocation *src,
                                          const size_t *soffset,
                                          const size_t *spitch,
                                          const size_t ndims,
                                          const size_t *dims);

/**
 * @brief Strategy builder dispatch table entry.
 *
 * These functions will be used to run a strategy. These are the entry point
 * provided to mmb_memory functions.
 */
struct mmb_dispatch_strategy_entry {
  const char *           name;
  mmb_strat_init_fct     init;
  mmb_strat_finalize_fct finalize;
  mmb_strat_allocate_fct allocate;
  mmb_strat_free_fct     free;
  mmb_strat_copy_nd_fct  copy_nd;
};

/**
 * @brief Provides the size of the extra data required in the structure when
 *        allocating it.
 *
 * @param [in] provider Offset in the provider array dispatch table. Cannot be
 *                      \c MMB_PROVIDER_DEFAULT
 * @param [in] strategy Offset in the strategy array dispatch table. Cannot be
 *                      \c MMB_STRATEGY_DEFAULT
 * @param [out] out_data_size Strategy dependent data size.
 *
 * @return \c MMB_OK on success, \c MMB_INVALID_ARG if a wrong mmbProvider
 *         or a wrong mmbStrategy are as parameters.
 */
mmbError mmb_strat_get_data_size(const mmbProvider provider,
                                 const mmbStrategy strategy,
                                 size_t *out_data_size);

/**
 * @brief Check on the availability of the requested provider for the
 *        requested memory layer with the requested execution context.
 *
 * @param [in] provider Memory provider requested
 * @param [in] layer Memory layer
 * @param [in] ex_con Execution context to be tested
 * @param [in] needs_numa_support Whether the provider needs to support NUMA
 *                                aware allocations, or page aligned
 *                                allocations.
 * @param [out] is_available Whether the requested memory \p provider supports
 *                           the given \p layer.
 *
 * @return \c MMB_OK on success, \c MMB_INVALID_ARG if a wrong mmbProvider,
 *         wrong mmbMemLayer or wrong mmbExecutionContext are given as
 *         parameters.
 */
mmbError mmb_provider_compatibility_check(const mmbProvider provider,
                                          const mmbMemLayer layer,
                                          const mmbExecutionContext ex_con,
                                          bool needs_numa_support,
                                          bool *is_available);

/**
 * @brief Retrieve the data size for provider specific interfaces.
 *
 * @param [in] provider Memory provider requested
 * @return The proper data size of success, 0 on error.
 */
size_t mmb_provider_get_data_size(const mmbProvider provider);

/**
 * @brief Initialize the internal structures for the third-party library
 *        mmbProvider, based on the given parameters.
 *
 * This function accepts \c NULL as a valid value for \p params, in which case
 * the default parameters are used. This function is not thread-safe.
 *
 * @param [in] params Set of initialization parameters, each entry for the
 *                    corresponding mmbProvider. May be \c NULL.
 *
 * @return \c MMB_OK on success, an error type otherwise.
 */
mmbError mmb_init_providers(const mmbProviderInitParamSet params);

/**
 * @brief Finalize the internal structures for the third-party library
 *        mmbProvider.
 *
 * This function is not thread-safe.
 *
 * @return \c MMB_OK on success, an error type otherwise.
 */
mmbError mmb_finalize_providers(void);

/**
 * @brief Initialize the internal structures for the mmbStrategy, based on the
 *        given parameters.
 *
 * This function accepts \c NULL as a valid value for \p params, in which case
 * the default parameters are used. This function is not thread-safe.
 *
 * @param [in] params Set of initialization parameters, each entry for the
 *                    corresponding mmbStrategy. May be \c NULL.
 *
 * @return \c MMB_OK on success, an error type otherwise.
 */
mmbError mmb_init_strategies(const mmbStrategyInitParamSet params);

/**
 * @brief Finalize the internal structures for the mmbStrategy.
 *
 * This function is not thread-safe.
 *
 * @return \c MMB_OK on success, an error type otherwise.
 */
mmbError mmb_finalize_strategies(void);

/**  @} */

/**  @} */

/**  @} */

#endif /* MMB_STRATEGY_H */
