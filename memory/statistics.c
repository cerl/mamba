/*
 * Copyright (C) 2021      Cray UK
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <stdlib.h>
#include <pthread.h>
#include <assert.h>
#include "../common/mmb_error.h"
#include "../common/mmb_logging.h"
#include "../common/i_mmb_hash.h"
#include "i_memory.h"
#include "strategies/i_statistics.h"

static inline void mmb_stats_lock_hashmap(mmbAllocStats *stats)
{
  int stat = pthread_mutex_lock(&stats->hash_mtx);
  assert(0 == stat);
  (void) stat;
}
static inline void mmb_stats_unlock_hashmap(mmbAllocStats *stats)
{
  int stat = pthread_mutex_unlock(&stats->hash_mtx);
  assert(0 == stat);
  (void) stat;
}

static mmbError
mmb_stats_get_key_and_record(mmbAllocStats *allocation_stats_records,
                             const mmbMemInterface *interface,
                             mmb_hash_iter_t *key,
                             struct mmb_strat_stats_data **data)
{
  *key = mmb_hash_get_alloc_stats(allocation_stats_records->hash, interface);
  if (mmb_hash_end_alloc_stats(allocation_stats_records->hash) == *key) {
    return MMB_NOT_FOUND;
  }
  *data = mmb_hash_value_alloc_stats(allocation_stats_records->hash, *key);
  return MMB_OK;
}

static mmbError mmb_stats_get_record(mmbAllocStats *allocation_stats_records,
                                     const mmbMemInterface *interface,
                                     struct mmb_strat_stats_data **data)
{
  mmb_hash_iter_t key;
  return mmb_stats_get_key_and_record(allocation_stats_records, interface, &key, data);
}

static mmbError mmb_stats_put_record(mmbAllocStats *allocation_stats_records,
                                     const mmbMemInterface *interface,
                                     struct mmb_strat_stats_data *data)
{
  int err;
  mmb_hash_iter_t key;
  key = mmb_hash_put_alloc_stats(allocation_stats_records->hash, interface, &err);
  if (0 > err) {
    MMB_ERR("Error while adding key statistics record. (%d)\n", err);
    return MMB_ERROR;
  }
  mmb_hash_value_alloc_stats(allocation_stats_records->hash, key) = data;
  return MMB_OK;
}

static mmbError mmb_stats_delete_record(mmbAllocStats *allocation_stats_records,
                                        const mmbMemInterface *interface,
                                        const struct mmb_strat_stats_data *data)
{
  mmbError stat;
  mmb_hash_iter_t key;
  struct mmb_strat_stats_data *data_req;
  stat = mmb_stats_get_key_and_record(allocation_stats_records, interface,
                                      &key, &data_req);
  if (MMB_OK == stat) {
    if (data != data_req) {
      MMB_ERR("Wrong data from shared interface.");
      stat = MMB_ERROR;
    } else {
      mmb_hash_del_alloc_stats(allocation_stats_records->hash, key);
    }
  }
  return stat;
}

static mmbError mmb_stats_record_exists(mmbAllocStats *allocation_stats_records,
                                        const mmbMemInterface *interface)
{
  struct mmb_strat_stats_data *data;
  return mmb_stats_get_record(allocation_stats_records, interface, &data);
}

mmbError mmb_stats_init(mmbAllocStats *allocation_stats_records)
{
  /* Initialize the hashmap */
  if (NULL == allocation_stats_records || NULL != allocation_stats_records->hash) {
    MMB_ERR("Invalid pointer to hashmap structure or already initialized.\n");
    return MMB_INVALID_ARG;
  }
  int err = pthread_mutex_init(&allocation_stats_records->hash_mtx, NULL);
  if (0 != err) {
    MMB_ERR("Unable to initialize mutex on statistics records (err=%d).\n", err);
    return MMB_ERROR;
  }
  allocation_stats_records->hash = mmb_hash_init_alloc_stats();
  return MMB_OK;
}

mmbError mmb_stats_finalize(mmbAllocStats *allocation_stats_records)
{
  if (NULL == allocation_stats_records || NULL == allocation_stats_records->hash) {
    MMB_ERR("Invalid hashmap, or already free'd.\n");
    return MMB_INVALID_ARG;
  }
  mmb_stats_lock_hashmap(allocation_stats_records);
  mmb_hash_destroy_alloc_stats(allocation_stats_records->hash);
  mmb_stats_unlock_hashmap(allocation_stats_records);
  int err = pthread_mutex_destroy(&allocation_stats_records->hash_mtx);
  if (0 != err) {
    MMB_ERR("Unable to destroy mutex on statistics records (err=%d).\n", err);
    return MMB_ERROR;
  }
  return MMB_OK;
}

mmbError mmb_stats_register_interface(mmbAllocStats *allocation_stats_records,
                                      mmbMemInterface *interface,
                                      struct mmb_strat_stats_data *data)
{
  mmbError stat = MMB_OK;
  if (NULL == allocation_stats_records || NULL == allocation_stats_records->hash) {
    MMB_ERR("hashmap is not initialised.\n");
    return MMB_ERROR;
  }
  mmb_stats_lock_hashmap(allocation_stats_records);
  stat = mmb_stats_record_exists(allocation_stats_records, interface);
  if (MMB_NOT_FOUND != stat) {
    MMB_ERR("Conflict of key in hashmap. Unable to record.\n");
    stat = MMB_ERROR;
  } else {
    stat = mmb_stats_put_record(allocation_stats_records, interface, data);
  }
  mmb_stats_unlock_hashmap(allocation_stats_records);
  return stat;
}

mmbError mmb_stats_deregister_interface(mmbAllocStats *allocation_stats_records,
                                        mmbMemInterface *interface,
                                        struct mmb_strat_stats_data *data)
{
  mmbError stat = MMB_OK;
  if (NULL == allocation_stats_records || NULL == allocation_stats_records->hash) {
    MMB_ERR("hashmap is not initialised.\n");
    return MMB_ERROR;
  }
  /** destroy specific data */
  mmb_stats_lock_hashmap(allocation_stats_records);
  stat = mmb_stats_delete_record(allocation_stats_records, interface, data);
  mmb_stats_unlock_hashmap(allocation_stats_records);
  return stat;
}

mmbError mmb_stats_get_current(mmbAllocStats *allocation_stats_records,
                               mmbMemInterface *interface,
                               size_t *n_allocations,
                               size_t *bytes_allocated)
{
  if (NULL == allocation_stats_records || NULL == allocation_stats_records->hash) {
    MMB_ERR("Statistics not initialized or invalid hashmap.\n");
    return MMB_ERROR;
  }
  mmbError stat = mmb_strat_stats_check_interface(interface);
  if (MMB_OK != stat) {
    return stat;
  }

  mmb_stats_lock_hashmap(allocation_stats_records);

  struct mmb_strat_stats_data *data;
  stat = mmb_stats_get_record(allocation_stats_records, interface, &data);

  mmb_stats_unlock_hashmap(allocation_stats_records);

  if (MMB_OK != stat) {
    MMB_ERR("There is no corresponding records.\n");
    return stat;
  }

  mmb_strat_stats_lock_data(data);
  if (NULL != n_allocations)
    *n_allocations = data->current_allocations;
  if (NULL != bytes_allocated)
    *bytes_allocated = data->current_allocated;
  mmb_strat_stats_unlock_data(data);

  return stat;
}

mmbError mmb_stats_get_total(mmbAllocStats *allocation_stats_records,
                             mmbMemInterface *interface,
                             size_t *n_allocations,
                             size_t *bytes_allocated)
{
  if (NULL == allocation_stats_records || NULL == allocation_stats_records->hash) {
    MMB_ERR("Statistics not initialized or invalid hashmap.\n");
    return MMB_ERROR;
  }
  mmbError stat = mmb_strat_stats_check_interface(interface);
  if (MMB_OK != stat) {
    return stat;
  }

  mmb_stats_lock_hashmap(allocation_stats_records);

  struct mmb_strat_stats_data *data;
  stat = mmb_stats_get_record(allocation_stats_records, interface, &data);

  mmb_stats_unlock_hashmap(allocation_stats_records);

  if (MMB_OK != stat) {
    MMB_ERR("There is no corresponding records.\n");
    return stat;
  }

  mmb_strat_stats_lock_data(data);
  if (NULL != n_allocations)
    *n_allocations = data->total_allocations;
  if (NULL != bytes_allocated)
    *bytes_allocated = data->total_allocated;
  mmb_strat_stats_unlock_data(data);

  return stat;
}

mmbError
mmb_stats_get_record_count(mmbAllocStats *allocation_stats_records, size_t *count)
{
  if (NULL == allocation_stats_records || NULL == allocation_stats_records->hash) {
    MMB_ERR("Statistics not initialized or invalid hashmap.\n");
    return MMB_INVALID_ARG;
  }
  if (NULL == count) {
    MMB_ERR("Invalid variable cannot be NULL.");
    return MMB_INVALID_ARG;
  }

  mmb_stats_lock_hashmap(allocation_stats_records);

  *count = mmb_hash_size_alloc_stats(allocation_stats_records->hash);

  mmb_stats_unlock_hashmap(allocation_stats_records);

  return MMB_OK;
}
