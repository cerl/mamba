/*
 * Copyright (C) 2021      Cray UK
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#if HAVE_NUMA
#include <numa.h>
#if HAVE_NUMAIF_H
#include <numaif.h>
#endif /* HAVE_NUMAIF_H */
#endif /* HAVE_NUMA */

#include "mmb_error.h"
#include "mmb_logging.h"
#include "numa_utils.h"

mmbError mmb_numa_available(void)
{
  mmbError stat = MMB_ERROR;
#if HAVE_NUMA
  if (0 == numa_available())
    stat = MMB_OK;
#endif /* HAVE_NUMA */
  return stat;
}

mmbError mmb_numa_max_node(int *max_node)
{
  mmbError stat = MMB_OK;
#if HAVE_NUMA
  *max_node = numa_max_node();
#else
  (void) max_node;
  stat= MMB_UNSUPPORTED;
  MMB_ERR("Missing libnuma, operation unsupported.\n");
#endif /* HAVE_NUMA */
  return stat;
}

mmbError
mmb_numa_move_page(void **allocation, size_t n_bytes, int numa_id)
{
  mmbError stat = MMB_OK;
#if HAVE_NUMAIF_H
  long page_size = sysconf(_SC_PAGESIZE);
  unsigned long n_pages = (n_bytes + page_size - 1) / page_size;
  void *pages[n_pages];
  int nodes[n_pages], status[n_pages];
  for (unsigned long p = 0; n_pages > p; ++p) {
    pages[p] = ((unsigned char *)*allocation) + p * page_size;
    nodes[p] = numa_id;
  }
  long ret = move_pages(0 /* local process */, n_pages, pages, nodes,
                        status, MPOL_MF_MOVE);
  if (0 != ret) {
    MMB_ERR("Errors occured while trying to move pages. (err=%ld)\n", ret);
    stat = MMB_ERROR;
  }
#else
  (void) allocation; (void) n_bytes; (void) numa_id;
  stat = MMB_UNSUPPORTED;
  MMB_ERR("Missing libnuma, operation unsupported.\n");
#endif /* HAVE_NUMAIF_H */
  return stat;
}

mmbError
mmb_numa_get_node_id(void **allocation, size_t count, int *numa_id)
{
  mmbError stat = MMB_OK;
#if HAVE_NUMA
  long ret = move_pages(0 /* local process */, count, allocation,
                        NULL /* returns numa ids */, numa_id, MPOL_MF_MOVE);
  if (0 != ret) {
    MMB_ERR("Errors occured while trying to move pages. (err=%ld)\n", ret);
    stat = MMB_ERROR;
  }
#else
  (void) allocation; (void) count; (void) numa_id;
  stat = MMB_UNSUPPORTED;
  MMB_ERR("Missing libnuma, operation unsupported.\n");
#endif /* HAVE_NUMA */
  return stat;
}

mmbError mmb_numa_alloc_on_node(const size_t n_bytes, const int node_id,
                                void **out_ptr)
{
  mmbError stat = MMB_OK;
#if HAVE_NUMA
  void *ptr = numa_alloc_onnode(n_bytes, node_id);
  if (NULL != ptr)
    *out_ptr = ptr;
  else
    stat = MMB_OUT_OF_MEMORY;
#else
  (void) n_bytes; (void) node_id; (void) out_ptr;
  stat = MMB_UNSUPPORTED;
  MMB_ERR("Missing libnuma, operation unsupported.\n");
#endif /* HAVE_NUMA */
  return stat;
}

mmbError mmb_numa_free(void *allocation, const size_t n_bytes)
{
  mmbError stat = MMB_OK;
#if HAVE_NUMA
  numa_free(allocation, n_bytes);
#else
  (void) allocation; (void) n_bytes;
  stat = MMB_UNSUPPORTED;
  MMB_ERR("Missing libnuma, operation unsupported.\n");
#endif /* HAVE_NUMA */
  return stat;
}
