/*
 * Copyright (C) 2020      Cray UK
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef MEMORY_OPTIONS_H
#define MEMORY_OPTIONS_H

#include "mmb_error.h"
#include "mmb_memory.h"
#include "mmb_memory_options.h"

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @name mmbMemInterface
 * @{ */

/**
 * @name Private API
 * @{ */

/*** NO FUNCTIONS REQUIRED FOR NOW ***/

/**  @} */

/**  @} */

/**
 * @name mmbStrategy
 * @{ */

/**
 * @name Private API
 * @{ */

/**
 * @brief Set the default size of a pool in the corresponding field of the
 *        memory system initialization parameter handle.
 *
 * This parameter affects the size of the memory pool allocated when the
 * \c MMB_POOLED strategy is used by the allocation interface.  The size is
 * given in bytes.
 *
 * @param [inout]   mem_opts Handle to mmbMemoryOptions to be updated.
 * @param [in]      new_size Requested size for the memory pool.
 * @return \c MMB_OK on success, an error type otherwise.
 */
mmbError
mmb_memory_options_strategy_pooled_set_pool_size(mmbMemoryOptions *mem_opts,
                                                 size_t new_size);
/**  @} */

/**  @} */

/**
 * @name mmbMemoryOptions
 * @{ */

/**
 * @name Private API
 * @{ */

/**
 * @brief Allocate a set of default parameters for memory system initialization.
 *
 * The allocated handle has to be free'd by a call to
 * \p mmb_memory_options_destroy.
 *
 * @param [out]  mem_opts pointer to a valid pointer. Cannot be \c NULL.
 * @return \c MMB_OK on success, an error type otherwise.
 */
mmbError mmb_memory_options_create_default(mmbMemoryOptions **mem_opts);

/**
 * @brief Deallocate a set of parameters used for memory system initialization.
 *
 * The given handle \p mem_opts must have been allocated with
 * \p mmb_memory_options_create_default.
 *
 * @param [in]  mem_opts pointer to a valid pointer. Cannot be \c NULL.
 * @return \c MMB_OK on success, an error type otherwise.
 */
mmbError mmb_memory_options_destroy(mmbMemoryOptions *mem_opts);

/**  @} */

/**  @} */

#ifdef __cplusplus
}
#endif

#endif /* MEMORY_OPTIONS_H */
