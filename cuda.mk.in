#
# Copyright (C) 2021      Cray UK
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are
# met:
#
# 1. Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
#
# 2. Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in the
#    documentation and/or other materials provided with the distribution.
#
# 3. Neither the name of the copyright holder nor the names of its
#    contributors may be used to endorse or promote products derived from
#    this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
# IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
# TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
# PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
# HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
# SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
# LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
# DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
# THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#

################################################################################
# CUDA related options and variables
#
# The not fastdep mode would likely break.  It would require some more work to
# patch the build-aux/depcomp file to accept a `depmode=cuda` and to add the
# includes for targets' dependencies.  Also it would be nice to automatically
# define the objects as $convenience_library_name-$source_name.  Finally, we
# are still lacking in the management of the linking functions; it would be
# better if the lib<taget>_la_LDFLAGS and lib<target>_la_LIBADD flags were
# added automatically.
#
# The following variables create the necessary targets to create .lo objects
# out of the .cu source files.

CUDADEPMODE = $(CXXDEPMODE)
AM_V_CUDA = $(am__v_CUDA_@AM_V@)
am__v_CUDA_ = $(am__v_CUDA_@AM_DEFAULT_V@)
am__v_CUDA_0 = @echo "  CUDA    " $@;
am__v_CUDA_1 =
CUDACOMPILE = $(NVCC) $(DEFS) $(DEFAULT_INCLUDES) $(INCLUDES) \
	$(AM_CPPFLAGS) $(CPPFLAGS) @CUDA_CPPFLAGS@ \
	--compiler-options="$(AM_CXXFLAGS) $(CXXFLAGS) @CUDA_CFLAGS@"
LTCUDACOMPILE = $(LIBTOOL) $(AM_V_lt) --tag=CXX $(AM_LIBTOOLFLAGS) \
	$(LIBTOOLFLAGS) --mode=compile $(NVCC) $(DEFS) \
	$(DEFAULT_INCLUDES) $(INCLUDES) $(AM_CPPFLAGS) $(CPPFLAGS) @CUDA_CPPFLAGS@ \
	--compiler-options="$(AM_CXXFLAGS) $(CXXFLAGS) @CUDA_CFLAGS@"

AM_CUDA_LDFLAGS = @CUDA_LDFLAGS@
AM_CUDA_LIBS = @CUDA_LIBS@ -lcudart

.cu.$(OBJEXT):
@am__fastdepCXX_TRUE@	$(AM_V_CUDA)depbase=`echo $@ | sed 's|[^/]*$$|$(DEPDIR)/&|;s|\.o$$||'`;\
@am__fastdepCXX_TRUE@	$(CUDACOMPILE) --compiler-options="-MT $@ -MD -MP -MF $$depbase.Tpo" \
@am__fastdepCXX_TRUE@	-c -o $@ $< &&\
@am__fastdepCXX_TRUE@	$(AM_V_at)$(AWK) '!/\.cpp|\.fatbin\.c|\.stub\.c/' < $$depbase.Tpo > $$depbase.Plo &&\
@am__fastdepCXX_TRUE@	$(RM) $$depbase.Tpo
@AMDEP_TRUE@@am__fastdepCXX_FALSE@	$(AM_V_CUDA)source='$<' object='$@' libtool=no \
@AMDEP_TRUE@@am__fastdepCXX_FALSE@	DEPDIR=$(DEPDIR) $(CUDADEPMODE) $(depcomp) \
@AMDEP_TRUE@@am__fastdepCXX_FALSE@	$(AM_V_CUDA_no)$(CUDACOMPILE) -c -o $@ $<

.cu.obj:
@am__fastdepCXX_TRUE@	$(AM_V_CUDA)depbase=`echo $@ | sed 's|[^/]*$$|$(DEPDIR)/&|;s|\.obj$$||'`;\
@am__fastdepCXX_TRUE@	$(CUDACOMPILE) --compiler-options="-MT $@ -MD -MP -MF $$depbase.Tpo" \
@am__fastdepCXX_TRUE@	-c -o $@ `$(CYGPATH_W) '$<'` &&\
@am__fastdepCXX_TRUE@	$(AM_V_at)$(AWK) '!/\.cpp|\.fatbin\.c|\.stub\.c/' < $$depbase.Tpo > $$depbase.Plo &&\
@am__fastdepCXX_TRUE@	$(RM) $$depbase.Tpo
@AMDEP_TRUE@@am__fastdepCXX_FALSE@	$(AM_V_CUDA)source='$<' object='$@' libtool=no \
@AMDEP_TRUE@@am__fastdepCXX_FALSE@	DEPDIR=$(DEPDIR) $(CUDADEPMODE) $(depcomp) \
@AMDEP_TRUE@@am__fastdepCXX_FALSE@	$(AM_V_CUDA_no)$(CUDACOMPILE) -c -o $@ `$(CYGPATH_W) '$<'`

.cu.lo:
@am__fastdepCXX_TRUE@	$(AM_V_CUDA)depbase=`echo $@ | sed 's|[^/]*$$|$(DEPDIR)/&|;s|\.lo$$||'`;\
@am__fastdepCXX_TRUE@	$(LTCUDACOMPILE) -prefer-non-pic --compiler-options="-MT $@ -MD -MP -MF $$depbase.Tpo" \
@am__fastdepCXX_TRUE@	-c -o $@ $< &&\
@am__fastdepCXX_TRUE@	$(AM_V_at)$(AWK) '!/\.cpp|\.fatbin\.c|\.stub\.c/' < $$depbase.Tpo > $$depbase.Plo &&\
@am__fastdepCXX_TRUE@	$(RM) $$depbase.Tpo
@AMDEP_TRUE@@am__fastdepCXX_FALSE@	$(AM_V_CUDA)source='$<' object='$@' libtool=yes \
@AMDEP_TRUE@@am__fastdepCXX_FALSE@	DEPDIR=$(DEPDIR) $(CUDADEPMODE) $(depcomp) \
@AMDEP_TRUE@@am__fastdepCXX_FALSE@	$(AM_V_CUDA_no)$(LTCUDACOMPILE) -c -o $@ $<
