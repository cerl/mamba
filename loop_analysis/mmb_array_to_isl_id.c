/*
 * Copyright (C) 2018-2020 Cray UK
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <mmb_array_to_isl_id.h>
#include "mmb_loop.h"
#include <isl/id.h>
#include <isl/hash.h>
#include <assert.h>
#include <string.h>

static
const char * const data_ref_types_table[MMB_DATA_REF_TYPE__MAX] = {
  [MMB_DATA_REF_UNDEFINED]   = "undefined",
  [MMB_DATA_REF_SCALAR]      = "scalar",
  [MMB_DATA_REF_ARRAY]       = "array"
};

const char* mmb_data_ref_type_to_string(enum mmbDataRefType type) {
  assert(type<MMB_DATA_REF_TYPE__MAX);
  return data_ref_types_table[type];
}


mmbError mmb_data_ref_create_array(void * ref, int dim, 
                             int type_size, const char* name, 
                             mmbDataRef ** out_ref) {
  *out_ref = NULL;
  mmbDataRefType type = MMB_DATA_REF_ARRAY;
  // Data refs are managed using ISL-like API, to
  // interact with ISL hash map
  mmbDataRef* r = mmbDataRef_alloc(ref, type, dim, type_size, name);
  if(!r) {
    MMB_ERR("Failed to allocate mmbDataRef\n");
    return MMB_ERROR;
  }
  *out_ref = r;
  return MMB_OK;
}
mmbError mmb_data_ref_create_scalar(void * ref, int type_size, 
                                   const char* name, mmbDataRef ** out_ref) { 

  *out_ref = NULL;
  mmbDataRefType type = MMB_DATA_REF_SCALAR;
  // Data refs are managed using ISL-like API, to
  // interact with ISL hash map
  mmbDataRef* r = mmbDataRef_alloc(ref, type, 1, type_size, name);
  if(!r) {
    MMB_ERR("Failed to allocate mmbDataRef\n");
    return MMB_ERROR;
  }
  *out_ref = r;
  return MMB_OK;
}

mmbError mmb_data_ref_destroy(mmbDataRef * in_ref) {
  mmbDataRef_free(in_ref);
  return MMB_OK;
}

mmbError mmb_data_ref_print(mmbDataRef* ar, int indent) {
  if(!ar)
    return MMB_INVALID_ARG;

  MMB_DEBUG("%*sRef address: %p, Type: %s, Name: %s\n",indent,"", ar->ptr, 
        mmb_data_ref_type_to_string(ar->type), 
        (ar->name) ? ar->name : "(none)");

  return MMB_OK;
}


#define isl_id_is_equal(id1,id2)	id1 == id2

#define ISL_KEY		mmbDataRef
#define ISL_VAL		isl_id
#define ISL_HMAP_SUFFIX	data_ref_to_isl_id
#define ISL_HMAP	mmb_data_ref_to_isl_id
#define ISL_KEY_IS_EQUAL	mmbDataRef_is_equal
#define ISL_VAL_IS_EQUAL	isl_id_is_equal
#define ISL_KEY_PRINT		isl_printer_print_mmbDataRef
#define ISL_VAL_PRINT		isl_printer_print_id

mmbDataRef* mmbDataRef_alloc(void *data_ptr, mmbDataRefType type,
                             int dim, int type_size, const char* name) {
  uint32_t id_hash = isl_hash_init();
  mmbDataRef* ar = (mmbDataRef*)calloc(1, sizeof(mmbDataRef));
  if(!ar)
    return NULL;
  ar->ptr = data_ptr;
  ar->type = type;
  ar->dim = dim;
  if(name){
    ar->name = strdup(name);
    if(!ar->name) {
      MMB_ERR("Out of memory\n");
      return NULL;
    }
  }

  ar->hash = isl_hash_builtin(id_hash, data_ptr);
  ar->ref = 1; 
  ar->id = isl_id_alloc(g_isl_ctx, ar->name, ar->ptr);
  if(!ar->id) {
    MMB_ERR("Failed to alloc isl_id for data ref\n");
    free(ar->name);
    free(ar);
    return NULL;
  }
  ar->type_size = type_size;
  return ar;
}

mmbDataRef* mmbDataRef_copy(mmbDataRef *ar) {
  if(ar == NULL)
    return NULL;
    
  ar->ref++;
  return ar;
}
mmbDataRef* mmbDataRef_free(mmbDataRef *ar) {
  if(ar == NULL)
    return NULL;

  if(--ar->ref > 0) 
    return NULL;

  isl_id_free(ar->id);
  free(ar->name);
  free(ar);
  return NULL;
  
}

__isl_give isl_printer* 
isl_printer_print_mmbDataRef(__isl_take isl_printer *p,
                              __isl_keep mmbDataRef *dr ) {
  if(!dr)
    goto BAILOUT;

  char buffer[50];
  snprintf(buffer, sizeof(buffer), "@%p", dr->ptr);
  p = isl_printer_print_str(p, buffer);

BAILOUT:
  isl_printer_free(p);
  return NULL;
}

uint32_t mmbDataRef_get_hash(__isl_keep mmbDataRef *dr) {
	return dr ? dr->hash : 0;
}

isl_bool mmbDataRef_is_equal(__isl_keep mmbDataRef *dr1,
	__isl_keep mmbDataRef *dr2) {
  if(!dr1 || !dr2) {
    return isl_bool_error;
  }

  if(dr1->ptr == dr2->ptr) {
    return isl_bool_true;
  } else {
    return isl_bool_false;
  }
}

#include <isl/hmap_templ.c>