/*
 * Copyright (C) 2018-2020 Cray UK
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef MMB_LOOP_TO_ISL_ID_H
#define MMB_LOOP_TO_ISL_ID_H

#include <isl/id_type.h>
#include <isl/maybe_id.h>

#include "../common/mmb_error.h"
#include "../common/mmb_logging.h"

typedef enum mmbDataRefType {
  MMB_DATA_REF_UNDEFINED,
  MMB_DATA_REF_SCALAR,
  MMB_DATA_REF_ARRAY,
  MMB_DATA_REF_TYPE__MAX
} mmbDataRefType;

typedef struct mmbDataRef {
  void *ptr;
  enum mmbDataRefType type;
  int dim;
  char *name;
  uint32_t hash;
  int ref;
  isl_id *id;
  int type_size;
} mmbDataRef;


const char* mmb_data_ref_type_to_string(enum mmbDataRefType);
mmbError mmb_data_ref_create_array(void * ref, int dim, 
                             int type_size, const char* name, 
                             mmbDataRef ** out_ref);
mmbError mmb_data_ref_create_scalar(void * ref, int type_size, 
                                   const char* name, mmbDataRef ** out_ref);
mmbError mmb_data_ref_destroy(mmbDataRef * in_ref);
mmbError mmb_data_ref_print(mmbDataRef* ref, int indent); 

mmbDataRef* mmbDataRef_alloc(void *data_ptr, enum mmbDataRefType type, 
                             int dim, int type_size, const char* name);
mmbDataRef* mmbDataRef_copy(mmbDataRef *ref);
mmbDataRef* mmbDataRef_free(mmbDataRef *ref);
isl_printer* 
isl_printer_print_mmbDataRef(isl_printer *p, mmbDataRef *ar);
uint32_t mmbDataRef_get_hash(mmbDataRef *ar);

#define ISL_KEY		mmbDataRef
#define ISL_VAL		isl_id
#define ISL_HMAP_SUFFIX	data_ref_to_isl_id
#define ISL_HMAP	mmb_data_ref_to_isl_id
#include <isl/hmap.h>
#undef ISL_KEY
#undef ISL_VAL
#undef ISL_HMAP_SUFFIX
#undef ISL_HMAP

#define ISL_TYPE	mmbDataRef
#include <isl/maybe_templ.h>
#undef ISL_TYPE


#endif
