! Copyright (C) 2018-2020 Cray UK
!
! Redistribution and use in source and binary forms, with or without
! modification, are permitted provided that the following conditions are
! met:
!
! 1. Redistributions of source code must retain the above copyright
!    notice, this list of conditions and the following disclaimer.
!
! 2. Redistributions in binary form must reproduce the above copyright
!    notice, this list of conditions and the following disclaimer in the
!    documentation and/or other materials provided with the distribution.
!
! 3. Neither the name of the copyright holder nor the names of its
!    contributors may be used to endorse or promote products derived from
!    this software without specific prior written permission.
!
! THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
! IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
! TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
! PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
! HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
! SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
! LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
! DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
! THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
! (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
! OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

program tile_duplicate
  use mamba
  implicit none
  logical, parameter :: debug=.true.
  integer, parameter :: array_size = 32, &
                        tile_size = 8
  integer(mmbErrorKind) err
  type(mmbMemSpace) dram_space
  type(mmbMemSpaceConfig) dram_config
  type(mmbMemInterface) dram_interface
  type(mmbArray) :: mba
  integer(mmbIndexKind),dimension(1) :: dims,chunks
  type(mmbLayout) layout
  type(mmbTileIterator) it
  type(mmbArrayTile) tile,duplicate_tile
  type(mmbIndex) :: idx
  integer(mmbSizeKind), allocatable, dimension(:) :: tiling_dims
  integer(mmbIndexKind) :: ti
  integer :: i
  
  print *,"Starting example tile_duplicate (Fortran)"
  
  call mmb_init(err=err)
  call check_status(err,"Failed to initialise Mamba")
  if (debug) print *,'Completed mmb_init()'

  call mmb_logging_set_level(MMB_LOG_DEBUG,err)
  call check_status(err,"Failed to enable mamba debug logging")
  
  dram_config = mmbMemSpaceConfig(mmbSizeConfig(MMB_SIZE_SET,.false.,8000),&
                                  MMB_MEMINTERFACE_CONFIG_DEFAULT)
  ! register memory but don't ask for memory space
  call mmb_register_memory(MMB_DRAM, MMB_EXECUTION_CONTEXT_DEFAULT, &
                           dram_config,err=err)
  call check_status(err,'Failed to register DRAM memory for Mamba')

  ! Get memory space, space config is optional
  call mmb_request_space(MMB_DRAM, MMB_EXECUTION_CONTEXT_DEFAULT, &
                         new_space=dram_space, err=err )
  call check_status(err,'Failed to get memory space')

  ! interface config_opts are optional
  call mmb_request_interface(dram_space, new_interface=dram_interface, err=err)
  call check_status(err,'Failed to get memory interface')

   
  dims = [array_size]

  ! routine is wrongly named, mmb_layout_create_padding_zero better
  call mmb_layout_create_regular_1d(int(storage_size(1.0)/8,mmbSizeKind),&
                       mmb_layout_padding_create_zero(), &
                       layout,err)
  call check_status(err,'Failed to create regular 1d layout')

  call mmb_array_create(dims, layout, dram_interface, MMB_READ_WRITE, mba,err )
  call check_status(err,'Failed to create mamba array')

  call init_matrix_buffer(mba)

  ! Request tiling of arrays with chunkx chunky
  
  chunks = [tile_size]
  
  call mmb_array_tile(mba, chunks, err)
  call check_status(err,'Failed to tile mamba array in chunks')

  ! Loop over tiles using standard indexing
  ! Duplicate each tile, write to it, and merge back to original

  call mmb_index_create(1_mmbIndexKind,idx,err)
  call check_status(err,'Failed to create mmb index')

  allocate( tiling_dims(1) )
  call mmb_tiling_dimensions(mba, tiling_dims, err)
  call check_status(err,'Failed to get mamba array tiling dimensions')
  
  tiles: do ti=1,tiling_dims(1)
  !tiles: do ti=1,2
     print *,'looping over tile ',ti
     ! Set tile index for traversal
     call mmb_index_set(idx,ti,err)
     call check_status(err,"Failed to set mmb index")

     call mmb_tile_at(mba,idx,tile,err)
     call check_status(err,"""Failed to get tile at index")

     ! Duplicate tile in DRAM; write to and merge duplicated tile
     call mmb_tile_duplicate(tile, dram_interface, MMB_READ_WRITE, &
          layout, duplicate_tile,err )
     call check_status(err,"Failed to set mmb index")

     call write_to_tile(duplicate_tile)

     call mmb_tile_merge(duplicate_tile,MMB_OVERWRITE,err)
     call check_status(err,"Failed to merge duplicate tile")
     
   end do tiles

  ! Cleanup intermediate objects, check result buffer, cleanup mamba
  call mmb_layout_destroy(layout,err)
  call check_status(err, "Failed to destroy mmb layout")

  deallocate( tiling_dims )  

  call mmb_index_destroy(idx,err)
  call check_status(err, "Failed to destroy mmb index")

  call check_result(mba,err)
  if (err == MMB_OK) then
     print *,"Array successfully tiled and duplicated in DRAM"
    else
     print *,"Array data validation check failed."
    end if

  call mmb_array_destroy(mba,err);
  call check_status(err, "Failed to destroy mmb array")

  call mmb_finalize(err)
   
  if (err .ne. MMB_OK) then
    ERROR STOP 'Failed to finalise Mamba'
   end if

  print *,"Ended example tile_duplicate (Fortran)"

  contains

  subroutine write_to_tile(tile)
    type(mmbArrayTile), intent(in) :: tile
    type(mmbArrayTile) :: dev_tile
    real, pointer, dimension(:) :: tp
    integer i
    
    call mmb_tile_get_space_local_handle(tile, dev_tile,err)
    call check_status(err,"Failed to obtain local tile handle")
    
    call mmb_tile_get_pointer(dev_tile,tp)
    call dump_tileData(dev_tile,"dev tile")

    ! duplicated tile is not part of a larger allocation
    ! but lower and upper bound the correct tile indexes
    do i=dev_tile%lower(1),dev_tile%upper(1)
       tp(i) = 1.0
    end do
 
  end subroutine write_to_tile

  ! helper
  subroutine dump_tileData(tile,label)
    type(mmbArrayTile),intent(in) :: tile
    character(len=*), intent(in) :: label

    print *,'tile_info for tile',label
    print '(tr1,a,tr1,i0)','         rank',tile%rank

    print '(tr1,a,tr1,*(i0:tr1))','        lower',tile%lower
    print '(tr1,a,tr1,*(i0:tr1))','        upper',tile%upper
    print '(tr1,a,tr1,*(i0:tr1))','       alower',tile%alower
    print '(tr1,a,tr1,*(i0:tr1))','       aupper',tile%aupper
    print '(tr1,a,tr1,*(i0:tr1))','          dim',tile%dim
    print '(tr1,a,tr1,*(i0:tr1))','      abs_dim',tile%abs_dim
    print *,'is_contiguous ',merge('yes','no ',tile%is_contiguous)

  end subroutine dump_tileData
   
  subroutine init_matrix_buffer(mba)
    type(mmbArray), intent(in) :: mba
    type(mmbTileIterator) it
    type(mmbArrayTile) tile
    
    ! tile array with single tile and initialize to 0

    call mmb_array_tile(mba,err=err)
    call check_status(err,'Failed to tile mamba array')

    call mmb_tile_iterator_create(mba,it,err)
    call check_status(err, "Failed to get tile iterator")

    call mmb_tile_iterator_first(it,tile,err)
    call check_status(err, "Failed to set tile iterator to first")
    
    !   This is the code directly using a pointer
    block
      real, pointer, dimension(:) :: tp
     
      call mmb_tile_get_pointer(tile,tp)
      tp = -9.0
    end block
    
!  mmbArrayTile* tile = it->tile;
! for (size_t i = tile->lower[0]; i < tile->upper[0] ;++i) {
!    MMB_IDX_1D(tile, i, float) = 0.f;
!  }

  call mmb_tile_iterator_destroy(it,err);
  call check_status(err, "Failed to free tile iterator")

  call mmb_array_untile(mba,err);
  call check_status(err, "Failed to untile mamba array")

  end subroutine init_matrix_buffer

  subroutine check_result(mba,err)
    type(mmbArray), intent(in) :: mba
    integer(mmbErrorKind), intent(out), optional :: err
    integer(mmbErrorKind) :: rerr
    type(mmbTileIterator) it
    type(mmbArrayTile) tile
    integer i

    ! check arra is full of 1s
    rerr=MMB_OK

    ! tile array with single tile

    call mmb_array_tile(mba,err=err)
    call check_status(err,'Failed to tile mamba array')

    call mmb_tile_iterator_create(mba,it,err)
    call check_status(err, "Failed to get tile iterator")

    call mmb_tile_iterator_first(it,tile,err)
    call check_status(err, "Failed to set tile iterator to first")
    
    !   This is the code directly using a pointer
    block
      real, pointer, dimension(:) :: tp
     
      call mmb_tile_get_pointer(tile,tp)

      do i=tile%lower(1),tile%upper(1)
         if (tp(i) .ne. 1.0) then
            print *,'Array data incorrect at index ',i,' value',tp(i)
            rerr = MMB_ERROR
            !exit
           end if
         end do

    end block

    call mmb_tile_iterator_destroy(it,err);
    call check_status(err, "Failed to free tile iterator")

    call mmb_array_untile(mba,err);
    call check_status(err, "Failed to untile mamba array")

    if (present(err)) err=rerr

  end subroutine check_result

  subroutine check_status(err,message)
    integer(mmbErrorKind), intent(in) :: err
    character(len=*), intent(in) :: message

    if (err .ne. MMB_OK) ERROR STOP message

  end subroutine check_status
  
end program tile_duplicate
