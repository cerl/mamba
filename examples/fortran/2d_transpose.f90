! Copyright (C) 2018-2020 Cray UK
!
! Redistribution and use in source and binary forms, with or without
! modification, are permitted provided that the following conditions are
! met:
!
! 1. Redistributions of source code must retain the above copyright
!    notice, this list of conditions and the following disclaimer.
!
! 2. Redistributions in binary form must reproduce the above copyright
!    notice, this list of conditions and the following disclaimer in the
!    documentation and/or other materials provided with the distribution.
!
! 3. Neither the name of the copyright holder nor the names of its
!    contributors may be used to endorse or promote products derived from
!    this software without specific prior written permission.
!
! THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
! IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
! TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
! PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
! HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
! SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
! LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
! DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
! THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
! (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
! OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

program transpose_2d
  use mamba
  implicit none
  logical, parameter :: debug=.true.
  integer            :: array_size_M = 16, &
                        array_size_N = 16, &
                        tile_size_M = 4, &
                        tile_size_N = 4
  integer(mmbErrorKind) err
  type(mmbMemSpace) dram_space
  type(mmbMemSpaceConfig) dram_config
  type(mmbMemInterface) dram_interface
  type(mmbArray) :: mba_a, mba_t
  integer(mmbIndexKind), dimension(2) :: dims,chunks
  type(mmbLayout) layout
  type(mmbTileIterator) it_a, it_t
  type(mmbArrayTile) tile_a, tile_t
  integer(mmbSizeKind), allocatable, dimension(:) :: tiling_dims
  integer(mmbIndexKind) :: ti,tj
  integer ei,ej
  real, dimension(:,:), allocatable, target :: a, t
  logical :: verbose=.true.
  logical success
  real, pointer, dimension(:,:) :: tp_a, tp_t
  
  print *,"Starting example 2d_transpose (Fortran)"

  print '(a,i0,tr1,i0)','matrix size is ',array_size_M,array_size_N
  print '(a,i0,tr1,i0)','tile size is ',tile_size_M,tile_size_N
  allocate( a(array_size_M,array_size_N) )  
  allocate( t(array_size_M,array_size_N) )  

  ! We set the array here before using it in Mamba but
  ! there is equivalent code later to set it within mamba
  call init_matrix_buffers(a,array_size_M,array_size_N)
  
  call mmb_init(err=err)
  call check_status(err,"Failed to initialise Mamba")
  if (debug) print *,'Completed mmb_init()'

! TODO remove this later
  call mmb_logging_set_level(MMB_LOG_DEBUG,err)
  call check_status(err,"Failed to enable mamba debug logging")
  
  dram_config = mmbMemSpaceConfig(mmbSizeConfig(MMB_SIZE_SET,.false.,8000),&
                                  MMB_MEMINTERFACE_CONFIG_DEFAULT)
  ! register memory but don't ask for memory space
  call mmb_register_memory(MMB_DRAM, MMB_EXECUTION_CONTEXT_DEFAULT, &
                           dram_config,err=err)
  call check_status(err,'Failed to register DRAM memory for Mamba')

  ! Get memory space, space config is optional
  call mmb_request_space(MMB_DRAM, MMB_EXECUTION_CONTEXT_DEFAULT, &
                         new_space=dram_space, err=err )
  call check_status(err,'Failed to get memory space')

  ! interface config_opts are optional
  call mmb_request_interface(dram_space, new_interface=dram_interface, err=err)
  call check_status(err,'Failed to get memory interface')

  dims = [array_size_M, array_size_N]
  ! routine is wrongly named, mmb_layout_create_padding_zero better
  call mmb_layout_create_regular_nd(int(storage_size(1.0)/8,mmbSizeKind),&
                                    2_mmbSizeKind,MMB_ROWMAJOR,&
                                    mmb_layout_padding_create_zero(), &
                                    layout,err)
  call check_status(err,'Failed to create regular 1d layout')

  call mmb_array_create_wrapped(a,dims, layout, &
                                 dram_interface, MMB_READ, mba_a,err )
  call check_status(err,'Failed to create mamba array a')
  call mmb_array_create_wrapped(t,dims, layout, &
                                 dram_interface, MMB_READ, mba_t,err )
  call check_status(err,'Failed to create mamba array t')

  call mmb_layout_destroy(layout,err)
  call check_status(err, "Failed to destroy mmb layout")

  ! Initialise mba_a using one tile
  call mmb_array_tile(mba_a, dims, err)
  call check_status(err,'Failed to tile mamba array in chunks')

  call mmb_tile_iterator_create(mba_a,it_a,err)
  call check_status(err, "Failed to get tile iterator for mba_a")

  call mmb_tile_iterator_first(it_a,tile_a,err)
  call check_status(err, "Failed to set tile iterator to first")

    block
      real, pointer, dimension(:,:) :: tp
      
      call mmb_tile_get_pointer(tile_a,tp)
      call dump_tileData(tile_a,'A, all of mba_A')
      if (debug) then
        call print_matrix(tp,'A')
       else
        print *,shape(tp)
       end if
      do ej=tile_a%lower(2),tile_a%upper(2)
        do ei=tile_a%lower(1),tile_a%upper(1)
!          tp(ei,ej)=element(ei,ej)
        end do
      end do
    end block

  call mmb_tile_iterator_destroy(it_a,err);
  call check_status(err, "Failed to free tile iterator")

  ! Request tiling of arrays with chunkx chunky

  chunks = [tile_size_M, tile_size_N]
  
  call mmb_array_tile(mba_a, chunks, err)
  call mmb_array_tile(mba_t, chunks, err)
  call check_status(err,'Failed to tile mamba array in chunks')

  ! Don't need to use the API as we would for C do do this
  allocate( tiling_dims(2) )
  call mmb_tiling_dimensions(mba_a, tiling_dims, err)
  call check_status(err,'Failed to get mamba array tiling dimensions')

  ! Check that orignal array was tiled ok
  ! Tiles are indexed from 1
  tiles_i: do ti = 1,tiling_dims(1)
    tiles_j: do tj = 1,tiling_dims(2)
      call mmb_tile_at_2d(mba_a,ti,tj,tile_a,err) 
      call mmb_tile_get_pointer(tile_a,tp_a)
      call check_status(err,'Failed to get tile for array A')
      print '(a,"(",i0,",",i0,") shape ",*(i0:" "))',&
           'info for tile on A at ',int(ti),int(tj),tile_a%dim
      call dump_tileData(tile_a,' A')
      call mmb_tile_at_2d(mba_t,tj,ti,tile_t,err) 
      call mmb_tile_get_pointer(tile_t,tp_t)
      call check_status(err,'Failed to get tile for array T')
      print '(a,"(",i0,",",i0,") shape",*(i0:" "))',&
            'info for tile on t at',int(tj),int(ti),int(tile_t%dim)
      call dump_tileData(tile_t,' T')

      if (debug) then
         call print_matrix(tp_a(tile_a%lower(1):tile_a%upper(1),tile_a%lower(2):tile_a%upper(2)),'tile of A')
      end if


      ! original tile data a -> t but transpose
      ! So map 1 up
      !  i=1,tile_a%upper(1)-tile_a%lower(1)+1
      !  j=1,tile_a%upper(2)-tile_a%lower(2)+1
      ! Index 1 now i+tile_a%lower(1)-1
      ! Index 2 now j+tile_a%lower(2)-1
      ! So a(i,j) -> t(j,i)
      ! With same loop index
      ! index 1 in t will be j+tile_t%lower(1)-1
      ! index 2 in t will be i+tile_t%lower(2)-1

      ! So map this back to original loop means adding 

      ! offsets from index range of A to index range of T

      do ei=1,tile_a%upper(1)-tile_a%lower(1)+1
        do ej=1,tile_a%upper(2)-tile_a%lower(2)+1
           tp_t(ej+tile_t%lower(1)-1,ei+tile_t%lower(2)-1) = &
                tp_a(ei+tile_a%lower(1)-1,ej+tile_a%lower(2)-1)
        end do
       end do
      
     end do tiles_j
   end do tiles_i

  deallocate( tiling_dims )

  call mmb_array_destroy(mba_a)  
  call mmb_array_destroy(mba_t)  

  if (verbose) call print_matrices(a,t)
  call check_result(a,t,success)

  if (success) then
    print *,'success.'
   else
    print *,'fail.'
   endif

  deallocate(a)
  deallocate(t)
   
  call mmb_finalize(err)
  call check_status(err, "Failed to finalise Mamba")
   
  print *,"Ended example matrix_multiply (Fortran)"

  contains

  real function element(i,j)
    integer i,j
    element = i + j/100.0
  end function element
      
  subroutine init_matrix_buffers(a,M,N)
    real, dimension(:,:),intent(out) :: a
    integer, intent(in) :: M,N
    integer i,j

    do j=1,N
       do i=1,M
         a(i,j) = element(i,j)
        end do
     end do

  end subroutine init_matrix_buffers

  subroutine print_matrices(a,b)
    real,dimension(:,:), intent(in) :: a,b

    call print_matrix(a,"A")
    call print_matrix(b,"T")
    
  end subroutine print_matrices

  subroutine print_matrix(a,label)
    real,dimension(:,:), intent(in) :: a
    character(len=*) :: label
    integer i,j

    print '("Matrix ",a, "(",i0,",",i0,")")',label,size(a,1),size(a,2)
    do i=1,size(a,1)
      print '(tr1,(32f6.2))',(a(i,j),j=1,size(a,2))
     end do
 
  end subroutine print_matrix
    
  subroutine check_result(a,t,success)
    real,dimension(:,:), intent(in) :: a,t
    logical, intent(out) :: success
    integer i,j,m,n
    m=size(a,1)
    n=size(a,2)

    success = .true. 
    do i=1,m
      do j=1,n
        if (a(i,j).ne.element(i,j) .or. a(i,j).ne.t(j,i) ) then
          print '("Check failed at (",i0,",",i0,"): ",f6.2," not ",f6.2," and",f6.2)',i,j,element(i,j),a(i,j),t(j,i)
          success=.false.
         end if
       end do
    end do
    
  end subroutine check_result
  
  ! helper
  subroutine dump_tileData(tile,label)
    type(mmbArrayTile),intent(in) :: tile
    character(len=*), intent(in) :: label

    print *,'tile_info for tile',label
    print '(tr1,a,tr1,i0)','         rank',tile%rank

    print '(tr1,a,tr1,*(i0:tr1))','        lower',tile%lower
    print '(tr1,a,tr1,*(i0:tr1))','        upper',tile%upper
    print '(tr1,a,tr1,*(i0:tr1))','       alower',tile%alower
    print '(tr1,a,tr1,*(i0:tr1))','       aupper',tile%aupper
    print '(tr1,a,tr1,*(i0:tr1))','          dim',tile%dim
    print '(tr1,a,tr1,*(i0:tr1))','      abs_dim',tile%abs_dim
    print *,'is_contiguous ',merge('yes','no ',tile%is_contiguous)

  end subroutine dump_tileData

  subroutine check_status(err,message)
    integer(mmbErrorKind), intent(in) :: err
    character(len=*), intent(in) :: message

    if (err .ne. MMB_OK) ERROR STOP message

  end subroutine check_status
  
end program transpose_2d

