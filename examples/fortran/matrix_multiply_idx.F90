! Copyright (C) 2018-2020 Cray UK
!
! Redistribution and use in source and binary forms, with or without
! modification, are permitted provided that the following conditions are
! met:
!
! 1. Redistributions of source code must retain the above copyright
!    notice, this list of conditions and the following disclaimer.
!
! 2. Redistributions in binary form must reproduce the above copyright
!    notice, this list of conditions and the following disclaimer in the
!    documentation and/or other materials provided with the distribution.
!
! 3. Neither the name of the copyright holder nor the names of its
!    contributors may be used to endorse or promote products derived from
!    this software without specific prior written permission.
!
! THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
! IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
! TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
! PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
! HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
! SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
! LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
! DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
! THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
! (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
! OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

program matrix_multiply
  use mamba
  implicit none
#include "mambaf.h"
  logical, parameter :: debug=.true.
  integer            :: array_size_M = 16, &
                        array_size_N = 16, &
                        tile_size_M = 4, &
                        tile_size_N = 4
  integer(mmbErrorKind) err
  type(mmbMemSpace) dram_space
  type(mmbMemSpaceConfig) dram_config
  type(mmbMemInterface) dram_interface
  type(mmbArray) :: mba_a, mba_b, mba_c
  integer(mmbIndexKind), dimension(2) :: dims,chunks
  type(mmbLayout) layout
  type(mmbTileIterator) it_a, it_b, it_c
  type(mmbArrayTile) tile_a, tile_b, tile_c
  integer(mmbSizeKind), allocatable, dimension(:) :: tiling_dims
  integer(mmbIndexKind) :: ti,tj,tk
  integer  ei,ej,ek
  real, dimension(:,:), allocatable, target :: a, b, c
  character arg*20
  integer apos
  logical :: b_is_identity = .false., &
             verbose=.false.
  logical success
  ! We use 1d pointers here to support index access
  real, pointer, dimension(:) :: tp_a, tp_b, tp_c
  
  print *,"Starting example matrix_multiply (Fortran)"

  ! ./matrix_muliply [I] [M N]
  
  if (command_argument_count()>0) then
     call get_command_argument(1,arg)
     print *,arg
     if (arg=='I') then
        b_is_identity = .true.
        apos = 1
       else
        apos = 0
     end if
     if (command_argument_count()>1) then
        call get_command_argument(apos+1,arg)
        print *,arg
        read(arg,fmt=*)array_size_M
        array_size_N = array_size_M
        call get_command_argument(apos+2,arg)
        read(arg,fmt=*)tile_size_M
        tile_size_N = tile_size_M
      end if
       
   end if

  print *,'matrix size is ',array_size_M,array_size_N
  print *,'tile size is ',tile_size_M,tile_size_N
  if (b_is_identity) print *,'Matrix B is I'
  allocate( a(array_size_M,array_size_N) )  
  allocate( b(array_size_M,array_size_N) )  
  allocate( c(array_size_M,array_size_N) )  

  call init_matrix_buffers(a,b,array_size_M,array_size_N,b_is_identity)
  
  call mmb_init(err=err)
  call check_status(err,"Failed to initialise Mamba")
  if (debug) print *,'Completed mmb_init()'

! TODO remove this later
  call mmb_logging_set_level(MMB_LOG_DEBUG,err)
  call check_status(err,"Failed to enable mamba debug logging")
  
  dram_config = mmbMemSpaceConfig(mmbSizeConfig(MMB_SIZE_SET,.false.,8000),&
                                  MMB_MEMINTERFACE_CONFIG_DEFAULT)
  ! register memory but don't ask for memory space
  call mmb_register_memory(MMB_DRAM, MMB_EXECUTION_CONTEXT_DEFAULT, &
                           dram_config,err=err)
  call check_status(err,'Failed to register DRAM memory for Mamba')

  ! Get memory space, space config is optional
  call mmb_request_space(MMB_DRAM, MMB_EXECUTION_CONTEXT_DEFAULT, &
                         new_space=dram_space, err=err )
  call check_status(err,'Failed to get memory space')

  ! interface config_opts are optional
  call mmb_request_interface(dram_space, new_interface=dram_interface, err=err)
  call check_status(err,'Failed to get memory interface')

  dims = [array_size_M, array_size_N]
  
  ! routine is wrongly named, mmb_layout_create_padding_zero better
  call mmb_layout_create_regular_nd(int(storage_size(1.0)/8,mmbSizeKind),&
                                    2_mmbSizeKind,MMB_ROWMAJOR,&
                                    mmb_layout_padding_create_zero(), &
                                    layout,err)
  call check_status(err,'Failed to create regular 1d layout')

  call mmb_array_create_wrapped(a,dims, layout, &
                                 dram_interface, MMB_READ, mba_a,err )
  call check_status(err,'Failed to create mamba array a')
  call mmb_array_create_wrapped(b,dims, layout, &
                                 dram_interface, MMB_READ, mba_b,err )
  call check_status(err,'Failed to create mamba array b')
  call mmb_array_create_wrapped(c,dims, layout, &
                                 dram_interface, MMB_READ_WRITE, mba_c,err )
  call check_status(err,'Failed to create mamba array c')

  call mmb_layout_destroy(layout,err)
  call check_status(err, "Failed to destroy mmb layout")

  ! Initialise mba_c
  call mmb_array_tile(mba_c, dims, err)
  call check_status(err,'Failed to tile mamba array in chunks')

  call mmb_tile_iterator_create(mba_c,it_c,err)
  call check_status(err, "Failed to get tile iterator for mba_c")

  call mmb_tile_iterator_first(it_c,tile_c,err)
  call check_status(err, "Failed to set tile iterator to first")

    block
      real, pointer, dimension(:,:) :: tp
     
      call mmb_tile_get_pointer(tile_c,tp)
      call dump_tileData(tile_c,'tile C all of mba_C')
      print *,shape(tp)
      tp = 0.0
    end block

  call mmb_tile_iterator_destroy(it_c,err);
  call check_status(err, "Failed to free tile iterator")

  ! Request tiling of arrays with chunkx chunky

  chunks = [tile_size_M, tile_size_N]
  
  call mmb_array_tile(mba_a, chunks, err)
  call mmb_array_tile(mba_b, chunks, err)
  call mmb_array_tile(mba_c, chunks, err)
  call check_status(err,'Failed to tile mamba array in chunks')

  ! Don't need to use the API as we would for C do do this
  allocate( tiling_dims(2) )
  call mmb_tiling_dimensions(mba_a, tiling_dims, err)
  call check_status(err,'Failed to get mamba array tiling dimensions')

  ! Tiles are indexed from 1
  tiles_i: do ti = 1,tiling_dims(1)
    tiles_j: do tj = 1,tiling_dims(2)
      call mmb_tile_at_2d(mba_c,ti,tj,tile_c,err) 
      call mmb_tile_get_pointer(tile_c,tp_c)
      call check_status(err,'Failed to get tile for array C')
      print '(a,"(",i0,",",i0,") shape ",*(i0:tr1))','info for tile on C at ',&
           ti,tj,shape(tp_c)
      call dump_tileData(tile_c,' C')
      do tk = 1,tiling_dims(2)
        ! Set tile indices for a & b arrays
        call mmb_tile_at_2d(mba_a,ti,tk,tile_a,err) 
        call mmb_tile_get_pointer(tile_a,tp_a)
        call check_status(err,'Failed to get tile for array a')
        !print *,'info for tile on A at',ti,tj
        !call dump_tileData(tile_a,'tile A')
        call mmb_tile_at_2d(mba_b,tk,tj,tile_b,err) 
        call mmb_tile_get_pointer(tile_b,tp_b)
        call check_status(err,'Failed to get tile for array a')
        !print *,'info for tile on B at',ti,tj
        !call dump_tileData(tile_b,' B')
        do ei = tile_c%lower(1),tile_c%upper(1)
          do ej = tile_c%lower(2),tile_c%upper(2)
             do ek = tile_a%lower(2),tile_a%upper(2)
                 !tp_c(ei,ej) = tp_c(ei,ej) + tp_a(ei,ek)*tp_b(ek,ej)
                 MMB_IDX_2D(tile_c,tp_c,ei,ej) = &
                   MMB_IDX_2D(tile_c,tp_c,ei,ej) + &
                   MMB_IDX_2D(tile_a,tp_a,ei,ek) * MMB_IDX_2D(tile_b,tp_b,ek,ej)
             end do
         end do
       end do

       ! tp_c(9,9)=99.0 for testing
      end do
      
     end do tiles_j
   end do tiles_i

  deallocate( tiling_dims )

  call mmb_array_destroy(mba_a)  
  call mmb_array_destroy(mba_b)  
  call mmb_array_destroy(mba_c)  

  if (verbose) call print_matrices(a,b,c)
  if (b_is_identity) then
     call check_result_identity(a,c,success)
   else
     call check_result(a,b,c,success)
   end if

   if (success) then
     print *,'success.'
    else
     print *,'fail.'
    endif

  deallocate(a)
  deallocate(b)
  deallocate(c)
   
  call mmb_finalize(err)
  call check_status(err, "Failed to finalise Mamba")
   
   
  print *,"Ended example matrix_multiply (Fortran)"

  contains

  subroutine init_matrix_buffers(a,b,M,N,b_is_identity)
    real, dimension(:,:),intent(out) :: a,b
    integer, intent(in) :: M,N
    logical, intent(in) :: b_is_identity
    integer i,j

    do j=1,N
       do i=1,M
          a(i,j)=i+j/1000.0
          if (b_is_identity) then
             b(i,j) = merge(1.0,0.0,i==j)
           else
             b(i,j) = -a(i,j)
           end if
        end do
     end do

  end subroutine init_matrix_buffers

  subroutine print_matrices(a,b,c)
    real,dimension(:,:), intent(in) :: a,b,c
    integer i,j

    print '("Matrix ",a, "(",i0,",",i0,")")','A',size(a,1),size(a,2)
    do j=1,size(a,2)
      print '(tr1,(32f9.3))',(a(i,j),i=1,size(a,1))
     end do
    print '("Matrix ",a, "(",i0,",",i0,")")','B',size(b,1),size(b,2)
    do j=1,size(b,2)
      print '(tr1,(32f9.3))',(b(i,j),i=1,size(b,1))
     end do
    print '("Matrix ",a, "(",i0,",",i0,")")','C',size(c,1),size(c,2)
    do j=1,size(c,2)
      print '(tr1,(32f9.3))',(c(i,j),i=1,size(c,1))
     end do
 
  end subroutine print_matrices
    
  subroutine check_result_identity(a,c,success)
    real,dimension(:,:), intent(in) :: a,c
    logical, intent(out) :: success
    integer i,j,m,n
    m=size(a,1)
    n=size(a,2)

    success = .true.
    do j=1,n
       do i=1,m
          if (a(i,j).ne.c(i,j)) then
             print '("Comparison failed at (",i0,",",i0,"): ",f10.4," not ",f10.4)',i,j,a(i,j),c(i,j)
             success=.false.
          end if
       end do
    end do
    
  end subroutine check_result_identity

  subroutine check_result(a,b,c,success)
    real,dimension(:,:), intent(in) :: a,b,c
    real :: check_c(size(a,1),size(a,2))
    real, parameter:: tol=1e-5
    logical, intent(out) :: success
    integer i,j,m,n
    m=size(a,1)
    n=size(a,2)

    check_c=matmul(a,b)
    success = .true.

    do j=1,n
       do i=1,m
        if (abs((c(i,j)-check_c(i,j))/c(i,j))>tol) then
          print '("Comparison failed at (",i0,",",i0,"): ",f10.4," not ",f10.4)',i,j,c(i,j),check_c(i,j)
          success=.false.
         end if
       end do
    end do
    
  end subroutine check_result

  ! helper
  subroutine dump_tileData(tile,label)
    type(mmbArrayTile),intent(in) :: tile
    character(len=*), intent(in) :: label

    print *,'tile_info for tile',label
    print '(tr1,a,tr1,i0)','         rank',tile%rank

    print '(tr1,a,tr1,*(i0:tr1))','        lower',tile%lower
    print '(tr1,a,tr1,*(i0:tr1))','        upper',tile%upper
    print '(tr1,a,tr1,*(i0:tr1))','       alower',tile%alower
    print '(tr1,a,tr1,*(i0:tr1))','       aupper',tile%aupper
    print '(tr1,a,tr1,*(i0:tr1))','          dim',tile%dim
    print '(tr1,a,tr1,*(i0:tr1))','      abs_dim',tile%abs_dim
    print *,'is_contiguous ',merge('yes','no ',tile%is_contiguous)

  end subroutine dump_tileData

  subroutine check_status(err,message)
    integer(mmbErrorKind), intent(in) :: err
    character(len=*), intent(in) :: message

    if (err .ne. MMB_OK) ERROR STOP message

  end subroutine check_status
  
end program matrix_multiply
