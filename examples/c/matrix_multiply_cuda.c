/*
 * Copyright (C) 2018-2021 Cray UK
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include <getopt.h>
#include <string.h>
#include <ctype.h>

#include <mamba.h>
#include <mmb_tile.h>
#include <mmb_tile_iterator.h>
#include <mmb_memory.h>
#include <mmb_logging.h>

#include "matrix_multiply_cuda.h"

mmbError init_matrix_buffers(mmbArray *mba_a, mmbArray *mba_b, int b_is_identity);
int check_result(mmbArray *mba_a, mmbArray *mba_b, mmbArray *mba_c, const size_t M, const size_t N);
int check_result_identity(mmbArray *mba_a, mmbArray *mba_c, const size_t M, const size_t N);

void print_matrices(const mmbArray* mba_a, const mmbArray* mba_b, const mmbArray* mba_c,
                    const size_t M, const size_t N);

void usage()
{
  printf("Usage (all args optional): ./matrix_multiply "
         "-v (for verbose mode) "
         "-t N (for tile size NxN) "
         "-m N (for matrix size NxN) "
         "-i (use identity for matrix B)\n");
}

mmbMemInterface *host_interface, *device_interface;

int main(int argc, char *const argv[])
{
  mmbError err;
  /* 2D MxN tiled matrix multiplication: C = A * B */
  /* Only M == N supported so far... */
  size_t array_size_M = 16;
  size_t array_size_N = 16;
  size_t tile_size_M = 4;
  size_t tile_size_N = 4;
  int verbose = 0;
  int b_is_identity = 0;

  /* Parse command line arguments, detailed in README */
  int c;
  optarg = NULL;
  while ((c = getopt(argc, argv, "vm:t:hi")) != -1) {
    switch (c) {
    case 'v':
      verbose = 1;
      break;
    case 'm':
      if (1 != sscanf(optarg, "%zu", &array_size_M)) {
        MMB_ERR("Bad value \"%s\" for matrix size.\n", optarg);
        return EXIT_FAILURE;
      }
      array_size_N = array_size_M;
      break;
    case 't':
      if (1 != sscanf(optarg, "%zu", &tile_size_M)) {
        MMB_ERR("Bad value \"%s\" for matrix size.\n", optarg);
        return EXIT_FAILURE;
      }
      tile_size_N = tile_size_M;
      break;
    case 'h':
      usage();
      return EXIT_SUCCESS;
      break;
    case 'i':
      b_is_identity = 1;
      break;
    case '?':
      if (optopt == 'c')
        MMB_ERR("Option -%c requires an argument.\n", optopt);
      else if (isprint(optopt))
        MMB_ERR("Unknown option `-%c'.\n", optopt);
      else
        MMB_ERR("Unknown option character `\\x%x'.\n", optopt);
      usage();
      return EXIT_FAILURE;
    default:
      abort();
    }
  }

  /* Initialize Mamba and enable debug logging */
  err = mmb_init(MMB_INIT_DEFAULT);
  if(err != MMB_OK) {
    MMB_ERR("Failed to initialise mamba\n");
    return EXIT_FAILURE;
  }
  
  err = mmb_logging_set_debug_level(MMB_LOG_DEBUG);
  if(err != MMB_OK) {
    MMB_ERR("Failed to set debug option for mamba\n");
    return EXIT_FAILURE;
  }

  int discovery_enabled = 0;
  err = mmb_discovery_is_enabled(&discovery_enabled);
  if (MMB_OK != err) {
    MMB_ERR("Failed to query discovery status. (%d)\n", err);
    return EXIT_FAILURE;
  }

  /* Space configuration handle is similar for all spaces as it does not
   * depends on the execution context nor on the layer. */
  const mmbMemSpaceConfig space_config = {
    .size_opts = { .action = MMB_SIZE_SET, .mem_size = 8000 },
    .interface_opts = MMB_MEMINTERFACE_CONFIG_DEFAULT,
  };

  /* Initialise allocator for host memory */
  mmbMemSpace *host_space;
  if (!discovery_enabled) {
    err = mmb_register_memory(MMB_DRAM, MMB_CPU, &space_config, &host_space);
    if (MMB_OK != err) {
      MMB_ERR("Failed to register dram memory for mamba\n");
      return EXIT_FAILURE;
    }
  } else {
    err = mmb_request_space(MMB_DRAM, MMB_CPU, NULL, &host_space);
    if (MMB_OK != err) {
      MMB_ERR("Unable to request DRAM space. (%d)\n", err);
      return EXIT_FAILURE;
    }
  }

  mmbProvider host_provider;
#if HAVE_SICM
  host_provider = MMB_SICM;
#else
  host_provider = MMB_PROVIDER_DEFAULT;
#endif /* HAVE_SICM */
  const mmbMemInterfaceConfig host_conf =
    { .provider = host_provider, .strategy = MMB_POOLED, .name = "host pool" };
  err = mmb_create_interface(host_space, &host_conf, &host_interface);
  if (MMB_OK != err) {
    MMB_ERR("CPU interface request failed. (%d)\n", err);
    return EXIT_FAILURE;
  }

  /* Initialise allocator for device memory */
  mmbMemSpace *device_space;
  if (!discovery_enabled) {
    err = mmb_register_memory(MMB_GDRAM, MMB_GPU_CUDA, &space_config, &device_space);
    if (MMB_OK != err) {
      MMB_ERR("Failed to register GDRAM memory from CUDA for mamba\n");
      return EXIT_FAILURE;
    }
  } else {
    err = mmb_request_space(MMB_GDRAM, MMB_GPU_CUDA, NULL, &device_space);
    if (MMB_OK != err) {
      MMB_ERR("Unable to request GDRAM space. (%d)\n", err);
      return EXIT_FAILURE;
    }
  }
  const mmbMemInterfaceConfig device_conf =
    { .provider = MMB_PROVIDER_DEFAULT, .strategy = MMB_POOLED, .name = "gpu pool" };
  err = mmb_create_interface(device_space, &device_conf, &device_interface);
  if (MMB_OK != err) {
    MMB_ERR("GPU interface request failed. (%d)\n", err);
    return EXIT_FAILURE;
  }

  mmbLayout *layout;
  size_t arrdims[2] = {array_size_M, array_size_N};
  mmbDimensions dims = {2, arrdims};
  mmb_layout_create_regular_nd(sizeof(float), 2, MMB_COLMAJOR, MMB_PADDING_NONE, &layout);

  // Allocate cuda buffers for arrays A, B, and C
  mmbArray *mba_a_cu, *mba_b_cu, *mba_c_cu;
  err = mmb_array_create(&dims, layout, device_interface, MMB_READ, &mba_a_cu);
  if (MMB_OK != err) {
    MMB_ERR("Cannot allocate mba_a_cu (%d).\n", err);
    return EXIT_FAILURE;
  }
  err = mmb_array_create(&dims, layout, device_interface, MMB_READ, &mba_b_cu);
  if (MMB_OK != err) {
    MMB_ERR("Cannot allocate mba_b_cu (%d).\n", err);
    return EXIT_FAILURE;
  }
  err = mmb_array_create(&dims, layout, device_interface, MMB_READ_WRITE, &mba_c_cu);
  if (MMB_OK != err) {
    MMB_ERR("Cannot allocate mba_c_cu (%d).\n", err);
    return EXIT_FAILURE;
  }

  // Create mmb arrays for each matrix
  mmbArray *mba_a, *mba_b, *mba_c;
  err = mmb_array_create(&dims, layout, host_interface, MMB_READ, &mba_a);
  if (MMB_OK != err) {
    MMB_ERR("Cannot allocate mba_a (%d).\n", err);
    return EXIT_FAILURE;
  }
  err = mmb_array_create(&dims, layout, host_interface, MMB_READ, &mba_b);
  if (MMB_OK != err) {
    MMB_ERR("Cannot allocate mba_b (%d).\n", err);
    return EXIT_FAILURE;
  }
  err = mmb_array_create(&dims, layout, host_interface, MMB_READ_WRITE, &mba_c);
  if (MMB_OK != err) {
    MMB_ERR("Cannot allocate mba_c (%d).\n", err);
    return EXIT_FAILURE;
  }

  mmb_layout_destroy(layout);

  init_matrix_buffers(mba_a, mba_b, b_is_identity);

  // Zero initialise mba_c
  // Create a tiling that covers whole array with single tile
  mmb_array_tile(mba_c, &dims);
  // Ask for the next (only) tile
  mmbTileIterator* it_c;
  mmb_tile_iterator_create(mba_c, &it_c);
  mmbArrayTile* tile;
  mmb_tile_iterator_first(it_c, &tile);
  // Initialize to 0
  for (size_t i = tile->lower[0]; tile->upper[0] > i; ++i) {
    for (size_t j = tile->lower[1]; tile->upper[1] > j; ++j) {
      MMB_IDX_2D(tile, i, j, float) = 0.f;
    }
  }

  // Free the iterator we were using
  mmb_tile_iterator_destroy(it_c);

  /* Initialize all CUDA matrices from DRAM buffers */
  err = mmb_copy(mba_a_cu->allocation, mba_a->allocation);
  if (MMB_OK != err) {
    MMB_ERR("Cannot copy data from buffer_a to mba_a_cu (%d).\n", err);
    return EXIT_FAILURE;
  }
  err = mmb_copy(mba_b_cu->allocation, mba_b->allocation);
  if (MMB_OK != err) {
    MMB_ERR("Cannot copy data from buffer_b to mba_b_cu (%d).\n", err);
    return EXIT_FAILURE;
  }
  err = mmb_copy(mba_c_cu->allocation, mba_c->allocation);
  if (MMB_OK != err) {
    MMB_ERR("Cannot copy data from buffer_c to mba_c_cu (%d).\n", err);
    return EXIT_FAILURE;
  }

  // Request tiling of all arrays with chunkx chunky
  size_t chunkdims[2] = {tile_size_M,tile_size_N};
  mmbDimensions chunks = {2, chunkdims};
  mmb_array_tile(mba_a_cu, &chunks);
  mmb_array_tile(mba_b_cu, &chunks);
  mmb_array_tile(mba_c_cu, &chunks);

  /*
     Loop over tiles using iterators
     TODO: Need to define the concept of iterator schedules to do this
     ...
  */

  /* Loop over tiles using standard indexing */
  mmbArrayTile *tile_a, *tile_b, *tile_c;
  mmbIndex *idx_a, *idx_b, *idx_c;
  mmb_index_create(2, &idx_a);
  mmb_index_create(2, &idx_b);
  mmb_index_create(2, &idx_c);
  mmbDimensions *tiling_dims;
  err = mmb_tiling_dimensions(mba_a_cu, &tiling_dims);
  if (MMB_OK != err) {
    MMB_ERR("Failed allocation of the dimension. (%d)\n", err);
    return EXIT_FAILURE;
  }

  /* We present all the relevant information as a blob to be copied to an
   * equivalent structure on the GPU memory. There is a bit a waste in
   * space, but it is in order to avoid coping just here and there some
   * attributes. */
  const size_t ntiles =  tiling_dims->d[0]      /* 3 tiles per each ti  */
                          * tiling_dims->d[1]   /*        ... times tj  */
                          * tiling_dims->d[1];  /*        ... times tk. */
  mmbAllocation *tiles_alloc;
  err = mmb_allocate(sizeof(serializedMmbArrayTile2D[ntiles][3]),
                     host_interface, &tiles_alloc);
  if (MMB_OK != err) {
    MMB_ERR("Unable to allocate serialized tile structure (%d).\n", err);
    return EXIT_FAILURE;
  }
  serializedMmbArrayTile2D *tiles =
    MMBALLOC2TBUF(tiles_alloc, serializedMmbArrayTile2D *);

  /* Tiling loops */
  for (size_t ti = 0; tiling_dims->d[0] > ti; ++ti) {
    for (size_t tj = 0; tiling_dims->d[1] > tj; ++tj) {
      /* Set tile indices for c array */
      mmb_index_set(idx_c,ti,tj);
      mmb_tile_at(mba_c_cu, idx_c, &tile_c);
      if (verbose) {
        MMB_DEBUG("Iterating over: C tile (%zu, %zu), Dims (%zu, %zu)\n",
              ti, tj, tile_c->dim[0], tile_c->dim[1]);
      }
      for (size_t tk = 0; tiling_dims->d[1] > tk; ++tk) {
        /* Set tile indices for a & b arrays */
        mmb_index_set(idx_a,ti,tk);
        mmb_index_set(idx_b,tk,tj);
        /* Extract tile for each array */
        mmb_tile_at(mba_a_cu, idx_a, &tile_a);
        mmb_tile_at(mba_b_cu, idx_b, &tile_b);

        if (verbose) {
          MMB_DEBUG("\tA tile (%zu, %zu), Dims (%zu, %zu)\n",
                ti, tk, tile_a->dim[0], tile_a->dim[1]);
          MMB_DEBUG("\tB tile (%zu, %zu), Dims (%zu, %zu)\n",
                tk, tj, tile_b->dim[0], tile_b->dim[1]);
        }

        /* Serialise and initialise the fields used for the tiled accesses */
        /* We present all the relevant information as a blob to be copied to an
         * equivalent structure on the GPU memory. There is a bit a waste in
         * space, but it is in order to avoid coping just here and there some
         * attributes. */
        serializedMmbArrayTile2D crt_tiles[3] = {
          [0] = { .ptr = MMBALLOC2TBUF(tile_a->allocation, float *),
            .lower    = { tile_a->lower[0],   tile_a->lower[1] },
            .upper    = { tile_a->upper[0],   tile_a->upper[1] },
            .dim      = { tile_a->dim[0],     tile_a->dim[1] },
            .abs_dim  = { tile_a->abs_dim[0], tile_a->abs_dim[1] }
          },
          [1] = { .ptr = MMBALLOC2TBUF(tile_b->allocation, float *),
            .lower    = { tile_b->lower[0],   tile_b->lower[1] },
            .upper    = { tile_b->upper[0],   tile_b->upper[1] },
            .dim      = { tile_b->dim[0],     tile_b->dim[1] },
            .abs_dim  = { tile_b->abs_dim[0], tile_b->abs_dim[1] }
          },
          [2] = { .ptr = MMBALLOC2TBUF(tile_c->allocation, float *),
            .lower    = { tile_c->lower[0],   tile_c->lower[1] },
            .upper    = { tile_c->upper[0],   tile_c->upper[1] },
            .dim      = { tile_c->dim[0],     tile_c->dim[1] },
            .abs_dim  = { tile_c->abs_dim[0], tile_c->abs_dim[1] }
          }
        };
        const size_t tid = (ti*tiling_dims->d[1] + tj)*tiling_dims->d[1] + tk;
        memcpy(&tiles[3 * tid], crt_tiles, sizeof(serializedMmbArrayTile2D[3]));
      }
    }
  }

  /* Call CUDA kernel */
  const size_t n_workers = tiling_dims->d[0] * tiling_dims->d[1];
  const size_t n_set_per_worker = tiling_dims->d[1];
  err = sgemm(tiles_alloc, n_workers, n_set_per_worker, verbose);
  if (MMB_OK != err) {
    MMB_ERR("Error while applying the tiling operation (%d).\n", err);
    return EXIT_FAILURE;
  }
  mmb_free(tiles_alloc);

  /* Cleanup mamba */
  mmb_dimensions_destroy(tiling_dims);
  mmb_index_destroy(idx_a);
  mmb_index_destroy(idx_b);
  mmb_index_destroy(idx_c);

  err = mmb_copy(mba_c->allocation, mba_c_cu->allocation);
  if (MMB_OK != err) {
    MMB_ERR("Cannot copy data from mba_c_cu to mba_c (%d).\n", err);
    return EXIT_FAILURE;
  }

  mmb_array_destroy(mba_a_cu);
  mmb_array_destroy(mba_b_cu);
  mmb_array_destroy(mba_c_cu);

  /* Check results */
  if (verbose) {
    print_matrices(mba_a, mba_b, mba_c, array_size_M, array_size_N);
  }

  int success;
  if (b_is_identity) {
    success = check_result_identity(mba_a, mba_c, array_size_M, array_size_N);
  } else {
    success = check_result(mba_a, mba_b, mba_c, array_size_M, array_size_N);
  }
  printf("Checked matrix multiply with buffer sizes (%zu,%zu) "
         "and tile sizes (%zu,%zu): %s",
         array_size_M, array_size_N, tile_size_M, tile_size_N,
         success ? "success.\n" : "fail.\n");

  mmb_array_destroy(mba_a);
  mmb_array_destroy(mba_b);
  mmb_array_destroy(mba_c);

  mmb_finalize();

  return success ? EXIT_SUCCESS : EXIT_FAILURE;
}


mmbError init_matrix_buffers(mmbArray *mba_a, mmbArray *mba_b,
                             int b_is_identity)
{
  srand((unsigned int)time(NULL));

  mmbError stat = mmb_array_tile(mba_a, &mba_a->dims);
  if(stat != MMB_OK) {
    MMB_ERR("Failed to tile mamba array (chunked tiles)\n");
    goto BAILOUT;
  }
  stat = mmb_array_tile(mba_b, &mba_b->dims);
  if(stat != MMB_OK) {
    MMB_ERR("Failed to tile mamba array (chunked tiles)\n");
    goto BAILOUT;
  }

  // Loop over tiles
  mmbTileIterator *it0, *it1;
  stat = mmb_tile_iterator_create(mba_a, &it0);
  if(stat != MMB_OK) {
    MMB_ERR("Failed to get tile iterator\n");
    goto BAILOUT;
  }
  stat = mmb_tile_iterator_create(mba_b, &it1);
  if(stat != MMB_OK) {
    MMB_ERR("Failed to get tile iterator\n");
    goto BAILOUT;
  }

  mmbArrayTile* tile0;
  stat = mmb_tile_iterator_first(it0, &tile0);
  if(stat != MMB_OK) {
    MMB_ERR("Failed to move tile iterator to first\n");
    goto BAILOUT;
  }
  mmbArrayTile* tile1;
  stat = mmb_tile_iterator_first(it1, &tile1);
  if(stat != MMB_OK) {
    MMB_ERR("Failed to move tile iterator to first\n");
    goto BAILOUT;
  }


  for (size_t i = tile0->lower[0]; i < tile0->upper[0] ;++i) {
    for (size_t j = tile0->lower[1]; j < tile0->upper[1]; ++j) {
      MMB_IDX_2D(tile0, i, j, float) = rand()%10;
      if (b_is_identity)
        MMB_IDX_2D(tile1, i, j, float) = (i==j) ? 1.f : 0.f;
      else
        MMB_IDX_2D(tile1, i, j, float) = (float)(rand()%10);
    }
  }

  stat = mmb_tile_iterator_destroy(it0);
  if(stat != MMB_OK) {
    MMB_ERR("Failed to free tile iterator\n");
    goto BAILOUT;
  }
  stat = mmb_tile_iterator_destroy(it1);
  if(stat != MMB_OK) {
    MMB_ERR("Failed to free tile iterator\n");
    goto BAILOUT;
  }
  // Remove tiling
  stat = mmb_array_untile(mba_a);
  if(stat != MMB_OK) {
    MMB_ERR("Failed to untile mamba array\n");
    goto BAILOUT;
  }

  stat = mmb_array_untile(mba_b);
  if(stat != MMB_OK) {
    MMB_ERR("Failed to untile mamba array\n");
    goto BAILOUT;
  }

BAILOUT:
  return stat;
}


int check_result(mmbArray *mba_a, mmbArray *mba_b, mmbArray *mba_c, const size_t M, const size_t N)
{
  int success = 1;
  float check_tmp;
  const float * restrict buf_a = MMBALLOC2TBUF(mba_a->allocation, const float *);
  const float * restrict buf_b = MMBALLOC2TBUF(mba_b->allocation, const float *);
  const float * restrict buf_c = MMBALLOC2TBUF(mba_c->allocation, const float *);

  for (size_t i = 0; M > i && success; ++i) {
    for (size_t j = 0; N > j && success; ++j) {
      check_tmp = 0;
      for (size_t k = 0; N > k; ++k) {
        check_tmp += buf_a[i * N + k] * buf_b[k * N + j];
      }
      if (buf_c[i*N+j] != check_tmp) {
        MMB_ERR("First error:\n\tcheck_buffer[%zu,%zu]: %f\n\tbuf_c[%zu,%zu]: %f\n",
            i, j, check_tmp, i, j, buf_c[i*N + j]);
        success = 0;
      }
    }
  }

  return success;
}

int check_result_identity(mmbArray *mba_a, mmbArray *mba_c, const size_t M, const size_t N)
{
  int success = 1;
  const float * restrict buf_a = MMBALLOC2TBUF(mba_a->allocation, const float *);
  const float * restrict buf_c = MMBALLOC2TBUF(mba_c->allocation, const float *);

  for (size_t i = 0; M > i && success; ++i) {
    for (size_t j = 0; N > j && success; ++j) {
      if (buf_c[i*N + j] != buf_a[i*N + j]) {
        MMB_ERR("First error:\n\buf_a[%zu,%zu]: %f\n\tbuf_c[%zu,%zu]: %f\n",
            i, j, buf_a[i*N + j], i, j, buf_c[i*N + j]);
        success = 0;
      }
    }
  }

  return success;
}

void print_matrices(const mmbArray* mba_a, const mmbArray* mba_b, const mmbArray* mba_c,
                    const size_t M, const size_t N)
{
  const float * restrict buf_a = MMBALLOC2TBUF(mba_a->allocation, const float *);
  const float * restrict buf_b = MMBALLOC2TBUF(mba_b->allocation, const float *);
  const float * restrict buf_c = MMBALLOC2TBUF(mba_c->allocation, const float *);

  printf("\nMatrix A: \n");
  for (size_t i = 0; M > i ; ++i) {
    for (size_t j = 0; N > j ; ++j) {
      printf("%2u ", (unsigned)buf_a[i*N+j]);
    }
    printf("\n");
  }

  printf("\nMatrix B: \n");
  for (size_t i = 0; M > i; ++i) {
    for (size_t j = 0; N > j; ++j) {
      printf("%2u ", (unsigned)buf_b[i*N+j]);
    }
    printf("\n");
  }

  printf("\nMatrix C: \n");
  for (size_t i = 0; M > i; ++i) {
    for (size_t j = 0; N > j; ++j) {
      printf("%4u ", (unsigned)buf_c[i*N+j]);
    }
    printf("\n");
  }
  printf("\n");
}

