/*
 * Copyright (C) 2018-2020 Cray UK
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <cstdlib>
#include <cstdio>
#include <mamba.h>
#include <mmb_logging.h>
#include "hip/hip_runtime.h"

__global__
void write_to_tile_hip_ker(mmbArrayTile* tile)
{
  const std::size_t tid = 0 + (hipBlockDim_x * hipBlockIdx_x
                        +  hipThreadIdx_x);

  if (tid >= tile->upper[0]) 
    return;

  MMB_IDX_1D(tile, tid, float) = 1;
}


extern "C" mmbError write_to_tile_hip(mmbArrayTile *tile, bool verbose)
{
  mmbError err = MMB_OK;
  /* Compute dimensions of the block grid and number of workers. */
  // const std::size_t CUDA_MAX_DIM = 0x7fff;
  const std::size_t th_per_block = 256;
  std::size_t block_grid_width = tile->dim[0] / th_per_block;
  if(tile->dim[0] % th_per_block > 0)
    block_grid_width++;
  // if (CUDA_MAX_DIM < block_grid_width) {
  //   MMB_ERR("Error: too many tiles,git diff  doesn't fit in %zu blocks "
  //       "of %zu threads.\n", CUDA_MAX_DIM, th_per_block);
  //   return MMB_ERROR;
  // }

  // Pick up the tile  from the allocated metadata
  mmbArrayTile *dev_tile;
  mmb_tile_get_space_local_handle(tile, &dev_tile);

  if (verbose) {
    MMB_DEBUG("Running kernel "
          "write_to_tile_hip_ker<<<%zu, %zu>>>(%p).\n",
          block_grid_width, th_per_block,
          dev_tile);
  }

  hipLaunchKernelGGL(write_to_tile_hip_ker, 
                  dim3(block_grid_width),
                  dim3(th_per_block),
                  0, 0, dev_tile);
  
  hipError_t hip_err = hipGetLastError();
  if (hipSuccess != hip_err) {
    MMB_ERR("Error while running kernel "
        "write_to_tile_hip_ker<<<%zu, %zu>>>(%p): %s.\n",
        block_grid_width, th_per_block, dev_tile,
        hipGetErrorString(hip_err));
    err = MMB_ERROR;
    goto BAILOUT;
  }

BAILOUT:
  return err;
}
