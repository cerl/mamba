/*
 * Copyright (C) 2018-2021 Cray UK
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include <getopt.h>
#include <string.h>
#include <ctype.h>

#include <mamba.h>
#include <mmb_memory.h>
#include <mmb_logging.h>

#include <mmb_pmem.h>
static mmbError init_host_buffer(mmbAllocation *host_allocation, size_t n_elements);
static mmbError check_result(mmbAllocation *host_allocation, size_t n_elements);

int main()
{
  printf("Running example buffer_copy...\n");

  mmbError stat;
  size_t n_elements = 32;

  /* Initialise Mamba and enable debug logging */
  stat = mmb_init(MMB_INIT_DEFAULT);
  CHECK_STATUS(stat, "Failed to initialise mamba\n", BAILOUT);

  stat = mmb_logging_set_debug_level(MMB_LOG_DEBUG);
  CHECK_STATUS(stat, "Failed to turn on debug logging for mamba\n", BAILOUT);

  mmbMemSpace *dram_space, *pmem_space;

  /* Check if discovery is enabled, set up memory spaces if not */
  int discovery_enabled = 0;
  /* Discovery not currently supported for fsdax nvdimms, and not tested for devdax*/
  /*stat = mmb_discovery_is_enabled(&discovery_enabled);
   *CHECK_STATUS(stat, "Failed to check the discovery status\n", BAILOUT); */
  if (!discovery_enabled) {
    // Memory configuration options
    const mmbMemSpaceConfig conf = {
      .size_opts = { .action = MMB_SIZE_SET, .mem_size = 8000 },
      .interface_opts = MMB_MEMINTERFACE_CONFIG_DEFAULT,
    };

    stat = mmb_register_memory(MMB_DRAM, MMB_EXECUTION_CONTEXT_DEFAULT, &conf, NULL);
    CHECK_STATUS(stat, "Failed to register dram memory for mamba\n", BAILOUT);
    
    /* This is the default path on the NextGenIO machine - may need changing for others */
    const char* nv_path = "/mnt/pmem_fsdax0";  
    struct mmb_pmem_t pmem_opt = {.path = nv_path};
    mmbMemInterfaceConfig pmem_conf = (mmbMemInterfaceConfig){
      .provider = MMB_PROVIDER_ANY,
      .strategy = MMB_STRATEGY_ANY,
      .name = "",
      .provider_opts = {.pmem = &pmem_opt}
    };
    const mmbMemSpaceConfig nv_conf = {
      .size_opts = { .action = MMB_SIZE_SET, .mem_size = SIZE_MAX },
      .interface_opts = pmem_conf,
      /* If using the environment variable MMB_CONFIG_NV_PATH_DEFAULT, 
       * you can do this instead of using dedicated pmem_conf/pmem_opt structs */
       /* .interface_opts = MMB_MEMINTERFACE_CONFIG_DEFAULT, */
    };

    stat = mmb_register_memory(MMB_NVDIMM, MMB_CPU, &nv_conf, NULL);
    CHECK_STATUS(stat, "Failed to register NVDIMM memory for mamba\n", BAILOUT);
  }

  /* Request memory spaces, and associated interfaces for DRAM, and GPU/OpenCL */
  stat = mmb_request_space(MMB_DRAM, MMB_EXECUTION_CONTEXT_DEFAULT, NULL, &dram_space);
  CHECK_STATUS(stat, "Failed to retrieve suitable dram memory\n", BAILOUT);

  stat = mmb_request_space(MMB_NVDIMM, MMB_CPU, NULL, &pmem_space);
  CHECK_STATUS(stat, "Failed to retrieve suitable gpu memory\n", BAILOUT);

  mmbMemInterface *dram_interface, *pmem_interface;	
  stat = mmb_request_interface(dram_space, NULL, &dram_interface);
  CHECK_STATUS(stat, "Failed to retrieve suitable dram memory interface\n", BAILOUT);
  
   stat = mmb_request_interface(pmem_space, NULL, &pmem_interface);
  CHECK_STATUS(stat, "Failed to create NVDIMM memory interface\n", BAILOUT);

  /* Allocate a block of local memory, and a block of device memory */
  mmbAllocation* host_allocation, *pmem_allocation;
  stat = mmb_allocate(n_elements*sizeof(float), dram_interface, &host_allocation);
  CHECK_STATUS(stat, "Failed to allocate host buffer\n", BAILOUT);

  stat = mmb_allocate(n_elements*sizeof(float), pmem_interface, &pmem_allocation);
  CHECK_STATUS(stat, "Failed to allocate pmem uffer\n", BAILOUT);

  /* Initialise local buffer */
  init_host_buffer(host_allocation, n_elements);

  /* Copy our local buffer to pmem partition */
  stat = mmb_copy(pmem_allocation, host_allocation);
  CHECK_STATUS(stat, "Failed to copy allocation to pmem partition\n", BAILOUT);

  /* Modify in pmem partition */
  float* buffer = (float*)pmem_allocation->ptr;
  for(unsigned i = 0; i < n_elements; i++) {
    buffer[i] *= i;
  } 

  /* Copy back to host */
  stat = mmb_copy(host_allocation, pmem_allocation);
  CHECK_STATUS(stat, "Failed to copy device allocation to host\n", BAILOUT);

  // Check result and cleanup

  stat = check_result(host_allocation, n_elements);
  if(stat == MMB_OK) {
      printf("Buffer copy to and from pmem memory successful.\n");
   } else {
      MMB_ERR("Buffer data validation check failed.\n");
      goto BAILOUT;
  }

  stat = mmb_free(pmem_allocation);
  CHECK_STATUS(stat, "Failed to free pmem allocation\n", BAILOUT);  
  stat = mmb_free(host_allocation);
  CHECK_STATUS(stat, "Failed to free host allocation\n", BAILOUT);
  stat = mmb_finalize(); 
  CHECK_STATUS(stat, "Failed to finalize mmb library\n", BAILOUT);

BAILOUT:
  return (stat == MMB_OK) ? EXIT_SUCCESS : EXIT_FAILURE;
}



static mmbError init_host_buffer(mmbAllocation *host_allocation, 
                                 size_t n_elements) {
  float* buffer = MMBALLOC2TBUF(host_allocation, float*);
  for(unsigned i = 0; i < n_elements; i++) 
    buffer[i] = i;
  return MMB_OK;
}

static mmbError check_result(mmbAllocation *host_allocation, 
                             size_t n_elements)
{
  mmbError stat = MMB_OK;
  float* buffer = MMBALLOC2TBUF(host_allocation, float*);
  for(unsigned i = 0; i < n_elements; i++) {
    if(buffer[i] != i*i) {
      MMB_ERR("Buffer data incorrect at idx %lu, expected idx^2 (%f), got: %f\n", i, 
              i*i, buffer[i]); 
      stat = MMB_ERROR;
      goto BAILOUT;      
    }
  }
BAILOUT:
  return stat;
}

