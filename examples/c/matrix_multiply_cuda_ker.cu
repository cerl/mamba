/*
 * Copyright (C) 2018-2020 Cray UK
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <cstdlib>
#include <cstdio>
#include <mamba.h>
#include <mmb_logging.h>
#include "matrix_multiply_cuda.h"

__global__
void sgemm_kernel(serializedMmbArrayTile2D *tiles,
                  const std::size_t n_workers,
                  const std::size_t n_set_per_worker)
{
  const std::size_t tid = (blockDim.y * blockIdx.y + blockIdx.x) * blockDim.x
                          + threadIdx.x;

  /* Check we are still within bounds */
  if (n_workers <= tid) return;

  for (std::size_t set_i = 0; n_set_per_worker > set_i; ++set_i) {
    struct mmbArrayTile arrayTile[3];
    const std::size_t crt_set = 3 * (tid * n_set_per_worker + set_i);
    /* Update pointer */
    for(std::size_t it = 0; 3 > it; ++it) {
      arrayTile[it].allocation  = BUF2MMBALLOC(tiles[crt_set + it].ptr);
      arrayTile[it].lower       = tiles[crt_set + it].lower;
      arrayTile[it].upper       = tiles[crt_set + it].upper;
      arrayTile[it].dim         = tiles[crt_set + it].dim;
      arrayTile[it].abs_dim     = tiles[crt_set + it].abs_dim;
    }
    mmbArrayTile * tile_a = &arrayTile[0],
                 * tile_b = &arrayTile[1],
                 * tile_c = &arrayTile[2];
    /* Element loops for single tile matrix multiply */
    for (std::size_t ei = 0; tile_c->dim[0] > ei; ++ei) {
      for (std::size_t ej = 0; tile_c->dim[1] > ej; ++ej) {
        for (std::size_t ek = 0; tile_a->dim[1] > ek; ++ek) {
          MMB_IDX_2D_NORM(tile_c, ei, ej, float) +=
            MMB_IDX_2D_NORM(tile_a, ei, ek, float) *
            MMB_IDX_2D_NORM(tile_b, ek, ej, float);
        }
      }
    }
  }
}


mmbError sgemm(mmbAllocation *tiles_alloc,
               const std::size_t n_workers,
               const std::size_t n_set_per_worker,
               const int verbose)
{
  mmbError err = MMB_OK;
  /* Compute dimensions of the block grid and number of workers. */
  const std::size_t CUDA_MAX_DIM = 0x7fff;
  const std::size_t th_per_block = 256;
  std::size_t block_grid_width = n_workers / th_per_block;
  if (0 == block_grid_width) block_grid_width = 1;
  else if (CUDA_MAX_DIM < block_grid_width) block_grid_width = CUDA_MAX_DIM;
  std::size_t block_grid_height = n_workers/(block_grid_width*th_per_block);
  if (0 == block_grid_height) block_grid_height = 1;
  else if (CUDA_MAX_DIM < block_grid_height) {
    MMB_ERR("Error: too many tiles, doesn't fit in grid of (%zu x %zu) blocks "
        "of %zu threads.\n", CUDA_MAX_DIM, CUDA_MAX_DIM, th_per_block);
    return MMB_ERROR;
  }
  const dim3 block(block_grid_width, block_grid_height);

  /* Allocate on GPU the equivalent structure and copy the values */
  cudaError_t c_err = cudaSuccess;
  const std::size_t ntiles = n_workers * n_set_per_worker;
  mmbAllocation *ker_tiles_alloc = NULL;
  err = mmb_allocate(sizeof(serializedMmbArrayTile2D[ntiles][3]),
                     device_interface, &ker_tiles_alloc);
  if (MMB_OK != err) {
    MMB_ERR("Unable to allocate serialized tile structure (%d).\n", err);
    return err;
  }

  mmb_copy(ker_tiles_alloc, tiles_alloc);

  serializedMmbArrayTile2D *ker_tiles =
    MMBALLOC2TBUF(ker_tiles_alloc, serializedMmbArrayTile2D *);
  if (verbose) {
    MMB_DEBUG("Running kernel "
          "sgemm_kernel<<<(%zu, %zu), %zu>>>(%p, %zu).\n",
          block_grid_width, block_grid_height, th_per_block,
          ker_tiles, n_workers, n_set_per_worker);
  }

  sgemm_kernel<<<block, th_per_block>>>(ker_tiles, n_workers, n_set_per_worker);

  c_err = cudaGetLastError();
  if (cudaSuccess != c_err) {
    MMB_ERR("Error while running kernel "
        "sgemm_kernel<<<(%zu, %zu), %zu>>>(%p, %zu, %zu): %s.\n",
        block_grid_width, block_grid_height, th_per_block, ker_tiles,
        n_workers, n_set_per_worker, cudaGetErrorString(c_err));
    err = MMB_ERROR;
    goto BAILOUT;
  }

BAILOUT:
  mmb_free(ker_tiles_alloc);

  return err;
}
