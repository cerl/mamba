/*
 * Copyright (C) 2021 Cray UK
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <stdio.h>
#include <stdlib.h>

#include <mamba.h>
#include <mmb_error.h>
#include <mmb_logging.h>

int main(void)
{
  mmbError stat = MMB_OK;
  size_t n_allocations, bytes_allocated;

  /* Initialize Mamba and set pool_size to 1KiB */
  mmbOptions *init_options;
  stat = mmb_options_create_default(&init_options);
  if (MMB_OK != stat) {
    MMB_ERR("Failed to create default initialisation options.\n");
    goto BAILOUT;
  }
  size_t pool_size = 1024;
  stat = mmb_options_strategy_pooled_set_pool_size(init_options, pool_size);
  if (MMB_OK != stat) {
    MMB_ERR("Failed to set the pool size.\n");
    goto BAILOUT;
  }
  stat = mmb_init(init_options);
  if(stat != MMB_OK) {
    MMB_ERR("Failed to initialise mamba\n");
    goto BAILOUT;
  }

  /* Register 8GB of DRAM */
  mmbMemSpace *space;
  const mmbMemSpaceConfig dram_config = {
    .size_opts = { .action = MMB_SIZE_SET, .mem_size = 8000 },
    .interface_opts = MMB_MEMINTERFACE_CONFIG_DEFAULT,
  };
  stat = mmb_register_memory(MMB_DRAM, MMB_CPU, &dram_config, &space);
  if (MMB_OK != stat) {
    MMB_ERR("Failed to register memory. Exiting.\n");
    goto BAILOUT;
  }

  /* Register an interface with statistics enabled */
  mmbMemInterface *stats_interface;
  mmbMemInterfaceConfig stats_config = {
    .provider = MMB_SYSTEM, .strategy = MMB_STATISTICS
  };
  stat = mmb_create_interface(space, &stats_config, &stats_interface);
  if (MMB_OK != stat) {
    MMB_ERR("Failed to create the basic interface. Exiting.\n");
    goto BAILOUT;
  }

  /* Allocate multiple data */
  mmbAllocation *alloc1;
  const size_t alloc_size1 = sizeof(double[4]);
  stat = mmb_allocate(alloc_size1, stats_interface, &alloc1);
  if (MMB_OK != stat) {
    MMB_ERR("Failed first allocation. Exiting.\n");
    goto BAILOUT;
  }
  mmbAllocation *alloc2;
  const size_t alloc_size2 = sizeof(double[12]);
  stat = mmb_allocate(alloc_size2, stats_interface, &alloc2);
  if (MMB_OK != stat) {
    MMB_ERR("Failed second allocation. Exiting.\n");
    goto BAILOUT;
  }

  /* Read the recorded stats */
  stat = mmb_statistics_get_current_statistics(stats_interface,
                                               &n_allocations,
                                               &bytes_allocated);
  if (MMB_OK != stat) {
    MMB_ERR("Failed to retrieve current stats. Exiting.\n");
    goto BAILOUT;
  }

  /* expected result:
   * `alloc_size1+alloc_size2` bytes, in `2` allocations */
  printf("So far, %zu bytes have been allocated in %zu allocations.\n",
         bytes_allocated, n_allocations);

  /* Check the update after freeing one allocation */
  mmb_free(alloc2);
  stat = mmb_statistics_get_current_statistics(stats_interface,
                                               &n_allocations,
                                               &bytes_allocated);
  if (MMB_OK != stat) {
    MMB_ERR("Failed to retrieve current stats. Exiting.\n");
    goto BAILOUT;
  }

  /* expected result:
   * `alloc_size1` bytes, in `1` allocations */
  printf("Now, only %zu bytes are still allocated in %zu allocations.\n",
         bytes_allocated, n_allocations);

  /* Check the total allocation record */
  stat = mmb_statistics_get_total_statistics(stats_interface,
                                             &n_allocations,
                                             &bytes_allocated);
  if (MMB_OK != stat) {
    MMB_ERR("Failed to retrieve total stats. Exiting.\n");
    goto BAILOUT;
  }

  /* expected result:
   * `alloc_size1+alloc_size2` bytes, in `2` allocations */
  printf("In total, %zu bytes have been allocated in %zu allocations.\n",
         bytes_allocated, n_allocations);

  /* Works with a pooled strategy as well */
  mmbMemInterface *stats_pool_interface;
  mmbMemInterfaceConfig stats_pool_conf = {
    .provider = MMB_SYSTEM, .strategy = MMB_POOLED_STATISTICS
  };
  stat = mmb_create_interface(space, &stats_pool_conf, &stats_pool_interface);
  if (MMB_OK != stat) {
    MMB_ERR("Failed to create the pooled interface. Exiting.\n");
    goto BAILOUT;
  }
  mmbAllocation *alloc3;
  const size_t alloc_size3 = sizeof(double[4]);
  stat = mmb_allocate(alloc_size3, stats_pool_interface, &alloc3);
  if (MMB_OK != stat) {
    MMB_ERR("Failed third allocation. Exiting.\n");
    goto BAILOUT;
  }
  mmbAllocation *alloc4;
  const size_t alloc_size4 = sizeof(double[12]);
  stat = mmb_allocate(alloc_size4, stats_pool_interface, &alloc4);
  if (MMB_OK != stat) {
    MMB_ERR("Failed fourth allocation. Exiting.\n");
    goto BAILOUT;
  }
  stat = mmb_statistics_get_current_statistics(stats_pool_interface,
                                               &n_allocations,
                                               &bytes_allocated);
  if (MMB_OK != stat) {
    MMB_ERR("Failed to retrieve current stats. Exiting.\n");
    goto BAILOUT;
  }

  /**
   * As you can see, the records are per-interface.
   * Also, the records are being made just before calling the provider.  Hence,
   * the number of allocations and bytes allocated correspond to the values
   * after application of the pooled strategy.  To word it differently, the
   * values correspond to how many times the provider has been called, and the
   * amount of memory it provided.
   */
  /* expected result:
   * `pool_size` bytes, in `1` allocations */
  printf("%zu bytes have been allocated in %zu allocations with the "
         "pooled strategy.\n", bytes_allocated, n_allocations);

  mmb_free(alloc1);
  mmb_free(alloc3);
  mmb_free(alloc4);
  stat = mmb_finalize();
  if (MMB_OK != stat) {
    MMB_ERR("Failed to finalise mamba library\n");
    goto BAILOUT;
  }
  stat = mmb_options_destroy(init_options);
  if (MMB_OK != stat) {
    MMB_ERR("Failed to destroy initialization option.\n");
    goto BAILOUT;
  }

BAILOUT:
  return MMB_OK == stat ? EXIT_SUCCESS : EXIT_FAILURE;
}
