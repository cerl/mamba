# -*- mode: autoconf -*-
#
# AX_CHECK_CL
#
# Check for an OpenCL implementation.  If CL is found, the required compiler
# and linker flags are included in the output variables "CL_CFLAGS" and
# "CL_LIBS", respectively.  If no usable CL implementation is found, "no_cl"
# is set to "yes".
#
# If the header "CL/cl.h" is found, "HAVE_CL_CL_H" is defined.  If the header
# "OpenCL/cl.h" is found, HAVE_OPENCL_CL_H is defined.  These preprocessor
# definitions may not be mutually exclusive.
#
# Based on AX_CHECK_GL, version: 2.4 author: Braden McDaniel
# <braden@endoframe.com>
#
# Based on https://github.com/amusecode/amuse/blob/master/m4/opencl.m4 from the
# version at commit fc8a55460e5ceadce16e3fc75438718ca08353cd
#
# Modified by Cray UK Ltd, a Hewlett Packard Enterprise company
# <emearesearchlab@hpe.com>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2, or (at your option)
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
# 02110-1301, USA.
#
# As a special exception, the you may copy, distribute and modify the
# configure scripts that are the output of Autoconf when processing
# the Macro.  You need not follow the terms of the GNU General Public
# License when using or distributing such scripts.
#
AC_DEFUN([AX_CHECK_CL],
  [dnl check if OpenCL support is requested
   AC_ARG_ENABLE([opencl],
     [AC_HELP_STRING([--enable-opencl],
       [Whether to enable OpenCL support or not. @<:@default=no@:>@])
     ],
     [dnl Check value given as parameter
      AS_IF([test "x$enable_opencl" != xyes -a "x$enable_opencl" != xno],
        [AC_MSG_FAILURE([Wrong value for --enable-opencl])])
     ],
     [enable_opencl=no])
   dnl Check whether some specific installation path is provided
   AC_ARG_WITH([opencl],
     [AC_HELP_STRING([--with-opencl=@<:@=/path/to/OpenCL/install@:>@],
       [Prefix where OpenCl has been installed])
     ], [], [with_opencl=default])
   dnl Configure depending on the two options given
   AS_IF([test "x$enable_opencl" = xyes],
     [dnl OpenCL is requested, check for the headers and libraries
      dnl availability.
      AS_IF([test "x$with_opencl" != xdefault],
        [CL_CPPFLAGS="-I$opencl_prefix/include"
         CL_LIBS_TRY="-L$opencl_prefix/lib -L$opencl_prefix/lib64 -L$opencl_prefix/lib/x86_64 -L$opencl_prefix/lib/x86"
        ],
        [CL_LIBS_TRY=""])
      AC_LANG_PUSH([C])
      ax_save_CPPFLAGS="$CPPFLAGS"
      CPPFLAGS="$OCL_CPPFLAGS $CPPFLAGS"
      AC_CHECK_HEADERS([CL/cl.h OpenCL/cl.h])
      CPPFLAGS="$ax_save_CPPFLAGS"

      m4_define([AX_CHECK_CL_PROGRAM],
        [AC_LANG_PROGRAM([[
# if defined(HAVE_CL_CL_H)
#   include <CL/cl.h>
# elif defined(HAVE_OPENCL_CL_H)
#   include <OpenCL/cl.h>
# else
#   error no cl.h
# endif
                        ]],
                        [[clFinish(0)]])
        ])
      AC_CACHE_CHECK([for OpenCL library], [ax_cv_check_cl_libcl],
        [dnl Look for proper library to includex
          ax_cv_check_cl_libcl=no
          case $host_cpu in
            x86_64) ax_check_cl_libdir=lib64 ;;
            *)    ax_check_cl_libdir=lib ;;
          esac
          ax_save_CPPFLAGS="$CPPFLAGS"
          CPPFLAGS="$OCL_CFLAGS $CPPFLAGS"
          ax_save_LIBS=$LIBS
          LIBS=""
          ax_check_libs="-lOpenCL -lCL"
          for cl_libflags in $CL_LIBS_TRY
          do
            for ax_lib in $ax_check_libs
            do
              # Check for OpenCL support
              LIBS="$ax_lib $cl_libflags $ax_save_LIBS"
              AC_LINK_IFELSE([AX_CHECK_CL_PROGRAM],
                [ax_cv_check_cl_libcl=$ax_lib; break],
                [# Check for NVidia provided support
                  ax_check_cl_nvidia_flags="-L/usr/$ax_check_cl_libdir/nvidia"
                  LIBS="$ax_lib $ax_check_cl_nvidia_flags $cl_libflags $ax_save_LIBS"
                  AC_LINK_IFELSE([AX_CHECK_CL_PROGRAM],
                    [ax_cv_check_cl_libcl="$ax_lib $ax_check_cl_nvidia_flags"; break],
                    [# Check from macOS dylibs
                    ax_check_cl_dylib_flag='-dylib_file /System/Library/Frameworks/OpenCL.framework/Versions/A/Libraries/libCL.dylib:/System/Library/Frameworks/OpenCL.framework/Versions/A/Libraries/libCL.dylib'
                    LIBS="$ax_lib $ax_check_cl_dylib_flag $cl_libflags $ax_save_LIBS"
                    AC_LINK_IFELSE([AX_CHECK_CL_PROGRAM],
                            [ax_cv_check_cl_libcl="$ax_lib $ax_check_cl_dylib_flag"; break],
                            [])
                    ])
                ])
            done
            AS_IF([test "X$ax_cv_check_cl_libcl" = Xno],
              [],
              [break])
          done

          AS_IF([test "X$ax_cv_check_cl_libcl" = Xno -a X$no_x = Xyes],
            [LIBS='-framework OpenCL'
            AC_LINK_IFELSE([AX_CHECK_CL_PROGRAM], [ax_cv_check_cl_libcl=$LIBS])
            ])

          LIBS="$ax_save_LIBS"
          CPPFLAGS="$ax_save_CPPFLAGS"
        ])
        AS_IF([test "X$ax_cv_check_cl_libcl" = Xno],
          [
            have_cl=no
            CL_CFLAGS=""; 
            CL_LIBS=""
            AC_MSG_ERROR(['failed to link OpenCL test binary, try --with-opencl=/path/to/install'])
          ],
          [
            have_cl=yes
            CL_LIBS="$cl_libflags $ax_cv_check_cl_libcl"
          ])
        AC_LANG_POP([C])
        
     ],
     [have_cl=no
      CL_CFLAGS=""
      CL_LIBS=""
     ])

     HAVE_OPENCL=$have_cl
     WITH_OPENCL=$have_cl


  AM_CONDITIONAL([WITH_OPENCL], [test x$WITH_OPENCL = xyes])
  AM_COND_IF([WITH_OPENCL], [
    AC_SUBST([CL_CFLAGS])
    AC_SUBST([CL_LIBS])
    ], [])

  AC_DEFINE_UNQUOTED([HAVE_OPENCL],
                    dnl test is inverted. `test false` has $?=1
                    [`test "x$WITH_OPENCL" = "xno"; echo $?`],
                    [Define to true if OpenCL support is available])
])dnl
