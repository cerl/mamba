# _AC_PROG_FC_UPPERCASE_MOD
# -------------------------
# Test if the Fortran compiler produces uppercase module filenames.
# Modified from Mike R Nolta, circa 2004
# https://lists.gnu.org/archive/html/automake/2004-01/msg00245.html
AC_DEFUN([_AC_PROG_FC_UPPERCASE_MOD],
[_AC_FORTRAN_ASSERT()dnl
AC_CACHE_CHECK([whether $[]_AC_FC[] produces uppercase module filenames],
               [ac_cv_prog_fc_uppercase_mod],
[AC_LANG_CONFTEST([AC_LANG_SOURCE([      module conftest
      end module])])
ac_try='$[]_AC_FC[] $[]_AC_LANG_PREFIX[]FLAGS -c conftest.$ac_ext 
>&AS_MESSAGE_LOG_FD'
if AC_TRY_EVAL(ac_try) &&
   test -f CONFTEST.mod ; then
        ac_cv_prog_fc_uppercase_mod=yes
        rm -f CONFTEST.mod
else
  ac_cv_prog_fc_uppercase_mod=no
fi
rm -f conftest*])
])# _AC_PROG_FC_UPPERCASE_MOD


# AC_PROG_FC_MOD
# ---------------
AC_DEFUN([AC_PROG_FC_UPPERCASE_MOD],
[AC_REQUIRE([AC_PROG_FC])dnl
AC_LANG_PUSH(Fortran)dnl
_AC_PROG_FC_UPPERCASE_MOD
AC_LANG_POP(Fortran)dnl
])# AC_PROG_FC_UPPERCASE_MOD

