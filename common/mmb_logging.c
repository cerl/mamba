/*
 * Copyright (C) 2019 Cray Computer GmbH
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICUMMBR PURPOSE ARE DISCMMBIMED. IN NO EVENT SHALL THE COPYRIGHT
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPMMBRY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/* -*- mode:c -*- */
/** @file
 ** @brief Logging infrastructure implementation of udj library
 **
 **/
#include "mmb_logging.h"
#include <pthread.h>
#include <stdbool.h>
#include <stdlib.h>
#include <unistd.h>

#include <assert.h>
#include <string.h>
#include <unistd.h>

/** MIN of two numbers */
#define MIN(x, y) ((x) < (y) ? (x) : (y))

/** number of log levels */
#define MMB_log__MAX 5

/** Indicator characters for log lines to indicate message severity */
static const char *const mmb_log_labels_short[MMB_log__MAX] = {[MMB_LOG_ERR] = "E",
                                                       [MMB_LOG_WARN] = "W",
                                                       [MMB_LOG_INFO] = "I",
                                                       [MMB_LOG_DEBUG] = "D",
                                                       [MMB_LOG_NOISE] = "N"};

/** Indicator characters for log lines to indicate message severity */
static const char *const mmb_log_labels_full[MMB_log__MAX] = {[MMB_LOG_ERR] = "MMB_LOG_ERR",
                                                              [MMB_LOG_WARN] = "MMB_LOG_WARN",
                                                              [MMB_LOG_INFO] = "MMB_LOG_INFO",
                                                              [MMB_LOG_DEBUG] = "MMB_LOG_DEBUG",
                                                              [MMB_LOG_NOISE] = "MMB_LOG_NOISE"};

static pthread_mutex_t global_logging_mtx = PTHREAD_MUTEX_INITIALIZER;
#define LOCK_GLOBAL_LOGGING do {                                  \
    int _gflstat = pthread_mutex_lock(&global_logging_mtx);       \
    assert(_gflstat==0);                                          \
    (void) _gflstat;                                              \
  } while(0);
 
#define UNLOCK_GLOBAL_LOGGING do {                                \
    int _gflstat = pthread_mutex_unlock(&global_logging_mtx);     \
    assert(_gflstat==0);                                          \
    (void) _gflstat;                                              \
  } while(0);


mmbUserLogFunc g_mmb_log_func = &mmb_location_aware_log;

/** debugging level
 *
 * Only messages with level <= g_debug_level will be printed
 *
 **/
static int g_debug_level = MMB_MAX_LOG_LEVEL;

static bool g_logging_initialized = false;
static bool g_log_level_set_from_env = false;
static char *g_env_debug_flag = NULL;

/* A default logging options object, which may be modified by accessors below */
mmbError mmb_logging_options_create_default(mmbLoggingOptions **opts) {
  mmbError stat = MMB_OK;
  if(!opts){
    MMB_ERR("mmbLoggingOptions can not be NULL\n");
    stat = MMB_INVALID_ARG;
    goto BAILOUT;
  }
  mmbLoggingOptions *l = malloc(sizeof(mmbLoggingOptions));
  if(!l) {
    stat = MMB_OUT_OF_MEMORY;
    goto BAILOUT;
  }

 l->max_logging = MMB_LOG_WARN;
 l->user_log_func = NULL;

 *opts = l;

BAILOUT:
  return stat;
}

/* Accessors for logging options */
mmbError mmb_logging_options_set_debug_level(mmbLoggingOptions *opts, int level){
  mmbError stat = MMB_OK;
  if(!opts){
    MMB_ERR("mmbLoggingOptions can not be NULL\n");
    stat = MMB_INVALID_ARG;
    goto BAILOUT;    
  }
  if (level < 0 || level >= MMB_log__MAX) {
    MMB_ERR("Attempt to set logging options debug level to invalid value %d\n", level);
    stat = MMB_INVALID_ARG;
    goto BAILOUT;   
  }
  opts->max_logging = level;
BAILOUT:
  return stat;
}

mmbError mmb_logging_options_set_user_log_func(mmbLoggingOptions *opts, mmbUserLogFunc user_log_func){
  mmbError stat = MMB_OK;
  if(!opts){
    MMB_ERR("mmbLoggingOptions can not be NULL\n");
    stat = MMB_INVALID_ARG;
    goto BAILOUT;
  }
  if (user_log_func == NULL) {
    MMB_ERR("Attempt to set logging function to NULL, must be a valid function\n");
    stat = MMB_INVALID_ARG;
    goto BAILOUT;
  }
  opts->user_log_func = user_log_func;
BAILOUT:
  return stat;
}

mmbError mmb_logging_options_destroy(mmbLoggingOptions *opts) {
  mmbError stat = MMB_OK;
  if (NULL == opts) {
    MMB_WARN("Logging options pointer invalid or already free'd.\n");
    stat = MMB_INVALID_ARG;
    goto BAILOUT;
  }
  free(opts);
BAILOUT:
  return stat;
}

/* Accessors for configuring logging infrastructure post-init */
mmbError mmb_logging_set_debug_level(int level){
  mmbError stat = MMB_OK;
  if (level < 0 || level >= MMB_log__MAX) {
    MMB_ERR("Attempt to set debug level to invalid value %d\n", level);
    stat = MMB_INVALID_ARG;
    goto BAILOUT;
  }
  if(g_log_level_set_from_env) {
    MMB_WARN("Attempt to set debug level via API ignored due to MMB_LOG_LEVEL found in environment\n");
    goto BAILOUT;
  }
  g_debug_level = MIN(MMB_MAX_LOG_LEVEL, level);
BAILOUT:
  return stat;
}

mmbError mmb_logging_set_user_log_func(mmbUserLogFunc user_log_func)
{
  mmbError stat = MMB_OK;
  if (user_log_func == NULL) {
    MMB_ERR("Attempt to set logging function to NULL, must be a valid function\n");
    stat = MMB_INVALID_ARG;
    goto BAILOUT;
  }
  g_mmb_log_func = user_log_func;
BAILOUT:
  return stat;
}

mmbError mmb_logging_init(mmbLoggingOptions *opts) {
  mmbError stat;
LOCK_GLOBAL_LOGGING;
  if(g_logging_initialized) {
    MMB_ERR("Logging infrastructure already initialized\n");
    stat = MMB_ERROR;
    goto BAILOUT;
  }

  mmbLoggingOptions default_opts = {.max_logging = MMB_LOG_WARN, .user_log_func = NULL};
  if(opts == NULL) {
    opts = &default_opts;
  }

  /* Parse API opts */
  stat = mmb_logging_set_debug_level(opts->max_logging);
  if(stat != MMB_OK)
    goto BAILOUT;
  if(opts->user_log_func) {
    stat = mmb_logging_set_user_log_func(opts->user_log_func);
    if(stat != MMB_OK)
      goto BAILOUT;
  }

  /* Parse environment opts, which override API opts */
  /* MMB_LOG_LEVEL is not used during user-defined logging */
  if(!opts->user_log_func) {
    g_env_debug_flag = getenv("MMB_LOG_LEVEL");
    if (g_env_debug_flag != NULL) {
      int env_level = atoi(g_env_debug_flag);
      if (env_level < 0 || env_level >= MMB_log__MAX) {
        MMB_WARN("Attempt to set debug level to %s in environment, ignored\n",
              g_env_debug_flag);
      } else {
        g_debug_level = MIN(MMB_MAX_LOG_LEVEL, env_level);
        g_log_level_set_from_env = true;
      }
      MMB_INFO("Log level set to %d from environment\n", g_debug_level);
    }
  }
  g_logging_initialized = true;
BAILOUT:
  UNLOCK_GLOBAL_LOGGING;
  return stat;
}

mmbError mmb_logging_finalize() {
  mmbError stat = MMB_OK;
  LOCK_GLOBAL_LOGGING;
  if(!g_logging_initialized) {
    MMB_ERR("Logging infrastructure not initialized\n");
    stat = MMB_ERROR;
    goto BAILOUT;
  }
  g_log_level_set_from_env = false;
  g_logging_initialized = false;
BAILOUT:
  UNLOCK_GLOBAL_LOGGING;
  return stat;
}


void mmb_vlocation_aware_log(int level, const char *func, const char *file,
                             int line, const char *fmtstring, va_list argp) {
  assert(level >= 0);
  assert(level < MMB_log__MAX);

  if (level <= g_debug_level) {
    /* later on we'll have a bit more global state */
    // char *thread_desc = ""; /*
    // pthread_getspecific(g_thread_descriptor_key);*/
    /* /\* Intentionally using our own rank ID to enable logging before */
    /*  * udj_init() and in, e.g., the dataspaces server application *\/ */
    /* udj_coop_group_rank(UDJ_COOP_GROUP_ALL, &g_rank); */
    /*if(thread_desc==NULL)
      thread_desc="(spurious thread)";*/
    char header[1024] = "";
    snprintf(&header[0], 1024, "[%s] (%ld): %s(%s:%d) %s",
             mmb_log_labels_short[level], (long)getpid(), func,
             file, line, fmtstring);
    vfprintf(stderr, header, argp);
  }
}

void mmb_location_aware_log(int level, const char *func, const char *file,
                            int line, const char *fmtstring, ...) {
  va_list ap;
  va_start(ap, fmtstring);
  mmb_vlocation_aware_log(level, func, file, line, fmtstring, ap);
  va_end(ap);
}

const char* mmb_log_level_to_str(int level) {
  if(level >= MMB_log__MAX){
    MMB_ERR("Mamba log level out of bounds\n");
    return NULL;
  }
  return mmb_log_labels_full[level];
}
