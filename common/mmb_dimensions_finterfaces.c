

#include "mmb_error.h"
#include "mmb_dimensions.h"
#include "mmb_set.h"
#include "mmb_logging.h"

mmbError mmb_dimensions_copy_toarray_f(size_t * restrict dst, const mmbDimensions * restrict src) {
  size_t i;

  MMB_DEBUG("mmb_dimensions_copy_toarray_f: size %lld\n",src->size);
  for(i=0;i<src->size;i++){
    dst[i] = src->d[i];
  }
  
  return MMB_OK;

}



