/*
 * Copyright (C) 2018-2021 Cray UK
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <stdio.h>
#include <stdbool.h>
#include <pthread.h>
#include <assert.h>
#include "mamba.h"

#include "mmb_tile.h"
#include "mmb_tile_iterator.h"
#include "mmb_logging.h"
#include "mmb_memory.h"
#include "i_mmb_hash.h"
#include "mmb_numa_sizes.h"

/* Keep the full path to ease the research of the correpsonding header */
#include "../memory/allocation.h"
#include "../memory/strategy.h"
#include "../memory/memory_options.h"
#include "../memory/i_memory.h"

#ifdef HAVE_LOOP_ANALYSIS
#include "mmb_loop.h"
#endif
#ifdef HAVE_COST_MODEL
#include "mmb_cost_model.h"
#else 
#define mmbCostModelHandle void*
#endif

#if ENABLE_DISCOVERY
#include "mmb_hwloc.h"
#else 
typedef void* hwloc_topology_t;
#endif

/* protecting initialization */
static pthread_mutex_t g_mamba_state = PTHREAD_MUTEX_INITIALIZER;
static inline void mmb_state_lock(void) {
  int stat = pthread_mutex_lock(&g_mamba_state);
  assert(stat==0);
  (void) stat;
}
static inline void mmb_state_unlock(void) {
  int stat = pthread_mutex_unlock(&g_mamba_state);
  assert(0 == stat);
  (void) stat;
}
/* Types */
/* Internal singleton Mamba instance */
typedef struct s_mmbMambaInstance {
  bool is_initialized;
  mmbManager *manager;
  mmbCostModelHandle *cm_handle;
} mmbMambaInstance;
static mmbMambaInstance g_mmb_instance = {false, NULL, NULL};


mmbError mmb_init( mmbOptions *_in_opts ) {
  mmbError stat = MMB_OK;
  mmbOptions *in_opts = _in_opts;

  /* Check before we take the lock */
  if(g_mmb_instance.is_initialized) { 
    MMB_ERR("Mamba library already initialized\n");
    stat = MMB_ERROR;
    goto UNLOCK_BAILOUT;
  }

  mmb_state_lock();

  /* Check again in case someone beat us here */
  if(g_mmb_instance.is_initialized) { 
    MMB_ERR("Mamba library already initialized\n");
    stat = MMB_ERROR;
    goto UNLOCK_BAILOUT;
  }

  if(!_in_opts) {
    stat = mmb_options_create_default(&in_opts);
    if(stat != MMB_OK){
      MMB_ERR("Failed to create default mmb options\n");
      goto UNLOCK_BAILOUT;
    }
  }

  stat = mmb_logging_init(in_opts->logging_opts);
  if (MMB_OK != stat) {
    MMB_ERR("Failed to initialize the logging infrastructure.\n");
    goto UNLOCK_BAILOUT;
  }

  stat = mmb_init_providers(in_opts->memory_opts->providers_opts);
  if (MMB_OK != stat) {
    MMB_ERR("Failed to initialize the memory provider libraries.\n");
    goto UNLOCK_BAILOUT;
  }

  stat = mmb_init_strategies(in_opts->memory_opts->strategies_opts);
  if (MMB_OK != stat) {
    MMB_ERR("Failed to initialize the memory allocation strategies.\n");
    goto UNLOCK_BAILOUT;
  }

  stat = mmb_manager_create(&g_mmb_instance.manager);
  if(stat != MMB_OK) {
    MMB_ERR("Failed to create memory manager\n");
    goto UNLOCK_BAILOUT;
  }

  stat = mmb_alloc_initialize();
  if (MMB_OK != stat) {
    MMB_ERR("Failed to initialize mmbAllocationFactory.\n");
    goto UNLOCK_BAILOUT;
  }

#ifdef HAVE_LOOP_ANALYSIS
  stat = mmb_loop_initialize();
  if(stat != MMB_OK) {
    MMB_ERR("Failed to initialize mmb_loop\n");
    goto UNLOCK_BAILOUT;
  } 
#endif 

#ifdef HAVE_COST_MODEL
  stat = mmb_cost_model_initialize(&g_mmb_instance.cm_handle);
  if(stat != MMB_OK) {
    MMB_ERR("Failed to initialize cost model library\n");
    goto UNLOCK_BAILOUT;
  } 
#endif

  g_mmb_instance.is_initialized = true;
  MMB_DEBUG("Mamba library initialised, version: %s\n", mmb_version());

UNLOCK_BAILOUT:

  if (!_in_opts)
    mmb_options_destroy(in_opts);

  mmb_state_unlock();

  return stat;
}

mmbError mmb_is_initialized(bool *initialized) {
  if(!initialized)
    return MMB_INVALID_ARG;
  *initialized = g_mmb_instance.is_initialized;
  return MMB_OK;
}

mmbError mmb_register_memory(const mmbMemLayer layer,
                             const mmbExecutionContext ex_context,
                             const mmbMemSpaceConfig *config_opts,
                             mmbMemSpace **new_space)
{
  const mmbMemSpaceConfig default_config = MMB_MEMSPACE_CONFIG_DEFAULT;
  if (NULL == config_opts) {
    config_opts = &default_config;
  }
  /* /!\ mmbMemSpaceConfig->mem_size is given in bytes, not MiB. */
  mmbMemSpaceConfig space_config = *config_opts;
  if (SIZE_MAX == space_config.size_opts.mem_size // SIZE_MAX is a special value
      || SIZE_MAX / (1024*1024) < space_config.size_opts.mem_size)
    space_config.size_opts.mem_size = SIZE_MAX;
  else
    space_config.size_opts.mem_size *= 1024 * 1024;
  return mmb_manager_register_memory(g_mmb_instance.manager, layer, ex_context, &space_config,
                                     new_space);
}

mmbError mmb_register_numa_memory(const mmbMemLayer layer,
                                  const mmbExecutionContext ex_context,
                                  const mmbMemSpaceConfig *config_opts,
                                  mmbMemSpace **new_space)
{
  if (NULL == config_opts) {
    MMB_ERR("No default value for NUMA memory registration.  \"config_opts\" "
            "cannot be NULL.\n");
    return MMB_INVALID_ARG;
  } else if (!config_opts->size_opts.is_numa_aware) {
    MMB_ERR("Invalid configuration options provided.\n");
    return MMB_INVALID_ARG;
  }
  /* /!\ mmbMemSpaceConfig->numa_sizes are given in bytes, not MiB. */
  mmbMemSpaceConfig space_config = *config_opts;
  const size_t n_numa_sizes = config_opts->size_opts.numa_sizes.size;
  size_t numa_sizes[n_numa_sizes];
  space_config.size_opts.numa_sizes.size = n_numa_sizes;
  space_config.size_opts.numa_sizes.d = numa_sizes;
  for (size_t id = 0; n_numa_sizes > id; ++id) {
    if (SIZE_MAX == config_opts->size_opts.numa_sizes.d[id]
        || SIZE_MAX / (1024*1024) < config_opts->size_opts.numa_sizes.d[id])
      numa_sizes[id] = UINT64_MAX;
    else
      numa_sizes[id] = config_opts->size_opts.numa_sizes.d[id] * 1024 * 1024;
  }
  return mmb_manager_register_memory(g_mmb_instance.manager, layer, ex_context,
                                     &space_config, new_space);
}

mmbError mmb_request_space(const mmbMemLayer layer,
                           const mmbExecutionContext ex_con,
                           const mmbMemSpaceConfig *config_opts,
                           mmbMemSpace **out_space)
{
  const mmbMemSpaceConfig default_config = MMB_MEMSPACE_CONFIG_DEFAULT;
  if (NULL == config_opts) {
    config_opts = &default_config;
  }
  return mmb_manager_request_space(g_mmb_instance.manager, layer, ex_con, config_opts, out_space);
}

mmbError mmb_create_interface(mmbMemSpace *space,
                              const mmbMemInterfaceConfig *config_opts,
                              mmbMemInterface **out_interface)
{
  mmbMemInterfaceConfig default_config = MMB_MEMINTERFACE_CONFIG_DEFAULT;
  default_config.name = "";
  if (NULL == config_opts) {
    config_opts = &default_config;
  }
  return mmb_memspace_create_interface(space, config_opts, out_interface);
}

mmbError mmb_request_interface(mmbMemSpace *space,
                               const mmbMemInterfaceConfig *config_opts,
                               mmbMemInterface **out_interface)
{
  const mmbMemInterfaceConfig default_config = MMB_MEMINTERFACE_CONFIG_DEFAULT;
  if (NULL == config_opts) {
    config_opts = &default_config;
  }
  return mmb_memspace_request_interface(space, config_opts, out_interface);
}

mmbError mmb_destroy_interface(mmbMemInterface *interface)
{
  return mmb_memspace_destroy_interface(interface);
}

mmbError mmb_memspace_configure(const mmbMemSpaceConfig *config_opts,
                                mmbMemSpace *space)
{
  if (NULL == config_opts) {
    MMB_WARN("NULL pointer given for configure option, no change to be done.\n");
    return MMB_OK;
  }
  return mmb_memspace_configure_defaults(g_mmb_instance.manager, config_opts, space);
}

mmbError mmb_dump_memory_state(FILE *out_stream)
{
  return mmb_manager_report_state(g_mmb_instance.manager, out_stream);
}

mmbError mmb_get_hwloc_topology(hwloc_topology_t **topo) {
  return mmb_manager_get_topology(g_mmb_instance.manager, topo);
}

mmbError mmb_discovery_is_enabled(int *is_enabled)
{
  return mmb_manager_discovery_is_enabled(is_enabled);
}

mmbError mmb_statistics_get_current_statistics(mmbMemInterface *interface,
                                               size_t *n_allocations,
                                               size_t *bytes_allocated)
{
  if(!g_mmb_instance.is_initialized) {
    MMB_ERR("Mamba library not initialized\n");
    return MMB_ERROR;
  }
  return mmb_manager_get_interface_current_statistics(g_mmb_instance.manager,
                                                      interface, n_allocations,
                                                      bytes_allocated);
}

mmbError mmb_statistics_get_total_statistics(mmbMemInterface *interface,
                                             size_t *n_allocations,
                                             size_t *bytes_allocated)
{
  if(!g_mmb_instance.is_initialized) {
    MMB_ERR("Mamba library not initialized\n");
    return MMB_ERROR;
  }
  return mmb_manager_get_interface_total_statistics(g_mmb_instance.manager,
                                                    interface, n_allocations,
                                                    bytes_allocated);
}

mmbError mmb_statistics_get_record_count(size_t *count)
{
  if(!g_mmb_instance.is_initialized) {
    MMB_ERR("Mamba library not initialized\n");
    return MMB_ERROR;
  }
  return mmb_manager_get_stats_record_count(g_mmb_instance.manager, count);
}

#if HAVE_COST_MODEL
mmbError mmb_report_cost_model_state() {
 return mmb_cost_model_write_hw_devices(g_mmb_instance.cm_handle);
}
#endif

mmbError mmb_finalize(void) {
  mmbError stat = MMB_OK;

  mmb_state_lock();

  if(!g_mmb_instance.is_initialized) {
      MMB_ERR("Mamba library not initialized\n");
      stat = MMB_ERROR;
      goto UNLOCK_BAILOUT;
  }

#ifdef HAVE_COST_MODEL
  stat = mmb_cost_model_finalize(g_mmb_instance.cm_handle);
  if(stat != MMB_OK) {
    MMB_ERR("Failed to finalize cost model library\n");
    goto UNLOCK_BAILOUT;
  } 
  g_mmb_instance.cm_handle = NULL;
#endif

  stat = mmb_manager_destroy(g_mmb_instance.manager);
  if(stat != MMB_OK) {
    MMB_ERR("Failed to destroy memory manager\n");
    goto UNLOCK_BAILOUT;
  }
  g_mmb_instance.manager = NULL;

  stat = mmb_alloc_finalize();
  if (MMB_OK != stat) {
    MMB_ERR("Failed to finalize the mmbAllocationFactory.\n");
    goto UNLOCK_BAILOUT;
  }

  stat = mmb_finalize_providers();
  if (MMB_OK != stat) {
    MMB_ERR("Failed to finalize the memory provider libraries.\n");
    goto UNLOCK_BAILOUT;
  }

  stat = mmb_finalize_strategies();
  if (MMB_OK != stat) {
    MMB_ERR("Failed to finalize the memory allocation strategies.\n");
    goto UNLOCK_BAILOUT;
  }

#ifdef HAVE_LOOP_ANALYSIS
  stat = mmb_loop_finalize();
  if(stat != MMB_OK) {
    MMB_ERR("Failed to finalize mmb_loop\n");
    goto UNLOCK_BAILOUT;
  } 
#endif

  stat = mmb_logging_finalize();
  if (MMB_OK != stat) {
    MMB_ERR("Failed to finalize the logging infrastructure.\n");
    goto UNLOCK_BAILOUT;
  }

  g_mmb_instance.is_initialized = false;

UNLOCK_BAILOUT:
  mmb_state_unlock();

 return stat;
}

