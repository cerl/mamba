/*
 * Copyright (C) 2018-2020 Cray UK
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "mmb_tile_iterator.h"
#include "mmb_logging.h"
#include "mmb_error.h"

mmbError mmb_tile_iterator_create(mmbArray *in_mba, mmbTileIterator** out_it){
  mmbError stat = MMB_OK;
  /* Check we have a tiling */
  if(!in_mba)
    return MMB_INVALID_ARG;

  if(!in_mba->tiling)
    return MMB_ERROR;

  if(!out_it)
    return MMB_INVALID_ARG;

  /* Allocate new iterator */
  mmbTileIterator *it = (mmbTileIterator *)calloc(1, sizeof(mmbTileIterator));
  
  /* Initialise */
  it->schedule = MMB_ITERATOR_SCHEDULE_DEFAULT;
  it->src = in_mba;
  stat = mmb_index_resize(&it->idx, in_mba->dims.size);
  if(stat != MMB_OK){
    goto BAILOUT;
  }
  *out_it = it;
BAILOUT:
  return stat;
}

mmbError mmb_tile_iterator_first(mmbTileIterator * in_it,
				 mmbArrayTile **out_tile){
  mmbError stat = MMB_OK;
  if(!in_it)
    return MMB_ERROR;

  switch(in_it->schedule){
    case MMB_ITERATOR_SCHEDULE_DEFAULT:
      for(unsigned i = 0; i < in_it->idx.size; i++)
        in_it->idx.d[i] = 0;
    break;
    default:
      stat = MMB_ERROR;
      goto BAILOUT;
    break;
  }

  stat = mmb_tile_at(in_it->src, &in_it->idx, &in_it->tile); 
  *out_tile = in_it->tile;
  
BAILOUT:
  return stat;
}

mmbError mmb_tile_iterator_next(mmbTileIterator * in_it,
				mmbArrayTile **out_tile){
  mmbError stat = MMB_OK;
  if(!in_it)
    return MMB_ERROR;

  switch(in_it->schedule){
    case MMB_ITERATOR_SCHEDULE_DEFAULT:
    {
      unsigned i = in_it->idx.size;
      do{
        i--;
        in_it->idx.d[i]++;
        if(in_it->idx.d[i] == in_it->src->tiling->tile_count.d[i]){
            in_it->idx.d[i] = 0;
        } else {
            break;
        }
      } while (i > 0);
    }
    break;
    default:
      stat = MMB_ERROR;
      goto BAILOUT;
    break;
  }

  stat = mmb_tile_at(in_it->src, &in_it->idx, &in_it->tile); 
  *out_tile = in_it->tile;

BAILOUT:
  return stat;
}

mmbError mmb_tile_iterator_count(mmbTileIterator * in_it, size_t* count){
  *count = in_it->src->tiling->total;
  return MMB_OK;
}

mmbError mmb_tile_iterator_destroy(mmbTileIterator * in_it){
  mmbError stat = MMB_OK;
  if(!in_it)
    return MMB_OK;
  stat = mmb_index_resize(&in_it->idx, 0);
  if(stat != MMB_OK) {

    MMB_ERR("Unable to resize tile iterator index: %s\n", mmb_error_description(stat));
    goto BAILOUT;
  }
  free(in_it);
BAILOUT:
  return stat;
}
