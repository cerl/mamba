/*
 * Copyright (C) 2018-2021 Cray UK
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef MMB_MAMBA_H
#define MMB_MAMBA_H

#include <stdlib.h>
#include <stdbool.h>
#include <errno.h>
#include <stdio.h>
#include <pthread.h>

#include "mmb_layout.h"
#include "mmb_dimensions.h"
#include "mmb_error.h"
#include "mmb_tile.h"
#include "mmb_memory.h"
#include "mmb_options.h"
#include "mmb_numa_sizes.h"
#include "mmb_discovery.h"

#ifdef __cplusplus
extern "C" {
#endif

/* Types */
typedef struct s_mmbArray {
  mmbAllocation *allocation;
  mmbLayout *layout;
  mmbMemInterface *interface;
  mmbTiling *tiling;
  pthread_rwlock_t tiling_lock;
  mmbAccessType access;
  mmbDimensions dims;
  bool data_owned;
  pthread_mutex_t lock;
} mmbArray;

/**
 * mmb_init accepts a default initialisation argument
 */
#define MMB_INIT_DEFAULT NULL

/* Functions */

/**
 * @brief      Initialize the mamba library.
 *
 * @param      in_opts  Input mamba options, may be NULL for default.
 *
 * @return     \c MMB_OK on success.
 */
mmbError mmb_init(mmbOptions *in_opts);

/**
 * @brief      Check if mamba library is initialized.
 *
 * @return     \c MMB_OK on success.
 */
mmbError mmb_is_initialized(bool *initialized);

/**
 * @brief      Register a new memory tier in which memory can be allocated.
 *
 * This function creates a new mmbMemSpace which get added to the general
 * manager and returned to the user via the \p new_space handle.
 * The new space is initialised with a maximum size of \p n_mbytes
 * mibi-bytes (2^20) of \p layer memory.
 * WARNING : if two spaces are registered to the same mmbMemLayer,
 * \p mmb_request_space will always return the first mmbMemSpace allocated.
 *
 * @param      layer The memory tier to register.
 * @param      ex_context The execution context for the space.
 * @param      config_opts Configuration parameters for the mmbMemSpace.  May
 *                         be NULL in which case \c MMB_MEMSPACE_CONFIG_DEFAULT
 *                         is assumed.  The size is given in MiB (2^20 bytes).
 * @param      new_space Newly created space.
 *
 * @return     \c MMB_OK on success.
 */
mmbError mmb_register_memory(const mmbMemLayer layer,
                             const mmbExecutionContext ex_context,
                             const mmbMemSpaceConfig *config_opts,
                             mmbMemSpace **new_space);

/**
 * @brief      Register a new memory tier in which memory can be allocated.
 *
 * This function creates a new mmbMemSpace which get added to the general
 * manager and returned to the user via the \p new_space handle.
 * The new space is initialised with a maximum size of \p n_mbytes
 * mibi-bytes (2^20) of \p layer memory.
 * WARNING : if two spaces are registered to the same mmbMemLayer,
 * \p mmb_request_space will always return the first mmbMemSpace allocated.
 *
 * @param      layer The memory tier to register.
 * @param      ex_context The execution context for the space.
 * @param      config_opts Configuration parameters for the mmbMemSpace.  May
 *                         not be NULL as no \c MMB_MEMSPACE_CONFIG_DEFAULT
 *                         exists for NUMA aware memory.  The sizes are given
 *                         in MiB (2^20 bytes).
 * @param      new_space Newly created space.
 *
 * @return     \c MMB_OK on success.
 */
mmbError mmb_register_numa_memory(const mmbMemLayer layer,
                                  const mmbExecutionContext ex_context,
                                  const mmbMemSpaceConfig *config_opts,
                                  mmbMemSpace **new_space);

/**
 * @brief      Request a mmbMemSpace handle for the given layer.
 *
 * @param      layer Memory layer to which the space must correspond.
 * @param      ex_con Targeted execution layer, \c MMB_CPU for any CPU oriented
 *                    code, or \c MMB_GPU_CUDA or \c MMB_CPU_OPENCL for GPU
 *                    specific allocation.
 * @param      config_opts Configuration parameters for the mmbMemSpace.  May
 *                         be \c NULL in which case \c MMB_MEMSPACE_CONFIG_DEFAULT
 *                         is assumed.
 * @param      out_space Pointer to a mmbMemSpace handle.
 *
 * @return     \c MMB_OK on success.
 */
mmbError mmb_request_space(const mmbMemLayer layer,
                           const mmbExecutionContext ex_con,
                           const mmbMemSpaceConfig *config_opts,
                           mmbMemSpace **out_space);

/**
 * @brief      Create a new mmbMemInterface handle for the given layer.
 *
 * An interface requires a unique name to be requested later.  If \c NULL is
 * given as \p config_opts, an anonymous interface is created.
 *
 * @param      space Handle to a mmbMemSpace where the data will be allocated.
 * @param      config_opts Configuration parameters for the mmbMemInterface
 *                         May be \c NULL in which case
 *                         \c MMB_MEMINTERFACE_CONFIG_DEFAULT is assumed.
 * @param      out_interface Pointer to a mmbMemInterface handle.
 *
 * @return     \c MMB_OK on success.
 */
mmbError mmb_create_interface(mmbMemSpace *space,
                              const mmbMemInterfaceConfig *config_opts,
                              mmbMemInterface **out_interface);
/**
 * @brief      Request a mmbMemInterface handle for the given layer.
 *
 * A valid request config must contain a name.
 *
 * @param      space Handle to a mmbMemSpace where the data will be allocated.
 * @param      config_opts Configuration parameters for the mmbMemInterface
 *                         May be \c NULL in which case
 *                         \c MMB_MEMINTERFACE_CONFIG_DEFAULT is assumed.
 * @param      out_interface Pointer to a mmbMemInterface handle.
 *
 * @return     \c MMB_OK on success.
 */
mmbError mmb_request_interface(mmbMemSpace *space,
                               const mmbMemInterfaceConfig *config_opts,
                               mmbMemInterface **out_interface);

/**
 * @brief       Deallocate a mmbMemInterface created with
 *              \p mmb_create_interface.
 *
 * @param       interface Handle to the mmbMemInterface to destroy.
 * @return      \c MMB_OK on success.
 */
mmbError mmb_destroy_interface(mmbMemInterface *interface);

/**
 * @brief       Configure the default provider and strategy for the given
 *              \p space.
 *
 * Any modification to the mem_size has no effect and is disregarded.
 * Any value set to MMB_ANY_* is kept unchanged in the target \p space.
 * Any value set to MMB_<type>_DEFAULT reset the default value with regards to
 * the manager defaults values set during initialization.
 *
 * @param       config_opts Pointer to structure containing the values to
 *                          change.
 * @param       space       Handle to space structure to modify.
 *
 * @return     \c MMB_OK on success.
 */
mmbError mmb_memspace_configure(const mmbMemSpaceConfig *config_opts,
                                mmbMemSpace *space);
/**
 * @brief      Dump the state of the memory as recorded by the mamba library.
 *
 * @return     \c MMB_OK on success.
 */
mmbError mmb_dump_memory_state(FILE *out_stream);

mmbError mmb_statistics_get_current_statistics(mmbMemInterface *interface,
                                               size_t *n_allocations,
                                               size_t *bytes_allocated);

mmbError mmb_statistics_get_total_statistics(mmbMemInterface *interface,
                                             size_t *n_allocations,
                                             size_t *bytes_allocated);

mmbError mmb_statistics_get_record_count(size_t *count);

#if HAVE_COST_MODEL
mmbError mmb_report_cost_model_state();
#endif

/**
 * @brief      Finalize the mamba library.
 *
 * @return     \c MMB_OK on success.
 */
mmbError mmb_finalize(void);

/**
 * @brief      Create new mamba array, allocating memory for specified dimensions.
 *
 * @param[in]  in_dims      Dimensions of data.
 * @param[in]  in_layout    Layout of user data.
 * @param[in]  in_interface Memory interface to be used to allocate data.
 * @param[in]  in_access    Data access rights
 * @param[out] out_mba      The output mamba array.
 *
 * @return     \c MMB_OK on success.
 */
mmbError mmb_array_create(mmbDimensions *in_dims,
                          mmbLayout *in_layout,
                          mmbMemInterface *in_interface,
                          mmbAccessType in_access,
                          mmbArray **out_mba);

/**
 * @brief      Create new mamba array wrapping an existing user array.
 *
 * @param[in]  in_data      Data from user.
 * @param[in]  in_dims      Dimensions of data.
 * @param[in]  in_layout    Layout of user data.
 * @param[in]  in_interface Memory interface to be used to allocate data.
 * @param[in]  in_access    Data access rights
 * @param[out] out_mba      The output mamba array.
 *
 * @return     \c MMB_OK on success.
 */
mmbError mmb_array_create_wrapped(void *in_data,
                                  mmbDimensions *in_dims,
                                  mmbLayout *in_layout,
                                  mmbMemInterface *in_interface,
                                  mmbAccessType in_access,
                                  mmbArray **out_mba);

/**
 * @brief      Destroy a mamba array.
 *
 * @param      in_mba   The mamba array to be destroyed.
 *
 * @return     \c MMB_OK on success.
 */
mmbError mmb_array_destroy(mmbArray *in_mba);

/**
 * @brief      Copy data from source array to destination array.
 *
 * @param      dst   The destination array to copy to.
 * @param      src   The source array to copy from.
 *
 * @return     \c MMB_OK on success.
 */
mmbError mmb_array_copy(mmbArray *dst, mmbArray *src);

/**
 * @brief     Reshape array dimensions, 
 *            equivalent to free and alloc: does not keep data valid
 * 
 * @param     in_mba  The array to reshape
 * @param     in_dims The new array dimensions
 * @return    \c MMB_OK on success. 
 */
mmbError mmb_array_reshape(mmbArray *in_mba, mmbDimensions *in_dims);

/**
 * @brief      Create a tiling of mmbArray mba.
 *
 * @param[in]   in_mba   The mamba array to tile.
 * @param[in]   in_dims  The tile dimensions.
 *
 * @return     \c MMB_OK on success.
 */
mmbError mmb_array_tile(mmbArray *in_mba, mmbDimensions *in_dims);

/**
 * @brief      Create a single tile tiling of mmbArray mba and return the tile
 *
 * @param[in]   in_mba   The mamba array to tile.
 * @param[out]  out_tile The single tile output
 *
 * @return     \c MMB_OK on success.
 */
mmbError mmb_array_tile_single(mmbArray *in_mba, mmbArrayTile **out_tile);

/**
 * @brief      Remove the current tiling of mmbArray in_mba.
 *
 * @param      in_mba  The mamba array to remove tiling on.
 *
 * @return     \c MMB_OK on success.
 */
mmbError mmb_array_untile(mmbArray *in_mba);

/**
 * @brief Check if array is currently tiled
 * 
 * @param[in]   in_mba          The mamba array input
 * @param[out]  out_is_tiled    The output boolean, true if tiles
 *
 * @return     \c MMB_OK on success.
 */
mmbError mmb_array_is_tiled(mmbArray *in_mba, bool *out_is_tiled);

/**
 * @brief      Write description of mmbArray to stream output
 *
 * @param      in_mba        The array to describe
 * @param      in_stream     The stream to output to 
 *
 * @return     \c MMB_OK on success.
 */
mmbError mmb_array_describe(mmbArray *in_mba, FILE* in_stream);

/**
 * @brief Check if array is empty 
 * 
 * @param      in_mba        The array to check
 * @param      out_empty     The return value, true if empty
 *
 * @return     \c MMB_OK on success.
 */
mmbError mmb_array_is_empty(mmbArray *in_mba, bool *out_empty);

/**
 * @brief Access the array's layout
 *
 * @param       mba         The array to introspect.
 * @param       layout      The return value.
 *
 * @return \c MMB_OK on success.
 */
mmbError mmb_array_get_layout(mmbArray *mba, mmbLayout **layout);

/**
 * @brief Provide a pointer to the dimension structure used for the dimension.
 *
 * This function provide access to the dimension, although at the cost of
 * losing the mutex protection.  Thus the pointer returned is `const` to
 * mitigate the issues of concurrent access.
 *
 * @param       mba         The array to introspect.
 * @param       dimensions  The return value.
 *
 * @return \c MMB_OK on success.
 */
mmbError mmb_array_get_dimensions(mmbArray *mba, mmbDimensions **dimensions);

/**
 * @brief Copy into \p *dimensions a duplicate of the array's dimensions.
 *
 * @param       mba         The array to introspect.
 * @param       dimensions  The return value.
 *
 * @return \c MMB_OK on success.
 */
mmbError mmb_array_copy_dimensions(mmbArray *mba,
                                   mmbDimensions *dimensions);

/**
 * @brief Access the array's data's memory layer.
 *
 * @param       mba         The array to introspect.
 * @param       layer       The return value.
 *
 * @return \c MMB_OK on success.
 */
mmbError mmb_array_get_layer(const mmbArray *mba, mmbMemLayer *layer);

/**
 * @brief Access the array's data's execution context.
 *
 * @param       mba         The array to introspect.
 * @param       ex_con      The return value.
 *
 * @return \c MMB_OK on success.
 */
mmbError mmb_array_get_execution_context(const mmbArray *mba,
                                         mmbExecutionContext *ex_con);

/**
 * @brief Access the array's data's memory space
 *
 * @param       mba         The array to introspect.
 * @param       space       The return value.
 *
 * @return \c MMB_OK on success.
 */
mmbError mmb_array_get_space(mmbArray *mba, mmbMemSpace **space);

/**
 * @brief Access the array's data's memory provider.
 *
 * @param       mba         The array to introspect.
 * @param       provider    The return value.
 *
 * @return \c MMB_OK on success.
 */
mmbError mmb_array_get_provider(const mmbArray *mba, mmbProvider *provider);

/**
 * @brief Access the array's data's allocation strategy.
 *
 * @param       mba         The array to introspect.
 * @param       strategy    The return value.
 *
 * @return \c MMB_OK on success.
 */
mmbError mmb_array_get_strategy(const mmbArray *mba, mmbStrategy *strategy);

/**
 * @brief Access the array's data's memory interface
 *
 * @param       mba         The array to introspect.
 * @param       space       The return value.
 *
 * @return \c MMB_OK on success.
 */
mmbError mmb_array_get_interface(mmbArray *mba, mmbMemInterface **interface);

/**
 * @brief Access the array's access type flags.
 *
 * @param       mba         The array to introspect.
 * @param       access      The return value.
 *
 * @return \c MMB_OK on success.
 */
mmbError mmb_array_get_access_type(const mmbArray *mba, mmbAccessType *access);

/**
 * @brief Check whether the array owns the data wrapped.
 *
 * @param       mba         The array to introspect.
 * @param       data_owned  The return value.
 *
 * @return \c MMB_OK on success.
 */
mmbError mmb_array_owns_data(const mmbArray *mba, bool *data_owned);

/**
 * @brief      Get number of tiles.
 *             TODO: 1d - should have version for tile count in each dimension
 *
 * @param[in]  in_mba      The tiled mamba array to count.
 * @param[out] out_count    The output number of tiles.
 *
 * @return     \c MMB_OK on success.
 */
mmbError mmb_tile_count(mmbArray *in_mba, int *out_count);


/**
 * @brief      Query the number of tiles in each dimension of the active tiling.
 *
 * @param      in_mba    In tiled mamba array.
 * @param      out_dims  The output dimensions.
 *
 * @return     \c MMB_OK on success.
 */
mmbError mmb_tiling_dimensions(mmbArray *in_mba, mmbDimensions **out_dims);

/**
 * @brief      Get tile at location defined by in_idx.
 *
 *
 * @param[in]      in_mba   The tiled mamba array.
 * @param[in]      in_idx   The index of the tile to return.
 * @param[out]     out_tile The output tile at index in_idx.
 *
 * @return         \c MMB_OK on success.
 */
mmbError mmb_tile_at(mmbArray *in_mba, mmbIndex *in_idx, 
                     mmbArrayTile **out_tile);

mmbError mmb_tile_at_1d(mmbArray *in_mba, size_t in_idx,
                     mmbArrayTile **out_tile);

mmbError mmb_tile_at_2d(mmbArray *in_mba, size_t in_idx0, size_t in_idx1,
                     mmbArrayTile **out_tile);

mmbError mmb_tile_at_3d(mmbArray *in_mba, size_t in_idx0, size_t in_idx1,
                        size_t in_idx2, mmbArrayTile **out_tile); 

/**
 * @brief      Get tile at location defined by \p in_idx, into memory
 *             corresponding to the interface \p in_interface.
 *
 *
 * @param[in]      in_mba       The tiled mamba array.
 * @param[in]      in_idx       The index of the tile to return.
 * @param[in]      in_interface The memory interface to use to create the new
 *                              tiles.
 * @param[out]     out_tile     The output tile at index in_idx.
 *
 * @return         \c MMB_OK on success.
 */
mmbError mmb_tile_duplicate_at(mmbArray *in_mba, mmbIndex *in_idx,
                               mmbMemInterface *in_interface,
                               mmbArrayTile **out_tile);

/**
 * @brief Return a printable string detailing the mamba version
 * 
 * @return const char* 
 */
const char* mmb_version(void);


/**
 * @brief      Cast a mmbAllocation into a void* buffer to directly
 *             access the data.
 *
 * @param      alloc Pointer to the mmbAllocation.
 *
 * @return a pointer to the corresponding buffer.
 */
#define MMBALLOC2BUF(alloc) MMBALLOC2TBUF(alloc, void *)

/**
 * @brief      Cast a mmbAllocation into a buffer of type \p type
 *             to directly access the data.
 *
 * @param      alloc Pointer to the mmbAllocation.
 * @param      type Ty.
 *
 * @return a pointer to the corresponding buffer.
 */
#ifdef __cplusplus
#define MMBALLOC2TBUF(alloc, type) (*reinterpret_cast<type *>(alloc))
#else
#define MMBALLOC2TBUF(alloc, type) (*(type *)(alloc))
#endif

/**
 * @brief      Cast a buffer as a *FAKE* mmbAllocation (no information
 *             set). This macro is only to be used in conjuction with
 *             \p MMBALLOC2BUF or \p MMBALLOC2TBUF .
 *
 * @param      buffer Buffer to be wrapped.
 *
 * @return a properly casted fake mmbAllocation handle.
 */
#ifdef __cplusplus
#define BUF2MMBALLOC(buf) reinterpret_cast<mmbAllocation *>(&(buf))
#else
#define BUF2MMBALLOC(buf) ((mmbAllocation *)(&(buf)))
#endif

/**
 * @brief      3D normalised indexing into a tile object.
 *             Assumes tile indices extend from 0 to tile->dim.
 *
 * @param      T     Tile object.
 * @param      I     Outer dimension iterator.
 * @param      J     Middle dimension iterator.
 * @param      K     Inner dimension iterator.
 * @param      TYPE  The pointer type to cast to during indexing.
 *
 * @return     Element of tile at \p I, \p J, \p K .
 */
#define MMB_IDX_3D_NORM(T, I, J, K, TYPE)                                   \
  MMBALLOC2TBUF((T)->allocation, TYPE *)[MMB_IDX_3D_NORM_GET_INDEX(T, I, J, K)]

#define MMB_IDX_3D_NORM_GET_INDEX(T, I, J, K)                               \
  ((((I) + (T)->lower[0]) * (T)->abs_dim[1] + (J) + (T)->lower[1]) * (T)->abs_dim[2] + (T)->lower[2] + (K))

static inline const void *
mmb_idx_3d_norm(const mmbArrayTile *tile, size_t dim1, size_t dim2, size_t dim3, size_t base_type_size)
{
  return &MMBALLOC2TBUF(tile->allocation, unsigned char *)[base_type_size * (MMB_IDX_3D_NORM_GET_INDEX(tile, dim1, dim2, dim3))];
}

/**
 * @brief      3D  indexing into a tile object.
 *             Assumes tile indices extend from tile->lower to tile->upper.
 *
 * @param      T     Tile object.
 * @param      I     Outer dimension iterator.
 * @param      J     Middle dimension iterator.
 * @param      K     Inner dimension iterator.
 * @param      TYPE  The pointer type to cast to during indexing.
 *
 * @return     Element of tile at \p I, \p J, \p K .
 */
#define MMB_IDX_3D(T, I, J, K, TYPE)                                        \
  MMBALLOC2TBUF((T)->allocation, TYPE *)[MMB_IDX_3D_GET_INDEX(T, I, J, K)]

#define MMB_IDX_3D_GET_INDEX(T, I, J, K) (((I) * (T)->abs_dim[1] + (J)) * (T)->abs_dim[2] + (K))

static inline const void *
mmb_idx_3d(const mmbArrayTile *tile, size_t dim1, size_t dim2, size_t dim3, size_t base_type_size)
{
  return &MMBALLOC2TBUF(tile->allocation, unsigned char *)[base_type_size * (MMB_IDX_3D_GET_INDEX(tile, dim1, dim2, dim3))];
}

/**
 * @brief      2D normalised indexing into a tile object.
 *             Assumes tile indices extend from 0 to tile->dim.
 *
 * @param      T     Tile object.
 * @param      I     Outer dimension iterator.
 * @param      J     Inner dimension iterator.
 * @param      TYPE  The pointer type to cast to during indexing.
 *
 * @return     Element of tile at \p I, \p J .
 */
#define MMB_IDX_2D_NORM(T, I, J, TYPE)                                      \
  MMBALLOC2TBUF((T)->allocation, TYPE *)[MMB_IDX_2D_NORM_GET_INDEX(T, I, J)]

#define MMB_IDX_2D_NORM_GET_INDEX(T, I, J)                                  \
  (((I) + (T)->lower[0]) * (T)->abs_dim[1] + (T)->lower[1] + (J))

static inline const void *
mmb_idx_2d_norm(const mmbArrayTile *tile, size_t dim1, size_t dim2, size_t base_type_size)
{
  return &MMBALLOC2TBUF(tile->allocation, unsigned char *)[base_type_size * (MMB_IDX_2D_NORM_GET_INDEX(tile, dim1, dim2))];
}

/**
 * @brief      2D  indexing into a tile object.
 *             Assumes tile indices extend from tile->lower to tile->upper.
 *
 * @param      T     Tile object.
 * @param      I     Outer dimension iterator.
 * @param      J     Inner dimension iterator.
 * @param      TYPE  The pointer type to cast to during indexing.
 *
 * @return     Element of tile at \p I, \p J .
 */
#define MMB_IDX_2D(T, I, J, TYPE)                                           \
  MMBALLOC2TBUF((T)->allocation, TYPE *)[MMB_IDX_2D_GET_INDEX(T, I, J)]

#define MMB_IDX_2D_GET_INDEX(T, I, J) ((I) * (T)->abs_dim[1] + (J))

static inline const void *
mmb_idx_2d(const mmbArrayTile *tile, size_t dim1, size_t dim2, size_t base_type_size)
{
  return &MMBALLOC2TBUF(tile->allocation, unsigned char *)[base_type_size * (MMB_IDX_2D_GET_INDEX(tile, dim1, dim2))];
}

/**
 * @brief      1D indexing into tile object.
 *
 * @param      T     Tile object.
 * @param      I     1D iterator.
 * @param      TYPE  The pointer type to cast to during indexing.
 *
 * @return     { description_of_the_return_value }
 */
#define MMB_IDX_1D(T, I, TYPE)                                           \
  MMBALLOC2TBUF((T)->allocation, TYPE *)[(I)]

static inline const void *
mmb_idx_1d(const mmbArrayTile *tile, size_t dim1, size_t base_type_size)
{
  return &MMB_IDX_1D(tile, dim1 * base_type_size, unsigned char);
}

#ifdef __cplusplus
}
#endif

#endif /* MMB_MAMBA_H */
