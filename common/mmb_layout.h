/*
 * Copyright (C) 2018-2020 Cray UK
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef MMB_LAYOUT_H
#define MMB_LAYOUT_H

#include <stdlib.h>

#include "mmb_dimensions.h"
#include "mmb_error.h"

#ifdef __cplusplus
extern "C" {
#endif

enum e_mmbAccessType {
  MMB_READ = 1,
  MMB_WRITE = 2,
  MMB_READ_WRITE = 3,
  MMB_WRITE_LOCKED = 4,
  MMB_CONTIGUOUS = 8,
  MMB_ACCESSTYPE_MAX
};

enum e_mmbLayoutElementType {
  MMB_LAYOUT_ELEMENTTYPE_NONE = 0,
  MMB_ELEMENT,
  MMB_STRUCTURE,
  MMB_SOA,
  MMB_LAYOUT_ELEMENTTYPE_MAX
};

enum e_mmbLayoutEquivalence {
  MMB_LAYOUT_EQUAL = 0,
  MMB_LAYOUT_DIFF_INDEX,
  MMB_LAYOUT_DIFF_FIELDS,
  MMB_LAYOUT_DIFF_TYPES,
  MMB_LAYOUT_DIFF_MAX
};

enum e_mmbLayoutOrder {
  MMB_LAYOUT_ORDER_NONE = 0,
  MMB_ROWMAJOR,
  MMB_COLMAJOR,
  MMB_GENERIC_ND,
  MMB_LAYOUT_ORDER_MAX
};

enum e_mmbLayoutType {
  MMB_LAYOUT_NONE = 0,
  MMB_REGULAR,
  MMB_REGULAR_BLOCK,
  MMB_IRREGULAR,
  MMB_LAYOUT_TYPE_MAX
};


typedef enum e_mmbAccessType mmbAccessType;
typedef enum e_mmbLayoutElementType mmbLayoutElementType;
typedef enum e_mmbLayoutOrder mmbLayoutOrder;
typedef enum e_mmbLayoutType mmbLayoutType;
typedef enum e_mmbLayoutEquivalence mmbLayoutEquivalence;

typedef struct mmbLayoutElement {
  mmbLayoutElementType type;
  size_t size_bytes;
  mmbLayoutOrder order;
  /* Structs */
  // size_t n_members;
  // size_t *member_size_bytes;
} mmbLayoutElement;

typedef struct mmbLayoutBlock {
  mmbLayoutOrder order;
  /* Regular Block-centric */
  mmbDimensions dimensions;
} mmbLayoutBlock;

typedef struct mmbLayoutIrregular {
  /* Irregular 1d
   TODO: irregular ND... */
  mmbDimensions n_blocks;
  size_t *offsets;
  size_t *lengths;
} mmbLayoutIrregular;

typedef struct mmbLayoutPadding {
  int element_is_padded;
  size_t element_pre_pad_bytes;
  size_t element_post_pad_bytes;
  int dimension_is_padded;
  size_t *dimension_pre_pad_bytes;
  size_t *dimension_post_pad_bytes;
} mmbLayoutPadding;

typedef struct mmbLayout {
  mmbLayoutType type;
  size_t n_dims;
  size_t index;
  mmbLayoutElement element;
  mmbLayoutPadding pad;
  union {
    mmbLayoutBlock block;
    mmbLayoutIrregular irregular;
  };
} mmbLayout;

/**
  * Describes the overlap between src-dst mmbLayout pieces
  */
typedef struct mmbLayoutOverlap {
  size_t src_offset;
  size_t dst_offset;
  size_t length;
} mmbLayoutOverlap;

/**
  * Describes how a src layout intersects with a certain dst layout piece
  * the length of mmbLayoutIntersection is equal to the number of src layout pieces
  */
typedef struct mmbLayoutIntersection {
  size_t n_dst_pieces;
  size_t n_src_pieces;
  mmbLayoutOverlap *overlap;
} mmbLayoutIntersection;



/**
 * mmbLayout constructors accept a default 0 padding argument
 */
#define MMB_PADDING_NONE \
  mmb_layout_padding_create_zero()

/* Stringifyers */
const char* mmb_layout_type_to_str(int type);
const char* mmb_element_type_to_str(int type);
const char* mmb_layout_order_to_str(int order);
const char* mmb_layout_equivalence_to_str(int result);

/* Construct default initialisation option */
mmbLayoutPadding *mmb_layout_padding_create_zero(void);

mmbError mmb_layout_alloc(mmbLayout **out_layout);

mmbError mmb_layout_create_regular_1d(size_t element_size_bytes,
                                   mmbLayoutPadding *pad,
                                   mmbLayout **out_layout);

mmbError mmb_layout_create_regular_nd(size_t element_size_bytes, size_t n_dims,
                                   mmbLayoutOrder element_order,
                                   mmbLayoutPadding *pad,
                                   mmbLayout **out_layout);

mmbError mmb_layout_create_block(size_t n_dims, size_t element_size_bytes,
                              mmbLayoutOrder element_order,
                              mmbDimensions *block_size,
                              mmbDimensions *block_order,
                              mmbLayoutPadding *pad,
                              mmbLayout **out_layout);

mmbError mmb_layout_create_irregular_1d(size_t element_size_bytes,
                                     size_t *offsets,
                                     size_t *sizes,
                                     mmbLayout **out_layout);

mmbError mmb_layout_create_dist_irregular_1d(size_t element_size_bytes,
                                      size_t index,
                                      size_t n_blocks,
                                      size_t *offsets,
                                      size_t *lengths,
                                      mmbLayout **out_layout);

                                
/**
 * @brief Cook up a default distributed, which consists of one piece holding
 *    all the data from all pieces ... not really distributed.
 *  
 * @param src_layout a layout describes one piece of the data 
 *                   that will be used as a blueprint to generate the default layout
 * @param default_layout output default layout 
 * @return MMB_OK on success 
 */
mmbError mmb_layout_dist_create_default_layout(mmbLayout *src_layout, mmbLayout **default_layout);

/**
 * @brief find the size in bytes of all the data that are distributed and described 
 *      the input layout
 * 
 * @param dist_layout input distributed layout
 * @param size  size in bytes
 * @return MMB_OK on success 
 */
mmbError mmb_layout_dist_find_entire_size(mmbLayout *dist_layout, int64_t *size);

/**
 * @brief find the size in bytes of a piece in a distributed layout
 * 
 * @param dist_layout input distributed layout
 * @param size  output size in bytes of the piece described by the first parameter
 * @return MMB_OK on success 
 */
mmbError mmb_layout_dist_find_piece_size(mmbLayout *dist_layout, int64_t *size);


mmbError mmb_layout_create_copy(mmbLayout *in_layout, mmbLayout **out_layout);


mmbError mmb_layout_compute_intersection(mmbLayout *src_layout,
                                      mmbLayout *dst_layout,
                                      mmbLayoutIntersection **out_li);

mmbError mmb_layout_destroy_mmbLayoutIntersection(mmbLayoutIntersection *in_li);

mmbError mmb_layout_compute_overlap(mmbLayout *src_layout,
                                mmbLayout *dst_layout,
                                mmbLayoutIntersection **out_li);
/**
 * @brief      Free layout object
 *
 * @param      in_layout  In layout to free
 *
 * @return      MMB_OK on success
 */
mmbError mmb_layout_destroy(mmbLayout *in_layout);


/**
 * @brief      Copy a layout from src to dst, both are expected to be allocated
 *
 * @param      in_src   The source layout
 * @param      in_dst   The destination layout
 *
 * @return     MMB_OK on success
 */
mmbError mmb_layout_copy(mmbLayout *dst, mmbLayout *src);

mmbError mmb_layout_cmp(mmbLayout *in_layout0, mmbLayout *in_layout1, mmbLayoutEquivalence* diff);
mmbError mmb_layout_buffer_size(mmbLayout *in_layout, mmbDimensions *dim,
                                size_t * total_bytes);
mmbError mmb_layout_buffer_size_nd(mmbLayout *in_layout, mmbDimensions *dim,
                                   size_t n_dims, size_t * total_bytes);
mmbError mmb_layout_buffer_pitch_nd(mmbLayout *in_layout, mmbDimensions *dim,
                                    size_t n_dims, size_t *pitch);

mmbError mmb_layout_element_get_size(mmbLayoutElement *elt, mmbLayoutPadding *pad,
                                     size_t *size);
mmbError mmb_layout_element_copy(mmbLayoutElement *dst, mmbLayoutElement *src);
mmbError mmb_layout_element_cmp(mmbLayoutElement *le0, mmbLayoutElement *le1, int *diff);
mmbError mmb_layout_padding_copy(mmbLayoutPadding *dst, mmbLayoutPadding *src, size_t ndim);
mmbError mmb_layout_padding_cmp(mmbLayoutPadding *lp0, mmbLayoutPadding *l10,
                                size_t ndim, int *diff);
mmbError mmb_layout_block_copy(mmbLayoutBlock *dst, mmbLayoutBlock *src);
mmbError mmb_layout_irregular_copy(mmbLayoutIrregular *dst, mmbLayoutIrregular *src);
mmbError mmb_layout_irregular_cmp(mmbLayoutIrregular *le0, mmbLayoutIrregular *le1, int *diff);

/**
 * @brief Copy \p layout into \p buffer.
 *
 * This function purpose is to deep-copy the mmbLayout \p layout in the buffer
 * \p buffer of size returned by the function \p mmb_layout_get_sizeof.  This
 * function main objective is to be able to transfer in one copy a layout and
 * all its dependances to a device which memory is not writtable directly from
 * the processor (e.g., GDRAM).  The last parameter \p base_addr is the base
 * address of the final allocation in order to update the internal links of the
 * layout structure so the object is ready and in a coherent state once used on
 * the target memory.  Its value can be \c NULL in which case the base address
 * considered is \p buffer.
 *
 * @param [out] buffer      Destination of the copy
 * @param [in]  layout      Source layout to be deep copied
 * @param [in]  base_addr   Base address of the final layout.  May be \c NULL.
 *
 * @return \c MMB_OK on success, an error type otherwise.
 */
mmbError mmb_layout_pack(void *buffer, const mmbLayout *layout, void *base_addr);

/**
 * @brief Compute the size of the \c mmbLayout structure and its dependencies.
 *
 * @param [in]  layout      Layout which size if to be determined.
 * @param [out] out_size    Size of the structure and its dependencies.
 *
 * @return \c MMB_OK on success, an error type otherwise.
 */
mmbError mmb_layout_get_sizeof(const mmbLayout *layout, size_t *out_size);

#ifdef __cplusplus
}
#endif


#endif
