/*
 * Copyright (C) 2020      Cray UK
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <stdlib.h>

#include "mmb_error.h"
#include "mmb_logging.h"
#include "mamba.h"
#include "mmb_memory_options.h"
#include "../memory/memory_options.h"


mmbError mmb_options_create_default(mmbOptions **out_opts){
  mmbError stat = MMB_OK;

  if(!out_opts) {
    MMB_ERR("mmbOptions** may not be NULL\n");
    stat = MMB_INVALID_ARG;
    goto BAILOUT;
  }

  mmbOptions *opt = malloc(sizeof(mmbOptions));
  if(!opt) {
    MMB_ERR("Out of memory\n");
    stat = MMB_OUT_OF_MEMORY;
    *out_opts = NULL;
    goto BAILOUT;
  }

 stat = mmb_logging_options_create_default(&opt->logging_opts);
 if(stat != MMB_OK) {
   MMB_ERR("Failed to create default logging options\n");
   goto BAILOUT;
 }

 stat = mmb_memory_options_create_default(&opt->memory_opts);
 if(stat != MMB_OK) {
   MMB_ERR("Failed to create default logging options\n");
   goto BAILOUT;
 }

 *out_opts = opt;

BAILOUT:
  return stat;
}

mmbError mmb_options_destroy(mmbOptions *opts)
{
  mmbError stat = MMB_OK;
  if (NULL == opts) {
    MMB_ERR("Invalid reference or object already free'd.\n");
    stat = MMB_INVALID_ARG;
    goto BAILOUT;
  }
  stat = mmb_logging_options_destroy(opts->logging_opts);
  if (MMB_OK != stat) {
    MMB_WARN("Unable to properly destroy logging options.\n");
  }
  stat = mmb_memory_options_destroy(opts->memory_opts);
  if (MMB_OK != stat) {
    MMB_WARN("Unable to properly destroy logging options.\n");
  }
  free(opts);
BAILOUT:
  return stat;
}

/* Provide passthroughs for memory options too */
mmbError mmb_options_set_debug_level(mmbOptions *opts, int level){
  mmbError stat = MMB_OK;
  if(!opts){
    MMB_ERR("mmbOptions** may not be NULL\n");
    stat = MMB_INVALID_ARG;
    goto BAILOUT;
  }
  stat = mmb_logging_options_set_debug_level(opts->logging_opts, level);
BAILOUT:
  return stat;
}

mmbError mmb_options_set_user_log_func(mmbOptions *opts, mmbUserLogFunc user_log_func){
  mmbError stat = MMB_OK;
  if(!opts){
    MMB_ERR("mmbOptions** may not be NULL\n");
    stat = MMB_INVALID_ARG;
    goto BAILOUT;
  }
  stat = mmb_logging_options_set_user_log_func(opts->logging_opts, user_log_func);
BAILOUT:
  return stat;
}

mmbError
mmb_options_strategy_pooled_set_pool_size(mmbOptions *opts, size_t pool_size)
{
  mmbError stat = MMB_OK;
  /* Check parameters */
  if (NULL == opts) {
    MMB_ERR("Invalid handle to initialization options. Cannot be NULL.\n");
    stat = MMB_INVALID_ARG;
    goto BAILOUT;
  }
  mmbMemoryOptions *mem_opts = opts->memory_opts;
  stat = mmb_memory_options_strategy_pooled_set_pool_size(mem_opts, pool_size);
BAILOUT:
  return stat;
}
