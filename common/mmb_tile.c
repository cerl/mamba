/*
 * Copyright (C) 2018-2020 Cray UK
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <assert.h>
#include <string.h>

#include "mmb_tile.h"
#include "mmb_logging.h"
#include "mmb_memory.h"
#include "mamba.h"

#include "i_memory.h"

/* protecting array state */
static inline void mmb_tiling_rdlock(mmbArray *mba) {
  int stat = pthread_rwlock_rdlock(&mba->tiling_lock);
  assert(stat==0);
  (void) stat;
}

static inline void mmb_tiling_wrlock(mmbArray *mba) {
  int stat = pthread_rwlock_wrlock(&mba->tiling_lock);
  assert(stat==0);
  (void) stat;
}

static inline void mmb_tiling_unlock(mmbArray *mba) {
  int stat = pthread_rwlock_unlock(&mba->tiling_lock);
  assert(stat==0);
  (void) stat;
}

static inline mmbError nd_to_1d_idx(mmbIndex *in_idx, mmbDimensions * in_dims,
                                    size_t *out_idx){
  /* Validity and range checks */
  if(!in_idx || !in_dims || !out_idx)
    return MMB_INVALID_ARG;

  if(in_idx->size != in_dims->size){
    MMB_ERR("Index size %zu does not match dimensions size %zu\n", in_idx->size, in_dims->size);
    return MMB_ERROR;
  }

  for (size_t i = 0; i < in_idx->size; i++)
    if(in_idx->d[i] >= in_dims->d[i]) {
      MMB_ERR("Index %zu out of bounds for dimension %u size %zu\n", in_idx->d[i], i, in_dims->d[i]);
      return MMB_ERROR;
    }

  /* Convert to 1d */
  *out_idx = 0;
  for (size_t d = 0; in_idx->size > d; ++d) {
    *out_idx *= in_dims->d[d];
    *out_idx += in_idx->d[d];
  }
  return MMB_OK;
}


static inline void mmb_tile_lock(mmbArrayTile *t) {
  int stat = pthread_mutex_lock(&t->lock);
  assert(stat==0);
  (void) stat;
}
static inline void mmb_tile_unlock(mmbArrayTile *t) {
  int stat = pthread_mutex_unlock(&t->lock);
  assert(0 == stat);
  (void) stat;
}

mmbError mmb_tile_create_1d(mmbArray *in_mba, size_t in_idx,
                            mmbTileOptions *in_opts, mmbArrayTile **out_tile) {
  mmbIndex idx = {1, &in_idx};
  return mmb_tile_create(in_mba, &idx, in_opts, out_tile);
}

mmbError mmb_tile_create_2d(mmbArray *in_mba, size_t in_idx0, size_t in_idx1,
                            mmbTileOptions *in_opts, mmbArrayTile **out_tile) {
  size_t idx_arr[2] = {in_idx0, in_idx1};
  mmbIndex idx = {2, &idx_arr[0]};
  return mmb_tile_create(in_mba, &idx, in_opts, out_tile);
}

mmbError mmb_tile_create_3d(mmbArray *in_mba, size_t in_idx0, size_t in_idx1,
                            size_t in_idx2, mmbTileOptions *in_opts,
                            mmbArrayTile **out_tile) {
  size_t idx_arr[3] = {in_idx0, in_idx1, in_idx2};
  mmbIndex idx = {3, &idx_arr[0]};
  return mmb_tile_create(in_mba, &idx, in_opts, out_tile);
}

mmbError mmb_tile_create(mmbArray *in_mba, mmbIndex* in_idx,
                         mmbTileOptions *in_opts, mmbArrayTile **out_tile) {
  /* Validate args */
  if(in_mba == NULL)
    return MMB_INVALID_ARG;

  if(in_idx == NULL)
    return MMB_INVALID_ARG;

  if(out_tile == NULL)
    return MMB_INVALID_ARG;

  mmbError stat = MMB_OK;

  mmbTileOptions default_opts = {0};
  /* TODO: DEFAULT */
  if(in_opts == NULL){
     in_opts = &default_opts;
  }
  mmbArrayTile *t = NULL;

  /* Lookup in hashmap for tile at in_idx */
  size_t tile_idx = 0;
  stat = nd_to_1d_idx(in_idx, &in_mba->tiling->tile_count, &tile_idx);
  if(stat != MMB_OK) {
    MMB_ERR("Failed to convert nd index to 1d index\n");
    goto BAILOUT;
  }

  /* Check if we should use the array cache */
  bool tile_exists = false;
  if(in_opts->cache_policy != MMB_TILECACHE_UNUSED) {
    mmb_tiling_rdlock(in_mba);
    mmb_hash_iter_t k = mmb_hash_get_array_tiles(in_mba->tiling->tiles, tile_idx);
    if(k != mmb_hash_end_array_tiles(in_mba->tiling->tiles)) {
      t = mmb_hash_value_array_tiles(in_mba->tiling->tiles, k);
    /* If found, move to top tile of the stack and return pointer */
      while (t->next != NULL){
        t = t->next;
      }
      stat = MMB_OK;
      tile_exists = true;
    }
    mmb_tiling_unlock(in_mba);
  }

  /* Create tile if it wasnt found */
  if(!tile_exists) {
    // Create tile
    t = (mmbArrayTile *)calloc(1, sizeof(mmbArrayTile));
    if(!t) {
      stat = MMB_OUT_OF_MEMORY;
      goto BAILOUT;
    }

    size_t *dims = malloc(sizeof(size_t[6][in_mba->layout->n_dims]));
    if (NULL == dims) {
      free(t);
      stat = MMB_OUT_OF_MEMORY;
      goto BAILOUT;
    }
    t->lower   = &dims[0 * in_mba->layout->n_dims];
    t->upper   = &dims[1 * in_mba->layout->n_dims];
    t->alower  = &dims[2 * in_mba->layout->n_dims];
    t->aupper  = &dims[3 * in_mba->layout->n_dims];
    t->dim     = &dims[4 * in_mba->layout->n_dims];
    t->abs_dim = &dims[5 * in_mba->layout->n_dims];

    t->interface = in_mba->interface;
    t->access = in_mba->access;
    t->cache_policy = in_opts->cache_policy;
    /* TODO: Could just use in_mba->layout pointer instead of duplicating? */
    stat = mmb_layout_create_copy(in_mba->layout, &t->layout);
    if (stat != MMB_OK) {
      MMB_ERR("Failed to copy-construct mmbLayout\n");
      free(t);
      free(dims);
      goto BAILOUT;
    }

    for (unsigned i = 0; i < t->layout->n_dims; i++) {
      t->abs_dim[i] = in_mba->dims.d[i];
      t->lower[i] = in_idx->d[i] * in_mba->tiling->tile_size.d[i];
      t->upper[i] = (in_idx->d[i] + 1) * in_mba->tiling->tile_size.d[i];
      if (t->upper[i] > t->abs_dim[i])
        t->upper[i] = t->abs_dim[i];
      t->alower[i] = t->lower[i];
      t->aupper[i] = t->upper[i];
      t->dim[i] = t->upper[i] - t->lower[i];
    }

    /* Check contiguity */
    /* TODO: This only checks if tile is 1d, or if it covers whole array,
    * not if subtiles are contiguous */
    int contiguous = 1;
    if(t->layout->n_dims > 1) {
      for (unsigned i = 0; i < t->layout->n_dims; i++) {
        if(t->abs_dim[i] != t->dim[i]) {
          contiguous = 0;
        }
      }
    }

    if(contiguous) {
      t->access |= MMB_CONTIGUOUS;
    }

    /* This pointer is offset using indexing macros */
    t->allocation = in_mba->allocation;

    /* Set tile index for hashmap */
    t->tidx = tile_idx;

    /* Initialise the tile lock */
    pthread_mutex_init(&t->lock, NULL);

    if(in_opts->cache_policy == MMB_TILECACHE_READ_WRITE) {

      mmb_tiling_wrlock(in_mba);
      /* Check if someone else already created this tile in the meantime
      * if so, free local data and return pre-existing tile */
      mmbArrayTile *t_temp;

      mmb_hash_iter_t k = mmb_hash_get_array_tiles(in_mba->tiling->tiles, tile_idx);
      if(k != mmb_hash_end_array_tiles(in_mba->tiling->tiles)) {
        t_temp = mmb_hash_value_array_tiles(in_mba->tiling->tiles, k);
        /* We dont need the tile we constructed ourselves anymore */
        stat = mmb_layout_destroy(t->layout);
        if(stat != MMB_OK)
          MMB_WARN("Failed to destroy temporary layout object\n");
        free(dims);
        pthread_mutex_destroy(&t->lock);
        free(t);

        t = t_temp;
        /* Move to top tile of the stack and return pointer */
        while (t->next != NULL){
          t = t->next;
        }
        stat = MMB_OK;
        goto BAILOUT_UNLOCK;
      }
      int ret;
      k = mmb_hash_put_array_tiles(in_mba->tiling->tiles, tile_idx, &ret);
      if(ret<0) {
        MMB_ERR("Unable to insert tile into hash map\n");
        stat = MMB_ERROR;
        goto BAILOUT_UNLOCK;
      }
      mmb_hash_value_array_tiles(in_mba->tiling->tiles, k) = t;

    BAILOUT_UNLOCK:
      mmb_tiling_unlock(in_mba);
    }
  }

  if(stat == MMB_OK)
    *out_tile = t;

BAILOUT:
  return stat;
}

mmbError mmb_tile_update_1d(mmbArray *in_mba, size_t in_idx,
                         mmbArrayTile **inout_tile) {
  mmbIndex idx = {1, &in_idx};
  return mmb_tile_update(in_mba, &idx, inout_tile);
}

mmbError mmb_tile_update_2d(mmbArray *in_mba, size_t in_idx0, size_t in_idx1,
                         mmbArrayTile **inout_tile){
  size_t idx_arr[2] = {in_idx0, in_idx1};
  mmbIndex idx = {2, &idx_arr[0]};
  return mmb_tile_update(in_mba, &idx, inout_tile);
}

mmbError mmb_tile_update_3d(mmbArray *in_mba, size_t in_idx0, size_t in_idx1,
                         size_t in_idx2, mmbArrayTile **inout_tile){
  size_t idx_arr[3] =  {in_idx0, in_idx1, in_idx2};
  mmbIndex idx = {3, &idx_arr[0]};
  return mmb_tile_update(in_mba, &idx, inout_tile);
}

mmbError mmb_tile_update(mmbArray *in_mba, mmbIndex *in_idx,
                         mmbArrayTile **inout_tile){

  /* Validate args */
  if(in_mba == NULL)
    return MMB_INVALID_ARG;

  if(in_idx == NULL)
    return MMB_INVALID_ARG;

  if(inout_tile == NULL)
    return MMB_INVALID_ARG;

  if(*inout_tile == NULL)
    return MMB_INVALID_ARG;

  mmbError stat = MMB_OK;

  mmbArrayTile *t = *inout_tile;

  // Check if new index differs from current index, if not, return
  size_t tile_idx = 0;
  stat = nd_to_1d_idx(in_idx, &in_mba->tiling->tile_count, &tile_idx);
  if(stat != MMB_OK) {
    MMB_ERR("Failed to convert nd index to 1d index\n");
    goto BAILOUT;
  }
  if(tile_idx == t->tidx) {
    goto BAILOUT;
  }

  /* Get tile from array cache and return, if policy indicates */
  mmbArrayTile *cached_tile;
  if(t->cache_policy != MMB_TILECACHE_UNUSED) {
    stat = mmb_tile_at(in_mba, in_idx, &cached_tile);
    if(stat != MMB_OK) {
      MMB_ERR("Failed to retreive tile from cache during update\n");
      goto BAILOUT;
    }
    *inout_tile = cached_tile;
  } else {
    /* Update the tile indices manually if no cache is being used */
  /* We just update indices, we not modify the tile struct,
   * layout, or tile cache policy */
    for (unsigned i = 0; i < t->layout->n_dims; i++) {
      t->abs_dim[i] = in_mba->dims.d[i];
      t->lower[i] = in_idx->d[i] * in_mba->tiling->tile_size.d[i];
      t->upper[i] = (in_idx->d[i] + 1) * in_mba->tiling->tile_size.d[i];
      if (t->upper[i] > t->abs_dim[i])
        t->upper[i] = t->abs_dim[i];
      t->alower[i] = t->lower[i];
      t->aupper[i] = t->upper[i];
      t->dim[i] = t->upper[i] - t->lower[i];
    }
    /* TODO - Do we need to update this? */
    /* Check contiguity */
    /* TODO: This only checks if tile is 1d, or if it covers whole array,
    * not if subtiles are contiguous */
    int contiguous = 1;
    if(t->layout->n_dims > 1) {
      for (unsigned i = 0; i < t->layout->n_dims; i++) {
        if(t->abs_dim[i] != t->dim[i]) {
          contiguous = 0;
        }
      }
    }

    if(contiguous) {
      t->access |= MMB_CONTIGUOUS;
    }
    /* We do not modify allocation */
    /* Set tile index for hashmap */
    t->tidx = tile_idx;
  }

BAILOUT:
  return stat;
}



mmbError mmb_tile_count(mmbArray *in_mba, int *out_count) {
  mmbError stat = MMB_OK;
  if(!in_mba || !out_count) {
    MMB_ERR("Arg can not be NULL\n");
    stat = MMB_INVALID_ARG;
    goto BAILOUT;
  }
  /* Return counted tiles */
  *out_count = in_mba->tiling->total;
BAILOUT:
  return stat;
}

mmbError mmb_tiling_dimensions(mmbArray *in_mba, mmbDimensions **out_dims) {
  mmbError stat = MMB_OK;

  if(!in_mba || !out_dims) {
    MMB_ERR("Arg can not be NULL\n");
    stat = MMB_INVALID_ARG;
    goto BAILOUT;
  }

  if(!in_mba->tiling) {
    MMB_ERR("mmbArray is not tiled\n");
    return MMB_ERROR;
  }

  mmbDimensions* dims;
  stat =  mmb_dimensions_create(0, &dims);
  if(stat != MMB_OK) {
    MMB_ERR("Failed to create mmb dimensions object\n");
    goto BAILOUT;
  }

  stat = mmb_dimensions_copy(dims, &in_mba->tiling->tile_count);
  if(stat != MMB_OK) {
    MMB_ERR("Could not retrieve mmbDimensions from array tiling\n");
    goto BAILOUT;
  }

  *out_dims = dims;

BAILOUT:
  return stat;
}

mmbError mmb_tile_at(mmbArray *in_mba, mmbIndex *in_idx,
                     mmbArrayTile **out_tile) {
  mmbError stat = MMB_OK;

  if(!in_mba || !in_idx || !out_tile) {
    MMB_ERR("Arg can not be NULL\n");
    stat = MMB_INVALID_ARG;
    goto BAILOUT;
  }

  *out_tile = NULL;
  if(!in_mba->tiling) {
    stat = MMB_ERROR;
    MMB_ERR("mmbArray not tiled\n");
    goto BAILOUT;
  }

  /* Lookup in hashmap for tile at in_idx */
  size_t tile_idx = 0;
  stat = nd_to_1d_idx(in_idx, &in_mba->tiling->tile_count, &tile_idx);
  if(stat != MMB_OK) {
    MMB_ERR("Failed to convert nd index to 1d index\n");
    goto BAILOUT;
  }

  mmbArrayTile *t = NULL;
  mmb_tiling_rdlock(in_mba);
  mmb_hash_iter_t k = mmb_hash_get_array_tiles(in_mba->tiling->tiles, tile_idx);
  if(k != mmb_hash_end_array_tiles(in_mba->tiling->tiles)) {
    t = mmb_hash_value_array_tiles(in_mba->tiling->tiles, k);
  /* If found, move to top tile of the stack and return pointer */
    while (t->next != NULL){
      t = t->next;
    }
    *out_tile = t;
    stat = MMB_OK;
    goto BAILOUT_UNLOCK;
  }

  mmb_tiling_unlock(in_mba);

  /* Otherwise, create tile and return using default memory space */

  /* Typical threaded usage would be multiple threads asking for different
    tiles, so we let each thread make its own tile before insertion
    This results in some wasted cycles if many threads ask for the same
    not-yet-constructed tile, but minimises blocking in the more-common scenario
    where they ask for different not-yet-constructed tiles. */
  t = (mmbArrayTile *)calloc(1, sizeof(mmbArrayTile));
  if(!t) {
    stat = MMB_OUT_OF_MEMORY;
    goto BAILOUT;
  }

  size_t *dims = malloc(sizeof(size_t[6][in_mba->layout->n_dims]));
  if (NULL == dims) {
    free(t);
    stat = MMB_OUT_OF_MEMORY;
    goto BAILOUT;
  }
  t->lower   = &dims[0 * in_mba->layout->n_dims];
  t->upper   = &dims[1 * in_mba->layout->n_dims];
  t->alower  = &dims[2 * in_mba->layout->n_dims];
  t->aupper  = &dims[3 * in_mba->layout->n_dims];
  t->dim     = &dims[4 * in_mba->layout->n_dims];
  t->abs_dim = &dims[5 * in_mba->layout->n_dims];

  t->interface = in_mba->interface;
  t->access = in_mba->access;
  /* TODO: Could just use in_mba->layout pointer instead of duplicating? */
  stat = mmb_layout_create_copy(in_mba->layout, &t->layout);
  if (stat != MMB_OK) {
    MMB_ERR("Failed to copy-construct mmbLayout\n");
    free(t);
    free(dims);
    goto BAILOUT;
  }

  for (unsigned i = 0; i < t->layout->n_dims; i++) {
    t->abs_dim[i] = in_mba->dims.d[i];
    t->lower[i] = in_idx->d[i] * in_mba->tiling->tile_size.d[i];
    t->upper[i] = (in_idx->d[i] + 1) * in_mba->tiling->tile_size.d[i];
    if (t->upper[i] > t->abs_dim[i])
      t->upper[i] = t->abs_dim[i];
    t->alower[i] = t->lower[i];
    t->aupper[i] = t->upper[i];
    t->dim[i] = t->upper[i] - t->lower[i];
  }

  /* Check contiguity */
  /* TODO: This only checks if tile is 1d, or if it covers whole array,
   * not if subtiles are contiguous */
  int contiguous = 1;
  if(t->layout->n_dims > 1) {
    for (unsigned i = 0; i < t->layout->n_dims; i++) {
      if(t->abs_dim[i] != t->dim[i]) {
        contiguous = 0;
      }
    }
  }

  if(contiguous) {
    t->access |= MMB_CONTIGUOUS;
  }

  /* This pointer is offset using indexing macros */
  t->allocation = in_mba->allocation;

  /* Set tile index for hashmap */
  t->tidx = tile_idx;

  /* Initialise the tile lock */
  pthread_mutex_init(&t->lock, NULL);

  mmb_tiling_wrlock(in_mba);
  /* Check if someone else already created this tile in the meantime
   * if so, free local data and return pre-existing tile */
  mmbArrayTile *t_temp;

  k = mmb_hash_get_array_tiles(in_mba->tiling->tiles, tile_idx);
  if(k != mmb_hash_end_array_tiles(in_mba->tiling->tiles)) {
    t_temp = mmb_hash_value_array_tiles(in_mba->tiling->tiles, k);
    /* We dont need the tile we constructed ourselves anymore */
    stat = mmb_layout_destroy(t->layout);
    if(stat != MMB_OK)
      MMB_WARN("Failed to destroy temporary layout object\n");
    free(dims);
    pthread_mutex_destroy(&t->lock);
    free(t);

    t = t_temp;
    /* Move to top tile of the stack and return pointer */
    while (t->next != NULL){
      t = t->next;
    }
    *out_tile = t;
    stat = MMB_OK;
    goto BAILOUT_UNLOCK;
  }
  int ret;
  k = mmb_hash_put_array_tiles(in_mba->tiling->tiles, tile_idx, &ret);
  if(ret<0) {
    MMB_ERR("Unable to insert tile into hash map\n");
    stat = MMB_ERROR;
    goto BAILOUT_UNLOCK;
  }
  mmb_hash_value_array_tiles(in_mba->tiling->tiles, k) = t;

  *out_tile = t;
BAILOUT_UNLOCK:
  mmb_tiling_unlock(in_mba);
BAILOUT:
  return stat;
}

mmbError mmb_tile_at_1d(mmbArray *in_mba, size_t in_idx,
                     mmbArrayTile **out_tile){
  mmbIndex idx = {1, &in_idx};
  return mmb_tile_at(in_mba, &idx, out_tile);
}

mmbError mmb_tile_at_2d(mmbArray *in_mba, size_t in_idx0, size_t in_idx1,
                     mmbArrayTile **out_tile){
  size_t idx_arr[2] = {in_idx0, in_idx1};
  mmbIndex idx = {2, &idx_arr[0]};
  return mmb_tile_at(in_mba, &idx, out_tile);

}

mmbError mmb_tile_at_3d(mmbArray *in_mba, size_t in_idx0, size_t in_idx1,
                        size_t in_idx2, mmbArrayTile **out_tile){
  size_t idx_arr[3] = {in_idx0, in_idx1, in_idx2};
  mmbIndex idx = {3, &idx_arr[0]};
  return mmb_tile_at(in_mba, &idx, out_tile);

}


mmbError mmb_tile_duplicate_at(mmbArray *in_mba, mmbIndex *in_idx,
                               mmbMemInterface *in_interface,
                               mmbArrayTile **out_tile)
{
  mmbError stat = MMB_OK;
  mmbArrayTile *t = NULL;
  stat = mmb_tile_at(in_mba, in_idx, &t);
  if (MMB_OK != stat) {
    MMB_ERR("Unable to retrive the corresponding tile.\n");
    return stat;
  }
  return mmb_tile_duplicate(t, in_interface, in_mba->access, in_mba->layout, out_tile);
}



mmbError mmb_tile_alloc_data(mmbArrayTile *in_tile) {
  /* Validate args */
  if(in_tile == NULL)
    return MMB_ERROR;

  if(in_tile->allocation != NULL)
    return MMB_ERROR;

  mmbError stat = MMB_OK;
  size_t total_bytes;

  mmbDimensions dims = mmb_dimensions_empty;
  stat = mmb_dimensions_fill(&dims, in_tile->dim, in_tile->layout->n_dims);
  if(stat != MMB_OK) {
    MMB_ERR("Unable to create tile dimensions for allocation\n");
    goto BAILOUT;
  }
  stat = mmb_layout_buffer_size(in_tile->layout, &dims, &total_bytes);
  if(stat != MMB_OK) {
    MMB_ERR("Unable to calculate tile buffer size\n");
    goto BAILOUT;
  }
  stat = mmb_dimensions_resize(&dims, 0);
  if(stat != MMB_OK) {
    MMB_ERR("Unable to destroy dimensions object\n");
    goto BAILOUT;
  }

  /* TODO: We need to consider access type here too */
  stat = mmb_allocate(total_bytes, in_tile->interface, &in_tile->allocation);
  if(stat != MMB_OK) {
    MMB_ERR("Unable to allocate %zu bytes for tile\n", total_bytes);
    goto BAILOUT;
  }

  in_tile->allocation_owned = 1;

BAILOUT:
  return stat;
}

mmbError mmb_tile_pack(mmbArrayTile *tile,
                       mmbAllocation *host_alloc,
                       mmbAllocation *device_alloc)
{
  mmbError stat = MMB_OK;
  if (NULL == tile) {
    MMB_ERR("Invalid tile cannot be packed.\n");
    return MMB_INVALID_ARG;
  }
  if (NULL == host_alloc || NULL == host_alloc->ptr) {
    MMB_ERR("Invalid host located allocation provided cannot be NULL.\n");
    return MMB_INVALID_ARG;
  }
  if (NULL == device_alloc || NULL == device_alloc->ptr) {
    MMB_ERR("Invalid device located allocation provided cannot be NULL.\n");
    return MMB_INVALID_ARG;
  }
  /* Write Tile */
  mmbArrayTile *buffer = MMBALLOC2TBUF(host_alloc, mmbArrayTile *);
  memcpy(buffer, tile, sizeof(mmbArrayTile));
  char *device_tmp = MMBALLOC2TBUF(device_alloc, char *);
  /* leave a gap just after the tile for the allocation */
  device_tmp += sizeof(mmbArrayTile);
  buffer->allocation = (mmbAllocation *)device_tmp;
  device_tmp += sizeof(mmbAllocation);
  /* write dims */
  size_t *dims = (size_t *)device_tmp;
  buffer->lower   = &dims[0 * tile->layout->n_dims];
  buffer->upper   = &dims[1 * tile->layout->n_dims];
  buffer->alower  = &dims[2 * tile->layout->n_dims];
  buffer->aupper  = &dims[3 * tile->layout->n_dims];
  buffer->dim     = &dims[4 * tile->layout->n_dims];
  buffer->abs_dim = &dims[5 * tile->layout->n_dims];
  char *tmp_buf = (char *)(buffer + 1) + sizeof(mmbAllocation);
  memcpy(tmp_buf, tile->lower, sizeof(size_t[6][tile->layout->n_dims]));
  /* write layout */
  device_tmp += sizeof(size_t[6][tile->layout->n_dims]);
  tmp_buf += sizeof(size_t[6][tile->layout->n_dims]);
  buffer->layout = (mmbLayout *)device_tmp;
  stat = mmb_layout_pack(tmp_buf, tile->layout, device_tmp);
  if (MMB_OK != stat) {
    MMB_ERR("Unable to pack layout for device tile.\n");
    return stat;
  }
  /* The allocation part that has been left out, is filled by the calling
   * function. */
  return stat;
}

static mmbError
mmb_tile_copy_nd(mmbArrayTile *dst_tile, mmbArrayTile *src_tile)
{
  mmbError stat = MMB_OK;

  /* Only consider dst dimensionality */
  const size_t n_dims = dst_tile->layout->n_dims;

  /* Check whether src fits in dst */
  size_t dst_tile_size = 0;
  mmbDimensions ddim = { n_dims, dst_tile->dim };
  stat = mmb_layout_buffer_size_nd(dst_tile->layout, &ddim, n_dims,  &dst_tile_size);
  if (MMB_OK != stat) {
    MMB_ERR("Unable to determine destination tile's size.\n");
    return stat;
  }
  size_t src_tile_size = 0;
  mmbDimensions sdim = { n_dims, src_tile->dim };
  stat = mmb_layout_buffer_size_nd(src_tile->layout, &sdim, n_dims,  &src_tile_size);
  if (MMB_OK != stat) {
    MMB_ERR("Unable to determine source tile's size.\n");
    return stat;
  }
  if (src_tile_size > dst_tile_size) {
    MMB_ERR("The destination tile is too small to contain the source data. Abort.\n");
    stat = MMB_ERROR;
    return stat;
  }

  /* Compute offset and pitch for source and destination */
  size_t dpitch[n_dims];
  ddim.d = dst_tile->abs_dim;
  stat = mmb_layout_buffer_pitch_nd(dst_tile->layout, &ddim, n_dims, dpitch);
  if (MMB_OK != stat) {
    MMB_ERR("Unable to compute destination tile's pitch.\n");
    return stat;
  }
  size_t spitch[n_dims];
  sdim.d = src_tile->abs_dim;
  stat = mmb_layout_buffer_pitch_nd(src_tile->layout, &sdim, n_dims, spitch);
  if (MMB_OK != stat) {
    MMB_ERR("Unable to compute source tile's pitch.\n");
    return stat;
  }

  /* Do the copy */
  stat = mmb_copy_nd(dst_tile->allocation, dst_tile->lower, dpitch,
                     src_tile->allocation, src_tile->lower, spitch,
                     n_dims, src_tile->dim);
  if(stat != MMB_OK) {
    MMB_ERR("Failed to copy src to dst tile\n");
    return stat;
  }

  /* Copy the metadata to have a coherent tile on the device if necessary */
  mmbExecutionContext dst_ex_con;
  stat = mmb_allocation_get_execution_context(dst_tile->allocation, &dst_ex_con);
  if (MMB_OK != stat) {
    MMB_ERR("Unable to read destination tiling execution context.\n");
    return stat;
  }
  if (MMB_CPU != dst_ex_con && NULL == src_tile->space_local_metadata) {
    mmbAllocation *src_alloc, *dst_alloc;
    size_t metadata_size = 0;
    /* Prepare the buffer to send to the device */
    stat = mmb_layout_get_sizeof(dst_tile->layout, &metadata_size);
    if (MMB_OK != stat) {
      MMB_ERR("Unable to determine the size of the metadata to transfert to the "
          "device.\n");
      return stat;
    }
    metadata_size += sizeof(mmbAllocation);
    metadata_size += sizeof(mmbArrayTile);
    metadata_size += sizeof(size_t[6][dst_tile->layout->n_dims]);
    stat = mmb_allocate(metadata_size, src_tile->allocation->interface, &src_alloc);
    if (MMB_OK != stat) {
      MMB_ERR("Unable to allocate memory for CPU side buffer for tile metadata.\n");
      return MMB_OUT_OF_MEMORY;
    }
    stat = mmb_allocate(metadata_size, dst_tile->allocation->interface, &dst_alloc);
    if (MMB_OK != stat) {
      MMB_ERR("Unable to allocate memory for device side tile metadata.\n");
      mmb_free(src_alloc);
      return stat;
    }
    stat = mmb_tile_pack(dst_tile, src_alloc, dst_alloc);
    if (MMB_OK != stat) {
      MMB_ERR("Unable to pack metadata for device side tile.\n");
      mmb_free(src_alloc);
      mmb_free(dst_alloc);
      return stat;
    }
    /* Update de mmbAllocation region of the buffer */
    mmbAllocation *device_alloc;
    device_alloc = (mmbAllocation *)(MMBALLOC2TBUF(src_alloc, mmbArrayTile *) + 1);
    memcpy(device_alloc, dst_tile->allocation, sizeof(mmbAllocation));
    /* Transfert the tile metadata to the device */
    stat = mmb_copy(dst_alloc, src_alloc);
    if (MMB_OK != stat) {
      MMB_ERR("Unable to transfert the metadata to the device.\n");
      mmb_free(src_alloc);
      mmb_free(dst_alloc);
      return stat;
    }
    mmb_free(src_alloc);
    dst_tile->space_local_metadata = dst_alloc;
  }

  return stat;
}

/* This can be lockfree due to the tile_duplicate implementation only
 * duplicating one-to-one tiles, when we extend to one-to-many this will
 * need reviewing */
mmbError mmb_tile_copy(mmbArrayTile *dst_tile, mmbArrayTile *src_tile) {
  /* Validate args */
  if(dst_tile == NULL || src_tile == NULL){
    MMB_ERR("Arg can not be NULL\n");
    return MMB_INVALID_ARG;
  }
  if(dst_tile == src_tile){
    MMB_ERR("dst_tile==src_tile, cannot copy tile to itself\n");
    return MMB_INVALID_ARG;
  }
  /* dst_tile must be writeable */
  if(!(dst_tile->access & MMB_WRITE)) {
    MMB_ERR("Destination tile not writeable for copy\n");
    return MMB_ERROR;
  }

  /* Only matching layouts are supported currently */
  mmbLayoutEquivalence diff;
  mmbError stat = mmb_layout_cmp(src_tile->layout, dst_tile->layout, &diff);
  if(stat != MMB_OK) {
    MMB_ERR("Failed to compare layouts\n");
    return stat;
  } else if (diff) {
    MMB_ERR("Layouts do not match - only identical layouts are currently "
            "supported for copy\n");
    return MMB_ERROR;
  }

  /* Only tiles of dimensionality <= 3 are supported currently */
  if (src_tile->layout->n_dims <= 3) {
    stat = mmb_tile_copy_nd(dst_tile, src_tile);
  } else {
    MMB_ERR("Tile layouts dimensionality > 3 - only N-D layouts <= 3 are "
            "currently supported for copy.\n");
    return MMB_ERROR;
  }

  return stat;
}

mmbError mmb_tile_duplicate(mmbArrayTile *in_tile, mmbMemInterface *in_interface,
                            mmbAccessType in_access, mmbLayout *in_layout,
                            mmbArrayTile **out_tile) {

  mmbError stat = MMB_OK;
  /*
    Validate input arguments
  */
  if(!in_tile || !in_interface || !in_layout || !out_tile) {
    MMB_ERR("Arg can not be NULL\n");
    stat = MMB_INVALID_ARG;
    goto BAILOUT;
  }

  /* If in_tile is read-only, and in_access is writeable, return error */
  if(!(in_tile->access & MMB_WRITE) && (in_access & MMB_WRITE)) {
    MMB_ERR("Unable to duplicate read-only tile to writeable tile.\n");
    return MMB_ERROR;
  }

  /* Lock tile for access. Currently you can only duplicate the top tile in the
   * stack, so this locking can be simple. Eventually we will support duplicating
   * many read-only tiles from a single source tile simultaneously, in which case this
   * will require updating */
  mmb_tile_lock(in_tile);

  /* Can only duplicate top tile in the stack */
  if(in_tile->next != NULL){
    MMB_ERR("Tile for duplication must not have dependent tiles\n");
    stat = MMB_INVALID_ARG;
    goto BAILOUT;
  }

  /* Initialise new tile from input parameters */
  mmbArrayTile *t = (mmbArrayTile *)calloc(1, sizeof(mmbArrayTile));
  if (NULL == t) {
    stat = MMB_OUT_OF_MEMORY;
    goto BAILOUT;
  }


  size_t *dims = malloc(sizeof(size_t[6][in_layout->n_dims]));
  if (NULL == dims) {
    free(t);
    stat = MMB_OUT_OF_MEMORY;
    goto BAILOUT;
  }
  t->lower   = &dims[0 * in_layout->n_dims];
  t->upper   = &dims[1 * in_layout->n_dims];
  t->alower  = &dims[2 * in_layout->n_dims];
  t->aupper  = &dims[3 * in_layout->n_dims];
  t->dim     = &dims[4 * in_layout->n_dims];
  t->abs_dim = &dims[5 * in_layout->n_dims];

  t->interface = in_interface;
  t->access = in_access;
  stat = mmb_layout_create_copy(in_layout, &t->layout);
  if (stat != MMB_OK) {
    free(t);
    free(dims);
    goto BAILOUT;
  }

  /* The duplicated tile lower and upper are now 0:dim in each dimension */
  memcpy(t->abs_dim, in_tile->dim, sizeof(size_t[in_layout->n_dims]));
  memset(t->lower, 0, sizeof(size_t[in_layout->n_dims]));
  memset(t->alower, 0, sizeof(size_t[in_layout->n_dims]));
  memcpy(t->upper, in_tile->dim, sizeof(size_t[in_layout->n_dims]));
  memcpy(t->aupper, in_tile->dim, sizeof(size_t[in_layout->n_dims]));
  memcpy(t->dim, in_tile->dim, sizeof(size_t[in_layout->n_dims]));
  t->tidx = in_tile->tidx;

  /* Allocate memory for new tile */
  stat = mmb_tile_alloc_data(t);
  if (stat != MMB_OK) {
    free(t);
    free(dims);
    goto BAILOUT;
  }

  /* Duplicated tile is now contiguous */
  t->access |= MMB_CONTIGUOUS;

  /* Copy input tile to new tile */
  stat = mmb_tile_copy(t, in_tile);
  if (stat != MMB_OK) {
    free(t);
    free(dims);
    goto BAILOUT;
  }

  /* Initialise the tile lock */
  pthread_mutex_init(&t->lock, NULL);

  /* Add to the stack of tiles */
  in_tile->next = t;
  t->prev = in_tile;

  /* If access type is writeable, then lock the tile below from being written */
  if(in_access & MMB_WRITE){
    in_tile->access |= MMB_WRITE_LOCKED;
  }

  /* Return tile */
  *out_tile = t;

BAILOUT:
  mmb_tile_unlock(in_tile);
  return stat;
}

mmbError mmb_tile_get_n_dims(mmbArrayTile *in_tile,
                             size_t *n_dims){

  mmbError stat = MMB_OK;
  /* Check parameters */
  if (NULL == in_tile) {
    MMB_ERR("Invalid tile given as parameter.\n");
    stat = MMB_INVALID_ARG;
    goto BAILOUT;
  }
  if (NULL == n_dims) {
    MMB_ERR("Invalid n_dims pointer given as parameter.\n");
    stat = MMB_INVALID_ARG;
    goto BAILOUT;
  }

  *n_dims = in_tile->layout->n_dims;

BAILOUT:
  return stat;
}

mmbError mmb_tile_get_space_local_handle(mmbArrayTile *in_tile,
                                         mmbArrayTile **out_tile)
{
  mmbError stat = MMB_OK;
  /* Check parameters */
  if (NULL == in_tile) {
    MMB_ERR("Invalid tile given as parameter.\n");
    stat = MMB_INVALID_ARG;
    goto BAILOUT;
  }
  if (NULL == out_tile) {
    MMB_ERR("Invalid handle given as parameter. \"out_tile\" cannot be NULL\n");
    stat = MMB_INVALID_ARG;
    goto BAILOUT;
  }
  if (NULL != in_tile->space_local_metadata) {
    *out_tile = MMBALLOC2TBUF(in_tile->space_local_metadata, mmbArrayTile *);
  } else {
    *out_tile = in_tile;
  }
BAILOUT:
  return stat;
}

mmbError mmb_tile_migrate(mmbArrayTile *in_tile, mmbMemInterface *in_interface,
                          mmbAccessType in_access, mmbLayout *in_layout) {

  /*
    Validate input arguments

    If in_tile is read-only, and in_access is writeable
      Check if tile below is writeable
      If not, return error.

    Allocate new tile with interface / layout / access type

    Copy data to new tile

    Replace old tile with new in tile stack

    Free old tile
  */
  (void) in_tile; (void) in_interface; (void) in_access; (void) in_layout;
  mmbError stat = MMB_UNIMPLEMENTED;
  return stat;
}

mmbError mmb_tile_merge(mmbArrayTile *in_tile, mmbMergeType in_merge) {

  /*
      Validate input arguments

      switch on in_merge
        case OVERWRITE:
          get reference to tile_below
          copy in_tile to tile_below

      Other cases we will consider:
        pull data from tile_below to in_tile
        fancy merges (e.g modified entries only, halos only, user defined function)

  */

  mmbError stat = MMB_OK;
  if(!in_tile) {
    stat = MMB_ERROR;
    goto BAILOUT;
  }
  mmb_tile_lock(in_tile);

  // TODO : This should either not destroy the bottom tile of the stack, or also
  //        remove it from the tile cache

  /* If tile is bottom of stack, or if it was read only, free the tile. */
  if(in_tile->prev == NULL || !(in_tile->access & MMB_WRITE)) {
    mmb_tile_unlock(in_tile);
    stat = mmb_tile_destroy(in_tile);
    if(stat != MMB_OK) {
      MMB_ERR("Failed to destroy tile during no copy merge\n");
    }
    goto BAILOUT;
  }

  mmb_tile_lock(in_tile->prev);
  switch(in_merge) {
    case MMB_DISCARD: /* no action needed */
      break;
    case MMB_OVERWRITE:
    {
      mmbArrayTile *prev = in_tile->prev;
      if(!(prev->access & MMB_WRITE)) {
        MMB_ERR("Trying to merge to read-only tile\n");
        stat = MMB_ERROR;
        goto BAILOUT_UNLOCK_FREE;
      }
      stat = mmb_tile_copy(prev, in_tile);
      if(stat != MMB_OK) {
        MMB_ERR("Failed to copy tile data during merge\n");
        goto BAILOUT_UNLOCK_FREE;
      }
    }
    break;
    default:
      MMB_ERR("Unsupported merge type\n");
      stat = MMB_ERROR;
      goto BAILOUT_UNLOCK_FREE;
    break;
  }

  in_tile->prev->next = NULL;

BAILOUT_UNLOCK_FREE:
  mmb_tile_unlock(in_tile->prev);
  mmb_tile_unlock(in_tile);

  /* Only destroy the tile if merge was successful */
  if(stat == MMB_OK){
    stat = mmb_tile_destroy(in_tile);
    if(stat != MMB_OK) {
      MMB_ERR("Failed to destroy merged tile\n");
      goto BAILOUT;
    }
  }

BAILOUT:
  return stat;
}

mmbError mmb_tile_destroy(mmbArrayTile *in_tile) {

  /*
    Validate input argument

    If tile has other tiles above it (in_tile->next != null)
      return error

    If tile has other tiles below it (in_tile->prev != null)
      Clear next ptr from lower tile and if it was write-locked, unlock it.

    if tile data is owned
      free tile data (opposite of allocation in duplicate/migrate)
  */

  mmbError stat = MMB_OK;
  if(!in_tile) {
    stat = MMB_ERROR;
    goto BAILOUT;
  }

  if(in_tile->next != NULL) {
    MMB_ERR("Cannot free tile with dependent tiles\n");
    stat = MMB_ERROR;
    goto BAILOUT;
  }

  if(in_tile->prev != NULL) {
    mmbArrayTile *prev = in_tile->prev;
    prev->next = NULL;
    if(prev->access & MMB_WRITE_LOCKED) {
      prev->access &= ~MMB_WRITE_LOCKED;
    }
  }

  if(in_tile->allocation_owned) {
    stat = mmb_free(in_tile->allocation);
    if(stat != MMB_OK) {
      MMB_ERR("Failed to free tile allocation\n");
      goto BAILOUT;
    }
  }

  if (NULL != in_tile->space_local_metadata) {
    stat = mmb_free(in_tile->space_local_metadata);
    if (MMB_OK != stat) {
      MMB_ERR("Failed to free device tiling metadata.\n");
      goto BAILOUT;
    }
  }

  stat = mmb_layout_destroy(in_tile->layout);
  if(stat != MMB_OK) {
    MMB_ERR("Failed to free tile layout\n");
    goto BAILOUT;
  }

  /* Destroy the tile lock */
  pthread_mutex_destroy(&in_tile->lock);

  free(in_tile->lower);
  free(in_tile);

  MMB_DEBUG("Tile destroyed\n");

BAILOUT:
  return stat;
}
