/*
 * Copyright (C) 2019 Cray UK Ltd. 
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/* -*- mode:c -*- */
/** @file
 ** @brief Logging infrastructure
 **
 ** This header exposes an API to do sensible logging, from debugging
 ** to production level information
 **
 **/

#ifndef MMB_LOGGING_H_
#define MMB_LOGGING_H_ 1

#include <math.h>
#include <stdarg.h>
#include <stdio.h>
#include <limits.h>

#include <mmb_error.h>

#ifdef __cplusplus
extern "C" {
#endif

/** @defgroup Logging Logging
 * @{
 *
 * @brief Simple logging API, from debugging to production level information
 *
 */
/** log level for errors */
#define MMB_LOG_ERR 0
/** log level for warnings */
#define MMB_LOG_WARN 1
/** log level for informational messages */
#define MMB_LOG_INFO 2
/** log level for debugging messages */
#define MMB_LOG_DEBUG 3
/** log level for really chatty logging */
#define MMB_LOG_NOISE 4

#ifndef MMB_MAX_LOG_LEVEL
/** compile-time log level cutoff */
#define MMB_MAX_LOG_LEVEL MMB_LOG_DEBUG
#endif

/* Default arg for logging options */
#define MMB_LOGGING_OPTIONS_DEFAULT = NULL

typedef void (*mmbUserLogFunc)(int level, const char *func, const char *file,
                    int line, const char *fmtstring, ...);  
typedef struct s_mmbLoggingOptions {
  int max_logging;
  mmbUserLogFunc user_log_func;
} mmbLoggingOptions;

const char* mmb_log_level_to_str(int level);
mmbError mmb_logging_options_create_default(mmbLoggingOptions **opts);
mmbError mmb_logging_options_set_debug_level(mmbLoggingOptions*opts, int level);
mmbError mmb_logging_options_set_user_log_func(mmbLoggingOptions *opts, mmbUserLogFunc user_log_func);
mmbError mmb_logging_options_destroy(mmbLoggingOptions *opts);

mmbError mmb_logging_set_debug_level(int level);
mmbError mmb_logging_set_user_log_func(mmbUserLogFunc user_log_func);

mmbError mmb_logging_init(mmbLoggingOptions *opts);
mmbError mmb_logging_finalize();

/** @brief core logging variadic wrapper
 */
void mmb_location_aware_log(int level, const char *func, const char *file,
                    int line, const char *fmtstring, ...);

/** @brief core logging worker function
 */
void mmb_vlocation_aware_log(int level, const char *func, const char *file,
                             int line, const char *fmtstring, va_list argp);
extern mmbUserLogFunc g_mmb_log_func;

/** @brief logging macro 
 */
#define MMB_LOG(level, format, ...)                                                \
  do {                                                                         \
    if (level <= MMB_MAX_LOG_LEVEL)                                            \
      g_mmb_log_func(level, __func__, __FILE__, __LINE__,                      \
                            format, ##__VA_ARGS__);                            \
} while (0)

/** @defgroup LogginMacros Logging Macros
 * @{
 *
 * @brief Intended API
 */

/** error messages */
#define MMB_ERR(format, ...) MMB_LOG(MMB_LOG_ERR, format, ##__VA_ARGS__)
/** warning messages */
#define MMB_WARN(format, ...) MMB_LOG(MMB_LOG_WARN, format, ##__VA_ARGS__)
/** informational messages */
#define MMB_INFO(format, ...) MMB_LOG(MMB_LOG_INFO, format, ##__VA_ARGS__)
/** debug messages */
#define MMB_DEBUG(format, ...) MMB_LOG(MMB_LOG_DEBUG, format, ##__VA_ARGS__)
/** chatty messages */
#define MMB_NOISE(format, ...) MMB_LOG(MMB_LOG_NOISE, format, ##__VA_ARGS__)

/** Convenience macro to check states and goto label **/
#define CHECK_STATUS(stat, message, label) \
  do {                                     \
    if(stat != MMB_OK) {                   \
      MMB_ERR(message);                    \
      goto label;                          \
    }                                      \
  } while (0)

/*
 * From tests/cheat.h
 * This computes an upper bound for the string length of an unsigned integer type.
 */
#define MMB_LOG_INTEGER_LENGTH(type) \
  (CHAR_BIT * sizeof (type) / 3 + 1) /* This is derived from the base 2 logarithm
                                      * of 10. */


/** @} (group macros) */

#ifdef __cplusplus
} /* end of extern "C" */
#endif

#endif /* MMB_LOGGING_H_ */
