#ifndef MMB_DIMENSIONS_H
#define MMB_DIMENSIONS_H

#include <stdlib.h>

#include "mmb_set.h"
#include "mmb_error.h"

#ifdef __cplusplus
extern "C" {
#endif

typedef mmbSet_size_t mmbDimensions;

extern const mmbDimensions mmb_dimensions_empty;

/**
 * @brief      Create a new dimensions object of size ndim
 *             ndim may be 0.
 *
 * @param[in]  ndim      The number of dimensions to be stored
 * @param      out_dims  The out dims
 *
 * @return     MMB_OK on success
 */
mmbError mmb_dimensions_create(const size_t ndim, mmbDimensions **out_dims);

/**
 * @brief      Populate a set with as many elements as size of \p in_dims.
 *
 * If less elements are given, the end of the set will be overwritten with
 * unpredictable values. If too many elements are given, only the
 * size(\p in_dims) first parameter values will be used.
 *
 * @param [in] in_dims    The set of dimensions to populate.
 * @param [in] dimN       First value to add to the set.  Correspond to the
 *                        slowest growing dimension.
 * @param [in] ...        \p in_dims->size - 1 other elements of the set.
 *
 * @return     MMB_OK on success
 */
mmbError mmb_dimensions_set(mmbDimensions *in_dims, size_t dimN, ...);

/**
 * @brief      Create a new dimensions object of size \p ndim filled with values
 *             \p values.
 *
 * \p ndim may be 0. \p values may be null, in which case missing values are
 * zeroed.
 *
 * @param[in]  ndim      The number of dimensions to be stored
 * @param[in]  values    The values to fill (slowest growing dimension first)
 * @param      out_dims  The out dims
 *
 * @return     MMB_OK on success
 */
mmbError mmb_dimensions_create_fill(const size_t ndim, const size_t* values, mmbDimensions **out_dims);

/**
 * @brief      Copy a set of dimensions from \p src to \p dst, both are
 *             expected to be allocated.
 *
 * @param      dst   The destination dimensions object
 * @param      src   The source dimensions object
 *
 * @return     MMB_OK on success
 */
mmbError mmb_dimensions_copy(mmbDimensions *dst, const mmbDimensions *src);

/**
 * @brief      Copy the buffer of dimensions from \p src to \p dst, both are
 *             expected to be allocated.
 *
 * @param      dst   The destination buffer
 * @param      src   The source dimensions object
 *
 * @return     MMB_OK on success
 */
mmbError mmb_dimensions_copy_buffer(size_t *dst, const mmbDimensions *src);

/**
 * @brief      Copy the buffer of size_t from \p src to the set \p dst, both
 *             are expected to be allocated. \p dst has to be at least \nelt.
 *
 * @param      dst   The destination dimensions set
 * @param      src   The source buffer
 * @param      nelt  The number of elements in \p src.
 *
 * @return     MMB_OK on success
 */
mmbError mmb_dimensions_fill(mmbDimensions *dst,
    const size_t *src, const size_t nelt);

/**
 * @brief      Resize a dimensions object to ndim, where \p ndim may be \c 0
 *
 * @param      in_dims  In dimension object to be resized
 * @param[in]  ndim     Size of new dimensions
 *
 * @return     MMB_OK on success
 */
mmbError mmb_dimensions_resize(mmbDimensions *in_dims, const size_t ndim);

/**
 * @brief      Destroy a dimensions object
 *
 * @param      in_dims  In dimensions object to be freed
 *
 * @return     MMB_OK on success
 */
mmbError mmb_dimensions_destroy(mmbDimensions *in_dims);

/**
 * @brief      Compare the two pointed objects
 *
 * This function compare the two objects pointed by the two parameters.
 * It first compare the two sizes, and then each elements until finding
 * two elements that differ.
 *
 * @param[in]  d1 Pointer to first element
 * @param[in]  d2 Pointer to second element
 *
 * @return     0 if the two objects are similar
 *            <0 if d1 < d2
 *            >0 if d1 > d2
 */
int mmb_dimensions_cmp(const mmbDimensions *d1, const mmbDimensions *d2);

/**
 * @brief      Compute the product of all dimensions
 *
 * @param      in_dims   In dimensions over which to compute product
 * @param      out_prod  The output product
 *
 * @return     MMB_OK on success, the appropriate mmbError otherwise.
 */
mmbError mmb_dimensions_prod(const mmbDimensions *in_dims, size_t *out_prod);

/**
 * @brief      Change the address of the buffer used to retain the dimensions
 *             information.
 *
 * WARNING: Use this function with care as it may lead to a memory leak.
 * WARNING: The data is not copied before the base pointer is changed.
 *
 * @param      in_dims   In dimensions which buffer is to be changed
 * @param      buffer    New buffer address
 *
 * @return     MMB_OK on success, the appropriate mmbError otherwise.
 */
mmbError mmb_dimensions_set_buf_addr(mmbDimensions *in_dims, size_t *buffer);

/**
 * @brief      Compute the number of elements in mmbDimensions.
 *
 * @param      in_dims   In dimensions of which we want the size
 * @param      size      The number of elements
 *
 * @return     MMB_OK on success, the appropriate mmbError otherwise.
 */
mmbError mmb_dimensions_get_size(const mmbDimensions *in_dims, size_t *size);

/**
 * @brief      Compute the size of the structure behind the given dimension.
 *
 * @param      in_dims   In dimensions of which we want the size
 * @param      size      The output size
 *
 * @return     MMB_OK on success, the appropriate mmbError otherwise.
 */
mmbError mmb_dimensions_get_sizeof(const mmbDimensions *in_dims, size_t *size);

#ifdef __cplusplus
}
#endif

#endif
