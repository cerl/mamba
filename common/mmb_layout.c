/*
 * Copyright (C) 2018-2020 Cray UK
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <string.h>
#include <stdlib.h>
#include <string.h>
#include "mmb_layout.h"
#include "mmb_logging.h"

#define MIN(x, y) ((x) < (y) ? (x) : (y))

static mmbLayoutPadding g_mmb_zero_padding = {0};
/** Layout type strings */
static const char *const
mmb_layout_types[MMB_LAYOUT_TYPE_MAX] = { [MMB_LAYOUT_NONE] = "None",
                                          [MMB_REGULAR] = "Regular",
                                          [MMB_REGULAR_BLOCK] = "Regular block",
                                          [MMB_IRREGULAR] = "Irregular"};

const char* mmb_layout_type_to_str(int type) {
  if(type >= MMB_LAYOUT_TYPE_MAX){
    MMB_ERR("Mamba array layout type out of bounds\n");
    return NULL;
  }
  return mmb_layout_types[type];
}

/** Element type strings */
static const char *const
mmb_element_types[MMB_LAYOUT_ELEMENTTYPE_MAX] = { [MMB_LAYOUT_ELEMENTTYPE_NONE] = "None",
                                                  [MMB_ELEMENT] = "Element",
                                                  [MMB_STRUCTURE] = "Structure",
                                                  [MMB_SOA] = "SOA"};

const char* mmb_element_type_to_str(int type) {
  if(type >= MMB_LAYOUT_ELEMENTTYPE_MAX){
    MMB_ERR("Mamba layout element type out of bounds\n");
    return NULL;
  }
  return mmb_element_types[type];
}

/** Layout type strings */
static const char *const
mmb_layout_order_types[MMB_LAYOUT_ORDER_MAX] = { [MMB_LAYOUT_ORDER_NONE] = "None",
                                          [MMB_ROWMAJOR] = "Row",
                                          [MMB_COLMAJOR] = "Column",
                                          [MMB_GENERIC_ND] = "Generic"};

const char* mmb_layout_order_to_str(int order) {
  if(order >= MMB_LAYOUT_ORDER_MAX){
    MMB_ERR("Mamba array layout order out of bounds\n");
    return NULL;
  }
  return mmb_layout_order_types[order];
}

static const char *const
mmb_layout_equivalence[MMB_LAYOUT_DIFF_MAX] = { [MMB_LAYOUT_EQUAL] = "Identical layouts",
                                          [MMB_LAYOUT_DIFF_INDEX] = "Same layouts, different index",
                                          [MMB_LAYOUT_DIFF_FIELDS] = "Same layout type, different offsets/lengths etc",
                                          [MMB_LAYOUT_DIFF_TYPES] = "Different layout types"};

const char* mmb_layout_equivalence_to_str(int result) {
  if(result >= MMB_LAYOUT_DIFF_MAX){
    MMB_ERR("Mamba layout compare result is out of bounds\n");
    return NULL;
  }
  return mmb_layout_equivalence[result];
}

mmbLayoutPadding *mmb_layout_padding_create_zero(void) {
  return &g_mmb_zero_padding;
}

mmbError mmb_layout_alloc(mmbLayout **out_layout) {
  mmbError stat = MMB_OK;
  if(!out_layout)
    return MMB_INVALID_ARG;
  mmbLayout *l = calloc(1, sizeof(mmbLayout));
  if (NULL == l)
    return MMB_OUT_OF_MEMORY;
  *out_layout = l;
  return stat;
}

mmbError mmb_layout_create_regular_1d(size_t element_size_bytes,
                                      mmbLayoutPadding *pad,
                                      mmbLayout **out_layout) {

  return mmb_layout_create_regular_nd(element_size_bytes, 1,
                                      MMB_LAYOUT_ORDER_NONE, pad, out_layout);
}

mmbError mmb_layout_create_regular_nd(size_t element_size_bytes, size_t n_dims,
                                      mmbLayoutOrder element_order,
                                      mmbLayoutPadding *pad,
                                      mmbLayout **out_layout) {
  mmbError stat = MMB_OK;
  mmbLayout *l;
  stat = mmb_layout_alloc(&l);
  if(stat != MMB_OK) {
    MMB_ERR("Failed to allocate new layout\n");
    goto BAILOUT;
  }

  l->type = MMB_REGULAR;
  l->n_dims = n_dims;
  l->element.type = MMB_ELEMENT;
  l->element.size_bytes = element_size_bytes;
  l->element.order = element_order;

  if(pad) {
    stat = mmb_layout_padding_copy(&l->pad, pad, l->n_dims);
    if(stat != MMB_OK) {
      MMB_ERR("Failed to copy mmbLayoutPadding\n");
      goto BAILOUT;
    }
  }

  *out_layout = l;
BAILOUT:
  return stat;
}


mmbError mmb_layout_create_block(size_t n_dims, size_t element_size_bytes,
                                 mmbLayoutOrder element_order,
                                 mmbDimensions *block_size,
                                 mmbDimensions *block_order,
                                 mmbLayoutPadding *pad,
                                 mmbLayout **out_layout) {
  (void) n_dims; (void) element_size_bytes; (void) element_order;
  (void) block_size; (void) block_order; (void) pad; (void) out_layout;
  return MMB_UNIMPLEMENTED;
}

mmbError mmb_layout_create_irregular_1d(size_t element_size_bytes,
                                     size_t *offsets,
                                     size_t *sizes,
                                     mmbLayout **out_layout) {
  (void) element_size_bytes; (void)offsets; (void) sizes; (void) out_layout;
  return MMB_UNIMPLEMENTED;
}

mmbError mmb_layout_dist_find_piece_size(mmbLayout *dist_layout, int64_t *size){
  
  mmbError status = MMB_UNIMPLEMENTED;
  *size = -1 ;
  /* FIXME assuming irregular 1D mmb_layout only */
  if((dist_layout->type == MMB_IRREGULAR) && (dist_layout->n_dims == 1)) {
    /* size = length of my piece in the distribution * size of element */
    *size =  dist_layout->irregular.lengths[dist_layout->index] * dist_layout->element.size_bytes;
    status = MMB_OK;
  }
  else {
    /* FIXME  support other mamba layout types*/
    MMB_ERR("mamba layout type is not yet supported \n");
    status = MMB_UNIMPLEMENTED;
  }  
  return status;  
}


mmbError mmb_layout_dist_find_entire_size(mmbLayout *dist_layout, int64_t *size){
  mmbError status = MMB_UNIMPLEMENTED;
  *size = -1 ;
  /* FIXME assuming irregular 1D mmb_layout only */
  if((dist_layout->type == MMB_IRREGULAR) && (dist_layout->n_dims == 1)) {
    *size = 0; //  size
    for (size_t i = 0; i < dist_layout->irregular.n_blocks.d[0]; i++) {
      *size += dist_layout->irregular.lengths[i]; //  size
    }
    *size = *size  * dist_layout->element.size_bytes; /*size in bytes */
    status = MMB_OK;
  }
  else {
    /* FIXME  support other mamba layout types*/
    MMB_ERR("mamba layout type is not yet supported \n");
    status = MMB_UNIMPLEMENTED;
  }

  return status;
}

/** 
  * Cook up a default distributed, which consists of one piece holding
  * all the data from all pieces ... not really distributed */
mmbError mmb_layout_dist_create_default_layout(mmbLayout *src_layout, mmbLayout **default_layout){
  mmbError status = MMB_UNIMPLEMENTED;

  /*FIXME only support irregular 1D layouts */
  if ((src_layout->type == MMB_IRREGULAR) && (src_layout->n_dims == 1)) {
    
    size_t *offsets, *sizes;
    offsets = (size_t *) malloc(sizeof(size_t) * 1);
    sizes = (size_t *) malloc(sizeof(size_t) * 1);

    if ((!offsets) || (!sizes) ) {
      MMB_ERR("Can not allocate memory for default layout offsets and sizes \n");
      return MMB_OUT_OF_MEMORY;
    }

    offsets[0] = 0; // data starts from the begining
    /* find the whole size  */
    sizes[0] = 0; //  size
    for (size_t i = 0; i < src_layout->irregular.n_blocks.d[0]; i++) {
      sizes[0] += src_layout->irregular.lengths[i]; // cdo size
    }

    status = mmb_layout_create_dist_irregular_1d(src_layout->element.size_bytes,
                                       0, /*index = 0*/
                                       1, /* nblocks = 1*/
                                       offsets,
                                       sizes,
                                       default_layout);
  }
  else {
    /* FIXME  support other mamba layout types*/
    MMB_ERR("mamba layout type is not yet supported \n");
    status = MMB_UNIMPLEMENTED;
  }

  return status;
  
}

mmbError mmb_layout_create_dist_irregular_1d(size_t element_size_bytes,
                                     size_t index,
                                     size_t n_blocks,
                                     size_t *offsets,
                                     size_t *lengths,
                                     mmbLayout **out_layout) {

   mmbError stat = MMB_OK;
   mmbLayout *l;
   stat = mmb_layout_alloc(&l);
   if(stat != MMB_OK) {
        MMB_ERR("Failed to allocate new layout\n");
        goto BAILOUT;
    }


     l->type = MMB_IRREGULAR;
     l->n_dims = 1;
     /*check index */
     if (index >= n_blocks) {
       stat = MMB_INVALID_ARG;
       MMB_ERR("index must be less than the number of blocks in irregular 1d distributed layout \n");
       goto BAILOUT;
     }
     l->index = index;
     l->element.type = MMB_ELEMENT;
     l->element.size_bytes = element_size_bytes;

     mmbDimensions *one_dim;
     stat = mmb_dimensions_create_fill(1, &n_blocks, &one_dim);
     if(stat != MMB_OK) {
          MMB_ERR("Failed to create mmbDimensions \n");
          /*clean the allocated layout now*/
          mmb_layout_destroy(l);
          goto BAILOUT;
      }

     l->irregular.n_blocks = *one_dim;
     /* Allocate offsets and lengths*/
     l->irregular.offsets = malloc(n_blocks * sizeof(size_t));
     if (l->irregular.offsets == NULL){
       MMB_ERR("Failed to allocate memory for layout offsets \n");
       stat = MMB_OUT_OF_MEMORY;
       goto BAILOUT;
     }
     l->irregular.lengths = malloc(n_blocks * sizeof(size_t));
     if (l->irregular.lengths == NULL){
       MMB_ERR("Failed to allocate memory for layout lengths \n");
       stat = MMB_OUT_OF_MEMORY;
       goto BAILOUT;
     }
     /* Copy the values */
    memcpy(l->irregular.offsets, offsets, sizeof(size_t)*n_blocks);
    memcpy(l->irregular.lengths, lengths, sizeof(size_t)*n_blocks);

     /* l->element.order = element_order; */


  *out_layout = l;

  BAILOUT:
    return stat;
}


mmbError mmb_layout_create_copy(mmbLayout *in_layout, mmbLayout **out_layout) {
  mmbError stat = MMB_OK;
  if(!in_layout)
    return MMB_INVALID_ARG;
  mmbLayout *l;
  stat = mmb_layout_alloc(&l);
  if(stat != MMB_OK) {
    MMB_ERR("Failed to allocate new layout\n");
    goto BAILOUT;
  }

  l->type = in_layout->type;
  l->n_dims = in_layout->n_dims;
  l->index = in_layout->index;
  stat = mmb_layout_element_copy(&l->element, &in_layout->element);
  if(stat != MMB_OK) {
    MMB_ERR("Unable to copy mmbLayoutElement \n");
    goto BAILOUT;
  }

  stat = mmb_layout_padding_copy(&l->pad, &in_layout->pad, in_layout->n_dims);
  if(stat != MMB_OK) {
    MMB_ERR("Unable to copy mmbLayoutPadding \n");
    goto BAILOUT;
  }

  if(in_layout->type == MMB_REGULAR_BLOCK) {
    stat = mmb_layout_block_copy(&l->block, &in_layout->block);
    if(stat != MMB_OK) {
      MMB_ERR("Unable to copy mmbLayoutBlock \n");
      goto BAILOUT;
    }
  } else if (in_layout->type == MMB_IRREGULAR) {
    stat = mmb_layout_irregular_copy(&l->irregular, &in_layout->irregular);
    if(stat != MMB_OK) {
      MMB_ERR("Unable to copy mmbLayoutIrregular \n");
      goto BAILOUT;
    }
  }

  *out_layout = l;

BAILOUT:
  return stat;
}

mmbError mmb_layout_compute_intersection(mmbLayout *src_layout, mmbLayout *dst_layout, mmbLayoutIntersection **out_li) {
  mmbError stat = MMB_OK;

  /*FIXME only irregular 1d is currently supported */
  if ((src_layout->type == MMB_IRREGULAR) && (dst_layout->type == MMB_IRREGULAR)) {
    if ((src_layout->irregular.n_blocks.size == 1) && (dst_layout->irregular.n_blocks.size == 1)) {
      /* For only irregular 1D layouts */
      /* Same layout, same index
       * Same layout, different index
       * Same layout type, different layout components*/
       stat = mmb_layout_compute_overlap(src_layout, dst_layout, out_li);
    }
    else {
      /* FIXME  support other mamba layout types*/
      MMB_ERR("mamba layout type is not yet supported \n");
      return MMB_UNIMPLEMENTED;
    }
  }
  else if (src_layout->type != dst_layout->type) {
    /* FIXME supoprt finding a mapping between two different mamba layout types */
    MMB_ERR(" Finding a mapping between two different mmbLayout types is unimplemented \n");
    stat = MMB_UNIMPLEMENTED;
  }
  else {
    /* FIXME  support other mamba layout types*/
    MMB_ERR("mamba layout type is not yet supported \n");
    return MMB_UNIMPLEMENTED;
  }

   return stat;
}

mmbError mmb_layout_destroy_mmbLayoutIntersection(mmbLayoutIntersection *in_li) {
  mmbError stat = MMB_OK;
  if(!in_li)
    return MMB_INVALID_ARG;

  if (in_li->overlap) {
    free(in_li->overlap);
  }

  free(in_li);
  return stat;
}

mmbError mmb_layout_compute_overlap(mmbLayout *src_layout, mmbLayout *dst_layout, mmbLayoutIntersection **out_li) {
  mmbError stat = MMB_OK;
  size_t dst_pieces = dst_layout->irregular.n_blocks.d[0]; /* number of pieces for dst */
  size_t src_pieces = src_layout->irregular.n_blocks.d[0]; /* number of pieces for src */
  int index;

  MMB_DEBUG("Number of src pieces %zu and dst pieces %zu \n", src_pieces, dst_pieces);
  *out_li = (mmbLayoutIntersection *) malloc(sizeof(mmbLayoutIntersection));
  if (*out_li == NULL) {
    MMB_ERR("Unable to allocate memory for layout intersections \n");
    return MMB_OUT_OF_MEMORY;
  }
  (*out_li)->n_dst_pieces =  dst_pieces;
  (*out_li)->n_src_pieces =  src_pieces;

  mmbLayoutOverlap *out;
  size_t src_start, src_end, dst_start, dst_end;
  out = (mmbLayoutOverlap *) malloc(sizeof(mmbLayoutOverlap)*dst_pieces*src_pieces);
  if (out == NULL) {
    MMB_ERR("Unable to allocate memory (%d bytes) for layout overlap \n", sizeof(mmbLayoutOverlap)*dst_pieces*src_pieces);
    mmb_layout_destroy_mmbLayoutIntersection(*out_li);
    return MMB_OUT_OF_MEMORY;
  }
  /* for every dst index */
  for (size_t i = 0; i < dst_pieces; i++) {
    /* for every src index */
    for (size_t j = 0; j < src_pieces; j++) {
      /*calculate the overlap between the two pieces*/
      index = i*src_pieces+j;
      src_start = src_layout->irregular.offsets[j];
      src_end = src_start +src_layout->irregular.lengths[j];
      dst_start = dst_layout->irregular.offsets[i];
      dst_end = dst_start + dst_layout->irregular.lengths[i];

      if ((dst_end < src_start) || (dst_start > src_end)) { // no overlap
          out[index].src_offset = -1;
          out[index].dst_offset = -1;
          out[index].length = 0;
      }
      else  {
        if (dst_start < src_start) {
          out[index].src_offset = 0;
          out[index].dst_offset = src_start - dst_start;
          /* how much data is left in the piece
           * length of piece - offset in piece */
          out[index].length  = MIN(src_layout->irregular.lengths[j],(dst_layout->irregular.lengths[i] - out[index].dst_offset));
        }
        else {
          out[index].src_offset =  dst_start - src_start;
          out[index].dst_offset = 0;
          out[index].length  = MIN(dst_layout->irregular.lengths[i],(src_layout->irregular.lengths[j] - out[index].src_offset));
        }
      }
    }
  }

  (*out_li)->overlap = out;
  return stat;
}

mmbError mmb_layout_cmp(mmbLayout *in_layout0, mmbLayout *in_layout1, mmbLayoutEquivalence *diff) {
  mmbError stat = MMB_OK;
  int result;

  /* Compare layout 0 and 1 */
  if(in_layout0->type != in_layout1->type) {
    *diff = MMB_LAYOUT_DIFF_TYPES;
    return MMB_OK;
  }

  if(in_layout0->n_dims != in_layout1->n_dims) {
    *diff = MMB_LAYOUT_DIFF_FIELDS;
    return MMB_OK;
  }

  stat = mmb_layout_element_cmp(&in_layout0->element, &in_layout1->element, &result);
  if(stat != MMB_OK) {
    MMB_ERR("Unable to compare layout elements");
    goto BAILOUT;
  }
  if(result) {
    *diff = MMB_LAYOUT_DIFF_FIELDS;
    return MMB_OK;
  }

  stat = mmb_layout_padding_cmp(&in_layout0->pad, &in_layout1->pad, in_layout0->n_dims, &result);
  if(stat != MMB_OK) {
    MMB_ERR("Unable to compare layout elements");
    goto BAILOUT;
  }
  if(result) {
    *diff = MMB_LAYOUT_DIFF_FIELDS;
    return MMB_OK;
  }

  if (in_layout0->type == MMB_IRREGULAR) {
    stat = mmb_layout_irregular_cmp(&in_layout0->irregular, &in_layout1->irregular, &result);
    if(stat != MMB_OK) {
      MMB_ERR("Unable to compare irregular layouts");
      goto BAILOUT;
    }
    if(result) {
      *diff = MMB_LAYOUT_DIFF_FIELDS;
      goto BAILOUT;
    }
  }
  else if(in_layout0->type == MMB_REGULAR_BLOCK) {
    MMB_ERR("Block layouts not yet supported\n");
    stat = MMB_UNIMPLEMENTED;
    goto BAILOUT;
  }

  if(in_layout0->index != in_layout1->index){
    *diff = MMB_LAYOUT_DIFF_INDEX;
    return MMB_OK;
  }

  *diff = MMB_LAYOUT_EQUAL;
BAILOUT:
  return stat;
}

mmbError mmb_layout_irregular_cmp(mmbLayoutIrregular *in_layout0, mmbLayoutIrregular *in_layout1, int *diff){
    mmbError stat = MMB_OK;
    size_t n_elements = 0;

    if(!in_layout0 || !in_layout1) {
      MMB_ERR("NULL input layouts \n");
      stat = MMB_INVALID_ARG;
      *diff = 1;
      goto BAILOUT;
    }

  if ((in_layout0->n_blocks.size == 1) && (in_layout1->n_blocks.size == 1))  {
    /*Compare nblocks */
    if (in_layout0->n_blocks.d[0] != in_layout1->n_blocks.d[0]) {
      *diff = 1;
      goto BAILOUT;
    }
    else{
      n_elements = in_layout0->n_blocks.d[0];
    }
    /*Compare offsets */
    /*Compare lengths*/
    int n0 = memcmp(in_layout0->offsets, in_layout1->offsets, n_elements*sizeof(size_t));
    int n1 = memcmp(in_layout0->lengths, in_layout1->lengths, n_elements*sizeof(size_t));

    if(n0 || n1) {
        *diff = 1;
        goto BAILOUT;
    }
  }
  else {
    MMB_ERR("ND Irregular layouts not yet implemented\n");
    *diff = 1;
    stat = MMB_UNIMPLEMENTED;
    goto BAILOUT;
  }
  *diff = 0;
BAILOUT:
  return stat;
}

mmbError mmb_layout_buffer_size(mmbLayout *in_layout, mmbDimensions *dim,
                                size_t * total_bytes)
{
  return mmb_layout_buffer_size_nd(in_layout, dim, in_layout->n_dims, total_bytes);
}

mmbError mmb_layout_buffer_size_nd(mmbLayout *in_layout, mmbDimensions *dim,
                                   size_t n_dims, size_t *total_bytes)
{
  mmbError stat = MMB_OK;

  if (!in_layout || !dim || !n_dims || !total_bytes)
    return MMB_INVALID_ARG;

  /* Work out size of single element (from type + padding) ... */
  size_t element_bytes;
  stat = mmb_layout_element_get_size(&in_layout->element, &in_layout->pad,
                                     &element_bytes);
  if (MMB_OK != stat)
    return stat;

  /* compute how many elements we have and multiply it by size of
   * one element ... */
  size_t n_elt = 1;
  for(size_t d = 0; d < n_dims; d++)
    n_elt *= dim->d[d];

  /* ... and then we compute the amount of overall padding. */
  size_t total_padding = 0;
  if(in_layout->pad.dimension_is_padded){
    for (size_t d = 0; n_dims > d; ++d) {
      total_padding *= dim->d[n_dims-1-d];
      total_padding += in_layout->pad.dimension_pre_pad_bytes[n_dims-1-d] +
                          in_layout->pad.dimension_post_pad_bytes[n_dims-1-d];
    }
  }

  *total_bytes = n_elt * element_bytes + total_padding;

  return stat;
}

mmbError mmb_layout_buffer_pitch_nd(mmbLayout *in_layout, mmbDimensions *dim,
                                    size_t n_dims, size_t *pitch)
{
  mmbError stat = MMB_OK;

  if (!in_layout || !dim || !n_dims || !pitch)
    return MMB_INVALID_ARG;

  /* Work out size of single element (from type + padding) */
  stat = mmb_layout_element_get_size(&in_layout->element, &in_layout->pad,
                                     &pitch[n_dims-1]);
  if (MMB_OK != stat)
    return stat;

  /* Compute pitches from end to start (slowest growing dimension first) */
  for (size_t d = 1; n_dims > d; ++d) {
    pitch[n_dims-1-d] = pitch[n_dims-d] * dim->d[n_dims-d];
    if (in_layout->pad.dimension_is_padded) {
      pitch[n_dims-1-d] += in_layout->pad.dimension_pre_pad_bytes[n_dims-1-d] +
                            in_layout->pad.dimension_post_pad_bytes[n_dims-1-d];
    }
  }

  return stat;
}

mmbError mmb_layout_destroy(mmbLayout *in_layout) {
  mmbError stat = MMB_OK;
  if(!in_layout)
    return MMB_INVALID_ARG;

  free(in_layout->pad.dimension_pre_pad_bytes);
  free(in_layout->pad.dimension_post_pad_bytes);

  if(MMB_REGULAR_BLOCK == in_layout->type) {
    mmb_dimensions_resize(&in_layout->block.dimensions, 0);
  }

  if(MMB_IRREGULAR == in_layout->type) {
    mmb_dimensions_resize(&in_layout->irregular.n_blocks, 0);
    free(in_layout->irregular.offsets);
    free(in_layout->irregular.lengths);
  }

  free(in_layout);
  return stat;
}

mmbError mmb_layout_element_get_size(mmbLayoutElement *elt, mmbLayoutPadding *pad,
                                     size_t *size)
{
  mmbError stat = MMB_OK;
  if (!elt || !pad || !size) {
    return MMB_INVALID_ARG;
  }
  size_t element_bytes = elt->size_bytes;
  if(pad->element_is_padded){
    element_bytes += pad->element_pre_pad_bytes + pad->element_post_pad_bytes;
  }
  *size = element_bytes;
  return stat;
}

mmbError mmb_layout_element_copy(mmbLayoutElement *dst, mmbLayoutElement *src) {
  mmbError stat = MMB_OK;
  if(!dst || !src) {
    return MMB_INVALID_ARG;
  }

  dst->type = src->type;
  dst->size_bytes = src->size_bytes;
  dst->order = src->order;

  /* TODO: Will be deeper copy for struct info here later */
  return stat;
}

mmbError mmb_layout_element_cmp(mmbLayoutElement *le0, mmbLayoutElement *le1, int *diff) {
  mmbError stat = MMB_OK;
  int n = memcmp(le0, le1, sizeof(mmbLayoutElement));
  if(n != 0){
    *diff = 1;
  } else {
    *diff = 0;
  }
  return stat;
}

mmbError mmb_layout_padding_copy(mmbLayoutPadding *dst, mmbLayoutPadding *src, size_t ndim) {
  mmbError stat = MMB_OK;
  if(!dst || !src) {
    return MMB_INVALID_ARG;
  }

  dst->element_is_padded = src->element_is_padded;
  dst->element_pre_pad_bytes = src->element_pre_pad_bytes;
  dst->element_post_pad_bytes = src->element_post_pad_bytes;
  dst->dimension_is_padded = src->dimension_is_padded;
  if(dst->dimension_is_padded) {
    dst->dimension_pre_pad_bytes = realloc(dst->dimension_pre_pad_bytes,
                                           sizeof(size_t[ndim]));
    dst->dimension_post_pad_bytes = realloc(dst->dimension_pre_pad_bytes,
                                            sizeof(size_t[ndim]));
    if(!dst->dimension_pre_pad_bytes || !dst->dimension_post_pad_bytes) {
      MMB_ERR("Unabled to allocate memory for dimension padding\n");
      stat = MMB_OUT_OF_MEMORY;
      goto BAILOUT;
    }
    memcpy(dst->dimension_pre_pad_bytes, src->dimension_pre_pad_bytes,
           sizeof(size_t[ndim]));
    memcpy(dst->dimension_post_pad_bytes, src->dimension_post_pad_bytes,
           sizeof(size_t[ndim]));
  }
BAILOUT:
  return stat;
}

mmbError mmb_layout_padding_cmp(mmbLayoutPadding *lp0, mmbLayoutPadding *lp1,
                                size_t ndim, int *diff) {

  if(lp0->element_is_padded != lp1->element_is_padded){
    *diff = 1;
    return MMB_OK;
  }

  if(lp0->element_is_padded) {
    if(lp0->element_pre_pad_bytes != lp1->element_pre_pad_bytes ||
       lp0->element_post_pad_bytes != lp1->element_post_pad_bytes) {
      *diff = 1;
      return MMB_OK;
    }
  }

  if(lp0->dimension_is_padded) {
    int n0 = memcmp(lp0->dimension_pre_pad_bytes, lp1->dimension_pre_pad_bytes,
                    sizeof(size_t[ndim]));
    int n1 = memcmp(lp0->dimension_post_pad_bytes, lp1->dimension_post_pad_bytes,
                    sizeof(size_t[ndim]));

    if(n0 || n1) {
      *diff = 1;
      return MMB_OK;
    }
  }

  *diff = 0;
  return MMB_OK;

}

mmbError mmb_layout_block_copy(mmbLayoutBlock *dst, mmbLayoutBlock *src) {
  mmbError stat = MMB_OK;
  if(!dst || !src) {
    return MMB_INVALID_ARG;
  }
  dst->order = src->order;
  stat = mmb_dimensions_copy(&dst->dimensions, &src->dimensions);
  if(stat != MMB_OK) {
    MMB_ERR("Unabled to copy dimensions of mmbLayoutBlock\n");
      stat = MMB_ERROR;
      goto BAILOUT;
  }
BAILOUT:
  return stat;
}

mmbError mmb_layout_irregular_copy(mmbLayoutIrregular *dst,
                                   mmbLayoutIrregular *src) {
  mmbError stat = MMB_OK;
  if(!dst || !src) {
    return MMB_INVALID_ARG;
  }

  stat = mmb_dimensions_copy(&dst->n_blocks, &src->n_blocks);
  if(stat != MMB_OK) {
    MMB_ERR("Unabled to resize dimensions of mmbLayoutIrregular\n");
      stat = MMB_ERROR;
      goto BAILOUT;
  }
  if(src->n_blocks.size == 1) {
    size_t *dst_offsets = NULL;
    size_t *dst_lengths = NULL;
    /* Allocate if required */
    if (NULL == dst->offsets)
      dst_offsets = malloc(sizeof(size_t)*src->n_blocks.d[0]);
    if (NULL == dst->lengths)
      dst_lengths = malloc(sizeof(size_t)*src->n_blocks.d[0]);
    /* check alloc */
    if((!dst->offsets && !dst_offsets) || (!dst->lengths && !dst_lengths)) {
      if (NULL == dst->offsets)
        free(dst_offsets);
      if (NULL == dst->lengths)
        free(dst_lengths);
      MMB_ERR("Unable to allocate irregular offsets and lengths\n");
      stat = MMB_OUT_OF_MEMORY;
      goto BAILOUT;
    }
    /* set allocated if required */
    if (NULL == dst->offsets)
      dst->offsets = dst_offsets;
    if (NULL == dst->lengths)
      dst->lengths = dst_lengths;
    /* Copy the values */
    memcpy(dst->offsets, src->offsets, sizeof(size_t)*src->n_blocks.d[0]);
    memcpy(dst->lengths, src->lengths, sizeof(size_t)*src->n_blocks.d[0]);
  } else {
    MMB_ERR("ND Irregular layouts not yet implemented\n");
    stat = MMB_UNIMPLEMENTED;
    goto BAILOUT;
  }

BAILOUT:
  return stat;
}

mmbError mmb_layout_get_sizeof(const mmbLayout *layout, size_t *out_size)
{
  mmbError stat;
  if (NULL == layout) {
    MMB_ERR("Invalid layout handle. Unable to get full size.\n");
    return MMB_INVALID_ARG;
  }
  if (NULL == out_size) {
    MMB_ERR("Invalid out parameter. \"out_size\" cannot be NULL.\n");
    return MMB_INVALID_ARG;
  }
  size_t size;
  switch (layout->type) {
    case MMB_LAYOUT_NONE:
    case MMB_REGULAR:
      size = 0;
      break;
    case MMB_REGULAR_BLOCK:
      stat = mmb_dimensions_get_sizeof(&layout->block.dimensions, &size);
      if (MMB_OK != stat) {
        MMB_ERR("Unable to compute block dimensions structure size.\n");
        return stat;
      }
      size += sizeof(mmbLayoutBlock);
      break;
    case MMB_IRREGULAR:
      {
        size_t ssize;
        stat = mmb_dimensions_prod(&layout->irregular.n_blocks, &ssize);
        if (MMB_OK != stat) {
          MMB_ERR("Unable to compute irregular \"offsets\" and \"lengths\" "
              "array sizes.\n");
          return stat;
        }
        stat = mmb_dimensions_get_sizeof(&layout->irregular.n_blocks, &size);
        if (MMB_OK != stat) {
          MMB_ERR("Unable to compute irregular dimensions structure size.\n");
          return stat;
        }
        size += sizeof(mmbLayoutIrregular) + sizeof(size_t[2][ssize]);
      }
      break;
    default:
      MMB_ERR("Unknown type of layout.\n");
      return MMB_ERROR;
  }
  if (layout->pad.dimension_is_padded) {
    size += sizeof(size_t[2][layout->n_dims]);
  }
  *out_size = size + sizeof(mmbLayout);
  return MMB_OK;
}

mmbError mmb_layout_pack(void *_buffer, const mmbLayout *layout, void *_base_addr)
{
  mmbError stat = MMB_OK;
  if (NULL == _buffer) {
    MMB_ERR("Invalid buffer.\n");
    return MMB_INVALID_ARG;
  }
  if (NULL == layout) {
    MMB_ERR("Invalid layout.\n");
    return MMB_INVALID_ARG;
  }
  /* base_addr is used to update the addresses of buffer w.r.t. the final
   * allocation. */
  if (NULL == _base_addr) {
    _base_addr = _buffer;
  }
  /* tmp_buffer is the layout view of the buffer, so we can access to the
   * fields and update values. */
  mmbLayout *tmp_buffer = _buffer;
  char *buffer = _buffer;
  char *base_addr = _base_addr;
  /* Deep copy the layout, array are put after the structure so we can first
   * read the structure to get all the proper parameters. */
  memcpy(buffer, layout, sizeof *layout);
  buffer += sizeof *layout;
  base_addr += sizeof *layout;
  /* Pack arrays from padding if dimension is padded */
  size_t *buf_sz = (size_t *)buffer;
  size_t *base_addr_sz = (size_t *)base_addr;
  if (layout->pad.dimension_is_padded) {
    memcpy(buf_sz, layout->pad.dimension_pre_pad_bytes,
           sizeof(size_t[layout->n_dims]));
    tmp_buffer->pad.dimension_pre_pad_bytes = base_addr_sz;
    buf_sz += layout->n_dims;
    base_addr_sz += layout->n_dims;
    memcpy(buf_sz, layout->pad.dimension_post_pad_bytes,
           sizeof(size_t[layout->n_dims]));
    tmp_buffer->pad.dimension_post_pad_bytes = base_addr_sz;
    buf_sz += layout->n_dims;
    base_addr_sz += layout->n_dims;
  }
  /* pack specific data */
  switch (layout->type) {
    case MMB_LAYOUT_NONE:
    case MMB_REGULAR:
      /* nothing to do */
      break;
    case MMB_REGULAR_BLOCK:
      stat = mmb_dimensions_copy_buffer(buf_sz, &layout->block.dimensions);
      if (MMB_OK != stat) {
        MMB_ERR("Unable to copy block dimensions buffer.\n");
        return stat;
      }
      stat = mmb_dimensions_set_buf_addr(&tmp_buffer->block.dimensions,
                                         base_addr_sz);
      if (MMB_OK != stat) {
        MMB_ERR("Unable to update the address of block dimensions buffer.\n");
        return stat;
      }
      break;
    case MMB_IRREGULAR:
      {
        size_t dim_size, arr_size;
        stat = mmb_dimensions_get_size(&layout->irregular.n_blocks, &dim_size);
        if (MMB_OK != stat) {
          MMB_ERR("Unable to retrieve the size of irregular dimensions.\n");
          return stat;
        }
        stat = mmb_dimensions_copy_buffer(buf_sz, &layout->irregular.n_blocks);
        if (MMB_OK != stat) {
          MMB_ERR("Unable to copy irregular dimensions buffer.\n");
          return stat;
        }
        stat = mmb_dimensions_set_buf_addr(&tmp_buffer->irregular.n_blocks,
                                           base_addr_sz);
        if (MMB_OK != stat) {
          MMB_ERR("Unable to update the address of irregular dimensions buffer.\n");
          return stat;
        }
        buf_sz += dim_size;
        base_addr_sz += dim_size;
        stat = mmb_dimensions_prod(&layout->irregular.n_blocks, &arr_size);
        if (MMB_OK != stat) {
          MMB_ERR("Unable to compute irregular \"offsets\" and \"lengths\" "
              "array sizes.\n");
          return stat;
        }
        memcpy(buf_sz, &layout->irregular.offsets, sizeof(size_t[arr_size]));
        tmp_buffer->irregular.offsets = base_addr_sz;
        buf_sz += arr_size;
        base_addr_sz += arr_size;
        memcpy(buf_sz, &layout->irregular.lengths, sizeof(size_t[arr_size]));
        tmp_buffer->irregular.lengths = base_addr_sz;
      }
      break;
    default:
      MMB_ERR("Unknown type of layout.\n");
      return MMB_ERROR;
  }
  return stat;
}
