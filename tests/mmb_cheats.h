/*
 * Copyright (C) 2021 Cray UK
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */


#ifndef CHEATS_H
	#error "the header file \"cheats.h\" is not available"
#endif

#ifndef MMB_CHEATS_H
#define MMB_CHEATS_H

#include "../common/i_mmb_hash.h"

CHEAT_GENERATE_INTEGER(mmb_hash_int, mmb_hash_int_t, "%" PRImmbHashInt)
CHEAT_GENERATE_INTEGER(mmb_hash_iter, mmb_hash_iter_t, "%" PRImmbHashIter)
CHEAT_GENERATE_INTEGER(mmb_hash_int32, mmb_hash_int32_t, "%" PRImmbHashInt32)
CHEAT_GENERATE_INTEGER(mmb_hash_int64, mmb_hash_int64_t, "%" PRImmbHashInt64)

#define cheat_assert_mmb_hash_int(actual, expected) \
	cheat_check_mmb_hash_int(&cheat_suite, false, actual, expected, \
		__FILE__, __LINE__)

#define cheat_assert_not_mmb_hash_int(actual, expected) \
	cheat_check_mmb_hash_int(&cheat_suite, true, actual, expected, \
		__FILE__, __LINE__)

#define cheat_assert_mmb_hash_iter(actual, expected) \
	cheat_check_mmb_hash_int(&cheat_suite, false, actual, expected, \
		__FILE__, __LINE__)

#define cheat_assert_not_mmb_hash_iter(actual, expected) \
	cheat_check_mmb_hash_int(&cheat_suite, true, actual, expected, \
		__FILE__, __LINE__)

#define cheat_assert_mmb_hash_int32(actual, expected) \
	cheat_check_mmb_hash_int32(&cheat_suite, false, actual, expected, \
		__FILE__, __LINE__)

#define cheat_assert_not_mmb_hash_int32(actual, expected) \
	cheat_check_mmb_hash_int32(&cheat_suite, true, actual, expected, \
		__FILE__, __LINE__)

#define cheat_assert_mmb_hash_int64(actual, expected) \
	cheat_check_mmb_hash_int64(&cheat_suite, false, actual, expected, \
		__FILE__, __LINE__)

#define cheat_assert_not_mmb_hash_int64(actual, expected) \
	cheat_check_mmb_hash_int64(&cheat_suite, true, actual, expected, \
		__FILE__, __LINE__)

__attribute__ ((__io__, __nonnull__ (1, 5), __unused__))
static void cheat_check_bool(struct cheat_suite* const suite,
		bool const negate,
		bool const actual,
		bool const expected,
		char const* const file,
		size_t const line) {
	if (cheat_further(suite->outcome) && (actual == expected) != !negate) {
		char const* comparator;
		char* expression;

		suite->outcome = CHEAT_FAILED;

		if (negate)
			comparator = "!=";
		else
			comparator = "==";

		expression = CHEAT_CAST(char*, cheat_allocate_total(4,
					(size_t)5, strlen(comparator), (size_t)5, (size_t)3));
		if (expression == NULL)
			cheat_death("failed to allocate memory", errno);

		if (cheat_print_string(expression,
					"%s %s %s", 3, actual ? "true" : "false",
					comparator, expected ? "true" : "false") < 0)
			cheat_death("failed to build a string", errno);

		cheat_print_failure(suite, expression, file, line);
	}
}

#define cheat_assert_bool(actual, expected) \
	cheat_check_bool(&cheat_suite, false, actual, expected, \
		__FILE__, __LINE__)

#define cheat_assert_not_bool(actual, expected) \
	cheat_check_bool(&cheat_suite, true, actual, expected, \
		__FILE__, __LINE__)

#endif /* MMB_CHEATS_H */
