/*
 * Copyright (C) 2020 Cray UK
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/* needed before inclusion of cheat.h: */
#ifndef __BASE_FILE__
#define __BASE_FILE__ __FILE__
#endif

#include "cheat.h"
#include "cheats.h"
#include "mmb_cheats.h"

#include <mamba.h>

/* Used to check the internal values of the space */
#include "../memory/i_memory.h"

#define ONE_KiB (1UL << 10)
#define ONE_MiB (ONE_KiB << 10)
#define ONE_GiB (ONE_MiB << 10)
#define ONE_TiB (ONE_GiB << 10)

/* Set-up / Tear-down */

#ifndef MAX_LOG_LEVEL
#  ifdef MMB_LOG_DEBUG
#    define MAX_LOG_LEVEL MMB_LOG_DEBUG
#  else
#    define MAX_LOG_LEVEL 3
#  endif
#endif

#ifndef INT_ERRONEOUS_MIN_EXECUTION_CONTEXT
/* To be updated when adding a new execution context */
#define INT_ERRONEOUS_MIN_EXECUTION_CONTEXT (-3)
#endif /* INT_ERRONEOUS_MIN_EXECUTION_CONTEXT */

#ifndef INT_ERRONEOUS_MIN_LAYER
/* To be updated when adding a new layer */
#define INT_ERRONEOUS_MIN_LAYER (-1)
#endif /* INT_ERRONEOUS_MIN_LAYER */

CHEAT_DECLARE( mmbOptions *MMB_TESTS_INIT_DEFAULT; )

/* Tests */

CHEAT_DECLARE(
  /* Space configuration handle is similar for all spaces as it does not
   * depends on the execution context nor on the layer. */
  mmbMemSpaceConfig g_config, g_config_numa;
  void *default_pointer_addr = (void *) 0xABBA;
  uint64_t numa_sizes[2] = { 8000, 8000 };
)

CHEAT_SET_UP(
  g_config = (mmbMemSpaceConfig) {
    .size_opts = { .action = MMB_SIZE_SET, .mem_size = 8000, },
    .interface_opts =  MMB_MEMINTERFACE_CONFIG_DEFAULT,
  };
  g_config_numa = (mmbMemSpaceConfig) {
    .size_opts = {
      .action = MMB_SIZE_SET, .is_numa_aware = true,
      .numa_sizes = { .size = 2, .d = numa_sizes },
    },
    .interface_opts = MMB_MEMINTERFACE_CONFIG_DEFAULT,
  };

  cheat_assert_int(mmb_options_create_default(&MMB_TESTS_INIT_DEFAULT), MMB_OK);
  cheat_assert_int(mmb_options_set_debug_level(MMB_TESTS_INIT_DEFAULT,
                                               MAX_LOG_LEVEL),          MMB_OK);
  cheat_assert_int(mmb_init(MMB_TESTS_INIT_DEFAULT), MMB_OK);
)

CHEAT_TEAR_DOWN(
    cheat_assert_int(mmb_finalize(), MMB_OK);
    cheat_assert_int(mmb_options_destroy(MMB_TESTS_INIT_DEFAULT), MMB_OK);
)



/* Check the error checking of the layer parameter */
CHEAT_TEST(register_invalid_def_null_null,
    mmbError stat = MMB_OK;
    mmbMemLayer              layer  = INT_ERRONEOUS_MIN_LAYER;
    mmbExecutionContext      ex_con = MMB_EXECUTION_CONTEXT_DEFAULT;
    const mmbMemSpaceConfig *conf   = NULL;
    stat = mmb_register_memory(layer, ex_con, conf, NULL);
    cheat_assert_not_int(stat, MMB_OK);
    cheat_assert_int(stat, MMB_INVALID_LAYER);
    /* max */
    layer = MMB_MEMLAYER__MAX;
    stat = mmb_register_memory(layer, ex_con, conf, NULL);
    cheat_assert_not_int(stat, MMB_OK);
    cheat_assert_int(stat, MMB_INVALID_LAYER);
    /* above */
    layer = 2 * MMB_MEMLAYER__MAX;
    stat = mmb_register_memory(layer, ex_con, conf, NULL);
    cheat_assert_not_int(stat, MMB_OK);
    cheat_assert_int(stat, MMB_INVALID_LAYER);
)

/* Check the error checking of the execution context parameter */
CHEAT_TEST(register_dram_invalid_def_null,
    mmbError stat = MMB_OK;
    mmbMemLayer              layer  = MMB_DRAM;
    mmbExecutionContext      ex_con = INT_ERRONEOUS_MIN_EXECUTION_CONTEXT;
    const mmbMemSpaceConfig *conf   = NULL;
    stat = mmb_register_memory(layer, ex_con, conf, NULL);
    cheat_assert_not_int(stat, MMB_OK);
    cheat_assert_int(stat, MMB_INVALID_EXECUTION_CONTEXT);
    /* NONE */
    ex_con = MMB_EXECUTION_CONTEXT_NONE;
    stat = mmb_register_memory(layer, ex_con, conf, NULL);
    cheat_assert_not_int(stat, MMB_OK);
    cheat_assert_int(stat, MMB_INVALID_EXECUTION_CONTEXT);
    /* max */
    ex_con = MMB_EXECUTION_CONTEXT__MAX;
    stat = mmb_register_memory(layer, ex_con, conf, NULL);
    cheat_assert_not_int(stat, MMB_OK);
    cheat_assert_int(stat, MMB_INVALID_EXECUTION_CONTEXT);
    /* above */
    ex_con = 2 * MMB_EXECUTION_CONTEXT__MAX;
    stat = mmb_register_memory(layer, ex_con, conf, NULL);
    cheat_assert_not_int(stat, MMB_OK);
    cheat_assert_int(stat, MMB_INVALID_EXECUTION_CONTEXT);
)

/* Check the space parameter is not changed when calling mmb_register_memory
 * with bad parameters. */
CHEAT_TEST(register_error_ptr_unchanged,
    mmbError stat = MMB_OK;
    /* Invalid layer, valid execution context */
    {
      mmbMemLayer              layer  = INT_ERRONEOUS_MIN_LAYER;
      mmbExecutionContext      ex_con = MMB_EXECUTION_CONTEXT_DEFAULT;
      const mmbMemSpaceConfig *conf   = NULL;
      mmbMemSpace             *space  = default_pointer_addr;
      stat = mmb_register_memory(layer, ex_con, conf, &space);
      cheat_assert_not_int(stat, MMB_OK);
      cheat_assert_int(stat, MMB_INVALID_LAYER);
      cheat_assert_pointer(space, default_pointer_addr);
      /* max */
      layer = MMB_MEMLAYER__MAX;
      stat = mmb_register_memory(layer, ex_con, conf, &space);
      cheat_assert_not_int(stat, MMB_OK);
      cheat_assert_int(stat, MMB_INVALID_LAYER);
      cheat_assert_pointer(space, default_pointer_addr);
      /* above */
      layer = 2 * MMB_MEMLAYER__MAX;
      stat = mmb_register_memory(layer, ex_con, conf, &space);
      cheat_assert_not_int(stat, MMB_OK);
      cheat_assert_int(stat, MMB_INVALID_LAYER);
      cheat_assert_pointer(space, default_pointer_addr);
    }
    /* Valid layer, invalid execution context */
    {
      mmbMemLayer              layer  = MMB_DRAM;
      mmbExecutionContext      ex_con = INT_ERRONEOUS_MIN_EXECUTION_CONTEXT;
      const mmbMemSpaceConfig *conf   = NULL;
      mmbMemSpace             *space  = default_pointer_addr;
      stat = mmb_register_memory(layer, ex_con, conf, &space);
      cheat_assert_not_int(stat, MMB_OK);
      cheat_assert_int(stat, MMB_INVALID_EXECUTION_CONTEXT);
      cheat_assert_pointer(space, default_pointer_addr);
      /* NONE */
      ex_con = MMB_EXECUTION_CONTEXT_NONE;
      stat = mmb_register_memory(layer, ex_con, conf, &space);
      cheat_assert_not_int(stat, MMB_OK);
      cheat_assert_int(stat, MMB_INVALID_EXECUTION_CONTEXT);
      cheat_assert_pointer(space, default_pointer_addr);
      /* max */
      ex_con = MMB_EXECUTION_CONTEXT__MAX;
      stat = mmb_register_memory(layer, ex_con, conf, &space);
      cheat_assert_not_int(stat, MMB_OK);
      cheat_assert_int(stat, MMB_INVALID_EXECUTION_CONTEXT);
      cheat_assert_pointer(space, default_pointer_addr);
      /* above */
      ex_con = 2 * MMB_EXECUTION_CONTEXT__MAX;
      stat = mmb_register_memory(layer, ex_con, conf, &space);
      cheat_assert_not_int(stat, MMB_OK);
      cheat_assert_int(stat, MMB_INVALID_EXECUTION_CONTEXT);
      cheat_assert_pointer(space, default_pointer_addr);
    }
)

/* Test correctness of not providing any configuration and no return pointer */
CHEAT_TEST(register_dram_def_null_null,
    mmbError stat = MMB_OK;
    mmbMemLayer              layer  = MMB_DRAM;
    mmbExecutionContext      ex_con = MMB_EXECUTION_CONTEXT_DEFAULT;
    const mmbMemSpaceConfig *conf   = NULL;
    stat = mmb_register_memory(layer, ex_con, conf, NULL);
    cheat_assert_int(stat, MMB_OK);
)

/* Test correctness of not providing default configuration and no return pointer */
CHEAT_TEST(register_dram_def_def_null,
    mmbError stat = MMB_OK;
    mmbMemLayer              layer  = MMB_DRAM;
    mmbExecutionContext      ex_con = MMB_EXECUTION_CONTEXT_DEFAULT;
    const mmbMemSpaceConfig *conf   = &g_config;
    stat = mmb_register_memory(layer, ex_con, conf, NULL);
    cheat_assert_int(stat, MMB_OK);
)

/* mmb_register_memory(DRAM, DEFAULT_EX_CONTEXT, NULL, valid_ptr) */
CHEAT_TEST(register_dram_def_null_ptr,
    mmbError stat = MMB_OK;
    mmbMemLayer              layer  = MMB_DRAM;
    mmbExecutionContext      ex_con = MMB_EXECUTION_CONTEXT_DEFAULT;
    const mmbMemSpaceConfig *conf   = NULL;
    mmbMemSpace             *space  = NULL;
    stat = mmb_register_memory(layer, ex_con, conf, &space);
    cheat_assert_int(stat, MMB_OK);
    cheat_assert_not_pointer(space, NULL);
    cheat_assert_bool(false, space->is_numa_aware);
    /* check with default_pointer_addr to check NULL is not a special value. */
    space = default_pointer_addr;
    stat = mmb_register_memory(layer, ex_con, conf, &space);
    cheat_assert_int(stat, MMB_OK);
    cheat_assert_not_pointer(space, NULL);
    cheat_assert_bool(false, space->is_numa_aware);
)

/* mmb_register_memory(DRAM, DEFAULT_EX_CON, &g_config, valid_ptr) */
CHEAT_TEST(register_dram_def_def_ptr,
    mmbError stat = MMB_OK;
    mmbMemLayer              layer  = MMB_DRAM;
    mmbExecutionContext      ex_con = MMB_EXECUTION_CONTEXT_DEFAULT;
    const mmbMemSpaceConfig  *conf  = &g_config;
    mmbMemSpace             *space  = NULL;
    stat = mmb_register_memory(layer, ex_con, conf, &space);
    cheat_assert_int(stat, MMB_OK);
    cheat_assert_not_pointer(space, NULL);
    cheat_assert_bool(false, space->is_numa_aware);
)

/* mmb_register_memory(DRAM, CPU, NULL, valid_ptr) */
CHEAT_TEST(register_dram_cpu_null_ptr,
    mmbError stat = MMB_OK;
    mmbMemLayer              layer  = MMB_DRAM;
    mmbExecutionContext      ex_con = MMB_CPU;
    const mmbMemSpaceConfig *conf   = NULL;
    mmbMemSpace             *space  = NULL;
    stat = mmb_register_memory(layer, ex_con, conf, &space);
    cheat_assert_int(stat, MMB_OK);
    cheat_assert_not_pointer(space, NULL);
    cheat_assert_bool(false, space->is_numa_aware);
)

/* mmb_register_memory(DRAM, CPU, DEFAULT, valid_ptr) */
CHEAT_TEST(register_dram_cpu_def_ptr,
    mmbError stat = MMB_OK;
    mmbMemLayer              layer  = MMB_DRAM;
    mmbExecutionContext      ex_con = MMB_CPU;
    const mmbMemSpaceConfig *conf   = &g_config;
    mmbMemSpace             *space  = NULL;
    stat = mmb_register_memory(layer, ex_con, conf, &space);
    cheat_assert_int(stat, MMB_OK);
    cheat_assert_not_pointer(space, NULL);
    cheat_assert_bool(false, space->is_numa_aware);
)

#if HAVE_CUDA || HAVE_OPENCL || HAVE_ROCM
/* mmb_register_memory(GDRAM, CPU, NULL, valid_ptr) */
CHEAT_TEST(register_gdram_gpu_null_ptr,
    mmbError stat = MMB_OK;
    mmbMemLayer              layer  = MMB_GDRAM;
    mmbExecutionContext      ex_con = MMB_EXECUTION_CONTEXT_DEFAULT;
    const mmbMemSpaceConfig *conf   = NULL;
    mmbMemSpace             *space  = NULL;
    stat = mmb_register_memory(layer, ex_con, conf, &space);
    cheat_assert_int(stat, MMB_OK);
    cheat_assert_not_pointer(space, NULL);
)
#endif /* HAVE_CUDA || HAVE_OPENCL || HAVE_ROCM */

/* test mmb_register_memory() with mem_size = 0 */
CHEAT_TEST(register_zero,
    mmbError stat = MMB_OK;
    mmbMemLayer         layer  = MMB_DRAM;
    mmbExecutionContext ex_con = MMB_CPU;
    mmbMemSpaceConfig   config = g_config;
    mmbMemSpace        *space  = NULL;
    config.size_opts.mem_size  = 0;
    stat = mmb_register_memory(layer, ex_con, &config, &space);
    cheat_assert_int(stat, MMB_OK);
    cheat_assert_not_pointer(space, NULL);
)

/* test mmb_register_memory() with mem_size = 1TiB */
CHEAT_TEST(register_big,
    mmbError stat = MMB_OK;
    mmbMemLayer         layer  = MMB_DRAM;
    mmbExecutionContext ex_con = MMB_CPU;
    mmbMemSpaceConfig   config = g_config;
    mmbMemSpace        *space  = NULL;
    config.size_opts.mem_size  = 1 * ONE_MiB;   // 1 TibiByte as the unit is
                                                // MiB in mmb_register_memory
    stat = mmb_register_memory(layer, ex_con, &config, &space);
    cheat_assert_int(stat, MMB_OK);
    cheat_assert_not_pointer(space, NULL);
)

#if HAVE_NUMA

/** Check register NUMA aware memory */

/* Check the error checking of the config parameter */
CHEAT_TEST(register_numa_dram_def_invalid_null,
    mmbError stat = MMB_OK;
    mmbMemLayer              layer  = MMB_DRAM;
    mmbExecutionContext      ex_con = MMB_EXECUTION_CONTEXT_DEFAULT;
    stat = mmb_register_numa_memory(layer, ex_con, NULL, NULL);
    cheat_assert_not_int(stat, MMB_OK);
    cheat_assert_int(stat, MMB_INVALID_ARG);
    uint64_t local_numa_sizes[2] = { 8000, 8000 };
    const mmbMemSpaceConfig conf = {
      .size_opts = {
        .action = MMB_SIZE_SET, .is_numa_aware = false,
        .numa_sizes = { .size = 2, .d = local_numa_sizes },
      },
      .interface_opts = MMB_MEMINTERFACE_CONFIG_DEFAULT,
    };
    stat = mmb_register_numa_memory(layer, ex_con, &conf, NULL);
    cheat_assert_not_int(stat, MMB_OK);
    cheat_assert_int(stat, MMB_INVALID_ARG);
)

/* Check the error checking of the layer parameter */
CHEAT_TEST(register_numa_invalid_def_null_null,
    mmbError stat = MMB_OK;
    mmbMemLayer              layer  = INT_ERRONEOUS_MIN_LAYER;
    mmbExecutionContext      ex_con = MMB_EXECUTION_CONTEXT_DEFAULT;
    const mmbMemSpaceConfig *conf   = &g_config_numa;
    stat = mmb_register_numa_memory(layer, ex_con, conf, NULL);
    cheat_assert_not_int(stat, MMB_OK);
    cheat_assert_int(stat, MMB_INVALID_LAYER);
    /* max */
    layer = MMB_MEMLAYER__MAX;
    stat = mmb_register_numa_memory(layer, ex_con, conf, NULL);
    cheat_assert_not_int(stat, MMB_OK);
    cheat_assert_int(stat, MMB_INVALID_LAYER);
    /* above */
    layer = 2 * MMB_MEMLAYER__MAX;
    stat = mmb_register_numa_memory(layer, ex_con, conf, NULL);
    cheat_assert_not_int(stat, MMB_OK);
    cheat_assert_int(stat, MMB_INVALID_LAYER);
)

/* Check the error checking of the execution context parameter */
CHEAT_TEST(register_numa_dram_invalid_def_null,
    mmbError stat = MMB_OK;
    mmbMemLayer              layer  = MMB_DRAM;
    mmbExecutionContext      ex_con = INT_ERRONEOUS_MIN_EXECUTION_CONTEXT;
    const mmbMemSpaceConfig *conf   = &g_config_numa;
    stat = mmb_register_numa_memory(layer, ex_con, conf, NULL);
    cheat_assert_not_int(stat, MMB_OK);
    cheat_assert_int(stat, MMB_INVALID_EXECUTION_CONTEXT);
    /* NONE */
    ex_con = MMB_EXECUTION_CONTEXT_NONE;
    stat = mmb_register_numa_memory(layer, ex_con, conf, NULL);
    cheat_assert_not_int(stat, MMB_OK);
    cheat_assert_int(stat, MMB_INVALID_EXECUTION_CONTEXT);
    /* max */
    ex_con = MMB_EXECUTION_CONTEXT__MAX;
    stat = mmb_register_numa_memory(layer, ex_con, conf, NULL);
    cheat_assert_not_int(stat, MMB_OK);
    cheat_assert_int(stat, MMB_INVALID_EXECUTION_CONTEXT);
    /* above */
    ex_con = 2 * MMB_EXECUTION_CONTEXT__MAX;
    stat = mmb_register_numa_memory(layer, ex_con, conf, NULL);
    cheat_assert_not_int(stat, MMB_OK);
    cheat_assert_int(stat, MMB_INVALID_EXECUTION_CONTEXT);
)

/* Check the space parameter is not changed when calling mmb_register_numa_memory
 * with bad parameters. */
CHEAT_TEST(register_numa_error_ptr_unchanged,
    mmbError stat = MMB_OK;
    /* Invalid layer, valid execution context */
    {
      mmbMemLayer              layer  = INT_ERRONEOUS_MIN_LAYER;
      mmbExecutionContext      ex_con = MMB_EXECUTION_CONTEXT_DEFAULT;
      const mmbMemSpaceConfig *conf   = &g_config_numa;
      mmbMemSpace             *space  = default_pointer_addr;
      stat = mmb_register_numa_memory(layer, ex_con, conf, &space);
      cheat_assert_not_int(stat, MMB_OK);
      cheat_assert_int(stat, MMB_INVALID_LAYER);
      cheat_assert_pointer(space, default_pointer_addr);
      /* max */
      layer = MMB_MEMLAYER__MAX;
      stat = mmb_register_numa_memory(layer, ex_con, conf, &space);
      cheat_assert_not_int(stat, MMB_OK);
      cheat_assert_int(stat, MMB_INVALID_LAYER);
      cheat_assert_pointer(space, default_pointer_addr);
      /* above */
      layer = 2 * MMB_MEMLAYER__MAX;
      stat = mmb_register_numa_memory(layer, ex_con, conf, &space);
      cheat_assert_not_int(stat, MMB_OK);
      cheat_assert_int(stat, MMB_INVALID_LAYER);
      cheat_assert_pointer(space, default_pointer_addr);
    }
    /* Valid layer, invalid execution context */
    {
      mmbMemLayer              layer  = MMB_DRAM;
      mmbExecutionContext      ex_con = INT_ERRONEOUS_MIN_EXECUTION_CONTEXT;
      const mmbMemSpaceConfig *conf   = &g_config_numa;
      mmbMemSpace             *space  = default_pointer_addr;
      stat = mmb_register_numa_memory(layer, ex_con, conf, &space);
      cheat_assert_not_int(stat, MMB_OK);
      cheat_assert_int(stat, MMB_INVALID_EXECUTION_CONTEXT);
      cheat_assert_pointer(space, default_pointer_addr);
      /* NONE */
      ex_con = MMB_EXECUTION_CONTEXT_NONE;
      stat = mmb_register_numa_memory(layer, ex_con, conf, &space);
      cheat_assert_not_int(stat, MMB_OK);
      cheat_assert_int(stat, MMB_INVALID_EXECUTION_CONTEXT);
      cheat_assert_pointer(space, default_pointer_addr);
      /* max */
      ex_con = MMB_EXECUTION_CONTEXT__MAX;
      stat = mmb_register_numa_memory(layer, ex_con, conf, &space);
      cheat_assert_not_int(stat, MMB_OK);
      cheat_assert_int(stat, MMB_INVALID_EXECUTION_CONTEXT);
      cheat_assert_pointer(space, default_pointer_addr);
      /* above */
      ex_con = 2 * MMB_EXECUTION_CONTEXT__MAX;
      stat = mmb_register_numa_memory(layer, ex_con, conf, &space);
      cheat_assert_not_int(stat, MMB_OK);
      cheat_assert_int(stat, MMB_INVALID_EXECUTION_CONTEXT);
      cheat_assert_pointer(space, default_pointer_addr);
    }
)

/* Test correctness of not providing any configuration and no return pointer */
CHEAT_TEST(register_numa_dram_def_def_null,
    mmbError stat = MMB_OK;
    mmbMemLayer              layer  = MMB_DRAM;
    mmbExecutionContext      ex_con = MMB_EXECUTION_CONTEXT_DEFAULT;
    const mmbMemSpaceConfig *conf   = &g_config_numa;
    stat = mmb_register_numa_memory(layer, ex_con, conf, NULL);
    cheat_assert_int(stat, MMB_OK);
)

/* mmb_register_numa_memory(DRAM, DEFAULT_EX_CONTEXT, DEFAULT, valid_ptr) */
CHEAT_TEST(register_numa_dram_def_conf_ptr,
    mmbError stat = MMB_OK;
    mmbMemLayer              layer  = MMB_DRAM;
    mmbExecutionContext      ex_con = MMB_EXECUTION_CONTEXT_DEFAULT;
    const mmbMemSpaceConfig *conf   = &g_config_numa;
    mmbMemSpace             *space  = NULL;
    stat = mmb_register_numa_memory(layer, ex_con, conf, &space);
    cheat_assert_int(stat, MMB_OK);
    cheat_assert_not_pointer(space, NULL);
    cheat_assert_bool(true, space->is_numa_aware);
    cheat_assert_not_size(0, space->max_bytes);
    /* check with default_pointer_addr to check NULL is not a special value. */
    space = default_pointer_addr;
    stat = mmb_register_numa_memory(layer, ex_con, conf, &space);
    cheat_assert_int(stat, MMB_OK);
    cheat_assert_not_pointer(space, NULL);
    cheat_assert_bool(true, space->is_numa_aware);
    cheat_assert_not_size(0, space->max_bytes);
)

/* mmb_register_numa_memory(DRAM, CPU, DEFAULT, valid_ptr) */
CHEAT_TEST(register_numa_dram_cpu_def_ptr,
    mmbError stat = MMB_OK;
    mmbMemLayer              layer  = MMB_DRAM;
    mmbExecutionContext      ex_con = MMB_CPU;
    const mmbMemSpaceConfig *conf   = &g_config_numa;
    mmbMemSpace             *space  = NULL;
    stat = mmb_register_numa_memory(layer, ex_con, conf, &space);
    cheat_assert_int(stat, MMB_OK);
    cheat_assert_not_pointer(space, NULL);
    cheat_assert_bool(true, space->is_numa_aware);
    cheat_assert_not_size(0, space->max_bytes);
)

/* test mmb_register_numa_memory() with mem_size = 0 */
CHEAT_TEST(register_numa_zero,
    mmbError stat = MMB_OK;
    mmbMemLayer         layer  = MMB_DRAM;
    mmbExecutionContext ex_con = MMB_CPU;
    mmbMemSpaceConfig   config = g_config_numa;
    mmbMemSpace        *space  = NULL;
    uint64_t local_numa_sizes[2] = { 0, 0 };
    config.size_opts.numa_sizes.d = local_numa_sizes;
    stat = mmb_register_numa_memory(layer, ex_con, &config, &space);
    cheat_assert_int(stat, MMB_OK);
    cheat_assert_not_pointer(space, NULL);
    cheat_assert_bool(true, space->is_numa_aware);
    cheat_assert_size(0, space->max_bytes);
    cheat_assert_size(0, space->numa_sizes[0]);
    cheat_assert_size(0, space->numa_sizes[1]);
)

/* test mmb_register_numa_memory() with 2x 1MiB numa nodes. */
CHEAT_TEST(register_numa_big,
    mmbError stat = MMB_OK;
    mmbMemLayer         layer  = MMB_DRAM;
    mmbExecutionContext ex_con = MMB_CPU;
    mmbMemSpaceConfig   config = g_config_numa;
    mmbMemSpace        *space  = NULL;
    // 1 TibiByte as the unit is MiB in mmb_register_numa_memory
    uint64_t local_numa_sizes[2] = { 1 * ONE_MiB, 1 * ONE_MiB };
    config.size_opts.numa_sizes.d = local_numa_sizes;
    stat = mmb_register_numa_memory(layer, ex_con, &config, &space);
    cheat_assert_int(stat, MMB_OK);
    cheat_assert_not_pointer(space, NULL);
    cheat_assert_bool(true, space->is_numa_aware);
    cheat_assert_size(2 * ONE_MiB * 1024*1024, space->max_bytes);
    cheat_assert_size(1 * ONE_MiB * 1024*1024, space->numa_sizes[0]);
    cheat_assert_size(1 * ONE_MiB * 1024*1024, space->numa_sizes[1]);
)

#endif /* HAVE_NUMA */
