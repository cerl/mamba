/*
 * Copyright (C) 2021     Cray UK
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#ifdef __APPLE__
#include <OpenCL/opencl.h>
#else
#include <CL/opencl.h>
#endif

#include "mamba.h"
#include "mmb_logging.h"
#include "mmb_error.h"
#include "mmb_memory.h"
#include "mmb_opencl.h"

#ifndef MAX_PLATFORMS
#define MAX_PLATFORMS 8
#endif /* MAX_PLATFORMS */
#ifndef MAX_DEVICES
#define MAX_DEVICES 16
#endif /* MAX_DEVICES */
#ifndef MAX_DEVICE_NAME
#define MAX_DEVICE_NAME 1024
#endif /* MAX_DEVICE_NAME */

static inline const char *
opencl_error_decode(const cl_int ocl_err);

static const char * check_tile_kernel =
  "__kernel "
  "void check_tile(__global long int *buff,"
  "                unsigned int halo,"
  "                unsigned int width,"
  "                unsigned int height,"
  "                __global int *is_valid)"
  "{"
  "  const unsigned int tid = get_global_id(0);"
  "  /* Only check with one worker */"
  "  if (0 != tid) return;"
  "  *is_valid = 1;"
  "  /* Get pitch size */"
  "  const unsigned int pitch = width + 2 * halo;"
  "  /* Check the tile into centered in the halo */"
  "  for (unsigned int row = 0; height > row; ++row)"
  "    for (unsigned int column = 0; width > column; ++column)"
  "      if (buff[pitch*(halo+row)+halo+column] != (long int)(row*width+column))"
  "        return;"
  "  /* Check rows from the halo */"
  "  for (unsigned int row = 0; halo > row; ++row)"
  "    for (unsigned int column = 0; pitch > column; ++column)"
  "      if (   buff[row*pitch+column] != -1   /* top row */"
  "          || buff[(row+halo+height)*pitch+column] != -1)    /* bottom row */"
  "        return;"
  "  /* Check column from the halo */"
  "  for (unsigned int row = 0; height > row; ++row)"
  "    for (unsigned int column = 0; halo > column; ++column)"
  "      if (   buff[pitch*(halo+row)+column] != -1    /* left column */"
  "          || buff[pitch*(halo+row)+width+halo+column] != -1)    /* right column */"
  "        return;"
  "  *is_valid = 1;"
  "}";

mmbError check_copy_2d_opencl(mmbAllocation *ocl_buff,
                              const size_t halo,
                              const size_t width,
                              const size_t height,
                              const struct mmb_opencl_t *oclopt,
                              int *err)
{
  cl_int ocl_err;
  mmbError stat = MMB_ERROR;

  /* Create OpenCL Program */
  const size_t src_len = strlen(check_tile_kernel);
  cl_program program = clCreateProgramWithSource(oclopt->context, 1,
                                                 &check_tile_kernel,
                                                 &src_len, &ocl_err);
  if (CL_SUCCESS != ocl_err) {
    MMB_ERR("Failed to create program with source, err: %s\n",
            opencl_error_decode(ocl_err));
    *err = 8;
    return stat;
  }

  /* Build Kernel Program */
  ocl_err = clBuildProgram(program, 1, &oclopt->device, NULL, NULL, NULL);
  if (CL_SUCCESS != ocl_err) {
    MMB_ERR("Failed to build program, err: %s\n",
            opencl_error_decode(ocl_err));
    *err = 9;
    if (ocl_err == CL_BUILD_PROGRAM_FAILURE) {
      /* Determine the size of the log */
      size_t log_size;
      ocl_err = clGetProgramBuildInfo(program, oclopt->device,
                                      CL_PROGRAM_BUILD_LOG,
                                      0, NULL, &log_size);
      if (CL_SUCCESS != ocl_err) {
        MMB_ERR("Failed to retrieve program build log size, err: %s\n",
                opencl_error_decode(ocl_err));
      }
      /* Allocate memory for the log */
      char *log = malloc(log_size);
      if (NULL == log) {
        MMB_ERR("Failed to allocate memory for build log retrieving.\n");
      } else {
        /* Get the log */
        ocl_err = clGetProgramBuildInfo(program, oclopt->device,
                                        CL_PROGRAM_BUILD_LOG,
                                        log_size, log, NULL);
        if (CL_SUCCESS != ocl_err) {
          MMB_ERR("Failed to retrieve program build log, err: %s\n",
              opencl_error_decode(ocl_err));
        }
        /* Print the log */
        MMB_DEBUG("%s\n", log);
      }
      free(log);
    }
    return stat;
  }

  /* Create OpenCL Kernel */
  cl_kernel kernel = clCreateKernel(program, "check_tile", &ocl_err);
  if (CL_SUCCESS != ocl_err) {
    MMB_ERR("Failed to build program, err: %s\n",
            opencl_error_decode(ocl_err));
    *err = 10;
    return stat;
  }

  /* Create check variable */
  int is_valid = -1;
  cl_mem ocl_is_valid;
  ocl_is_valid = clCreateBuffer(oclopt->context,
                                CL_MEM_WRITE_ONLY | CL_MEM_COPY_HOST_PTR,
                                sizeof(is_valid), &is_valid, &ocl_err);
  if (CL_SUCCESS != ocl_err) {
    MMB_ERR("Failed to allocate the return value. err: %s\n",
            opencl_error_decode(ocl_err));
    *err = 11;
    return stat;
  }

  /* Set arguments */
  ocl_err = clSetKernelArg(kernel, 0, sizeof(cl_mem), (cl_mem *)&ocl_buff->ptr);
  if (CL_SUCCESS != ocl_err) {
    MMB_ERR("Error: Unable to set kernel parameter #%d. err: %s.\n",
            0, opencl_error_decode(ocl_err));
    *err = 14;
    return stat;
  }
  ocl_err = clSetKernelArg(kernel, 1, sizeof(cl_uint), (void *)&halo);
  if (CL_SUCCESS != ocl_err) {
    MMB_ERR("Error: Unable to set kernel parameter #%d. err: %s.\n",
            1, opencl_error_decode(ocl_err));
    *err = 15;
    return stat;
  }
  ocl_err = clSetKernelArg(kernel, 2, sizeof(cl_uint), (void *)&width);
  if (CL_SUCCESS != ocl_err) {
    MMB_ERR("Error: Unable to set kernel parameter #%d. err: %s.\n",
            2, opencl_error_decode(ocl_err));
    *err = 16;
    return stat;
  }
  ocl_err = clSetKernelArg(kernel, 3, sizeof(cl_uint), (void *)&height);
  if (CL_SUCCESS != ocl_err) {
    MMB_ERR("Error: Unable to set kernel parameter #%d. err: %s.\n",
            3, opencl_error_decode(ocl_err));
    *err = 17;
    return stat;
  }
  ocl_err = clSetKernelArg(kernel, 4, sizeof(cl_mem), (void *)&ocl_is_valid);
  if (CL_SUCCESS != ocl_err) {
    MMB_ERR("Error: Unable to set kernel parameter #%d. err: %s.\n",
            4, opencl_error_decode(ocl_err));
    *err = 18;
    return stat;
  }

  /* Check 2D copy */
  size_t work_dim = 1;
  ocl_err = clEnqueueNDRangeKernel(oclopt->queue, kernel, 1, NULL, &work_dim,
                                   &work_dim, 0, NULL, NULL);
  if (CL_SUCCESS != ocl_err) {
    MMB_ERR("Failed to enqueue task. err: %s\n",
            opencl_error_decode(ocl_err));
    *err = 19;
    return stat;
  }

  /* Check is_value returned value */
  ocl_err = clEnqueueReadBuffer(oclopt->queue, ocl_is_valid, CL_TRUE, 0,
                                sizeof(is_valid), &is_valid, 0, NULL, NULL);
  if (CL_SUCCESS != ocl_err) {
    MMB_ERR("Failed to retrieve validity check. (err: %s).\n",
            opencl_error_decode(ocl_err));
    *err = 20;
    return stat;
  } else if (1 != is_valid) {
    MMB_ERR("Invalid check. Bad data copy. (is_valid=%d)\n", is_valid);
    *err = 21;
    if (MMB_LOG_DEBUG <= MMB_MAX_LOG_LEVEL) {
      long int local_buff[(2*halo+width)*(2*halo+height)];
      ocl_err = clEnqueueReadBuffer(oclopt->queue, (cl_mem) ocl_buff->ptr, CL_TRUE,
                                    0, sizeof local_buff, local_buff, 0, NULL, NULL);
      if (CL_SUCCESS != ocl_err) {
        MMB_ERR("Error: unable to 2D copy back from device buffer. err: %s\n",
                opencl_error_decode(ocl_err));
        *err = 22;
        return stat;
      } else {
        int line_s = 0;
        const size_t pitch = width + 2 * halo;
        const size_t max_line = 5 * pitch;
        char line[max_line];
        MMB_DEBUG("Buffer after copy:\n");
        for (size_t row = 0; height + 2 * halo > row; ++row) {
          for (size_t column = 0; pitch > column; ++column)
            line_s += snprintf(&line[line_s], max_line - line_s, " %2ld%c",
                               local_buff[row*pitch+column],
                               column + 1 == pitch ? '\n' : ',');
          MMB_DEBUG("%s", line);
          line_s = 0;
        }
      }
    }
    return stat;
  }

  /* Freeing memory back */
  ocl_err = clReleaseMemObject(ocl_is_valid);
  if (CL_SUCCESS != ocl_err) {
      MMB_ERR("Failed to release validity return value. err: %s.\n",
              opencl_error_decode(ocl_err));
      *err = 23;
      return stat;
  }

  stat = MMB_OK;
  return stat;
}

mmbError init_opencl_parameters(cl_device_id *device,
                                cl_context *context,
                                cl_command_queue *queue,
                                int *err)
{
  cl_int ocl_err;
  mmbError stat = MMB_ERROR;

  /* Get list of platforms */
  cl_uint num_platforms = 0;
  cl_platform_id platforms[MAX_PLATFORMS];
  ocl_err = clGetPlatformIDs(MAX_PLATFORMS, platforms, &num_platforms);
  if (CL_SUCCESS == ocl_err) {
    MMB_NOISE("%u platforms found.\n", num_platforms);
  } else if (CL_SUCCESS != ocl_err) {
    MMB_ERR("Unable to gather plarforms ID's (%s).\n",
            opencl_error_decode(ocl_err));
    *err = 1;
    return stat;
  }

  /* Get list of devices */
  cl_uint total_devices = 0;
  cl_device_id devices[MAX_DEVICES];
  for (cl_uint p = 0; p < num_platforms; ++p) {
    cl_uint num_devices = 0;
    ocl_err = clGetDeviceIDs(platforms[p], CL_DEVICE_TYPE_ALL,
                             MAX_DEVICES - total_devices, devices + total_devices,
                             &num_devices);
    if (CL_SUCCESS != ocl_err) {
      MMB_ERR("Failed to get device name. (%s).\n",
              opencl_error_decode(ocl_err));
      *err = 2;
      return stat;
    }
    total_devices += num_devices;
  }

  /* Print list of devices */
  char name[MAX_DEVICE_NAME];
  size_t name_len = MAX_DEVICE_NAME;
  if (MMB_LOG_NOISE <= MMB_MAX_LOG_LEVEL) {
    MMB_NOISE("Available OpenCL devices:\n");
    for (cl_uint d = 0; d < total_devices; ++d) {
      ocl_err = clGetDeviceInfo(devices[d], CL_DEVICE_NAME,
                                name_len, name, &name_len);
      if (CL_SUCCESS == ocl_err) {
        MMB_NOISE("\t%2d: %s%s\n", d, name,
                  MAX_DEVICE_NAME < name_len ? " (possibly truncated)" : "");
      } else {
        MMB_NOISE("\t%2d: (err: %s)\n", d, opencl_error_decode(ocl_err));
        MMB_NOISE("\t\tname_len=%zu, MAX_DEVICE_NAME=%d\n",
                  name_len, MAX_DEVICE_NAME);
      }
    }
  }

  /* Use first device unless OCL_DEVICE environment variable is given */
  cl_uint device_index = 0;
  char *dev_env = getenv("OCL_DEVICE");
  if (NULL != dev_env) {
    char *end;
    device_index = strtol(dev_env, &end, 10);
    if (0 < strlen(end)) {
      MMB_ERR("Invalid OCL_DEVICE variable \"%s\".\n", dev_env);
      *err = 3;
      return stat;
    }
  }

  if (device_index >= total_devices) {
    MMB_ERR("Device index set to %d but only %d devices available.\n",
            device_index, total_devices);
    *err = 4;
    return stat;
  }

  /* set device */
  *device = devices[device_index];

  /* Print OpenCL device name */
  ocl_err = clGetDeviceInfo(*device, CL_DEVICE_NAME, name_len, name, &name_len);
  if (CL_SUCCESS == ocl_err) {
    MMB_DEBUG("Selected OpenCL device: %s%s (index=%d).\n", name,
              MAX_DEVICE_NAME < name_len ? " (possibly truncated)" : "",
              device_index);
  } else {
    MMB_ERR("Unable to retrieve device's name (err: %s).\n",
            opencl_error_decode(ocl_err));
    *err = 5;
    return stat;
  }

  /* Create OpenCL context */
  cl_context_properties properties[] = {
    CL_CONTEXT_PLATFORM, (cl_context_properties) platforms[0],
    0 };
  *context = clCreateContext(properties, 1, device, NULL, NULL, &ocl_err);
  if (CL_SUCCESS != ocl_err) {
    MMB_ERR("Failed to create opencl context, err: %s\n",
            opencl_error_decode(ocl_err));
    *err = 6;
    return stat;
  }

  /* Create Command Queue */
  *queue = clCreateCommandQueue(*context, *device, 0, &ocl_err);
  if (CL_SUCCESS != ocl_err) {
    MMB_ERR("Failed to create opencl command queue, err: %s\n",
            opencl_error_decode(ocl_err));
    *err = 7;
    return stat;
  }

  stat = MMB_OK;
  return stat;
}

#ifndef CASE_TO_STR
#define CASE_TO_STR(a) case a: return #a
#endif /* CASE_TO_STR */

static inline const char *
opencl_error_decode(const cl_int ocl_err)
{
  switch(ocl_err) {
    /* run-time and JIT compiler errors */
    CASE_TO_STR(CL_SUCCESS);
    CASE_TO_STR(CL_DEVICE_NOT_FOUND);
    CASE_TO_STR(CL_DEVICE_NOT_AVAILABLE);
    CASE_TO_STR(CL_COMPILER_NOT_AVAILABLE);
    CASE_TO_STR(CL_MEM_OBJECT_ALLOCATION_FAILURE);
    CASE_TO_STR(CL_OUT_OF_RESOURCES);
    CASE_TO_STR(CL_OUT_OF_HOST_MEMORY);
    CASE_TO_STR(CL_PROFILING_INFO_NOT_AVAILABLE);
    CASE_TO_STR(CL_MEM_COPY_OVERLAP);
    CASE_TO_STR(CL_IMAGE_FORMAT_MISMATCH);
    CASE_TO_STR(CL_IMAGE_FORMAT_NOT_SUPPORTED);
    CASE_TO_STR(CL_BUILD_PROGRAM_FAILURE);
    CASE_TO_STR(CL_MAP_FAILURE);
    CASE_TO_STR(CL_MISALIGNED_SUB_BUFFER_OFFSET);
    CASE_TO_STR(CL_EXEC_STATUS_ERROR_FOR_EVENTS_IN_WAIT_LIST);
    CASE_TO_STR(CL_COMPILE_PROGRAM_FAILURE);
    CASE_TO_STR(CL_LINKER_NOT_AVAILABLE);
    CASE_TO_STR(CL_LINK_PROGRAM_FAILURE);
    CASE_TO_STR(CL_DEVICE_PARTITION_FAILED);
    CASE_TO_STR(CL_KERNEL_ARG_INFO_NOT_AVAILABLE);

    /* compile-time errors */
    CASE_TO_STR(CL_INVALID_VALUE);
    CASE_TO_STR(CL_INVALID_DEVICE_TYPE);
    CASE_TO_STR(CL_INVALID_PLATFORM);
    CASE_TO_STR(CL_INVALID_DEVICE);
    CASE_TO_STR(CL_INVALID_CONTEXT);
    CASE_TO_STR(CL_INVALID_QUEUE_PROPERTIES);
    CASE_TO_STR(CL_INVALID_COMMAND_QUEUE);
    CASE_TO_STR(CL_INVALID_HOST_PTR);
    CASE_TO_STR(CL_INVALID_MEM_OBJECT);
    CASE_TO_STR(CL_INVALID_IMAGE_FORMAT_DESCRIPTOR);
    CASE_TO_STR(CL_INVALID_IMAGE_SIZE);
    CASE_TO_STR(CL_INVALID_SAMPLER);
    CASE_TO_STR(CL_INVALID_BINARY);
    CASE_TO_STR(CL_INVALID_BUILD_OPTIONS);
    CASE_TO_STR(CL_INVALID_PROGRAM);
    CASE_TO_STR(CL_INVALID_PROGRAM_EXECUTABLE);
    CASE_TO_STR(CL_INVALID_KERNEL_NAME);
    CASE_TO_STR(CL_INVALID_KERNEL_DEFINITION);
    CASE_TO_STR(CL_INVALID_KERNEL);
    CASE_TO_STR(CL_INVALID_ARG_INDEX);
    CASE_TO_STR(CL_INVALID_ARG_VALUE);
    CASE_TO_STR(CL_INVALID_ARG_SIZE);
    CASE_TO_STR(CL_INVALID_KERNEL_ARGS);
    CASE_TO_STR(CL_INVALID_WORK_DIMENSION);
    CASE_TO_STR(CL_INVALID_WORK_GROUP_SIZE);
    CASE_TO_STR(CL_INVALID_WORK_ITEM_SIZE);
    CASE_TO_STR(CL_INVALID_GLOBAL_OFFSET);
    CASE_TO_STR(CL_INVALID_EVENT_WAIT_LIST);
    CASE_TO_STR(CL_INVALID_EVENT);
    CASE_TO_STR(CL_INVALID_OPERATION);
    CASE_TO_STR(CL_INVALID_GL_OBJECT);
    CASE_TO_STR(CL_INVALID_BUFFER_SIZE);
    CASE_TO_STR(CL_INVALID_MIP_LEVEL);
    CASE_TO_STR(CL_INVALID_GLOBAL_WORK_SIZE);
    CASE_TO_STR(CL_INVALID_PROPERTY);
    CASE_TO_STR(CL_INVALID_IMAGE_DESCRIPTOR);
    CASE_TO_STR(CL_INVALID_COMPILER_OPTIONS);
    CASE_TO_STR(CL_INVALID_LINKER_OPTIONS);
    CASE_TO_STR(CL_INVALID_DEVICE_PARTITION_COUNT);
    default: return "Unknown OpenCL error";
  }
}

#undef CASE_TO_STR
