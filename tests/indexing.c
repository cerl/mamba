/*
 * Copyright (C) 2021      Cray UK
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/* needed before inclusion of cheat.h: */
#ifndef __BASE_FILE__
#define __BASE_FILE__ __FILE__
#endif


#include "cheat.h"
#include "cheats.h"

#include <stdlib.h>
#include <stdbool.h>

#include <mamba.h>
#include <mmb_memory.h>
#include <mmb_layout.h>

#ifndef MAX_LOG_LEVEL
#  ifdef MMB_LOG_DEBUG
#    define MAX_LOG_LEVEL MMB_LOG_DEBUG
#  else
#    define MAX_LOG_LEVEL 3
#  endif
#endif

/***************************************************************************
 *
 *  Set-up / Tear-down
 *
 ***************************************************************************/

CHEAT_DECLARE(
  mmbOptions *MMB_TESTS_INIT_DEFAULT;
  const mmbProvider TESTED_PROVIDER = MMB_SYSTEM;
  const mmbStrategy TESTED_STRATEGY = MMB_STRATEGY_NONE;
  const mmbAccessType TESTED_ARRAY_TYPE = MMB_READ_WRITE;

  mmbMemSpace *dram_space = NULL;
  mmbMemInterface *dram_interface = NULL;
  const uint32_t buffer[12] = { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11 };
)

CHEAT_DECLARE(
  mmbError register_test_memory (void)
  {
    int discovery_enabled = 0;
    cheat_assert_int(MMB_OK, mmb_discovery_is_enabled(&discovery_enabled));
    if (discovery_enabled) {
      return MMB_OK;
    }
    const mmbMemSpaceConfig dram_config = {
      .size_opts = { .action = MMB_SIZE_SET, .mem_size = 8000 },
      .interface_opts = { .provider = MMB_SYSTEM, .strategy = MMB_STRATEGY_NONE },
    };
    cheat_assert_int(MMB_OK,
                     mmb_register_memory(MMB_DRAM, MMB_CPU, &dram_config, NULL));
    return MMB_OK;
  }
)

CHEAT_SET_UP(
  cheat_assert_int(MMB_OK, mmb_options_create_default(&MMB_TESTS_INIT_DEFAULT));
  cheat_assert_int(MMB_OK,
                   mmb_options_set_debug_level(MMB_TESTS_INIT_DEFAULT,
                                               MAX_LOG_LEVEL));
  // Initialise mamba and register some memory
  cheat_assert_int(MMB_OK, mmb_init(MMB_TESTS_INIT_DEFAULT));
  cheat_assert_int(MMB_OK, register_test_memory());

  /* Whether we registered the memory ourselves or we let the discovery do it,
   * we don't need to provide any specific arguments for the space request,
   * hence we can give a NULL pointer. */
  cheat_assert_int(MMB_OK, mmb_request_space(MMB_DRAM, MMB_CPU, NULL, &dram_space));
  cheat_assert_int(MMB_OK, mmb_request_interface(dram_space, NULL, &dram_interface));
)

CHEAT_TEAR_DOWN(
  /* Cleanup intermediate objects, cleanup mamba */
  cheat_assert_int(MMB_OK, mmb_finalize());
  cheat_assert_int(MMB_OK, mmb_options_destroy(MMB_TESTS_INIT_DEFAULT));
)

/****************************************************************************
 *
 *  Check Accessors
 *
 * *************************************************************************/

CHEAT_TEST(1d_layout,
  size_t arrdims1d[1] = { 12 };
  mmbDimensions dims1d = {1, arrdims1d};
  mmbLayout *layout1d = NULL;
  mmbArray *mba1d = NULL;
  mmbArrayTile *tile1d = NULL;
  /* Create non-empty mmb arrays */
  cheat_assert_int(MMB_OK, mmb_layout_create_regular_1d(sizeof(uint32_t), MMB_PADDING_NONE, &layout1d));
  cheat_assert_int(MMB_OK, mmb_array_create_wrapped((void *)buffer, &dims1d, layout1d, dram_interface, MMB_READ, &mba1d));
  /* Ensure we got a non-empty array */
  bool empty = true;
  cheat_assert_int(MMB_OK, mmb_array_is_empty(mba1d, &empty));
  cheat_assert_int(false, empty);
  /* Tile the arrays */
  cheat_assert_int(MMB_OK, mmb_array_tile_single(mba1d, &tile1d));
  for (size_t i = 0; i<arrdims1d[0]; ++i)
      cheat_assert_size(i, (size_t)buffer[i]);
  for (size_t i = 0; i<arrdims1d[0]; ++i)
    cheat_assert_uint32(buffer[i], MMB_IDX_1D(tile1d, i, uint32_t));
  for (size_t i = 0; i<arrdims1d[0]; ++i)
    cheat_assert_uint32(buffer[i],
                        *(const uint32_t *)mmb_idx_1d(tile1d, i, sizeof(uint32_t)));
  cheat_assert_int(MMB_OK, mmb_array_untile(mba1d));
  cheat_assert_int(MMB_OK, mmb_array_destroy(mba1d));
  cheat_assert_int(MMB_OK, mmb_layout_destroy(layout1d));
)

CHEAT_TEST(2d_layout,
  size_t arrdims2d[2] = { 3, 4 };
  mmbDimensions dims2d = {2, arrdims2d};
  mmbLayout *layout2d = NULL;
  mmbArray *mba2d = NULL;
  mmbArrayTile *tile2d = NULL;
  /* Create non-empty mmb arrays */
  cheat_assert_int(MMB_OK, mmb_layout_create_regular_nd(sizeof(uint32_t), 2, MMB_ROWMAJOR, MMB_PADDING_NONE, &layout2d));
  cheat_assert_int(MMB_OK, mmb_array_create_wrapped((void *)buffer, &dims2d, layout2d, dram_interface, MMB_READ, &mba2d));
  /* Ensure we got a non-empty array */
  bool empty = true;
  cheat_assert_int(MMB_OK, mmb_array_is_empty(mba2d, &empty));
  cheat_assert_int(false, empty);
  /* Tile the arrays */
  cheat_assert_int(MMB_OK, mmb_array_tile_single(mba2d, &tile2d));
  for (size_t i = 0; i<arrdims2d[0]; ++i)
    for (size_t j = 0; j<arrdims2d[1]; ++j)
      cheat_assert_size(i*arrdims2d[1]+j, (size_t)buffer[i*arrdims2d[1]+j]);
  for (size_t i = 0; i<arrdims2d[0]; ++i)
    for (size_t j = 0; j<arrdims2d[1]; ++j)
      cheat_assert_uint32(buffer[i*arrdims2d[1]+j],
                          MMB_IDX_2D(tile2d, i, j, uint32_t));
  for (size_t i = 0; i<arrdims2d[0]; ++i)
    for (size_t j = 0; j<arrdims2d[1]; ++j)
      cheat_assert_uint32(buffer[i*arrdims2d[1]+j],
                          *(const uint32_t *)mmb_idx_2d(tile2d, i, j, sizeof(uint32_t)));
  cheat_assert_int(MMB_OK, mmb_array_untile(mba2d));
  cheat_assert_int(MMB_OK, mmb_array_destroy(mba2d));
  cheat_assert_int(MMB_OK, mmb_layout_destroy(layout2d));
)

CHEAT_TEST(2d_norm_layout,
  size_t arrdims2d[2] = { 3, 4 };
  mmbDimensions dims2d = {2, arrdims2d};
  mmbLayout *layout2d = NULL;
  mmbArray *mba2d = NULL;
  mmbArrayTile *tile2d = NULL;
  /* Create non-empty mmb arrays */
  cheat_assert_int(MMB_OK, mmb_layout_create_regular_nd(sizeof(uint32_t), 2, MMB_ROWMAJOR, MMB_PADDING_NONE, &layout2d));
  cheat_assert_int(MMB_OK, mmb_array_create_wrapped((void *)buffer, &dims2d, layout2d, dram_interface, MMB_READ, &mba2d));
  /* Ensure we got a non-empty array */
  bool empty = true;
  cheat_assert_int(MMB_OK, mmb_array_is_empty(mba2d, &empty));
  cheat_assert_int(false, empty);
  /* Tile the arrays */
  cheat_assert_int(MMB_OK, mmb_array_tile_single(mba2d, &tile2d));
  for (size_t i = 0; i<arrdims2d[0]; ++i)
    for (size_t j = 0; j<arrdims2d[1]; ++j)
      cheat_assert_size(i*arrdims2d[1]+j, (size_t)buffer[i*arrdims2d[1]+j]);
  for (size_t i = 0; i<arrdims2d[0]; ++i)
    for (size_t j = 0; j<arrdims2d[1]; ++j)
      cheat_assert_uint32(buffer[i*arrdims2d[1]+j],
                          MMB_IDX_2D_NORM(tile2d, i, j, uint32_t));
  for (size_t i = 0; i<arrdims2d[0]; ++i)
    for (size_t j = 0; j<arrdims2d[1]; ++j)
      cheat_assert_uint32(buffer[i*arrdims2d[1]+j],
                          *(const uint32_t *)mmb_idx_2d_norm(tile2d, i, j, sizeof(uint32_t)));
  cheat_assert_int(MMB_OK, mmb_array_untile(mba2d));
  cheat_assert_int(MMB_OK, mmb_array_destroy(mba2d));
  cheat_assert_int(MMB_OK, mmb_layout_destroy(layout2d));
)
