/*
 * Copyright (C) 2021 Cray UK
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/* needed before inclusion of cheat.h: */
#ifndef __BASE_FILE__
#define __BASE_FILE__ __FILE__
#endif

#include "cheat.h"
#include "cheats.h"
#include "mmb_cheats.h"

#include <mamba.h>
#include <strategy.h>
#include <mmb_provider_options.h>
#include <strategies/i_strategy.h>
#include <strategies/i_pooled.h>
#include <strategies/i_statistics.h>

#define ONE_KiB (1UL << 10)
#define ONE_MiB (ONE_KiB << 10)
#define ONE_GiB (ONE_MiB << 10)
#define ONE_TiB (ONE_GiB << 10)

#ifndef MAX_LOG_LEVEL
#  ifdef MMB_LOG_DEBUG
#    define MAX_LOG_LEVEL MMB_LOG_DEBUG
#  else
#    define MAX_LOG_LEVEL 3
#  endif
#endif

/***************************************************************************
 *
 *  Set-up / Tear-down
 *
 ***************************************************************************/

#if HAVE_OPENCL

#ifdef __APPLE__
#include <OpenCL/opencl.h>
#else
#include <CL/opencl.h>
#endif

#include <mmb_opencl.h>

#ifndef MAX_PLATFORMS
#define MAX_PLATFORMS 8
#endif /* MAX_PLATFORMS */
#ifndef MAX_DEVICES
#define MAX_DEVICES 16
#endif /* MAX_DEVICES */
#ifndef MAX_DEVICE_NAME
#define MAX_DEVICE_NAME 1024
#endif /* MAX_DEVICE_NAME */

CHEAT_DECLARE(

  static inline const char * opencl_error_decode(const cl_int ocl_err);

  struct mmb_opencl_t oclopt = {0};

  mmbError set_provider_options(mmbExecutionContext ex_con, mmbProviderOptions *opts)
  {
    cl_int ocl_err;
    cl_device_id device;
    cl_context context;
    cl_command_queue queue;
    mmbError stat = MMB_ERROR;

    /* Check the requested execution context is OpenCL.
     * Exit with success otherwise */
    if (MMB_OPENCL != ex_con)
      return MMB_OK;

    /* Get list of platforms */
    cl_uint num_platforms = 0;
    cl_platform_id platforms[MAX_PLATFORMS];
    ocl_err = clGetPlatformIDs(MAX_PLATFORMS, platforms, &num_platforms);
    if (CL_SUCCESS == ocl_err) {
      MMB_NOISE("%u platforms found.\n", num_platforms);
    } else if (CL_SUCCESS != ocl_err) {
      MMB_ERR("Unable to gather plarforms ID's (%s).\n",
              opencl_error_decode(ocl_err));
      return stat;
    }

    /* Get list of devices */
    cl_uint total_devices = 0;
    cl_device_id devices[MAX_DEVICES];
    for (cl_uint p = 0; p < num_platforms; ++p) {
      cl_uint num_devices = 0;
      ocl_err = clGetDeviceIDs(platforms[p], CL_DEVICE_TYPE_ALL,
                               MAX_DEVICES - total_devices, devices + total_devices,
                               &num_devices);
      if (CL_SUCCESS != ocl_err) {
        MMB_ERR("Failed to get device name. (%s).\n",
                opencl_error_decode(ocl_err));
        return stat;
      }
      total_devices += num_devices;
    }

    /* Print list of devices */
    char name[MAX_DEVICE_NAME];
    size_t name_len = MAX_DEVICE_NAME;
    if (MMB_LOG_NOISE <= MMB_MAX_LOG_LEVEL) {
      MMB_NOISE("Available OpenCL devices:\n");
      for (cl_uint d = 0; d < total_devices; ++d) {
        ocl_err = clGetDeviceInfo(devices[d], CL_DEVICE_NAME,
                                  name_len, name, &name_len);
        if (CL_SUCCESS == ocl_err) {
          MMB_NOISE("\t%2d: %s%s\n", d, name,
                    MAX_DEVICE_NAME < name_len ? " (possibly truncated)" : "");
        } else {
          MMB_NOISE("\t%2d: (err: %s)\n", d, opencl_error_decode(ocl_err));
          MMB_NOISE("\t\tname_len=%zu, MAX_DEVICE_NAME=%d\n",
                    name_len, MAX_DEVICE_NAME);
        }
      }
    }

    /* Use first device unless OCL_DEVICE environment variable is given */
    cl_uint device_index = 0;
    char *dev_env = getenv("OCL_DEVICE");
    if (NULL != dev_env) {
      char *end;
      device_index = strtol(dev_env, &end, 10);
      if (0 < strlen(end)) {
        MMB_ERR("Invalid OCL_DEVICE variable \"%s\".\n", dev_env);
        return stat;
      }
    }

    if (device_index >= total_devices) {
      MMB_ERR("Device index set to %d but only %d devices available.\n",
              device_index, total_devices);
      return stat;
    }

    /* set device */
    device = devices[device_index];

    /* Print OpenCL device name */
    ocl_err = clGetDeviceInfo(device, CL_DEVICE_NAME, name_len, name, &name_len);
    if (CL_SUCCESS == ocl_err) {
      MMB_DEBUG("Selected OpenCL device: %s%s (index=%d).\n", name,
                MAX_DEVICE_NAME < name_len ? " (possibly truncated)" : "",
                device_index);
    } else {
      MMB_ERR("Unable to retrieve device's name (err: %s).\n",
              opencl_error_decode(ocl_err));
      return stat;
    }

    /* Create OpenCL context */
    cl_context_properties properties[] = {
      CL_CONTEXT_PLATFORM, (cl_context_properties) platforms[0],
      0
    };
    context = clCreateContext(properties, 1, &device, NULL, NULL, &ocl_err);
    if (CL_SUCCESS != ocl_err) {
      MMB_ERR("Failed to create opencl context, err: %s\n",
              opencl_error_decode(ocl_err));
      return stat;
    }

    /* Create Command Queue */
    queue = clCreateCommandQueue(context, device, 0, &ocl_err);
    if (CL_SUCCESS != ocl_err) {
      MMB_ERR("Failed to create opencl command queue, err: %s\n",
              opencl_error_decode(ocl_err));
      return stat;
    }

    /* Set provider's options */
    oclopt = (struct mmb_opencl_t) {
      .device = device,
      .context = context,
      .queue = queue
    };
    opts->ocl = &oclopt;
    stat = MMB_OK;
    return stat;
  }
)
#else
CHEAT_DECLARE(
  mmbError set_provider_options(mmbExecutionContext ex_con, mmbProviderOptions *opts)
  { (void) ex_con; (void) opts; return MMB_OK; }
)
#endif /* HAVE_OPENCL */

CHEAT_DECLARE( mmbOptions *MMB_TESTS_INIT_DEFAULT; )

CHEAT_SET_UP(
  cheat_assert_int(MMB_OK,
    mmb_options_create_default(&MMB_TESTS_INIT_DEFAULT));
  cheat_assert_int(MMB_OK,
    mmb_options_set_debug_level(MMB_TESTS_INIT_DEFAULT, MAX_LOG_LEVEL));
)

CHEAT_TEAR_DOWN(
  cheat_assert_int(MMB_OK, mmb_options_destroy(MMB_TESTS_INIT_DEFAULT));
)

CHEAT_DECLARE(
  static void common_set_up(mmbMemLayer layer, mmbProvider provider, mmbStrategy strategy, const char *name, mmbMemInterface **interface)
  {
    mmbMemSpace *space = NULL;
    cheat_assert_int(MMB_OK, mmb_init(MMB_TESTS_INIT_DEFAULT));
    const mmbMemSpaceConfig dram_config = {
      .size_opts = { .action = MMB_SIZE_SET, .mem_size = 8000 },
      .interface_opts = MMB_MEMINTERFACE_CONFIG_DEFAULT,
    };
    cheat_assert_int(MMB_OK,
        mmb_register_memory(layer, MMB_EXECUTION_CONTEXT_DEFAULT, &dram_config, &space));
    cheat_assert_not_pointer(NULL, space);
    mmbMemInterfaceConfig interface_config = {
      .provider = provider, .strategy = strategy, .name = name
    };
    cheat_assert_int(MMB_OK,
        set_provider_options(space->ex_context, &interface_config.provider_opts));
    cheat_assert_int(MMB_OK, mmb_create_interface(space, &interface_config, interface));
  }
)

CHEAT_DECLARE(
  static void common_tear_down(void)
  {
    cheat_assert_int(MMB_OK, mmb_finalize());
  }
)

/***************************************************************************
 *  POOLED
 ***************************************************************************/

CHEAT_DECLARE(
  static void pooled_set_up(size_t size, mmbMemLayer layer, mmbProvider provider, const char *name, mmbMemInterface **interface)
  {
    cheat_assert_int(MMB_OK,
      mmb_options_strategy_pooled_set_pool_size(MMB_TESTS_INIT_DEFAULT, size));
    common_set_up(layer, provider, MMB_POOLED, name, interface);
  }
)

CHEAT_DECLARE(
  static void pooled_tear_down(void)
  {
    common_tear_down();
  }
)

/***************************************************************************
 *  STATISTICS
 ***************************************************************************/

CHEAT_DECLARE(
  static void stats_set_up(mmbMemLayer layer, mmbProvider provider, const char *name, mmbMemInterface **interface)
  {
    common_set_up(layer, provider, MMB_STATISTICS, name, interface);
  }
)

CHEAT_DECLARE(
  static void stats_tear_down(void)
  {
    common_tear_down();
  }
)

/***************************************************************************
 *  POOLED STATISTICS
 ***************************************************************************/

CHEAT_DECLARE(
  static void pooled_stats_set_up(size_t size, mmbMemLayer layer, mmbProvider provider, const char *name, mmbMemInterface **interface)
  {
    cheat_assert_int(MMB_OK,
      mmb_options_strategy_pooled_set_pool_size(MMB_TESTS_INIT_DEFAULT, size));
    common_set_up(layer, provider, MMB_POOLED_STATISTICS, name, interface);
  }
)

CHEAT_DECLARE(
  static void pooled_stats_tear_down(void)
  {
    common_tear_down();
  }
)

/***************************************************************************
 *
 *  Tests
 *
 ***************************************************************************/

/***************************************************************************
 *  POOLED
 ***************************************************************************/

CHEAT_DECLARE(
  typedef struct {
      void *prev;
      struct mmb_strat_pooled_data *slab;
      size_t length;
      unsigned char ptr[];
  } strat_pooled_ielt;

  #define CHECK_IELT_INLINED(ielt, prev_, slab_, len_, adr_) do { \
    cheat_assert_not_pointer(NULL, ielt);                         \
    cheat_assert_pointer(adr_, ielt);                             \
    if (ielt) {                                                   \
      if (prev_) cheat_assert_not_pointer(NULL, ielt->prev);      \
      cheat_assert_pointer(prev_, ielt->prev);                    \
      cheat_assert_not_pointer(NULL, ielt->slab);                 \
      cheat_assert_pointer(slab_, ielt->slab);                    \
      cheat_assert_size(len_, ielt->length);                      \
    }                                                             \
  } while (0)
)

CHEAT_DECLARE(
  typedef struct {
    void *prev, *next;
    size_t length;
    void  *ptr;
  } strat_pooled_elt;

  #define CHECK_ELT_PLAIN(elt, prev_, next_, len_, ptr_) do { \
    cheat_assert_not_pointer(NULL, elt);                      \
    if (elt) {                                                \
      if (prev_) cheat_assert_not_pointer(NULL, elt->prev);   \
      cheat_assert_pointer(prev_, elt->prev);                 \
      if (next_) cheat_assert_not_pointer(NULL, elt->next);   \
      cheat_assert_pointer(next_, elt->next);                 \
      cheat_assert_size(len_, elt->length);                   \
      cheat_assert_not_pointer(NULL, elt->ptr);               \
      cheat_assert_pointer(ptr_, elt->ptr);                   \
    }                                                         \
  } while (0)
)

CHEAT_TEST(allocate_pooled_cpu,
  mmbMemInterface *interface;
  const size_t size = 1024; /* Reduce the slab sizes to 1kiB */
  pooled_set_up(size, MMB_DRAM, MMB_SYSTEM, "dram system pooled", &interface);
  /* Before any allocation. Check the sentinel values. */
  void *data_ptr = interface->data + mmb_provider_get_data_size(MMB_SYSTEM);
  struct mmb_strat_pooled_data *data = data_ptr;
  cheat_assert_size(0, data->length);
  cheat_assert_pointer(NULL, data->data_);
  cheat_assert_pointer(NULL, data->next);
  cheat_assert_pointer(NULL, *(void **)&data->last);
  /* Check non-empty first allocation. */
  /* NOTE: I would have tried with an empty allocation, but the memory manager
   * returns early in that case, so the call is never forwarded to the actual
   * memory allocation. */
  mmbAllocation *alloc1;
  const size_t alloc_size1 = 8;
  strat_pooled_ielt *meta1;
  cheat_assert_int(MMB_OK, mmb_allocate(alloc_size1, interface, &alloc1));
  cheat_assert_size(size-alloc_size1-2*sizeof *meta1, data->length);
  cheat_assert_pointer(NULL, data->next->next);
  meta1 = (strat_pooled_ielt *) data->last.inlined;
  strat_pooled_ielt * const exp_meta1 = ((strat_pooled_ielt *)data->data_)+1;
  CHECK_IELT_INLINED(meta1, data->data_, data, alloc_size1, exp_meta1);
  /* Check sentinel */
  strat_pooled_ielt *meta0 = data->data_;
  CHECK_IELT_INLINED(meta0, NULL, data, 0, data->data_);
  /* Check non-empty allocation. */
  mmbAllocation *alloc2;
  strat_pooled_ielt *meta2;
  const size_t alloc_size2 = sizeof(int[4]);
  cheat_assert_int(MMB_OK, mmb_allocate(alloc_size2, interface, &alloc2));
  cheat_assert_size(size-alloc_size1-alloc_size2-3*sizeof *meta2, data->length);
  meta2 = (strat_pooled_ielt *) data->last.inlined;
  CHECK_IELT_INLINED(meta2, meta1, data, alloc_size2, &((unsigned char *)meta1->ptr)[alloc_size1]);
  CHECK_IELT_INLINED(meta0, NULL, data, 0, data->data_);
  /* Check after freeing tail (not last) allocation */
  mmbAllocation *alloc3;
  size_t old_size = data->length;
  cheat_assert_int(MMB_OK, mmb_allocate(alloc_size2, interface, &alloc3));
  cheat_assert_size(old_size-alloc_size2-sizeof *meta2, data->length);
  cheat_assert_int(MMB_OK, mmb_free(alloc3));
  cheat_assert_size(old_size, data->length);
  cheat_assert_size(alloc_size2, meta2->length);
  /* Check after freeing central allocation */
  old_size = data->length;
  cheat_assert_int(MMB_OK, mmb_free(alloc1));
  cheat_assert_size(old_size, data->length);
  CHECK_IELT_INLINED(meta0, NULL, data, alloc_size1+sizeof *meta1, data->data_);
  CHECK_IELT_INLINED(meta2, meta0, data, alloc_size2, &((unsigned char *)meta0->ptr)[alloc_size1+sizeof *meta1]);
  /* Check allocating new slab */
  mmbAllocation *alloc4;
  cheat_assert_int(MMB_OK, mmb_allocate(size, interface, &alloc4));
  cheat_assert_not_pointer(NULL, data->next->next);
  cheat_assert_not_pointer(NULL, data->next->data_);
  cheat_assert_not_pointer(NULL, *(void **)&data->next->last);
  cheat_assert_pointer(NULL, data->next->next->next);
  cheat_assert_size(1.4*size-size-2*sizeof *meta2, data->next->length);
  /* Check freeing the last entry of the first slab */
  cheat_assert_int(MMB_OK, mmb_free(alloc2));
  cheat_assert_size(0, data->length);
  cheat_assert_pointer(NULL, *(void **)&data->last);
  /* Check requesting a new slab to replace the first */
  mmbAllocation *alloc5;
  strat_pooled_ielt *meta5;
  cheat_assert_int(MMB_OK, mmb_allocate(size/2, interface, &alloc5));
  cheat_assert_size((size/2)-2*sizeof *meta5, data->length);
  meta5 = (strat_pooled_ielt *) data->last.inlined;
  strat_pooled_ielt * const exp_meta5 = ((strat_pooled_ielt *)data->data_)+1;
  CHECK_IELT_INLINED(meta5, data->data_, data, size/2, exp_meta5);
  cheat_assert_not_pointer(NULL, data->next);
  cheat_assert_not_pointer(NULL, data->next->next);
  /* Check requesting a new slab at the end of the list */
  mmbAllocation *alloc6;
  strat_pooled_ielt *meta6;
  cheat_assert_int(MMB_OK, mmb_allocate(size/2, interface, &alloc6));
  data = data->next->next;
  cheat_assert_size((size/2)-2*sizeof *meta6, data->length);
  cheat_assert_not_pointer(NULL, data);
  meta6 = (strat_pooled_ielt *) data->last.inlined;
  strat_pooled_ielt * const exp_meta6 = ((strat_pooled_ielt *)data->data_)+1;
  CHECK_IELT_INLINED(meta6, data->data_, data, size/2, exp_meta6);
  cheat_assert_not_pointer(NULL, data->next);
  cheat_assert_pointer(NULL, data->next->next);
  /* Cleaning remaining allocations */
  cheat_assert_int(MMB_OK, mmb_free(alloc4));
  cheat_assert_int(MMB_OK, mmb_free(alloc5));
  cheat_assert_int(MMB_OK, mmb_free(alloc6));
  pooled_tear_down();
)

#if HAVE_CUDA || HAVE_OPENCL || HAVE_ROCM

CHEAT_TEST(allocate_pooled_gpu,
  mmbMemInterface *interface;
  const size_t size = 1024; /* Reduce the slab sizes to 1kiB */
  pooled_set_up(size, MMB_GDRAM, MMB_SYSTEM, "gpu system pooled", &interface);
  /* Before any allocation. Check the sentinel values. */
  void *data_ptr = interface->data + mmb_provider_get_data_size(MMB_SYSTEM);
  struct mmb_strat_pooled_data *data = data_ptr;
  cheat_assert_size(0, data->length);
  cheat_assert_pointer(NULL, data->data_);
  cheat_assert_pointer(NULL, data->next);
  cheat_assert_pointer(NULL, *(void **)&data->last);
  /* Check non-empty first allocation. */
  /* NOTE: I would have tried with an empty allocation, but the memory manager
   * returns early in that case, so the call is never forwarded to the actual
   * memory allocation. */
  mmbAllocation *alloc1;
  const size_t alloc_size1 = 8;
  cheat_assert_int(MMB_OK, mmb_allocate(alloc_size1, interface, &alloc1));
  cheat_assert_size(size-alloc_size1, data->length);
  cheat_assert_not_pointer(NULL, data->next);
  cheat_assert_pointer(NULL, data->next->next);
  cheat_assert_pointer(NULL, data->next->data_);
  strat_pooled_elt *meta1 = (strat_pooled_elt *) data->last.plain;
  CHECK_ELT_PLAIN(meta1, meta1->prev, NULL, alloc_size1, data->data_);
  /* Check sentinel elt */
  strat_pooled_elt *meta0 = (strat_pooled_elt *) meta1->prev;
  cheat_assert_not_pointer(NULL, meta1->prev);
  CHECK_ELT_PLAIN(meta0, NULL, meta1, 0, data->data_);
  /* Check non-empty allocation. */
  mmbAllocation *alloc2;
  const size_t alloc_size2 = sizeof(int[4]);
  cheat_assert_int(MMB_OK, mmb_allocate(alloc_size2, interface, &alloc2));
  cheat_assert_size(size-alloc_size1-alloc_size2, data->length);
  strat_pooled_elt *meta2 = (strat_pooled_elt *) data->last.plain;
  CHECK_ELT_PLAIN(meta2, meta1, NULL, alloc_size2, &((unsigned char *)meta1->ptr)[alloc_size1]);
  CHECK_ELT_PLAIN(meta1, meta0, meta2, alloc_size1, data->data_);
  CHECK_ELT_PLAIN(meta0, NULL, meta1, 0, data->data_);
  /* Check after freeing tail (not last) allocation */
  mmbAllocation *alloc3;
  size_t old_size = data->length;
  cheat_assert_int(MMB_OK, mmb_allocate(alloc_size2, interface, &alloc3));
  cheat_assert_size(old_size-alloc_size2, data->length);
  strat_pooled_elt *meta3 = (strat_pooled_elt *) data->last.plain;
  CHECK_ELT_PLAIN(meta3, meta2, NULL, alloc_size2, &((unsigned char *)meta2->ptr)[alloc_size2]);
  CHECK_ELT_PLAIN(meta2, meta1, meta3, alloc_size2, &((unsigned char *)meta1->ptr)[alloc_size1]);
  CHECK_ELT_PLAIN(meta1, meta0, meta2, alloc_size1, data->data_);
  CHECK_ELT_PLAIN(meta0, NULL, meta1, 0, data->data_);
  cheat_assert_int(MMB_OK, mmb_free(alloc3));
  cheat_assert_size(old_size, data->length);
  CHECK_ELT_PLAIN(meta2, meta1, NULL, alloc_size2, &((unsigned char *)meta1->ptr)[alloc_size1]);
  /* Check after freeing central allocation */
  old_size = data->length;
  cheat_assert_int(MMB_OK, mmb_free(alloc1));
  cheat_assert_size(old_size, data->length);
  CHECK_ELT_PLAIN(meta0, NULL, meta2, alloc_size1, data->data_);
  CHECK_ELT_PLAIN(meta2, meta0, NULL, alloc_size2, &((unsigned char *)meta0->ptr)[alloc_size1]);
  /* Check allocating new slab */
  mmbAllocation *alloc4;
  const size_t alloc_size4 = size + sizeof(int);
  cheat_assert_int(MMB_OK, mmb_allocate(alloc_size4, interface, &alloc4));
  cheat_assert_not_pointer(NULL, data->next->next);
  cheat_assert_not_pointer(NULL, data->next->data_);
  cheat_assert_not_pointer(NULL, *(void **)&data->next->last);
  cheat_assert_pointer(NULL, data->next->next->next);
  cheat_assert_size(1.4*size-alloc_size4, data->next->length);
  /* Check freeing the last entry of the first slab */
  cheat_assert_int(MMB_OK, mmb_free(alloc2));
  cheat_assert_size(0, data->length);
  cheat_assert_pointer(NULL, *(void **)&data->last);
  /* Check requesting a new slab to replace the first */
  mmbAllocation *alloc5;
  const size_t alloc_size5 = 2*size/3;
  cheat_assert_int(MMB_OK, mmb_allocate(alloc_size5, interface, &alloc5));
  cheat_assert_size(size-alloc_size5, data->length);
  cheat_assert_not_pointer(NULL, data->next);
  cheat_assert_not_pointer(NULL, data->next->next);
  strat_pooled_elt *meta5 = (strat_pooled_elt *) data->last.plain;
  CHECK_ELT_PLAIN(meta5, meta5->prev, NULL, alloc_size5, data->data_);
  /* Check requesting a new slab at the end of the list */
  mmbAllocation *alloc6;
  cheat_assert_int(MMB_OK, mmb_allocate(alloc_size5, interface, &alloc6));
  data = data->next->next;
  cheat_assert_not_pointer(NULL, data);
  cheat_assert_size(size-alloc_size5, data->length);
  cheat_assert_not_pointer(NULL, data->next);
  cheat_assert_pointer(NULL, data->next->next);
  cheat_assert_pointer(NULL, data->next->data_);
  strat_pooled_elt *meta6 = (strat_pooled_elt *) data->last.plain;
  CHECK_ELT_PLAIN(meta6, meta6->prev, NULL, alloc_size5, data->data_);
  /* Cleaning remaining allocations */
  cheat_assert_int(MMB_OK, mmb_free(alloc4));
  cheat_assert_int(MMB_OK, mmb_free(alloc5));
  cheat_assert_int(MMB_OK, mmb_free(alloc6));
  pooled_tear_down();
)

#endif /* HAVE_CUDA || HAVE_OPENCL || HAVE_ROCM */

/***************************************************************************
 *  STATISTICS
 ***************************************************************************/


CHEAT_TEST(check_errors_get_record_count,
  size_t count = SIZE_MAX;
  /* Uninitialized library */
  cheat_assert_int(MMB_ERROR, mmb_statistics_get_record_count(&count));
  cheat_assert_size(SIZE_MAX, count);
  cheat_assert_int(MMB_OK, mmb_init(MMB_TESTS_INIT_DEFAULT));
  /* Invalid NULL pointer */
  cheat_assert_int(MMB_INVALID_ARG, mmb_statistics_get_record_count(NULL));
  cheat_assert_int(MMB_OK, mmb_finalize());
)

CHEAT_TEST(check_errors_get_records,
  /* check wrong interface handle pre-init */
  cheat_assert_int(MMB_ERROR,
                   mmb_statistics_get_total_statistics(NULL, NULL, NULL));
  cheat_assert_int(MMB_ERROR,
                   mmb_statistics_get_total_statistics(NULL, NULL, NULL));
  /* Test invalid (pooled) interface. */
  mmbMemInterface *interface = NULL;
  const size_t size = 1024; /* Reduce the slab sizes to 1kiB */
  pooled_set_up(size, MMB_DRAM, MMB_SYSTEM, "dummy dram system pooled", &interface);
  /* check wrong interface handle post init */
  cheat_assert_int(MMB_INVALID_INTERFACE,
                   mmb_statistics_get_total_statistics(NULL, NULL, NULL));
  cheat_assert_int(MMB_INVALID_INTERFACE,
                   mmb_statistics_get_total_statistics(NULL, NULL, NULL));
  /* Test wrong kind of interface (strategy: MMB_POOLED */
  cheat_assert_not_pointer(NULL, interface);
  cheat_assert_int(MMB_INVALID_INTERFACE,
                   mmb_statistics_get_current_statistics(interface, NULL, NULL));
  cheat_assert_int(MMB_INVALID_INTERFACE,
                   mmb_statistics_get_total_statistics(interface, NULL, NULL));
  /* Check the values are unchanged. */
  struct mmb_strat_stats_data data = { {0}, 3, 5, 8, 13 };
  cheat_assert_int(MMB_INVALID_INTERFACE,
                   mmb_statistics_get_current_statistics(interface,
                                                         &data.current_allocations,
                                                         &data.current_allocated));
  cheat_assert_int(MMB_INVALID_INTERFACE,
                   mmb_statistics_get_total_statistics(interface,
                                                       &data.total_allocations,
                                                       &data.total_allocated));
  cheat_assert_size(3, data.current_allocations);
  cheat_assert_size(5, data.current_allocated);
  cheat_assert_size(8, data.total_allocations);
  cheat_assert_size(13, data.total_allocated);
  pooled_tear_down();
)

CHEAT_TEST(stat_interface_init,
  mmbMemSpace *space;
  size_t count = SIZE_MAX;
  const mmbMemLayer layer = MMB_DRAM;
  const mmbProvider provider = MMB_SYSTEM;
  cheat_assert_int(MMB_OK, mmb_init(MMB_TESTS_INIT_DEFAULT));
  cheat_assert_int(MMB_OK, mmb_statistics_get_record_count(&count));
  cheat_assert_size(0, count);
  const mmbMemSpaceConfig dram_config = {
    .size_opts = { .action = MMB_SIZE_SET, .mem_size = 8000 },
    .interface_opts = MMB_MEMINTERFACE_CONFIG_DEFAULT,
  };
  cheat_assert_int(MMB_OK,
      mmb_register_memory(layer, MMB_EXECUTION_CONTEXT_DEFAULT, &dram_config, &space));
  /* Create interface 1: basic */
  {
    mmbMemInterface *interface = NULL;
    const mmbStrategy strategy = MMB_STRATEGY_NONE;
    const char *name = "dummy interface";
    mmbMemInterfaceConfig interface_config = {
      .provider = provider, .strategy = strategy, .name = name
    };
    cheat_assert_int(MMB_OK, mmb_create_interface(space, &interface_config, &interface));
    cheat_assert_not_pointer(NULL, interface);
    cheat_assert_int(MMB_OK, mmb_statistics_get_record_count(&count));
    cheat_assert_size(0, count);
  }
  /* Create interface 2: statistics */
  {
    mmbMemInterface *interface = NULL;
    const mmbStrategy strategy = MMB_STATISTICS;
    const char *name = "dummy stats interface";
    mmbMemInterfaceConfig interface_config = {
      .provider = provider, .strategy = strategy, .name = name
    };
    cheat_assert_int(MMB_OK, mmb_create_interface(space, &interface_config, &interface));
    cheat_assert_not_pointer(NULL, interface);
    cheat_assert_int(MMB_OK, mmb_statistics_get_record_count(&count));
    cheat_assert_size(1, count);
    cheat_assert_int(MMB_OK, mmb_destroy_interface(interface));
    cheat_assert_int(MMB_OK, mmb_statistics_get_record_count(&count));
    cheat_assert_size(0, count);
  }
  /* Create interface 3: pooled with statistics */
  {
    mmbMemInterface *interface = NULL;
    const mmbStrategy strategy = MMB_POOLED_STATISTICS;
    const char *name = "dummy pooled stats interface";
    mmbMemInterfaceConfig interface_config = {
      .provider = provider, .strategy = strategy, .name = name
    };
    cheat_assert_int(MMB_OK, mmb_create_interface(space, &interface_config, &interface));
    cheat_assert_not_pointer(NULL, interface);
    cheat_assert_int(MMB_OK, mmb_statistics_get_record_count(&count));
    cheat_assert_size(1, count);
    cheat_assert_int(MMB_OK, mmb_destroy_interface(interface));
    cheat_assert_int(MMB_OK, mmb_statistics_get_record_count(&count));
    cheat_assert_size(0, count);
  }
  cheat_assert_int(MMB_OK, mmb_finalize());
)

CHEAT_TEST(allocate_stats_cpu,
  mmbMemInterface *interface = NULL;
  size_t count = SIZE_MAX;
  stats_set_up(MMB_DRAM, MMB_SYSTEM, "dram system stats", &interface);
  cheat_assert_not_pointer(NULL, interface);
  /* Before any allocation. Check the initial values. */
  cheat_assert_int(MMB_OK, mmb_statistics_get_record_count(&count));
  cheat_assert_size(1, count);
  struct mmb_strat_stats_data ret_data = { {0}, 3, 5, 8, 13 };
  const unsigned char *data_ptr = interface->data + mmb_provider_get_data_size(MMB_SYSTEM);
  const struct mmb_strat_stats_data *data = (struct mmb_strat_stats_data *)data_ptr;
  cheat_assert_size(0, data->current_allocations);
  cheat_assert_size(0, data->current_allocated);
  cheat_assert_size(0, data->total_allocations);
  cheat_assert_size(0, data->total_allocated);
  cheat_assert_int(MMB_OK,
    mmb_statistics_get_current_statistics(interface,
                                          &ret_data.current_allocations,
                                          &ret_data.current_allocated));
  cheat_assert_size(data->current_allocations, ret_data.current_allocations);
  cheat_assert_size(data->current_allocated, ret_data.current_allocated);
  cheat_assert_int(MMB_OK,
    mmb_statistics_get_total_statistics(interface,
                                        &ret_data.total_allocations,
                                        &ret_data.total_allocated));
  cheat_assert_size(data->total_allocations, ret_data.total_allocations);
  cheat_assert_size(data->total_allocated, ret_data.total_allocated);
  /* Check first allocation properly update records */
  mmbAllocation *alloc1;
  const size_t alloc_size1 = 8;
  cheat_assert_int(MMB_OK, mmb_allocate(alloc_size1, interface, &alloc1));
  cheat_assert_size(1, data->current_allocations);
  cheat_assert_size(alloc_size1, data->current_allocated);
  cheat_assert_size(1, data->total_allocations);
  cheat_assert_size(alloc_size1, data->total_allocated);
  cheat_assert_int(MMB_OK,
      mmb_statistics_get_current_statistics(interface,
                                            &ret_data.current_allocations,
                                            &ret_data.current_allocated));
  cheat_assert_size(data->current_allocations, ret_data.current_allocations);
  cheat_assert_size(data->current_allocated, ret_data.current_allocated);
  cheat_assert_int(MMB_OK,
      mmb_statistics_get_total_statistics(interface,
                                          &ret_data.total_allocations,
                                          &ret_data.total_allocated));
  cheat_assert_size(data->total_allocations, ret_data.total_allocations);
  cheat_assert_size(data->total_allocated, ret_data.total_allocated);
  /* Check with a second allocation */
  mmbAllocation *alloc2;
  const size_t alloc_size2 = sizeof(int[4]);
  cheat_assert_int(MMB_OK, mmb_allocate(alloc_size2, interface, &alloc2));
  cheat_assert_size(2, data->current_allocations);
  cheat_assert_size(alloc_size1 + alloc_size2, data->current_allocated);
  cheat_assert_size(2, data->total_allocations);
  cheat_assert_size(alloc_size1 + alloc_size2, data->total_allocated);
  cheat_assert_int(MMB_OK,
      mmb_statistics_get_current_statistics(interface,
                                            &ret_data.current_allocations,
                                            &ret_data.current_allocated));
  cheat_assert_size(data->current_allocations, ret_data.current_allocations);
  cheat_assert_size(data->current_allocated, ret_data.current_allocated);
  cheat_assert_int(MMB_OK,
      mmb_statistics_get_total_statistics(interface,
                                          &ret_data.total_allocations,
                                          &ret_data.total_allocated));
  cheat_assert_size(data->total_allocations, ret_data.total_allocations);
  cheat_assert_size(data->total_allocated, ret_data.total_allocated);
  /* Check after removing one of the allocations */
  cheat_assert_int(MMB_OK, mmb_free(alloc2));
  cheat_assert_size(1, data->current_allocations);
  cheat_assert_size(alloc_size1, data->current_allocated);
  cheat_assert_size(2, data->total_allocations);
  cheat_assert_size(alloc_size1 + alloc_size2, data->total_allocated);
  cheat_assert_int(MMB_OK,
      mmb_statistics_get_current_statistics(interface,
                                            &ret_data.current_allocations,
                                            &ret_data.current_allocated));
  cheat_assert_size(data->current_allocations, ret_data.current_allocations);
  cheat_assert_size(data->current_allocated, ret_data.current_allocated);
  cheat_assert_int(MMB_OK,
      mmb_statistics_get_total_statistics(interface,
                                          &ret_data.total_allocations,
                                          &ret_data.total_allocated));
  cheat_assert_size(data->total_allocations, ret_data.total_allocations);
  cheat_assert_size(data->total_allocated, ret_data.total_allocated);
  /* Check third allocation properly update records */
  mmbAllocation *alloc3;
  cheat_assert_int(MMB_OK, mmb_allocate(alloc_size1, interface, &alloc3));
  cheat_assert_size(2, data->current_allocations);
  cheat_assert_size(2*alloc_size1, data->current_allocated);
  cheat_assert_size(3, data->total_allocations);
  cheat_assert_size(2*alloc_size1 + alloc_size2, data->total_allocated);
  cheat_assert_int(MMB_OK,
      mmb_statistics_get_current_statistics(interface,
                                            &ret_data.current_allocations,
                                            &ret_data.current_allocated));
  cheat_assert_size(data->current_allocations, ret_data.current_allocations);
  cheat_assert_size(data->current_allocated, ret_data.current_allocated);
  cheat_assert_int(MMB_OK,
      mmb_statistics_get_total_statistics(interface,
                                          &ret_data.total_allocations,
                                          &ret_data.total_allocated));
  cheat_assert_size(data->total_allocations, ret_data.total_allocations);
  cheat_assert_size(data->total_allocated, ret_data.total_allocated);
  /* Cleaning remaining allocations */
  cheat_assert_int(MMB_OK, mmb_free(alloc1));
  cheat_assert_int(MMB_OK, mmb_free(alloc3));
  stats_tear_down();
)

/***************************************************************************
 *  POOLED STATISTICS
 ***************************************************************************/

CHEAT_TEST(allocate_pooled_stats_cpu,
  mmbMemInterface *interface = NULL;
  size_t count = SIZE_MAX;
  const size_t size = 1024; /* Reduce the slab sizes to 1kiB */
  pooled_stats_set_up(size, MMB_DRAM, MMB_SYSTEM, "dram system pooled stats", &interface);
  cheat_assert_not_pointer(NULL, interface);
  /* Before any allocation. Check the initial values. */
  cheat_assert_int(MMB_OK, mmb_statistics_get_record_count(&count));
  cheat_assert_size(1, count);
  struct mmb_strat_stats_data data = { {0}, 3, 5, 8, 13 };
  cheat_assert_int(MMB_OK,
    mmb_statistics_get_current_statistics(interface,
                                          &data.current_allocations,
                                          &data.current_allocated));
  cheat_assert_size(0, data.current_allocations);
  cheat_assert_size(0, data.current_allocated);
  cheat_assert_int(MMB_OK,
    mmb_statistics_get_total_statistics(interface,
                                        &data.total_allocations,
                                        &data.total_allocated));
  cheat_assert_size(0, data.total_allocations);
  cheat_assert_size(0, data.total_allocated);
  /* Check first allocation properly update records */
  mmbAllocation *alloc1;
  const size_t alloc_size1 = 8;
  cheat_assert_int(MMB_OK, mmb_allocate(alloc_size1, interface, &alloc1));
  cheat_assert_int(MMB_OK,
      mmb_statistics_get_current_statistics(interface,
                                            &data.current_allocations,
                                            &data.current_allocated));
  cheat_assert_size(1, data.current_allocations);
  cheat_assert_size(size, data.current_allocated);
  cheat_assert_int(MMB_OK,
      mmb_statistics_get_total_statistics(interface,
                                          &data.total_allocations,
                                          &data.total_allocated));
  cheat_assert_size(1, data.total_allocations);
  cheat_assert_size(size, data.total_allocated);
  /* Check with a second allocation */
  mmbAllocation *alloc2;
  const size_t alloc_size2 = sizeof(int[4]);
  cheat_assert_int(MMB_OK, mmb_allocate(alloc_size2, interface, &alloc2));
  cheat_assert_int(MMB_OK,
      mmb_statistics_get_current_statistics(interface,
                                            &data.current_allocations,
                                            &data.current_allocated));
  cheat_assert_size(1, data.current_allocations);
  cheat_assert_size(size, data.current_allocated);
  cheat_assert_int(MMB_OK,
      mmb_statistics_get_total_statistics(interface,
                                          &data.total_allocations,
                                          &data.total_allocated));
  cheat_assert_size(1, data.total_allocations);
  cheat_assert_size(size, data.total_allocated);
  /* Check after removing one of the allocations */
  cheat_assert_int(MMB_OK, mmb_free(alloc2));
  cheat_assert_int(MMB_OK,
      mmb_statistics_get_current_statistics(interface,
                                            &data.current_allocations,
                                            &data.current_allocated));
  cheat_assert_size(1, data.current_allocations);
  cheat_assert_size(size, data.current_allocated);
  cheat_assert_int(MMB_OK,
      mmb_statistics_get_total_statistics(interface,
                                          &data.total_allocations,
                                          &data.total_allocated));
  cheat_assert_size(1, data.total_allocations);
  cheat_assert_size(size, data.total_allocated);
  /* Check third allocation properly update records */
  mmbAllocation *alloc3;
  cheat_assert_int(MMB_OK, mmb_allocate(size, interface, &alloc3));
  cheat_assert_int(MMB_OK,
      mmb_statistics_get_current_statistics(interface,
                                            &data.current_allocations,
                                            &data.current_allocated));
  cheat_assert_size(2, data.current_allocations);
  cheat_assert_size(size + size * 1.4, data.current_allocated);
  cheat_assert_int(MMB_OK,
      mmb_statistics_get_total_statistics(interface,
                                          &data.total_allocations,
                                          &data.total_allocated));
  cheat_assert_size(2, data.total_allocations);
  cheat_assert_size(size + size * 1.4, data.total_allocated);
  /* Check after freeing the first slab */
  cheat_assert_int(MMB_OK, mmb_free(alloc1));
  cheat_assert_int(MMB_OK,
      mmb_statistics_get_current_statistics(interface,
                                            &data.current_allocations,
                                            &data.current_allocated));
  cheat_assert_size(1, data.current_allocations);
  cheat_assert_size(size * 1.4, data.current_allocated);
  cheat_assert_int(MMB_OK,
      mmb_statistics_get_total_statistics(interface,
                                          &data.total_allocations,
                                          &data.total_allocated));
  cheat_assert_size(2, data.total_allocations);
  cheat_assert_size(size + size * 1.4, data.total_allocated);
  /* Cleaning remaining allocations */
  cheat_assert_int(MMB_OK, mmb_free(alloc3));
  pooled_stats_tear_down();
)

#if HAVE_CUDA || HAVE_ROCM

CHEAT_TEST(allocate_pooled_stats_gpu,
  mmbMemInterface *interface = NULL;
  size_t count = SIZE_MAX;
  const size_t size = 1024; /* Reduce the slab sizes to 1kiB */
  pooled_stats_set_up(size, MMB_DRAM, MMB_SYSTEM, "dram system pooled stats", &interface);
  cheat_assert_not_pointer(NULL, interface);
  /* Before any allocation. Check the initial values. */
  cheat_assert_int(MMB_OK, mmb_statistics_get_record_count(&count));
  cheat_assert_size(1, count);
  struct mmb_strat_stats_data data = { {0}, 3, 5, 8, 13 };
  cheat_assert_int(MMB_OK,
    mmb_statistics_get_current_statistics(interface,
                                          &data.current_allocations,
                                          &data.current_allocated));
  cheat_assert_size(0, data.current_allocations);
  cheat_assert_size(0, data.current_allocated);
  cheat_assert_int(MMB_OK,
    mmb_statistics_get_total_statistics(interface,
                                        &data.total_allocations,
                                        &data.total_allocated));
  cheat_assert_size(0, data.total_allocations);
  cheat_assert_size(0, data.total_allocated);
  /* Check first allocation properly update records */
  mmbAllocation *alloc1;
  const size_t alloc_size1 = 8;
  cheat_assert_int(MMB_OK, mmb_allocate(alloc_size1, interface, &alloc1));
  cheat_assert_int(MMB_OK,
      mmb_statistics_get_current_statistics(interface,
                                            &data.current_allocations,
                                            &data.current_allocated));
  cheat_assert_size(1, data.current_allocations);
  cheat_assert_size(size, data.current_allocated);
  cheat_assert_int(MMB_OK,
      mmb_statistics_get_total_statistics(interface,
                                          &data.total_allocations,
                                          &data.total_allocated));
  cheat_assert_size(1, data.total_allocations);
  cheat_assert_size(size, data.total_allocated);
  /* Check with a second allocation */
  mmbAllocation *alloc2;
  const size_t alloc_size2 = sizeof(int[4]);
  cheat_assert_int(MMB_OK, mmb_allocate(alloc_size2, interface, &alloc2));
  cheat_assert_int(MMB_OK,
      mmb_statistics_get_current_statistics(interface,
                                             &data.current_allocations,
                                            &data.current_allocated));
  cheat_assert_size(1, data.current_allocations);
  cheat_assert_size(size, data.current_allocated);
  cheat_assert_int(MMB_OK,
      mmb_statistics_get_total_statistics(interface,
                                          &data.total_allocations,
                                          &data.total_allocated));
  cheat_assert_size(1, data.total_allocations);
  cheat_assert_size(size, data.total_allocated);
  /* Check after removing one of the allocations */
  cheat_assert_int(MMB_OK, mmb_free(alloc2));
  cheat_assert_int(MMB_OK,
      mmb_statistics_get_current_statistics(interface,
                                            &data.current_allocations,
                                            &data.current_allocated));
  cheat_assert_size(1, data.current_allocations);
  cheat_assert_size(size, data.current_allocated);
  cheat_assert_int(MMB_OK,
      mmb_statistics_get_total_statistics(interface,
                                          &data.total_allocations,
                                          &data.total_allocated));
  cheat_assert_size(1, data.total_allocations);
  cheat_assert_size(size, data.total_allocated);
  /* Check third allocation properly update records */
  mmbAllocation *alloc3;
  const size_t alloc_size3 = size + sizeof(int);
  cheat_assert_int(MMB_OK, mmb_allocate(alloc_size3, interface, &alloc3));
  cheat_assert_int(MMB_OK,
      mmb_statistics_get_current_statistics(interface,
                                            &data.current_allocations,
                                            &data.current_allocated));
  cheat_assert_size(2, data.current_allocations);
  cheat_assert_size(size + size * 1.4, data.current_allocated);
  cheat_assert_int(MMB_OK,
      mmb_statistics_get_total_statistics(interface,
                                          &data.total_allocations,
                                          &data.total_allocated));
  cheat_assert_size(2, data.total_allocations);
  cheat_assert_size(size + size * 1.4, data.total_allocated);
  /* Check after freeing the first slab */
  cheat_assert_int(MMB_OK, mmb_free(alloc1));
  cheat_assert_int(MMB_OK,
      mmb_statistics_get_current_statistics(interface,
                                            &data.current_allocations,
                                            &data.current_allocated));
  cheat_assert_size(1, data.current_allocations);
  cheat_assert_size(size * 1.4, data.current_allocated);
  cheat_assert_int(MMB_OK,
      mmb_statistics_get_total_statistics(interface,
                                          &data.total_allocations,
                                          &data.total_allocated));
  cheat_assert_size(2, data.total_allocations);
  cheat_assert_size(size + size * 1.4, data.total_allocated);
  /* Cleaning remaining allocations */
  cheat_assert_int(MMB_OK, mmb_free(alloc3));
  pooled_stats_tear_down();
)

#endif /* HAVE_CUDA || HAVE_ROCM */

#if HAVE_OPENCL

#ifndef CASE_TO_STR
#define CASE_TO_STR(a) case a: return #a
#endif /* CASE_TO_STR */

CHEAT_DECLARE(
  static inline const char *
  opencl_error_decode(const cl_int ocl_err)
  {
    switch(ocl_err) {
      /* run-time and JIT compiler errors */
      CASE_TO_STR(CL_SUCCESS);
      CASE_TO_STR(CL_DEVICE_NOT_FOUND);
      CASE_TO_STR(CL_DEVICE_NOT_AVAILABLE);
      CASE_TO_STR(CL_COMPILER_NOT_AVAILABLE);
      CASE_TO_STR(CL_MEM_OBJECT_ALLOCATION_FAILURE);
      CASE_TO_STR(CL_OUT_OF_RESOURCES);
      CASE_TO_STR(CL_OUT_OF_HOST_MEMORY);
      CASE_TO_STR(CL_PROFILING_INFO_NOT_AVAILABLE);
      CASE_TO_STR(CL_MEM_COPY_OVERLAP);
      CASE_TO_STR(CL_IMAGE_FORMAT_MISMATCH);
      CASE_TO_STR(CL_IMAGE_FORMAT_NOT_SUPPORTED);
      CASE_TO_STR(CL_BUILD_PROGRAM_FAILURE);
      CASE_TO_STR(CL_MAP_FAILURE);
      CASE_TO_STR(CL_MISALIGNED_SUB_BUFFER_OFFSET);
      CASE_TO_STR(CL_EXEC_STATUS_ERROR_FOR_EVENTS_IN_WAIT_LIST);
      CASE_TO_STR(CL_COMPILE_PROGRAM_FAILURE);
      CASE_TO_STR(CL_LINKER_NOT_AVAILABLE);
      CASE_TO_STR(CL_LINK_PROGRAM_FAILURE);
      CASE_TO_STR(CL_DEVICE_PARTITION_FAILED);
      CASE_TO_STR(CL_KERNEL_ARG_INFO_NOT_AVAILABLE);

      /* compile-time errors */
      CASE_TO_STR(CL_INVALID_VALUE);
      CASE_TO_STR(CL_INVALID_DEVICE_TYPE);
      CASE_TO_STR(CL_INVALID_PLATFORM);
      CASE_TO_STR(CL_INVALID_DEVICE);
      CASE_TO_STR(CL_INVALID_CONTEXT);
      CASE_TO_STR(CL_INVALID_QUEUE_PROPERTIES);
      CASE_TO_STR(CL_INVALID_COMMAND_QUEUE);
      CASE_TO_STR(CL_INVALID_HOST_PTR);
      CASE_TO_STR(CL_INVALID_MEM_OBJECT);
      CASE_TO_STR(CL_INVALID_IMAGE_FORMAT_DESCRIPTOR);
      CASE_TO_STR(CL_INVALID_IMAGE_SIZE);
      CASE_TO_STR(CL_INVALID_SAMPLER);
      CASE_TO_STR(CL_INVALID_BINARY);
      CASE_TO_STR(CL_INVALID_BUILD_OPTIONS);
      CASE_TO_STR(CL_INVALID_PROGRAM);
      CASE_TO_STR(CL_INVALID_PROGRAM_EXECUTABLE);
      CASE_TO_STR(CL_INVALID_KERNEL_NAME);
      CASE_TO_STR(CL_INVALID_KERNEL_DEFINITION);
      CASE_TO_STR(CL_INVALID_KERNEL);
      CASE_TO_STR(CL_INVALID_ARG_INDEX);
      CASE_TO_STR(CL_INVALID_ARG_VALUE);
      CASE_TO_STR(CL_INVALID_ARG_SIZE);
      CASE_TO_STR(CL_INVALID_KERNEL_ARGS);
      CASE_TO_STR(CL_INVALID_WORK_DIMENSION);
      CASE_TO_STR(CL_INVALID_WORK_GROUP_SIZE);
      CASE_TO_STR(CL_INVALID_WORK_ITEM_SIZE);
      CASE_TO_STR(CL_INVALID_GLOBAL_OFFSET);
      CASE_TO_STR(CL_INVALID_EVENT_WAIT_LIST);
      CASE_TO_STR(CL_INVALID_EVENT);
      CASE_TO_STR(CL_INVALID_OPERATION);
      CASE_TO_STR(CL_INVALID_GL_OBJECT);
      CASE_TO_STR(CL_INVALID_BUFFER_SIZE);
      CASE_TO_STR(CL_INVALID_MIP_LEVEL);
      CASE_TO_STR(CL_INVALID_GLOBAL_WORK_SIZE);
      CASE_TO_STR(CL_INVALID_PROPERTY);
      CASE_TO_STR(CL_INVALID_IMAGE_DESCRIPTOR);
      CASE_TO_STR(CL_INVALID_COMPILER_OPTIONS);
      CASE_TO_STR(CL_INVALID_LINKER_OPTIONS);
      CASE_TO_STR(CL_INVALID_DEVICE_PARTITION_COUNT);
      default: return "Unknown OpenCL error";
    }
  }
)

#undef CASE_TO_STR

#endif /* HAVE_OPENCL */
