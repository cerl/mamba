/*
 * Copyright (C) 2020 Cray UK
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/* needed before inclusion of cheat.h: */
#ifndef __BASE_FILE__
#define __BASE_FILE__ __FILE__
#endif

#include "cheat.h"
#include "cheats.h"

#include <limits.h>

#include <mamba.h>
#include <mmb_options.h>

#include "../memory/memory_options.h"

/* Set-up / Tear-down */

#ifndef MAX_LOG_LEVEL
#  ifdef MMB_LOG_DEBUG
#    define MAX_LOG_LEVEL MMB_LOG_DEBUG
#  else
#    define MAX_LOG_LEVEL 3
#  endif
#endif

#ifndef MMB_NUM_INTERFACE_TEST_NAME
#define MMB_NUM_INTERFACE_TEST_NAME 3
#endif /* MMB_NUM_INTERFACE_TEST_NAME */

#ifndef INT_ERRONEOUS_PROVIDER
/* To be updated when adding a new provider */
#define INT_ERRONEOUS_PROVIDER      4
#endif /* INT_ERRONEOUS_PROVIDER */

#ifndef INT_ERRONEOUS_STRATEGY
/* To be updated when adding a new strategy */
#define INT_ERRONEOUS_STRATEGY      6
#endif /* INT_ERRONEOUS_STRATEGY */

CHEAT_DECLARE(
    mmbOptions *MMB_TESTS_INIT_DEFAULT = NULL;
    static const char *INTERFACE_NAME = "toto";
)

CHEAT_SET_UP(
    cheat_assert_int(MMB_OK, mmb_options_create_default(&MMB_TESTS_INIT_DEFAULT));
    cheat_assert_int(MMB_OK, mmb_options_set_debug_level(MMB_TESTS_INIT_DEFAULT,
                                                         MAX_LOG_LEVEL));
    cheat_assert_int(MMB_OK, mmb_init(MMB_TESTS_INIT_DEFAULT));
)

CHEAT_TEAR_DOWN(
    cheat_assert_int(MMB_OK, mmb_finalize());
    cheat_assert_int(MMB_OK, mmb_options_destroy(MMB_TESTS_INIT_DEFAULT));
)

CHEAT_DECLARE(
    void MMB_DUMMY_LOG(int level, const char *func, const char *file,
                       int line, const char *fmtstring, ...)
    { (void)level; (void)func; (void)file; (void)line; (void)fmtstring; }
)

/* Tests */

/* mmbMemoryOptions */

/* Test error detection */
CHEAT_TEST(create_memory_default_null,
  cheat_assert_int(MMB_INVALID_ARG, mmb_memory_options_create_default(NULL));
)

/* Test error detection */
CHEAT_TEST(destroy_memory_default_null,
  cheat_assert_int(MMB_INVALID_ARG, mmb_memory_options_destroy(NULL));
)

/* Test the memory parameter generation */
CHEAT_TEST(create_memory_default,
  const mmbMemoryOptions DEFAULT_MEM = {
    .providers_opts = MMB_PROVIDER_DEFAULT_INIT_PARAMS,
    .strategies_opts = MMB_STRATEGY_DEFAULT_INIT_PARAMS,
  };
  mmbMemoryOptions *mem_opts = NULL;
  cheat_assert_int(MMB_OK, mmb_memory_options_create_default(&mem_opts));
  cheat_assert_int(DEFAULT_MEM.providers_opts[MMB_SYSTEM].__placeholder,
                   mem_opts->providers_opts[MMB_SYSTEM].__placeholder);
  cheat_assert_int(DEFAULT_MEM.providers_opts[MMB_SICM].__placeholder,
                   mem_opts->providers_opts[MMB_SICM].__placeholder);
  cheat_assert_int(DEFAULT_MEM.providers_opts[MMB_UMPIRE].__placeholder,
                   mem_opts->providers_opts[MMB_UMPIRE].__placeholder);
  cheat_assert_int(DEFAULT_MEM.strategies_opts[MMB_STRATEGY_NONE].__placeholder,
                   mem_opts->strategies_opts[MMB_STRATEGY_NONE].__placeholder);
  cheat_assert_size(DEFAULT_MEM.strategies_opts[MMB_POOLED].pooled.pool_size,
                    mem_opts->strategies_opts[MMB_POOLED].pooled.pool_size);
  cheat_assert_int(DEFAULT_MEM.strategies_opts[MMB_THREAD_SAFE].__placeholder,
                   mem_opts->strategies_opts[MMB_THREAD_SAFE].__placeholder);
  cheat_assert_int(DEFAULT_MEM.strategies_opts[MMB_STATISTICS].__placeholder,
                   mem_opts->strategies_opts[MMB_STATISTICS].__placeholder);
  cheat_assert_int(DEFAULT_MEM.strategies_opts[MMB_POOLED_STATISTICS].__placeholder,
                   mem_opts->strategies_opts[MMB_POOLED_STATISTICS].__placeholder);
  cheat_assert_int(MMB_OK, mmb_memory_options_destroy(mem_opts));
)

/* Check fields of the returned structure */
CHEAT_TEST(memory_set_pooled_size,
  mmbMemoryOptions *mem_opts = NULL;
  cheat_assert_int(MMB_OK, mmb_memory_options_create_default(&mem_opts));
  /* zero sized pool */
  cheat_assert_int(MMB_OK,
                   mmb_memory_options_strategy_pooled_set_pool_size(mem_opts, 0));
  cheat_assert_size(mem_opts->strategies_opts[MMB_POOLED].pooled.pool_size, 0);
  /* 1 MiB sized pool */
  cheat_assert_int(MMB_OK,
                   mmb_memory_options_strategy_pooled_set_pool_size(mem_opts,
                                                                   1024*1024));
  cheat_assert_size(mem_opts->strategies_opts[MMB_POOLED].pooled.pool_size,
                    1024*1024);
  /* SIZE_MAX sized pool */
  cheat_assert_int(MMB_OK,
                   mmb_memory_options_strategy_pooled_set_pool_size(mem_opts,
                                                                    SIZE_MAX));
  cheat_assert_size(mem_opts->strategies_opts[MMB_POOLED].pooled.pool_size,
                    SIZE_MAX);
  cheat_assert_int(MMB_OK, mmb_memory_options_destroy(mem_opts));
)

/* mmbLoggingOptions */

/* Test error detection */
CHEAT_TEST(create_logging_default_null,
  cheat_assert_int(MMB_INVALID_ARG, mmb_logging_options_create_default(NULL));
)

/* Test error detection */
CHEAT_TEST(destroy_logging_default_null,
  cheat_assert_int(MMB_INVALID_ARG, mmb_logging_options_destroy(NULL));
)

/* Test the logging parameter generation */
CHEAT_TEST(create_logging_default,
  const mmbLoggingOptions DEFAULT_LOG = {
    .max_logging = MMB_LOG_WARN,
    .user_log_func = NULL,
  };
  mmbLoggingOptions *log_opts = NULL;
  cheat_assert_int(MMB_OK, mmb_logging_options_create_default(&log_opts));
  cheat_assert_size(DEFAULT_LOG.max_logging, log_opts->max_logging);
  cheat_assert(DEFAULT_LOG.user_log_func == log_opts->user_log_func);
  cheat_assert_int(MMB_OK, mmb_logging_options_destroy(log_opts));
)

/* Check fields of the returned structure */
CHEAT_TEST(logging_set_pooled_size,
  mmbUserLogFunc logf = MMB_DUMMY_LOG;
  mmbLoggingOptions *log_opts = NULL;
  cheat_assert_int(MMB_OK, mmb_logging_options_create_default(&log_opts));
  cheat_assert_int(MMB_INVALID_ARG,
                   mmb_logging_options_set_debug_level(NULL, -1));
  cheat_assert_int(MMB_INVALID_ARG,
                   mmb_logging_options_set_debug_level(log_opts, -1));
  cheat_assert_int(MMB_INVALID_ARG,
                   mmb_logging_options_set_debug_level(log_opts, 5));
  cheat_assert_int(MMB_INVALID_ARG,
                   mmb_logging_options_set_debug_level(log_opts, INT_MAX));
  for (int i = 0; i < 5; ++i) {
    cheat_assert_int(MMB_OK, mmb_logging_options_set_debug_level(log_opts, i));
    cheat_assert_int(i, log_opts->max_logging);
  }
  cheat_assert_int(MMB_OK, mmb_logging_options_set_user_log_func(log_opts, logf));
  cheat_assert(MMB_DUMMY_LOG == log_opts->user_log_func);
  cheat_assert_int(MMB_OK, mmb_logging_options_destroy(log_opts));
)

/* mmbOptions */

/* Test error detection */
CHEAT_TEST(create_options_null,
  cheat_assert_int(MMB_INVALID_ARG, mmb_options_create_default(NULL));
)

/* Test error detection */
CHEAT_TEST(destroy_options_null,
  cheat_assert_int(MMB_INVALID_ARG, mmb_options_destroy(NULL));
)

/* Test error detection */
CHEAT_TEST(options_set_debug_level,
  mmbOptions *opts = NULL;
  cheat_assert_int(MMB_OK, mmb_options_create_default(&opts));
  cheat_assert_int(MMB_INVALID_ARG, mmb_options_set_debug_level(NULL, -1));
  cheat_assert_int(MMB_INVALID_ARG, mmb_options_set_debug_level(opts, -1));
  cheat_assert_int(MMB_INVALID_ARG, mmb_options_set_debug_level(opts, 5));
  cheat_assert_int(MMB_INVALID_ARG, mmb_options_set_debug_level(opts, INT_MAX));
  for (int i = 0; i < 5; ++i)
    cheat_assert_int(MMB_OK, mmb_options_set_debug_level(opts, i));
  cheat_assert_int(MMB_OK, mmb_options_destroy(opts));
)

/* Test error detection */
CHEAT_TEST(options_set_pooled_size,
  mmbOptions *opts = NULL;
  cheat_assert_int(MMB_OK, mmb_options_create_default(&opts));
  cheat_assert_int(MMB_INVALID_ARG, mmb_options_set_debug_level(NULL, INT_MAX));
  /* zero sized pool */
  cheat_assert_int(MMB_OK, mmb_options_strategy_pooled_set_pool_size(opts, 0));
  cheat_assert_size(opts->memory_opts->strategies_opts[MMB_POOLED].pooled.pool_size,
                    0);
  /* 1 MiB sized pool */
  cheat_assert_int(MMB_OK,
                   mmb_options_strategy_pooled_set_pool_size(opts, 1024*1024));
  cheat_assert_size(opts->memory_opts->strategies_opts[MMB_POOLED].pooled.pool_size,
                    1024*1024);
  /* SIZE_MAX sized pool */
  cheat_assert_int(MMB_OK,
                   mmb_options_strategy_pooled_set_pool_size(opts, SIZE_MAX));
  cheat_assert_size(opts->memory_opts->strategies_opts[MMB_POOLED].pooled.pool_size,
                    SIZE_MAX);
  cheat_assert_int(MMB_OK, mmb_options_destroy(opts));
)

/* Check fields of the returned structure */
CHEAT_TEST(options_creation,
  const mmbLoggingOptions log_opts = (const mmbLoggingOptions) {
    .max_logging = MAX_LOG_LEVEL,
    .user_log_func = NULL,
  };
  mmbOptions *opts = NULL;
  cheat_assert_int(MMB_OK, mmb_options_create_default(&opts));
  cheat_assert_not_pointer(NULL, opts->logging_opts);
  cheat_assert_not_pointer(NULL, opts->memory_opts);
  cheat_assert_int(MMB_OK, mmb_options_set_debug_level(opts, MAX_LOG_LEVEL));
  cheat_assert_int(log_opts.max_logging, opts->logging_opts->max_logging);
  cheat_assert(log_opts.user_log_func == opts->logging_opts->user_log_func);
  cheat_assert_int(MMB_OK, mmb_options_set_user_log_func(opts, MMB_DUMMY_LOG));
  cheat_assert(MMB_DUMMY_LOG == opts->logging_opts->user_log_func);
  cheat_assert_int(MMB_OK, mmb_options_destroy(opts));
)

/* mmbMemInterfaceConfig */

/* Test error detection */
CHEAT_TEST(meminterface_config_create_null,
  cheat_assert_int(MMB_INVALID_ARG, mmb_meminterface_config_create_default(NULL));
)

/* Test error detection */
CHEAT_TEST(meminterface_config_destroy_null,
  cheat_assert_int(MMB_INVALID_ARG, mmb_meminterface_config_destroy(NULL));
)

/* Test error detection */
CHEAT_TEST(meminterface_config_errors,
  mmbMemInterfaceConfig *config = NULL;
  cheat_assert_int(MMB_OK, mmb_meminterface_config_create_default(&config));
  cheat_assert_int(MMB_INVALID_ARG,
                   mmb_meminterface_config_set_provider(NULL, MMB_PROVIDER_ANY));
  cheat_assert_int(MMB_INVALID_PROVIDER,
                   mmb_meminterface_config_set_provider(config, -3));
  cheat_assert_int(MMB_INVALID_PROVIDER,
                   mmb_meminterface_config_set_provider(config, MMB_PROVIDER__MAX));
  cheat_assert_int(MMB_INVALID_PROVIDER,
                   mmb_meminterface_config_set_provider(config, INT_ERRONEOUS_PROVIDER));
  cheat_assert_int(MMB_INVALID_PROVIDER,
                   mmb_meminterface_config_set_provider(config, INT_MAX));
  cheat_assert_int(MMB_INVALID_ARG,
                   mmb_meminterface_config_set_strategy(NULL, MMB_STRATEGY_ANY));
  cheat_assert_int(MMB_INVALID_STRATEGY,
                   mmb_meminterface_config_set_strategy(config, -3));
  cheat_assert_int(MMB_INVALID_STRATEGY,
                   mmb_meminterface_config_set_strategy(config, MMB_STRATEGY__MAX));
  cheat_assert_int(MMB_INVALID_STRATEGY,
                   mmb_meminterface_config_set_strategy(config, INT_ERRONEOUS_STRATEGY));
  cheat_assert_int(MMB_INVALID_STRATEGY,
                   mmb_meminterface_config_set_strategy(config, INT_MAX));
  cheat_assert_int(MMB_INVALID_ARG,
                   mmb_meminterface_config_set_name(NULL, NULL));
  cheat_assert_int(MMB_OK, mmb_meminterface_config_destroy(config));
)

/* Check fields of the returned structure */
CHEAT_TEST(meminterface_config_create,
  const mmbMemInterfaceConfig CONFIG_DEFAULT = MMB_MEMINTERFACE_CONFIG_DEFAULT;
  mmbMemInterfaceConfig *config = NULL;
  cheat_assert_int(MMB_OK, mmb_meminterface_config_create_default(&config));
  cheat_assert_int(CONFIG_DEFAULT.provider, config->provider);
  cheat_assert_int(CONFIG_DEFAULT.strategy, config->strategy);
  cheat_assert_int(0, strcmp(CONFIG_DEFAULT.name, config->name));
  cheat_assert_int(MMB_OK, mmb_meminterface_config_destroy(config));
)

/* Check fields of the returned structure */
CHEAT_TEST(meminterface_config_set_provider,
  mmbMemInterfaceConfig *config = NULL;
  cheat_assert_int(MMB_OK, mmb_meminterface_config_create_default(&config));
  for (int provider = MMB_PROVIDER_ANY; provider < MMB_PROVIDER__MAX; ++provider) {
    cheat_assert_int(MMB_OK,
                     mmb_meminterface_config_set_provider(config, provider));
    cheat_assert_int(provider, config->provider);
  }
  cheat_assert_int(MMB_OK, mmb_meminterface_config_destroy(config));
)

/* Check fields of the returned structure */
CHEAT_TEST(meminterface_config_set_strategy,
  mmbMemInterfaceConfig *config = NULL;
  cheat_assert_int(MMB_OK, mmb_meminterface_config_create_default(&config));
  for (int strat = MMB_STRATEGY_ANY; strat < MMB_STRATEGY__MAX; ++strat) {
    cheat_assert_int(MMB_OK, mmb_meminterface_config_set_strategy(config, strat));
    cheat_assert_int(strat, config->strategy);
  }
  cheat_assert_int(MMB_OK, mmb_meminterface_config_destroy(config));
)

/* Check fields of the returned structure */
CHEAT_TEST(meminterface_config_set_name,
  mmbMemInterfaceConfig *config = NULL;
  const size_t num_names = MMB_NUM_INTERFACE_TEST_NAME;
  const char *tested_names[MMB_NUM_INTERFACE_TEST_NAME] = { "", INTERFACE_NAME, "\ntruc\t" };
  cheat_assert_int(MMB_OK, mmb_meminterface_config_create_default(&config));
  for (size_t name_id = 0; num_names > name_id; ++name_id) {
    cheat_assert_int(MMB_OK,
                     mmb_meminterface_config_set_name(config, tested_names[name_id]));
    cheat_assert_pointer(tested_names[name_id], config->name);
  }
  /* corner cases */
  cheat_assert_int(MMB_OK, mmb_meminterface_config_set_name(config, NULL));
  cheat_assert_int(0, strcmp("", config->name));
  cheat_assert_int(MMB_OK, mmb_meminterface_config_destroy(config));
)

/* Check fields of the returned structure */
CHEAT_TEST(meminterface_config_set_default,
  const mmbMemInterfaceConfig CONFIG_DEFAULT = MMB_MEMINTERFACE_CONFIG_DEFAULT;
  mmbMemInterfaceConfig *config = NULL;
  cheat_assert_int(MMB_OK, mmb_meminterface_config_create_default(&config));
  cheat_assert_int(MMB_OK,
                   mmb_meminterface_config_set_provider(config, MMB_SYSTEM));
  cheat_assert_int(MMB_SYSTEM, config->provider);
  cheat_assert_int(MMB_OK,
                   mmb_meminterface_config_set_strategy(config, MMB_THREAD_SAFE));
  cheat_assert_int(MMB_THREAD_SAFE, config->strategy);
  cheat_assert_int(MMB_OK,
                   mmb_meminterface_config_set_name(config, INTERFACE_NAME));
  cheat_assert_pointer(INTERFACE_NAME, config->name);
  cheat_assert_int(MMB_OK, mmb_meminterface_config_set_default(config));
  cheat_assert_int(CONFIG_DEFAULT.provider, config->provider);
  cheat_assert_int(CONFIG_DEFAULT.strategy, config->strategy);
  cheat_assert_int(0, strcmp(CONFIG_DEFAULT.name, config->name));
  cheat_assert_int(MMB_OK, mmb_meminterface_config_destroy(config));
)

/* mmbMemSpaceConfig */

/* Test error detection */
CHEAT_TEST(memspace_config_create_init_null,
  cheat_assert_int(MMB_INVALID_ARG, mmb_memspace_config_create_init_default(NULL));
)

/* Test error detection */
CHEAT_TEST(memspace_config_create_null,
  cheat_assert_int(MMB_INVALID_ARG, mmb_memspace_config_create_default(NULL));
)

/* Test error detection */
CHEAT_TEST(memspace_config_destroy_null,
  cheat_assert_int(MMB_INVALID_ARG, mmb_memspace_config_destroy(NULL));
)

/* Test error detection */
CHEAT_TEST(memspace_config_size_errors,
  mmbMemSpaceConfig *config = NULL;
  cheat_assert_int(MMB_OK, mmb_memspace_config_create_default(&config));
  /* Tests errors size */
  cheat_assert_int(MMB_INVALID_ARG, mmb_memspace_config_size(NULL, 0, 0));
  cheat_assert_int(MMB_INVALID_ARG, mmb_memspace_config_size(config, -3, 0));
  cheat_assert_int(MMB_INVALID_ARG,
                   mmb_memspace_config_size(config, MMB_SIZE_INVALID, 0));
  cheat_assert_int(MMB_INVALID_ARG, mmb_memspace_config_size(config, 1, 0));
  cheat_assert_int(MMB_INVALID_ARG, mmb_memspace_config_set_size(NULL, 0));
  cheat_assert_int(MMB_INVALID_ARG, mmb_memspace_config_any_size(NULL));
  cheat_assert_int(MMB_OK, mmb_memspace_config_destroy(config));
)

/* Check fields of the returned structure */
CHEAT_TEST(memspace_config_set_any_size,
  mmbMemSpaceConfig *config = NULL;
  cheat_assert_int(MMB_OK, mmb_memspace_config_create_default(&config));
  cheat_assert_not_size(0, config->size_opts.mem_size);
  cheat_assert_int(MMB_OK, mmb_memspace_config_any_size(config));
  cheat_assert_int(MMB_SIZE_ANY, config->size_opts.action);
  cheat_assert_size(0, config->size_opts.mem_size);
  cheat_assert_int(MMB_OK, mmb_memspace_config_destroy(config));
)

/* Check fields of the returned structure */
CHEAT_TEST(memspace_config_set_size,
  mmbMemSpaceConfig *config = NULL;
  cheat_assert_int(MMB_OK, mmb_memspace_config_create_default(&config));
  /* 0 sized memory */
  cheat_assert_int(MMB_OK, mmb_memspace_config_any_size(config));
  cheat_assert_not_int(MMB_SIZE_SET, config->size_opts.action);
  cheat_assert_int(MMB_OK, mmb_memspace_config_set_size(config, 0));
  cheat_assert_int(MMB_SIZE_SET, config->size_opts.action);
  cheat_assert_size(0, config->size_opts.mem_size);
  /* 1 MiB memory space */
  cheat_assert_int(MMB_OK, mmb_memspace_config_any_size(config));
  cheat_assert_not_int(MMB_SIZE_SET, config->size_opts.action);
  cheat_assert_int(MMB_OK, mmb_memspace_config_set_size(config, 1024*1024));
  cheat_assert_int(MMB_SIZE_SET, config->size_opts.action);
  cheat_assert_size(1024*1024, config->size_opts.mem_size);
  /* SIZE_MAX memory space */
  cheat_assert_int(MMB_OK, mmb_memspace_config_any_size(config));
  cheat_assert_not_int(MMB_SIZE_SET, config->size_opts.action);
  cheat_assert_int(MMB_OK, mmb_memspace_config_set_size(config, SIZE_MAX));
  cheat_assert_int(MMB_SIZE_SET, config->size_opts.action);
  cheat_assert_size(SIZE_MAX, config->size_opts.mem_size);
  cheat_assert_int(MMB_OK, mmb_memspace_config_destroy(config));
)

/* Check fields of the returned structure */
CHEAT_TEST(memspace_config_size,
  mmbMemSpaceConfig *config = NULL;
  cheat_assert_int(MMB_OK, mmb_memspace_config_create_default(&config));
  /* ANY memory space */
  cheat_assert_int(MMB_OK, mmb_memspace_config_any_size(config));
  cheat_assert_not_int(MMB_SIZE_SET, config->size_opts.action);
  cheat_assert_int(MMB_OK, mmb_memspace_config_set_size(config, 1024));
  cheat_assert_size(1024, config->size_opts.mem_size);
  cheat_assert_int(MMB_OK, mmb_memspace_config_size(config, MMB_SIZE_ANY, 0));
  cheat_assert_int(MMB_SIZE_ANY, config->size_opts.action);
  cheat_assert_size(0, config->size_opts.mem_size);
  /* ANY memory space, check size stays 0 */
  cheat_assert_int(MMB_OK, mmb_memspace_config_size(config, MMB_SIZE_ANY, 1024));
  cheat_assert_int(MMB_SIZE_ANY, config->size_opts.action);
  cheat_assert_size(0, config->size_opts.mem_size);
  /* 1 MiB memory space */
  cheat_assert_int(MMB_OK,
                   mmb_memspace_config_size(config, MMB_SIZE_SET, 1024*1024));
  cheat_assert_int(MMB_SIZE_SET, config->size_opts.action);
  cheat_assert_size(1024*1024, config->size_opts.mem_size);
  /* SIZE_MAX memory space */
  cheat_assert_int(MMB_OK,
                   mmb_memspace_config_size(config, MMB_SIZE_SET, SIZE_MAX));
  cheat_assert_int(MMB_SIZE_SET, config->size_opts.action);
  cheat_assert_size(SIZE_MAX, config->size_opts.mem_size);
  /* 0 sized memory space */
  cheat_assert_int(MMB_OK,
                   mmb_memspace_config_size(config, MMB_SIZE_SET, 0));
  cheat_assert_int(MMB_SIZE_SET, config->size_opts.action);
  cheat_assert_size(0, config->size_opts.mem_size);
  /* NONE, size argument ignored, sized unchanged */
  cheat_assert_int(MMB_OK, mmb_memspace_config_size(config, MMB_SIZE_SET, 1024));
  cheat_assert_int(MMB_SIZE_SET, config->size_opts.action);
  cheat_assert_size(1024, config->size_opts.mem_size);
  cheat_assert_int(MMB_OK, mmb_memspace_config_size(config, MMB_SIZE_NONE, 0));
  cheat_assert_int(MMB_SIZE_NONE, config->size_opts.action);
  cheat_assert_size(1024, config->size_opts.mem_size);
  cheat_assert_int(MMB_OK, mmb_memspace_config_destroy(config));
)

/* Test error detection */
CHEAT_TEST(memspace_config_provider_errors,
  mmbMemSpaceConfig *config = NULL;
  cheat_assert_int(MMB_OK, mmb_memspace_config_create_default(&config));
  /* Tests errors provider */
  cheat_assert_int(MMB_INVALID_ARG,
                   mmb_memspace_config_interface_set_provider(NULL,
                                                              MMB_PROVIDER_ANY));
  cheat_assert_int(MMB_INVALID_PROVIDER,
                   mmb_memspace_config_interface_set_provider(config, -3));
  cheat_assert_int(MMB_INVALID_PROVIDER,
                   mmb_memspace_config_interface_set_provider(config,
                                                              MMB_PROVIDER__MAX));
  cheat_assert_int(MMB_INVALID_PROVIDER,
                   mmb_memspace_config_interface_set_provider(config,
                                                              INT_ERRONEOUS_PROVIDER));
  cheat_assert_int(MMB_INVALID_PROVIDER,
                   mmb_memspace_config_interface_set_provider(config, INT_MAX));
  cheat_assert_int(MMB_OK, mmb_memspace_config_destroy(config));
)

/* Test error detection */
CHEAT_TEST(memspace_config_strategy_errors,
  mmbMemSpaceConfig *config = NULL;
  cheat_assert_int(MMB_OK, mmb_memspace_config_create_default(&config));
  /* Tests errors strategy */
  cheat_assert_int(MMB_INVALID_ARG,
                   mmb_memspace_config_interface_set_strategy(NULL,
                                                              MMB_STRATEGY_ANY));
  cheat_assert_int(MMB_INVALID_STRATEGY,
                   mmb_memspace_config_interface_set_strategy(config, -3));
  cheat_assert_int(MMB_INVALID_STRATEGY,
                   mmb_memspace_config_interface_set_strategy(config,
                                                              MMB_STRATEGY__MAX));
  cheat_assert_int(MMB_INVALID_STRATEGY,
                   mmb_memspace_config_interface_set_strategy(config,
                                                              INT_ERRONEOUS_STRATEGY));
  cheat_assert_int(MMB_INVALID_STRATEGY,
                   mmb_memspace_config_interface_set_strategy(config, INT_MAX));
  cheat_assert_int(MMB_OK, mmb_memspace_config_destroy(config));
)

/* Test error detection */
CHEAT_TEST(memspace_config_interface_errors,
  mmbMemSpaceConfig *config = NULL;
  cheat_assert_int(MMB_OK, mmb_memspace_config_create_default(&config));
  /* Tests errors both */
  cheat_assert_int(MMB_INVALID_ARG, mmb_memspace_config_interface(NULL, 0, 0, NULL));
  cheat_assert_int(MMB_INVALID_PROVIDER,
                   mmb_memspace_config_interface(config, -3, 0, NULL));
  cheat_assert_int(MMB_INVALID_PROVIDER,
                   mmb_memspace_config_interface(config, MMB_PROVIDER__MAX, 0, NULL));
  cheat_assert_int(MMB_INVALID_PROVIDER,
                   mmb_memspace_config_interface(config, INT_ERRONEOUS_PROVIDER, 0, NULL));
  cheat_assert_int(MMB_INVALID_PROVIDER,
                   mmb_memspace_config_interface(config, INT_MAX, 0, NULL));
  cheat_assert_int(MMB_INVALID_STRATEGY,
                   mmb_memspace_config_interface(config, 0, -3, NULL));
  cheat_assert_int(MMB_INVALID_STRATEGY,
                   mmb_memspace_config_interface(config, 0, MMB_STRATEGY__MAX, NULL));
  cheat_assert_int(MMB_INVALID_STRATEGY,
                   mmb_memspace_config_interface(config, 0, INT_ERRONEOUS_STRATEGY, NULL));
  cheat_assert_int(MMB_INVALID_STRATEGY,
                   mmb_memspace_config_interface(config, 0, INT_MAX, NULL));
  cheat_assert_int(MMB_OK, mmb_memspace_config_destroy(config));
)

/* Check fields of the returned structure */
CHEAT_TEST(memspace_config_create_init_default,
  const mmbMemSpaceConfig CONFIG_DEFAULT = MMB_MEMSPACE_CONFIG_INIT_DEFAULT;
  mmbMemSpaceConfig *config = NULL;
  cheat_assert_int(MMB_OK, mmb_memspace_config_create_init_default(&config));
  cheat_assert_int(CONFIG_DEFAULT.interface_opts.provider,
                   config->interface_opts.provider);
  cheat_assert_int(CONFIG_DEFAULT.interface_opts.strategy,
                   config->interface_opts.strategy);
  cheat_assert_int(CONFIG_DEFAULT.size_opts.action, config->size_opts.action);
  cheat_assert_size(CONFIG_DEFAULT.size_opts.mem_size, config->size_opts.mem_size);
  cheat_assert_int(MMB_OK, mmb_memspace_config_destroy(config));
)

/* Check fields of the returned structure */
CHEAT_TEST(memspace_config_create_default,
  const mmbMemSpaceConfig CONFIG_DEFAULT = MMB_MEMSPACE_CONFIG_DEFAULT;
  mmbMemSpaceConfig *config = NULL;
  cheat_assert_int(MMB_OK, mmb_memspace_config_create_default(&config));
  cheat_assert_int(CONFIG_DEFAULT.interface_opts.provider,
                   config->interface_opts.provider);
  cheat_assert_int(CONFIG_DEFAULT.interface_opts.strategy,
                   config->interface_opts.strategy);
  cheat_assert_int(MMB_OK, mmb_memspace_config_destroy(config));
)

/* Check fields of the returned structure */
CHEAT_TEST(memspace_config_set_provider,
  mmbMemSpaceConfig *config = NULL;
  cheat_assert_int(MMB_OK, mmb_memspace_config_create_default(&config));
  for (int provider = MMB_PROVIDER_ANY; provider < MMB_PROVIDER__MAX; ++provider) {
    cheat_assert_int(MMB_OK,
                     mmb_memspace_config_interface_set_provider(config, provider));
    cheat_assert_int(provider, config->interface_opts.provider);
  }
  cheat_assert_int(MMB_OK, mmb_memspace_config_destroy(config));
)

/* Check fields of the returned structure */
CHEAT_TEST(memspace_config_set_strategy,
  mmbMemSpaceConfig *config = NULL;
  cheat_assert_int(MMB_OK, mmb_memspace_config_create_default(&config));
  for (int strat = MMB_STRATEGY_ANY; strat < MMB_STRATEGY__MAX; ++strat) {
    cheat_assert_int(MMB_OK,
                     mmb_memspace_config_interface_set_strategy(config, strat));
    cheat_assert_int(strat, config->interface_opts.strategy);
  }
  cheat_assert_int(MMB_OK, mmb_memspace_config_destroy(config));
)

/* Check fields of the returned structure */
CHEAT_TEST(memspace_config_interface,
  mmbMemSpaceConfig *config = NULL;
  cheat_assert_int(MMB_OK, mmb_memspace_config_create_default(&config));
  cheat_assert_int(MMB_OK, mmb_memspace_config_interface(config,
                                                         MMB_SYSTEM,
                                                         MMB_THREAD_SAFE,
                                                         "foo"));
  cheat_assert_int(MMB_SYSTEM, config->interface_opts.provider);
  cheat_assert_int(MMB_THREAD_SAFE, config->interface_opts.strategy);
  cheat_assert_int(0, strcmp("foo", config->interface_opts.name));
  cheat_assert_int(MMB_OK, mmb_memspace_config_destroy(config));
)

/* Check fields of the returned structure */
CHEAT_TEST(memspace_config_set_default_interface,
  const mmbMemSpaceConfig CONFIG_DEFAULT = MMB_MEMSPACE_CONFIG_DEFAULT;
  mmbMemSpaceConfig *config = NULL;
  cheat_assert_int(MMB_OK, mmb_memspace_config_create_default(&config));
  cheat_assert_int(MMB_OK,
                   mmb_memspace_config_interface_set_provider(config,
                                                              MMB_SYSTEM));
  cheat_assert_int(MMB_SYSTEM, config->interface_opts.provider);
  cheat_assert_int(MMB_OK,
                   mmb_memspace_config_interface_set_strategy(config,
                                                              MMB_THREAD_SAFE));
  cheat_assert_int(MMB_THREAD_SAFE, config->interface_opts.strategy);
  cheat_assert_int(MMB_OK, mmb_memspace_config_interface_set_name(config, "a"));
  cheat_assert_int(0, strcmp("a", config->interface_opts.name));
  cheat_assert_int(MMB_OK, mmb_memspace_config_interface_set_default(config));
  cheat_assert_int(CONFIG_DEFAULT.interface_opts.provider,
                   config->interface_opts.provider);
  cheat_assert_int(CONFIG_DEFAULT.interface_opts.strategy,
                   config->interface_opts.strategy);
  cheat_assert_int(0, strcmp(CONFIG_DEFAULT.interface_opts.name,
                             config->interface_opts.name));
  cheat_assert_int(MMB_OK, mmb_memspace_config_destroy(config));
)
